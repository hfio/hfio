# use to build a Docker image for running the server for local testing

FROM ubuntu:16.04

# install node (npm)
RUN apt-get update && apt-get install -y curl \
  && curl -sL https://deb.nodesource.com/setup_7.x | bash - \
  && apt-get install -y nodejs

# global npm install of forever (used in the run script)
RUN npm install forever -g

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# install MongoDB
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN apt-get install -y --no-install-recommends software-properties-common
RUN echo "deb http://repo.mongodb.org/apt/ubuntu $(cat /etc/lsb-release | grep DISTRIB_CODENAME | cut -d= -f2)/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
RUN apt-get update && apt-get install -y mongodb-org

# Create the MongoDB data directory
RUN mkdir -p /data/db

# Expose port used by mongodb
EXPOSE 27017

#Copy files aop to container and install app dependencies
COPY ./Server/ /usr/src/app/
RUN npm install

# expose ports used by the application
EXPOSE 3010 80

# start mongodb in the background and start the app using the script
CMD /usr/bin/mongod --fork --logpath /var/log/mongod.log && sh run_docker.sh
