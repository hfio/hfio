# creates a new docker container from the hf-server image (created by docker_build_server.sh) and starts the game backend within it
# you need to stop the game backend if it is already running with docker_stop_server.sh
# make sure you have ports 3010 and 80 free (redirect your HTTP server from port 80 (or stop it) if you have one installed!)
# you should see server logs appearing in the terminal after starting this script
# http://localhost/pcount should display player count if the server is working

docker run -p 3010:3010 -p 80:80 hf-server
