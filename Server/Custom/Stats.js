//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var fetch = require('node-fetch');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oStats(callback) {
    var Stats = this;
    var loading = 0;

    var onSuccess = function() {
        loading--;
        if(loading === 0) {
            callback();
        }
    };

    var onFailure = function(err) {
        console.log(err);
        console.log('Server will now terminate.');
        process.exit();
    };

    //Stats.Ships, an array of objects like { move_radius: _, accel_sec: _, hitbox: _, etc... }
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/1/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            console.log('Successfully fetched stats: Ships');
            Stats.Ships = json.feed.entry.map((entry) => {
                return {
                    accel_sec: parseFloat(entry.gsx$accelsec.$t),
                    data_name: entry.gsx$dataname.$t,
                    fire_drop: parseFloat(entry.gsx$firedrop.$t),
                    fire_drop_rot: parseFloat(entry.gsx$firedroprot.$t),
                    difficulty_weight: parseInt(entry.gsx$difficultyweight.$t),
                    health: parseFloat(entry.gsx$health.$t),
                    total_shots: parseInt(entry.gsx$totalshots.$t),
                    hitbox: parseFloat(entry.gsx$hitbox.$t),
                    max_speed: parseFloat(entry.gsx$maxspeed.$t),
                    move_radius: parseFloat(entry.gsx$moveradius.$t),
                    move_radius_max: parseFloat(entry.gsx$moveradiusmax.$t),
                    real_name: entry.gsx$realname.$t,
                    rotation_speed: parseFloat(entry.gsx$rotationspeed.$t),
                    default_weapon: entry.gsx$defaultweapon.$t,
                    credits_for_kill: parseFloat(entry.gsx$creditsforkill.$t),
                    spawn_chance_alien: entry.gsx$spawnchancealien.$t,
                    spawn_chance_terran: entry.gsx$spawnchanceterran.$t,
                    ai_check_sec: entry.gsx$aichecksec.$t,
                    ai_overshoot_factor: entry.gsx$aiovershootfactor.$t,
                    ai_need_1: entry.gsx$aineed1.$t,
                    ai_need_2: entry.gsx$aineed2.$t,
                    ai_need_3: entry.gsx$aineed3.$t,
                    ai_need_4: entry.gsx$aineed4.$t,
                    ai_need_5: entry.gsx$aineed5.$t,
                    unlock_price_credits: parseFloat(entry.gsx$unlockpricecredits.$t),
                    repair_cost: entry.gsx$repaircost.$t,
                    restock_cost: entry.gsx$restockcost.$t,
                    spawned_ship: entry.gsx$spawnedship.$t,
                    ship_spawn_time: parseFloat(entry.gsx$shipspawntime.$t),
                    max_ships_spawned: parseInt(entry.gsx$maxshipsspawned.$t)
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);

    // Tech
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/3/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            console.log('Successfully fetched stats: Tech');
            Stats.Tech = [];
            var lastId = -1;
            for (var i = 0; i < json.feed.entry.length; i++) {
                var entry = json.feed.entry[i];
                var id = parseInt(entry.gsx$id.$t);
                var tech;
                if (id === lastId) {
                    tech = Stats.Tech[Stats.Tech.length - 1];
                } else {
                    tech = {id: id};
                    Stats.Tech.push(tech);
                }
                var lvl = parseInt(entry.gsx$lvl.$t);
                if (lvl === 1) {
                    tech.cardImage = entry.gsx$techcardimage.$t;
                    tech.name = entry.gsx$name.$t;
                    tech.description = entry.gsx$description.$t;
                    tech.slot = parseInt(entry.gsx$slot.$t);
                    tech.effects = entry.gsx$effects.$t;
                    tech.sellTechPoints = [parseInt(entry.gsx$selltechpoints.$t)];
                    tech.cooldown = [parseFloat(entry.gsx$cooldown.$t)];
                    tech.range = [parseFloat(entry.gsx$range.$t)];
                    tech.duration = [parseFloat(entry.gsx$duration.$t)];
                    tech.levels = [parseFloat(entry.gsx$levels.$t)];
                    tech.upgradeGivesBonus = [oStats.parseBool(entry.gsx$upgradegivesbonus.$t)];
                    tech.upgradeCosts = [parseFloat(entry.gsx$upgradecosts.$t)];
                    tech.localizeStringId = entry.gsx$localizestringid.$t;
                } else {
                    tech.sellTechPoints[lvl - 1] = parseInt(entry.gsx$selltechpoints.$t);
                    tech.cooldown[lvl - 1] = parseFloat(entry.gsx$cooldown.$t);
                    tech.range[lvl - 1] = parseFloat(entry.gsx$range.$t);
                    tech.duration[lvl -1] = parseFloat(entry.gsx$duration.$t);
                    tech.levels[lvl -1] = parseFloat(entry.gsx$levels.$t);
                    tech.upgradeGivesBonus[lvl -1] = oStats.parseBool(entry.gsx$upgradegivesbonus.$t);
                    tech.upgradeCosts[lvl -1] = parseFloat(entry.gsx$upgradecosts.$t);
                }
                lastId = id;
            }
        })
        .then(onSuccess)
        .catch(onFailure);

    // Bonuses
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/4/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            console.log('Successfully fetched stats: Bonuses');
            Stats.Bonuses = json.feed.entry.map((entry) => {
                return {
                    id: parseInt(entry.gsx$id.$t),
                    stat: entry.gsx$stat.$t,
                    localizeStringId: entry.gsx$localizestringid.$t,
                    min: parseInt(entry.gsx$statmin.$t),
                    max: parseInt(entry.gsx$statmax.$t)
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);

    // Missions
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/5/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            console.log('Successfully fetched stats: Missions');
            Stats.Missions = json.feed.entry.map((entry) => {
                return {
                    id: parseInt(entry.gsx$missionid.$t),
                    cardImage: entry.gsx$image.$t,
                    type: entry.gsx$missiontype.$t,
                    name: entry.gsx$name.$t,
                    description: entry.gsx$description.$t,
                    target: entry.gsx$target.$t && entry.gsx$target.$t.split(",") || [],
                    targetAmount: parseInt(entry.gsx$targetamount.$t),
                    refreshTime: parseInt(entry.gsx$minutesuntilrefresh.$t),
                    dropTable: entry.gsx$playfablootdroptable.$t
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);

    // Stations (droppable and main)
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/6/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            console.log('Successfully fetched stats: Stations');
            Stats.Stations = json.feed.entry.map((entry) => {
                return {
                    id: parseInt(entry.gsx$id.$t),
                    cardImage: entry.gsx$cardimage.$t,
                    image: entry.gsx$image.$t,
                    name: entry.gsx$name.$t,
                    description: entry.gsx$description.$t,
                    duration: parseFloat(entry.gsx$duration.$t),
                    maxCount: parseInt(entry.gsx$maxcount.$t),
                    cooldown: parseFloat(entry.gsx$cooldown.$t),
                    cost: parseInt(entry.gsx$cost.$t),
                    health: parseFloat(entry.gsx$health.$t),
                    turret: parseInt(entry.gsx$turret.$t),
                    hitbox: parseFloat(entry.gsx$hitbox.$t),
                    dockRadius: parseFloat(entry.gsx$dockradius.$t),
                    rotationRate: parseFloat(entry.gsx$rotationrate.$t),
                    anchor: entry.gsx$anchor.$t
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);   

    // Waves
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/7/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            console.log('Successfully fetched stats: Waves');
            Stats.Waves = json.feed.entry.map((entry) => {
                return {
                    number: parseInt(entry.gsx$waves.$t),
                    minDifficulty: parseInt(entry.gsx$mindifficulty.$t),
                    maxDifficulty: parseInt(entry.gsx$maxdifficulty.$t),
                    spawnOverMins: parseFloat(entry.gsx$spawnovermins.$t),
                    healthMultiply: parseFloat(entry.gsx$healthmultiply.$t)/100,
                    damageMultiply: parseFloat(entry.gsx$techdamagemultiply.$t)/100
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);

    //Stats.Global[<key>] returns corresponding value (with correct type)
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/2/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            console.log('Successfully fetched stats: Global');
            Stats.Global = {};
            json.feed.entry.forEach((entry) => {
                var key = entry.gsx$key.$t;
                var value = entry.gsx$value.$t;
                var type = entry.gsx$type.$t;
                if(type === 'number') {
                    value = parseFloat(value);
                } else if(type === 'boolean') {
                    value = (value == 'true');
                }
                Stats.Global[key] = value;
            });
        })
        .then(onSuccess)
        .catch(onFailure);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.parseBool = function(boolString){
    return boolString.toLowerCase() === "true";
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.parseCSV = function(csv){
    if (csv == '') return null;
    return JSON.parse(csv);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getBonus = function(id){
    var bonusKeys = Object.keys(this.Bonuses);
    for (var i = 0; i < bonusKeys.length; i++) {
        var bonus = this.Bonuses[bonusKeys[i]];
        if (bonus.id === id) {
            return bonus;
        }
    }
    return null;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getMission = function(missionId) {
    var stats = this.Missions.find((stats) => stats.id === missionId);
    if (stats) {
        return stats;
    } else {
        return null;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getStation = function(stationId) {
    var stats = this.Stations.find((stats) => stats.id === stationId);
    if (stats) {
        return stats;
    } else {
        return null;
    }
};

module.exports = oStats;