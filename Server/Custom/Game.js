//*********************************************************************************************************************************************
//Game Specific Custom Code (Runs on top of Core)
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//Custom Modules
var oData = require('./Data');
var oLogic = require('./Logic');
var oAPI = require('./API');
var oAI = require('./AI');
var oPlayfab = require('./Playfab');
//var oClient = require('./sClient');

//Custom Objects
var oBase = require('./Objects/Base');
var oShip = require('./Objects/Ship');
var oAsteroid = require('./Objects/Asteroid');
//var oCoin = require('./Objects/Coin');
//var oProjectile = require('./Objects/Projectile');
//Core Engine
var oCore = require('../Engine/Core');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oGame(App) {
    
    //debugging
    this.DEBUG = true;

    this.APP = App;
    this.Core = null;
    
    //Modules
    this.DATA = new oData(this);
    this.LOGIC = new oLogic(this);
    this.API = new oAPI(this);
    this.AI = new oAI(this);
    this.PLAYFAB = new oPlayfab(this);

    this.Name = "HF";
    this.Server = true;
    
    this.WaveMode = true; // whether in wave game mode
    this.WaveIndex = 0; // the index of the current wave (enemies spawn in a wave over time and the next wave begins when all enemies are destroyed)
    this.WaveConfig = null; // the configuration object holding the data for the current wave
    this.WaveShipCount = 0; // the count of enemy ships in the current wave (total, including spawned, unspawned, destroyed)
    this.WaveShipNames = null; // the list of names of ship( type)s of the enemies (total, including spawned, unspawned, destroyed, in order of spawning)
    this.WaveShipLeftCount = 0; // the amount of enemy ships still alive from the current wave (both spawned and unspawned)
    this.WaveShipSpawnIndex = 0; // the index of the next enemy ship to be spawned from the current wave (from the WaveShipNames list)
    this.WaveShipSpawnTimeLeft = 0; // the time left before the next enemy ship from the current wave is to be spawned (seconds)
    this.WaveRestartTime = 0;
    this.WaveRestartTimeSent = 0;
    
    this.Asteroids = []; // references to the asteroids per area, for checking if we need to respawn
    
    return this;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Log = function(m) { if(this.DEBUG){ console.log("[GAME] " + m); }};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.ALIEN_AI_TEAM = 1;
oGame.prototype.TERRAN_AI_TEAM = 2;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Init = function() {
    this.Log("Custom Init!");
    
    this.Core = new oCore(this, this.DATA.CoreData);
	
    //Start Core
    this.Core.Init();
    
    //Database Data
    //this.DATA.DBSetupSchema(this.APP.db);
    
	//Database Schemas
	this.APP.DB.MakeSchema("GlobalConfig");
	this.APP.DB.MakeSchema("Account");
	this.APP.DB.MakeSchema("UpgradePath");
	this.APP.DB.MakeSchema("Upgrade");
	this.APP.DB.MakeSchema("ShipTemplate");

    //Hook up custom processing
    //this.Core.GM.CustomProcess = this.Process;
    
    this.spawnAsteroids();
    this.SpawnMainBase();
};
//*********************************************************************************************************************************************
// Returns the gae state data related to waves in the format it is sent to clients
//*********************************************************************************************************************************************
oGame.prototype.GetWaveData = function() {
    return [this.WaveIndex, this.WaveShipCount, this.WaveShipLeftCount];
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Respawn = function(view, time) {
    // signal the client to move the camera
    this.LOGIC.Send(view.socket, 1, "respawn", [time]);
    if (time === 0) {
        // move the view to the station / origo, so that the client will see the objects there
        if (this.DATA.base2_obj) {
            //this.Log("moving view to base: " + this.DATA.base2_obj.x.get() + ", " + this.DATA.base2_obj.y.get());
            view.MoveView(this.DATA.base2_obj.x.get(), this.DATA.base2_obj.y.get());
        } else {
            //this.Log("moving view to origo");
            view.MoveView(0, 0);
        }
        view.Respawned = true;
    }
    view.RespawnTimeSent = time;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Process = function(dt) {
    
    //this.Log("CustomProc " + dt + " " + this.Name);
    //return;
    var D = this.DATA;

    if (this.WaveMode) {
        // wave mode logic
        // restart the game if needed (respawn station, reset waves)
        if (this.DATA.base2_obj.hp.get() <= 0) {
            // start countdown
            if (this.WaveRestartTime === 0) {
                this.WaveRestartTime = this.DATA.wave_restart_time;
                // send countdown info to clients
                this.WaveRestartTimeSent = this.WaveRestartTime;
                this.LOGIC.SendAll('wave', [0, this.WaveRestartTimeSent, 0]);
            } else {
                // progress with countdown
                this.WaveRestartTime -= dt;
                if (this.WaveRestartTime <= 0) {
                    this.WaveRestartTime = 0;
                    this.ResetWaves();
                } else {
                    // update countdown for clients
                    if (Math.ceil(this.WaveRestartTime) < this.WaveRestartTimeSent) {
                        this.WaveRestartTimeSent = Math.ceil(this.WaveRestartTime);
                        this.LOGIC.SendAll('wave', [0, this.WaveRestartTimeSent, 0]);
                    }
                }
            }
        } else {
            // spawn new enemy if there are enemies left to spawn and the spawn time elapsed 
            if (this.WaveShipSpawnIndex < this.WaveShipCount) {
                if (this.WaveShipSpawnTimeLeft <= 0) {
                    this.SpawnShip(this.WaveShipNames[this.WaveShipSpawnIndex], this.ALIEN_AI_TEAM); 
                    this.WaveShipSpawnIndex++;
                    //this.Log("Spawned enemy " + this.WaveShipSpawnIndex + "/" + this.WaveShipCount);
                    this.WaveShipSpawnTimeLeft = (this.WaveConfig.spawnOverMins * 60) / this.WaveShipCount;
                } else {
                    this.WaveShipSpawnTimeLeft -= dt;
                }
            }
            // count remaining enemies
            var tc = this.TeamCount();
            var shipsLeft = (this.WaveShipCount - this.WaveShipSpawnIndex) + tc[0]; // number of enemies yet to spawn + number of enemies alive
            // start new wave if all aliens were destroyed 
            if (shipsLeft === 0) {
                this.StartNewWave();
            } else if (shipsLeft !== this.WaveShipLeftCount) {
                this.WaveShipLeftCount = shipsLeft;
                this.LOGIC.SendAll('wave', this.GetWaveData());
            }
        }
    }
    //spawn AI enemies/friends timer
    if (D.MapEnemyCTime >= D.MapEnemyTime) {
        this.SpawnShips(this.WaveMode);
        D.MapEnemyCTime = 0;    
    }
    else
    {
        D.MapEnemyCTime += dt;    
    }
    
    //respawn asteroids timer
    if (D.AsteroidCTime >= D.AsteroidRespawnTime) {
        this.respawnAsteroids();
        D.AsteroidCTime = 0;    
    }
    else
    {
        D.AsteroidCTime += dt;    
    }
    
    // "respawn" process for all views - returning the camera and player to the station to equip a new ship a few seconds after being destroyed
    var Views = this.APP.NET.Views;
    var respawnTime = D.player_respawn_time;
    for (var v in Views) {
        if (Views.hasOwnProperty(v)) {
            var view = Views[v];
            if (view.GO && view.GO.Obj._remove && !view.Respawned) {
                // send updated respawn state (seconds left) to the client if needed (0 left means respawn)
                var t = Math.ceil(respawnTime - view.RespawnTime);
                if (t !== view.RespawnTimeSent) {
                    this.Respawn(view, t);
                }
                if (view.RespawnTime < respawnTime) {
                    view.RespawnTime = Math.min(respawnTime, view.RespawnTime + dt);
                }
            }
        }
    }
    
    //respawning players timers
    // ships are no longer respawned, player needs to buy them again
//    var ids = Object.keys(D.DeadPlayers);
//    for (var i = 0; i < ids.length; i++) {
//        if (D.DeadPlayers[ids[i]].time >= D.player_respawn_time) {
//            this.LOGIC.ResetShip(D.DeadPlayers[ids[i]].ship._view.socket, {spawnAtBase: true});
//            delete D.DeadPlayers[ids[i]];
//        } else {
//            D.DeadPlayers[ids[i]].time += dt;
//        }
//    }
    
    //globals processing
    //D.base_rstep += 1 * dt;//bases rotation
    if (D.base_rstep >= 360) { D.base_rstep = 0; }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.NetProcess = function() {
    //this.Log("Net " + " " + this.Name);
    this.APP.NET.UpdateAllViews();
    
    this.Core.CleanProcess();

    //Object scoping complete mark all data on datalist as Clean
    

};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.ScopeProcess = function(CObj) {
    //this.Log("Scope " + " " + this.Name);
    this.APP.NET.ScopeAllViews(CObj);
    
};
//*********************************************************************************************************************************************
// Returns random coordinates that are dist distance away from (x,y) and do not collide with any asteroids
//*********************************************************************************************************************************************
oGame.prototype._GetShipSpawnCoordinates = function(x, y, dist) {
    var xy;
    var failCount = 0;
    var radius = 200; // minimum distance from asteroids
    var asteroids = this.Core.GetGroup(this.Core.goTypes.indexOf("Asteroid"));
    var collision;
    
    do {
        xy = this.Core.MATH.rotate_by_pivot(x, y, Math.random() * 360, dist, 0);
        
        collision = !!this.Core.CD.CheckCollisionByRadius(xy[0], xy[1], radius, asteroids);
        if (collision) {
            //this.Log("Cannot spawn ship at "+xy[0]+","+xy[1]+" because of collision with asteroid");
            failCount++;
        }
    } while (collision && (failCount < 40));
    if (collision) {
        this.Log("Spawning ship anyway, too many collisions");
    }
    return xy;
} 
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.NewView = function(View) {

    //this.Log("GameView");

    //Setup Player Object in View
    var d = this.Core.Copy(this.DATA.Object_Defaults);
    d.type = this.DATA.CoreData.goTypes.indexOf("Ship");
    d.subtype = "_default"; 
    d.id = d.owner = View.NETID;//use Views id as id and owner (same)
    d.team = this.Core.GetNextTeamGroup();//2;//this.Core.GetNextTeam();
    d.w = 50; d.h = 50;
    this.Log("Player Ship - id: " + d.id + " team: " + d.team + " (players: " + this.APP.NET.ViewCount + ")");
    View.GO = new oShip(this, d);
    View.GO.side.set(2);
    View.GO._view = View;
    
    // spawn around center (dist distance from (x,y) point)
    var D = this.DATA;
    var dist = Math.floor(Math.random() * (D.player_spawn_distance_max - D.player_spawn_distance_min)) + D.player_spawn_distance_min;
    var x = 0;
    var y = 0;
    
    // spawn outside of main station
    if (this.DATA.base2_obj) {
        dist += this.DATA.base2_obj.dock_radius.get();
        x = this.DATA.base2_obj.x.get();
        y = this.DATA.base2_obj.y.get();
    }
    
    // avoid spawning into asteroids
    var xy = this._GetShipSpawnCoordinates(x, y, dist);
    
    View.GO.x.set(xy[0]);
    View.GO.y.set(xy[1]);
    
    //Use default ship if available
    this.LOGIC.UseShipTemplate(this.DATA.starting_ship, View.GO, true, null);
    
    //Setup Firing
    View.GO._f1a = View.GO.Shoot;//shooting function

};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype._spawnAsteroid = function(bounds) {
    var d = this.Core.Copy(this.DATA.Object_Defaults);
    d.id = this.Core.GetNextID();
    d.type = this.DATA.CoreData.goTypes.indexOf("Asteroid");
    
    var asteroidRadius = this.DATA.asteroidRadiusMin + Math.floor(Math.random() * (this.DATA.asteroidRadiusMax - this.DATA.asteroidRadiusMin));
    d.radius = asteroidRadius;
    d.w = asteroidRadius * 2;
    d.h = asteroidRadius * 2;
    
    // try different spawn positions until the asteroid doesn't collide with any ships and bases or the spawn fails too many times
    
    var collision = false;
    var ships = this.Core.GetGroup(this.Core.goTypes.indexOf("Ship"));
    var bases = this.Core.GetGroup(this.Core.goTypes.indexOf("Base"));
    var failCount = 0;

    do {
        d.x = bounds[0] + Math.floor((Math.random() * this.Core.AREA.Size));
        d.y = bounds[1] + Math.floor((Math.random() * this.Core.AREA.Size));
        
        collision = !!this.Core.CD.CheckCollisionByRadius(d.x, d.y, asteroidRadius + this.DATA.asteroid_spawn_min_distance, ships);
        if (collision) {
            //this.Log("Cannot spawn asteroid at "+d.x+","+d.y+" because of collision with ship");
            failCount++;
        } else {
            collision = !!this.Core.CD.CheckCollisionByRadius(d.x, d.y, asteroidRadius + this.DATA.asteroid_spawn_min_distance, bases);
            if (collision) {
                //this.Log("Cannot spawn asteroid at "+d.x+","+d.y+" because of collision with base");
                failCount++;
            }
        }
    } while (collision && (failCount < 10));
    
    if (!collision) {

        d.vx = Math.floor((Math.random() * 200) - 100);
        d.vy = Math.floor((Math.random() * 200) - 100);

        var ratio = (asteroidRadius - this.DATA.asteroidRadiusMin) / (this.DATA.asteroidRadiusMax - this.DATA.asteroidRadiusMin);
        d.hp = d.max_hp = this.DATA.asteroidHealthMin + Math.floor(ratio * (this.DATA.asteroidHealthMax - this.DATA.asteroidHealthMin));

        var GO = new oAsteroid(this, d);
        this.Core.ObjectAdd(GO);

        // this.Core.AREA.AddObj(GO); // do we need to register with area, nothing else does?

        //this.Log("Asteroid: " + d.type + " " + d.id);

        return GO;
    }
    
    this.Log("Asteroid spawn failed too many times and is aborted");
    return null;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.spawnAsteroids = function() {
    this.Log("spawnAsteroids");
    
    for(var i=0; i < this.Core.AREA.Areas.length; i++)
    {
        //var n = this.Core.AREA.Count(i, this.DATA.CoreData.goTypes.indexOf("Asteroid"));
        var n = 0;
        var targetCount = this.DATA.asteroidCountPerAreaMin + Math.floor(Math.random() * (this.DATA.asteroidCountPerAreaMax - this.DATA.asteroidCountPerAreaMin));
        this.Asteroids[i] = {
            targetCount: targetCount,
            objs: []
        };
        //this.Log("Area: "+i+" Asteroid targetCount: " + targetCount);
        var bounds = this.Core.AREA.AreaBounds(i);
        for (var s=n;s<targetCount;s++){
            var ast = this._spawnAsteroid(bounds);
            if (ast) {
                this.Asteroids[i].objs.push(ast);
            }
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.clearAsteroids = function() {
    this.Log("clearAsteroids");
    var TYPE_ASTEROID = this.DATA.CoreData.goTypes.indexOf("Asteroid"); // game object type
    var objs = this.Core.Objects[TYPE_ASTEROID];
    for (var id in objs) {
        if (objs.hasOwnProperty(id)) {
            objs[id].Obj.KILL();
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.respawnAsteroids = function() {
    //this.Log("respawnAsteroids");
    
    for(var i=0; i < this.Core.AREA.Areas.length; i++) {
        var asteroidsInArea = this.Asteroids[i].objs;
        for (var j=0; j<asteroidsInArea.length; j++) {
            if (asteroidsInArea[j].Obj._remove) {
                asteroidsInArea.splice(j,1);
                j--;
            }
        }
        
        if(asteroidsInArea.length < this.Asteroids[i].targetCount) {
            var bounds = this.Core.AREA.AreaBounds(i);
            var ast = this._spawnAsteroid(bounds);
            if (ast) {
                asteroidsInArea.push(ast);
            }
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.SpawnCoins = function() {
    /*
    //C.Log("SpawnCoins");    
    //this.GD.MapArea
    for(var i=0; i < this.AO.Areas.length; i++)
    {
        var n = this.AO.Count(i, this.DATA.CoreData.goTypes.indexOf("Coin"));
        if (n < this.GD.MapCoinTarget) {
            var bounds = this.AO.AreaBounds(i);
            //var n = this.GD.MapCoinTarget - this.GD.MapCoinCount;
            //var b_width = this.GD.MapBoundX;//bounds
            //var b_height = this.GD.MapBoundY;
            //var b_widthH = b_width/2;//mid bounds
            //var b_heightH = b_height/2;
            //for (var i=0; i< n; i++)
            //{
            var x = bounds[0] + Math.floor((Math.random() * this.GD.MapAreaSize));
            var y = bounds[1] + Math.floor((Math.random() * this.GD.MapAreaSize));
            var id = this.Core.GetNextID();
            var d = {type:this.DATA.CoreData.goTypes.indexOf("Coin"), subtype:0, w:30, h:30, x:x, y:y, r:0, vx:0, vy:0, move:"None"}
            var O = new oGO(this, id, d, -1);
            GD.Objects.Static[id] = O
            this.AO.AddObj(O);
            //}
        }
    }
    
    //slowly for peformance reasons
    //if(this.GD.MapCoinCount < this.GD.MapCoinTarget)
    //{
        //C.Log(n)
        
    //}
    */
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.SpawnMainBase = function() {
    this.Log("SpawnMainBase");
    var d, GO;
   
    d = this.Core.Copy(this.DATA.Object_Defaults);
    d.id = this.Core.GetNextID();
    d.type = this.DATA.CoreData.goTypes.indexOf("Base");
    d.subtype = this.DATA.main_station_id;
    d.team = this.TERRAN_AI_TEAM;
    this.DATA.base_rstep = 0;//reset
    d.x = -this.DATA.base_radius; 
    d.y = 0;
    var config = this.Stats.getStation(this.DATA.main_station_id);
    d.w = config.hitbox;
    d.h = config.hitbox;
    d.radius = config.dockRadius;
    d.hp = config.health;
    GO = new oBase(this, d, config);
    GO.side.set(2);
    this.Core.ObjectAdd(GO);
    this.DATA.base2_obj = GO;
    GO.alwaysTransfer = true; // transfer to client even when not in its scope

    //DEBUG
    this.Log("Base: " + d.id + " team: " + d.team);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.generateName = function() {
    var aiNames = ['Escobar','Picard','Lundgren','Apollo','Deckard','Reynolds','Lannister','Sepiroth','Mario','Starbuck','Sisko','Worf','Baltar','Six','Boomer','Saul','Trump',"O'Brien",'Rush','Young','Greer','Maverick','Iceman','Khan','Paladin','Vagabond','Hawk','Vansen','West','Hawk','Ross'];
    var ix = Math.floor(Math.random() * aiNames.length);
    var aiName = aiNames[ix];
    return aiName;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.TeamCount = function() {
    var t1=0, t2=0;
    this.Core.ExecuteForObjects("Ship",(ship)=>{
        if (!ship.mothership) { // ships spawned by motherships do not count
            if(ship.team.get() === this.ALIEN_AI_TEAM) {
                t1+=1;
            } else {
                t2+=1;
            }
        }
    });
    return [t1, t2]; // t1: aliens, t2: everyones else (AI terrans, player terrans)
};
//*********************************************************************************************************************************************
// Chooses and returns a ship type id for the passed spawner team, based on the spawning chances for that team
//*********************************************************************************************************************************************
oGame.prototype._ShipForTeam = function(spawnerTeam) {
    var r = Math.floor((Math.random() * this.DATA.SpawnerRange) + 1);
    for (var i in this.DATA.ShipList)
    {
        if (this.DATA.ShipList.hasOwnProperty(i)) {
            var ShipName = this.DATA.ShipList[i];
            var sP_val = spawnerTeam[ShipName];
            if(sP_val !== "0"){
                sP_val = sP_val.split(",");
                if (r >= parseInt(sP_val[0]) && r <= parseInt(sP_val[1]))
                {
                    return ShipName;    
                }
            }
        }
    }
    return null;
}
//*********************************************************************************************************************************************
// Spawns an AI ship at random position
//*********************************************************************************************************************************************
oGame.prototype.SpawnShip = function(shipName, team, mothership) {
//    this.Log("Spawn ship: " + shipName + ", team: " + team);
    var d = this.Core.Copy(this.DATA.Object_Defaults);
    d.id = this.Core.GetNextID();
    d.type = this.DATA.CoreData.goTypes.indexOf("Ship");
    d.team = team;
    d.w = 50; d.h = 50;
    var GO = new oShip(this, d);
    GO._AI.active = true;
    GO._AI.name = 'AI ' + this.generateName() + " " + d.id;
    GO.username.set(GO._AI.name + "()");
    this.Core.ObjectAdd(GO);
    //this.Log("AI Ship - type: " + d.type + " id: " + d.id + " team: " + d.team);
    
    if (!mothership) {
        var D = this.DATA;
        var dist = Math.floor(Math.random() * (D.base_redzone - D.ai_spawn_distance_min)) + D.ai_spawn_distance_min;
        // avoid collision with asteroids
        var xy = this._GetShipSpawnCoordinates(0, 0, dist);

        GO.x.set(xy[0]);
        GO.y.set(xy[1]);
    } else {
        GO.x.set(mothership.x.get());
        GO.y.set(mothership.y.get());
        GO.r.set(mothership.r.get());
        GO.mothership = mothership;
        GO.new.set(false); // spawned ship does not count as new - no warp in animation
    }

    this.LOGIC.UseShipTemplate(shipName, GO, true, null);
    GO._f1a = GO.Shoot;//Setup Firing
    return GO;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.SpawnShips = function(terranOnly) {
    //C.Log("SpawnEnemies");

    //spawn if less than needed
    var C = this.Core;
    
    var NextTeam;
    if (terranOnly) {
        NextTeam = this.TERRAN_AI_TEAM;
    } else {
        var tc = this.TeamCount();
        var tratio;

        //get ratio - avoid divide by 0
        if(tc[0] > 0){
            tratio = tc[1] / tc[0];    
            if(tratio > 1 - (this.DATA.AI_spawn_ratio / 100)) {
                //this.Log("[1]" + JSON.stringify(this.TeamCount()) + " ratio " + tratio + " target ratio: "  + (1 - (this.DATA.AI_spawn_ratio / 100)));
                NextTeam = this.ALIEN_AI_TEAM;
            } else {
               //this.Log("[2]" + JSON.stringify(this.TeamCount()) + " ratio " + tratio + " target ratio: "  + (1 - (this.DATA.AI_spawn_ratio / 100)));
                NextTeam = this.TERRAN_AI_TEAM;
            }

        } else {
            NextTeam = this.ALIEN_AI_TEAM;
        }
    }
    
    // when spawning terran (friendly) ships only, spawn until the count of terran ships reaches MapFriendTarget
    // otherwise, spawn until the count of all ships reaches MapEnemyTarget
    var shipCount = 0;
    if (terranOnly) {
        C.ExecuteForObjects("Ship", (ship)=>{
            if (ship.team.get() === this.TERRAN_AI_TEAM) {
                shipCount++;
            }
        });
    } else {
        shipCount = C.CountDict(C.GetGroup(C.goTypes.indexOf("Ship")));
    }
    
    if(shipCount < (terranOnly ? this.DATA.MapFriendTarget : this.DATA.MapEnemyTarget))
    {
        
        //var NextTeam = this.Core.GetNextTeam();
        var sP_team;
        if(NextTeam === this.ALIEN_AI_TEAM){ sP_team = this.DATA.SpawnerTeamAlien; }
        else { sP_team = this.DATA.SpawnerTeamTerran; }
        
        var STemplate = this._ShipForTeam(sP_team);
        //Create if Random Template name Selected
        if(STemplate){
            this.SpawnShip(STemplate, NextTeam);
            
        }
        
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.ClearWave = function () {
    // remove alien ships
    this.Core.ExecuteForObjects("Ship",(ship)=>{
        if (ship.team.get() === this.ALIEN_AI_TEAM) {
            ship.Obj.KILL();
        }
    });
    // mark all ships as spawned
    this.WaveShipSpawnIndex = this.WaveShipCount;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.ResetWaves = function() {
    this.Log("RESETTING WAVES");
    this.WaveIndex = 0;
    this.WaveShipCount = 0;
    this.WaveShipLeftCount = 0;
    this.ClearWave();
    // reset base
    if (this.DATA.base2_obj) {
        this.DATA.base2_obj.Obj.KILL();
    }
    this.SpawnMainBase();
    this.LOGIC.SendAll('wave', this.GetWaveData());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.StartNewWave = function() {
    this.WaveIndex++;
    this.WaveShipCount = 0;
    this.WaveShipLeftCount = 0;
    this.WaveShipNames = [];
    this.WaveShipSpawnIndex = 0;
    this.WaveConfig = this.Stats.Waves.find((c)=>(c.number === this.WaveIndex));
    if (!this.WaveConfig) {
        this.Log("No wave config found for wave number " + this.WaveIndex + ", resetting waves.");
        this.ResetWaves();
        return;
    }
    var spawnedDiff = 0;
    while (spawnedDiff < this.WaveConfig.minDifficulty) {
        // spawn enemy
        var ships = this.Stats.Ships.filter((ship) => (ship.difficulty_weight > 0));
        var ship;
        do {
            var shipName = this._ShipForTeam(this.DATA.SpawnerTeamAlien);
            ship = ships.find((s)=>(s.data_name === shipName));
            
        } while ((spawnedDiff + ship.difficulty_weight) > this.WaveConfig.maxDifficulty);
        this.WaveShipNames.push(ship.data_name);
        spawnedDiff += ship.difficulty_weight;
        this.WaveShipCount++;
    }
    this.WaveShipLeftCount = this.WaveShipCount;
    this.Log("STARTING NEW WAVE: " + this.WaveIndex + " (ships: " + this.WaveShipCount + ", health: " + Math.round(this.WaveConfig.healthMultiply*100) + "%, damage: " + Math.round(this.WaveConfig.damageMultiply*100) + "%)");
    this.LOGIC.SendAll('wave', this.GetWaveData());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.getHealthMultiply = function (team) {
    return (this.WaveConfig && (team === this.ALIEN_AI_TEAM)) ? this.WaveConfig.healthMultiply : 1;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.getDamageMultiply = function (team) {
    return (this.WaveConfig && (team === this.ALIEN_AI_TEAM)) ? this.WaveConfig.damageMultiply : 1;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oGame;
