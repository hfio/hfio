//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Client APIs
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oAPI(Game)
{
    //debugging - disable for production to avoid accepting admin API requests
    this.DEBUG = true;
    
    this.Game = Game;
    this.Send = this.Game.APP.NET.Send;//ref

}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oAPI.prototype.Log = function(m) { if(this.DEBUG){ console.log("[API] " + m); }}
//*********************************************************************************************************************************************
// Validate an incoming message format (basic)
//*********************************************************************************************************************************************
oAPI.prototype.ValidateMessage = function(d) {
    try{ d = JSON.parse(d); } catch(e){ return false; }
    if(!this.ValidateValue(d, 'object')){ return false; }
    if(d.hasOwnProperty("cmd") == false ){ return false; }
    return d;
}
//*********************************************************************************************************************************************
//Validate type = 'number', 'string', 'boolean', 'object', 'function' (not null or undefined as well)
//*********************************************************************************************************************************************
oAPI.prototype.ValidateValue = function(val, type) {
    //this.Log("ValidateValue: " + val);
    if (val == null || val == undefined){ return false; }
    //this.Log((typeof val) + " " + type);
    if (typeof val != type) { return false; }
    return true;
}
//*********************************************************************************************************************************************
// Validate dict has keys
//*********************************************************************************************************************************************
oAPI.prototype.ValidateFields = function(d, fields) {
    for(var i=0; i < fields.length; i++)
    {
        if (d.hasOwnProperty(fields[i]) == false) {
            return false;
        }
    }
    return true;
}
//*********************************************************************************************************************************************
// Validate dict has field and field is this type kvs = [[<key>,<type>]]
//*********************************************************************************************************************************************
oAPI.prototype.ValidateFieldsAndTypes = function(d, kvs) {
    for(var i=0; i < kvs.length; i++)
    {
        var key = kvs[i][0];
        var type = kvs[i][1];
        this.Log("VF " + key + " " + type);
        if (d.hasOwnProperty(key) == false) { return false; }//key must exist
        this.Log("VF " + d[key] + " " + type);
        if (!this.ValidateValue(d[key], type)) { return false; }//data value must be of type
    }
    return true;
}
//*********************************************************************************************************************************************
//Standard name validationt (name)
//*********************************************************************************************************************************************
oAPI.prototype.ValidateName = function(d, key, max) {
    if(this.ValidateFields(d, [key]) == false){ return false; }
    if(this.ValidateValue(d.name, "string") == false) { return false; }
    if(d.name.length < 1){ return false; }
    if(d.name.length > max){ return false; }
    return true;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oAPI.prototype.Call = function(socket, d) {
    //DEBUG
    //C.Log("IN: ");
//        sio.app.Message(socket, d);
    var L = this.Game.LOGIC;

    //this.Log("msg" + d);

    var out = false;

    //Basic Validation
    d = this.ValidateMessage(d);//JSON parse, = object, has cmd
    if (d == false) { this.Send(this.socket, 0, 'none', "Not valid data"); return; }
    
    //Client APIs / Commands available
    var cmd = d.cmd;
    
    //update our inputs (if valid)
    if (cmd == "input") { this.Input(socket, d); }
    if (cmd == 'global') { if(this.ValidateFields(d, ['key', 'value'])){  L.set_global(socket, d['key'], d['value'], d); } }
    if (cmd == 'getglobal') { if(this.ValidateFields(d, ['key'])){  L.get_global(socket, d['key'], d); } }    
    if (cmd == "scope") { if(this.ValidateFields(d, ['w', 'h'])){ socket.VIEW.UpdateWH(d.w, d.h); } }
    if (cmd == 'tweak') { this.Tweak(d); }
    if (cmd == 'api_list') { if(this.ValidateFields(d, ['obj'])){ L.api_list(socket, d['obj'], d); } }
    if (cmd == 'api_read') { if(this.ValidateFields(d, ['obj', 'name'])){  L.api_read(socket, d['obj'], d['name'], d); } }
    if (cmd == 'api_cu') {  if(this.ValidateFields(d, ['obj', 'name'])){ L.api_cu(socket, d['obj'], d['name'], d); } }
    if (cmd == 'api_delete') {  if(this.ValidateFields(d, ['obj', 'name'])){ L.api_delete(socket, d['obj'], d['name'], d); } }
    if (cmd == 'startgame') { L.StartGame(socket, d); }
    if (cmd == 'unlockship') { if(this.ValidateFields(d, ['name'])){ L.UnlockShip(socket, d['name'], d); } }
    if (cmd == 'pickship') { if(this.ValidateFields(d, ['name'])){ L.PickShip(socket, d['name'], d); } }
    if (cmd == 'respawn') { L.ResetShip(socket, d); }
    if (cmd == 'repair') { L.RepairShip(socket, d); }
    //if (cmd == 'refuel') { L.RefuelShip(socket, d); } // unused for now!
    if (cmd == 'restock') { L.RestockShip(socket, d); }
    if (cmd == 'startmission') { if(this.ValidateFields(d, ['missionId'])){ L.StartMission(socket, d['missionId'], d); } }
    if (cmd == 'claimmission') { L.ClaimMission(socket, d); }
    if (cmd == 'auth') { L.Auth(socket, d); }
    if (cmd == 'namechange') { L.NameChange(socket, d); }
    if (cmd == 'dock') { L.DockShip(socket, d); }
    if (cmd == 'undock') { L.UndockShip(socket, d); }
    //if (cmd == 'toggle') { L.ToggleSlot(socket, d); }
    if (cmd == 'activate') { L.ActivateSlot(socket, d); }
    if (cmd == 'equip') { L.EquipBuff(socket, d); }
    if (cmd == 'unequip') { L.UnequipBuff(socket, d); }
    if (cmd == 'upgrade') { L.UpgradeBuff(socket, d); }
    if (cmd == 'sellbuff') { L.SellBuff(socket, d); }
    if (cmd == 'reroll') { L.RerollBuff(socket, d); }
    if (cmd == 'buystation') { L.BuyStation(socket, d); }
    if (cmd == 'deploystation') { L.DeployStation(socket, d); }
    if (cmd == 'equipstation') { L.EquipStation(socket, d); }
    if (cmd == 'unequipstation') { L.UnequipStation(socket, d); }
    if (cmd == 'reset_ships') { L.ResetOwnedShips(socket, d); }
    if (cmd == 'reset_tech') { L.ResetOwnedTech(socket, d); }
    if (cmd == 'api_ref') { L.JoinFleet(socket, d); }
    if (cmd == 'api_refexit') { L.ExitFleet(socket, d); }
    if (cmd == 'say') { L.Say(socket, d); }
    if (cmd == 'cashout') { L.CashOut(socket, d); }
    if (this.DEBUG) {
        if (cmd == 'freemoney') { L.FreeMoney(socket, d); }
        if (cmd == 'freetechpoints') { L.FreeTechPoints(socket, d); }
        if (cmd == 'add_missing_tech') { L.AddMissingTech(socket, d); }
        if (cmd == 'complete_mission') { L.CompleteMission(socket, d); }
        if (cmd == 'clear_wave') { L.ClearWave(socket, d); }
    }
   
    if (out != false) {
        this.Send(socket, out[0], out[1], out[2]);//result, cmd, data
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oAPI.prototype.Input = function(socket, d) {
    //this.Log(JSON.stringify(d))
    if(this.ValidateFields(d, ['up', 'down', 'left', 'right', 'f1', 'vx', 'vy', 'x', 'y','r'])){
        //if (d.up == 0 || d.up == 1) { this.GO.up = d.up; }
        //if (d.down == 0 || d.down == 1) { this.GO.down = d.down; }
        //if (d.left == 0 || d.left == 1) { this.GO.left = d.left; }
        //if (d.right == 0 || d.right == 1) { this.GO.right = d.right; }
        if (d.f1 == 0 || d.f1 == 1) { socket.VIEW.GO._f1 = d.f1; }
        if (typeof d.vx == 'number') { socket.VIEW.GO._mx = parseInt(d.vx); }//mouse vector
        if (typeof d.vy == 'number') { socket.VIEW.GO._my = parseInt(d.vy); }
        if (typeof d.x == 'number') { socket.VIEW.GO._clx = parseInt(d.x); }//clients local coords (client predicted)
        if (typeof d.y == 'number') { socket.VIEW.GO._cly = parseInt(d.y); }//clients local coords (client predicted)
        if (typeof d.r == 'number') { socket.VIEW.GO._clr = parseInt(d.r); }//clients local coords (client predicted)
        
        //C.Log(C.JSON(d));
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oAPI;
