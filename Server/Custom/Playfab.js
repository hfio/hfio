//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Playfab server side module
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var _ = require('lodash');
var PlayFab = require('../node_modules/playfab-sdk/Scripts/PlayFab/PlayFab.js');
//var PlayFabServer = require('../node_modules/playfab-sdk/Scripts/PlayFab/PlayFabServer.js');

function CompileErrorReport(error) {
    if (error === null)
        return "";
    var fullErrors = error.errorMessage;
    for (var paramName in error.errorDetails)
        for (var msgIdx in error.errorDetails[paramName])
            fullErrors += "\n" + paramName + ": " + error.errorDetails[paramName][msgIdx];
    return fullErrors + "\n" + JSON.stringify(error);
}

function oPlayfab(Game) {
    //debugging
    this.DEBUG = true;

    this.Game = Game;

    this.userDataDefaults = {
        xp: 0,
        kills: 0,
        deaths: 0,
        credits: 0,
        buffs: null
    }

    this.Log('Initialising Playfab');

}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oPlayfab.prototype.Log = function(m) { if(this.DEBUG){ console.log("[PLAYFAB] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oPlayfab.prototype.verifyTicket = function(ticket,callback) {
    //this.Log('Verifying session ticket...'+ticket);
    var self = this;

    PlayFab.settings.titleId = '7B61';
    PlayFab.settings.developerSecretKey = '7U74R47R3BOHMQD5YHZTSUC1QMKOWUAOAD73SEG9I9OTYDAZF3';

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/AuthenticateSessionTicket", { SessionTicket: ticket }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            
            var playfabId = result.data.UserInfo.PlayFabId;
            //self.Log('pfid from ticket: '+playfabId);
            if (callback) callback(playfabId);
            
        } else if (error !== null) {
            self.Log("AuthenticateSessionTicket error");
            self.Log(CompileErrorReport(error));
        }
    });
}


//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// User details from playfab
oPlayfab.prototype.getUser = function(pfid,callback) {
    var self = this;
    
    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/GetUserAccountInfo", { PlayFabId: pfid }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            
            var userAccount = result.data.UserInfo;
            //self.Log('userAccount:'+JSON.stringify(userAccount));

            // copy defaults
            userAccount = JSON.parse(JSON.stringify(userAccount));
            userAccount.data = JSON.parse(JSON.stringify(self.userDataDefaults));

            // load our custom internal data fields
            self.loadUserData(pfid,userAccount,callback);

        } else if (error !== null) {
            self.Log("GetUserAccountInfo error");
            self.Log(CompileErrorReport(error));
        }
    });

}

// we want to remove this at some point
// and go straight to loadUserStatistics
oPlayfab.prototype.loadUserData = function(pfid,userAccount,callback) {
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/GetUserInternalData", { PlayFabId: pfid }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            
            var userData = result.data.Data;

            _.each(userData,function(value,key){
                //self.Log('key:'+key + ' value:'+value.Value);
                userAccount.data[key] = value.Value; // overwrite
            });

            self.loadUserStatistics(pfid,userAccount,callback);

        } else if (error !== null) {
            self.Log("GetUserInternalData error");
            self.Log(CompileErrorReport(error));
        }
    });

}

// playfab player stats
oPlayfab.prototype.loadUserStatistics = function(pfid,userAccount,callback) {
    var self = this;


    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/GetPlayerStatistics", { PlayFabId: pfid }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            
            var stats = result.data.Statistics;

            _.each(stats,function(item){
                //self.Log('Load player stat '+item.StatisticName + ' => ' + item.Value);
                userAccount.data[item.StatisticName] = item.Value; // overwrite
            });

            // load playfab inventory and currency info
            self.loadUserInventory(pfid,userAccount,callback);

        } else if (error !== null) {
            self.Log("GetPlayerStatistics error");
            self.Log(CompileErrorReport(error));
        }
    });

}

// playfab player inventory
oPlayfab.prototype.loadUserInventory = function(pfid,userAccount,callback) {
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/GetUserInventory", { PlayFabId: pfid }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));

            var inventory = result.data.Inventory;
            var bankedCredits = result.data.VirtualCurrency.CR;
            var techpoints = result.data.VirtualCurrency.TP;

            userAccount.data['bank'] = bankedCredits;
            userAccount.data['techpoints'] = techpoints;

            var ownedShips = [];
            _.each(inventory,function(item){
                var itemClass = item.ItemClass;
                if (itemClass === 'SHIP'){
                    ownedShips.push({
                        ItemId: item.ItemId,
                        ItemInstanceId: item.ItemInstanceId,
                        CustomData: item.CustomData
                    });
                }
            });
            userAccount.data['ownedShips'] = ownedShips;

            var ownedTech = [];
            _.each(inventory,function(item){
                var itemClass = item.ItemClass;
                if (itemClass === 'TECH'){
                    ownedTech.push({
                        ItemId: item.ItemId,
                        ItemInstanceId: item.ItemInstanceId,
                        CustomData: item.CustomData
                    });
                }
            });
            userAccount.data['ownedTech'] = ownedTech;
            
            var ownedStations = [];
            _.each(inventory,function(item){
                var itemClass = item.ItemClass;
                if (itemClass === 'STATION'){
                    ownedStations.push({
                        ItemId: item.ItemId,
                        ItemInstanceId: item.ItemInstanceId,
                        CustomData: item.CustomData
                    });
                }
            });
            userAccount.data['ownedStations'] = ownedStations;

            if (callback) callback(userAccount);
        } else if (error !== null) {
            self.Log("GetUserInventory error");
            self.Log(CompileErrorReport(error));
        }
    });

}


// Save player data to playfab
oPlayfab.prototype.saveUserData = function(pfid, playerAccountData, includeStatistics) {

    // custom data - we want to remove this
    var data = {
        shipName: playerAccountData.shipName.get(),
        baseID: playerAccountData.baseID.get(),
        activeMission: playerAccountData.activeMission.get(),
        missionProgress: playerAccountData.missionProgress.get(),
        missions: playerAccountData.missions.get(),
    }
    
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/UpdateUserInternalData", { PlayFabId: pfid, Data: data }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
        } else if (error !== null) {
            self.Log("UpdateUserInternalData error");
            self.Log(CompileErrorReport(error));
        }
    });
    
    if (includeStatistics) {
        // player stats
        var statistics = [
            {
              "StatisticName": "xp",
              "Value": playerAccountData.xp.get()
            },
            {
              "StatisticName": "kills",
              "Value": playerAccountData.kills.get()
            },
            {
              "StatisticName": "deaths",
              "Value": playerAccountData.deaths.get()
            }
        ];
        PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/UpdatePlayerStatistics", { PlayFabId: pfid, Statistics: statistics }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
            if (result !== null) {
                //self.Log(JSON.stringify(result));
            } else if (error !== null) {
                self.Log("UpdatePlayerStatistics error");
                self.Log(CompileErrorReport(error));
            }
        });
    }

}

oPlayfab.prototype.CURRENCY = {
    CREDITS: 'CR',
    TECHPOINTS: 'TP'
};

oPlayfab.prototype.addCredits = function(pfid, currency, amount, callback) {
    
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/AddUserVirtualCurrency", { PlayFabId: pfid, VirtualCurrency: currency, Amount: amount }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            if (callback) callback();
        } else if (error !== null) {
            self.Log("AddUserVirtualCurrency error");
            self.Log(CompileErrorReport(error));
        }
    });

}

oPlayfab.prototype.removeCredits = function(pfid, currency, amount, callback) {
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/SubtractUserVirtualCurrency", { PlayFabId: pfid, VirtualCurrency: currency, Amount: amount }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            if (callback) callback();
        } else if (error !== null) {
            self.Log("SubtractUserVirtualCurrency error");
            self.Log(CompileErrorReport(error));
        }
    });

}

oPlayfab.prototype.grantItem = function(pfid,itemId,callback) {
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/GrantItemsToUser", { PlayFabId: pfid, ItemIds: [itemId] }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            if (callback) callback(result.data.ItemGrantResults);
        } else if (error !== null) {
            self.Log("GrantItemsToUser error");
            self.Log(CompileErrorReport(error));
        }
    });

}

oPlayfab.prototype.updateItemData = function (pfid, itemInstanceId, data, callback) {
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/UpdateUserInventoryItemCustomData", { PlayFabId: pfid, ItemInstanceId: itemInstanceId, "Data": data }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            if (callback) callback();
        } else if (error !== null) {
            self.Log("UpdateUserInventoryItemCustomData error");
            self.Log(CompileErrorReport(error));
        }
    });
}

oPlayfab.prototype.grantItemFromDropTable = function(pfid,dropTableId,callback) {
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/EvaluateRandomResultTable", { PlayFabId: pfid, TableId: dropTableId }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            this.grantItem(pfid,result.data.ResultItemId,callback);
        } else if (error !== null) {
            self.Log("EvaluateRandomResultTable error");
            self.Log(CompileErrorReport(error));
        }
    }.bind(this));

}

oPlayfab.prototype.revokeItem = function(pfid, itemInstanceId, callback) {
    var self = this;

    PlayFab.MakeRequest(PlayFab.GetServerUrl() + "/Server/RevokeInventoryItem", { PlayFabId: pfid, ItemInstanceId: itemInstanceId }, "X-SecretKey", PlayFab.settings.developerSecretKey, function (error, result) {
        if (result !== null) {
            //self.Log(JSON.stringify(result));
            if (callback) callback();
        } else if (error !== null) {
            self.Log("RevokeInventoryItem error");
            self.Log(CompileErrorReport(error));
        }
    });

}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oPlayfab;