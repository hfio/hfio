var Filter = require('bad-words');
var Web3 = require('web3');
var solc = require("solc");
var fs = require("fs");

////*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oLogic(Game) {
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;
    this.Send = this.Game.APP.NET.Send;//ref
    this.SendAll = this.Game.APP.NET.SendAll.bind(this.Game.APP.NET);//ref
    
    this.filter = new Filter(); // profanity filter
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.Log = function(m) { if(this.DEBUG){ console.log("[LOGIC] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// spawn by base if given, or randomly around the center of the map, if not
oLogic.prototype.SpawnByBase = function(GO, base, atCenter) {
    var d = this.Game.DATA;
    var m = d.base_spawn_range / 2;
    var x,y,bx,by;
    
    bx = base ? base.x.get() : 0; 
    by = base ? base.y.get() : 0;
    
    x = bx + ((base && atCenter) ? 0 : Math.floor((Math.random() * d.base_spawn_range) - m));
    y = by + ((base && atCenter) ? 0 : Math.floor((Math.random() * d.base_spawn_range) - m));
    
    GO.x.set(x);
    GO.y.set(y);
    
    //this.Log("Client Coords: " + (GO.x.get() - d.base1_obj.x.get()) + " " + (GO.y.get() - d.base1_obj.y.get()));
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.UseShipTemplate = function(name, go, equipWeapon, callback) {
    var logic = this;
    var game = this.Game;

    //Find stats which match ship name
    var stats = game.Stats.Ships.find((stats) => stats.data_name === name);

    if (stats) {
        if (go.buffManager) {
            go.buffManager.unequipAll(true); // noSave: true -> no need to save here, we either equip the default weapon later or set the loadout from stored data, both of which
                                             // will trigger a buffmanager save   
        }
        if (go.stationManager) {
            go.stationManager.unequip();
        }
        game.LOGIC.SetShipStats(go, {
            subtype: name,
            marea: stats.move_radius,
            marea_max: stats.move_radius_max,
            health: stats.health * game.getHealthMultiply(go.team.get()),
            total_shots: stats.total_shots || 0,
            fdropr: stats.fire_drop_rot,
            fdrop: stats.fire_drop,
            rot_accelerate: stats.rotation_speed,
            accelerate: stats.accel_sec,
            speed: stats.max_speed,
            name: stats.data_name,
            w: stats.hitbox,
            h: stats.hitbox,
            radius: stats.hitbox * 0.5,
            credits_for_kill: stats.credits_for_kill,            
            ai_check_sec: stats.ai_check_sec,
            ai_overshoot_factor: stats.ai_overshoot_factor,
            ai_need_1: stats.ai_need_1,
            ai_need_2: stats.ai_need_2,
            ai_need_3: stats.ai_need_3,
            ai_need_4: stats.ai_need_4,
            ai_need_5: stats.ai_need_5,
            spawned_ship: stats.spawned_ship,
            ship_spawn_time: stats.ship_spawn_time,
            max_ships_spawned: stats.max_ships_spawned
        });
        if (equipWeapon) {
            go.EquipWeapon(stats.default_weapon);
        }
        //logic.Log("Using Ship! " + name);
        if (callback) { callback(stats); }
        return true;
    }

    logic.Log("No Ship Available! " + name)
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.SetShipStats = function(GO, d) {
    //this.Log("SET STATS " + JSON.stringify(d))
    GO.subtype.set(d.name);
    GO.max_speed.set(parseFloat(d.speed));
    GO._max_speedSQ = GO.maxspeed * GO.maxspeed;
    GO.acceleration.set(parseFloat(d.accelerate));
    GO.rot_speed.set(parseFloat(d.rot_accelerate));
    GO.shoot_drop.set(parseFloat(d.fdrop) / 100);//drop speed percentage on fire
    GO.shoot_drop_r.set(parseFloat(d.fdropr) / 100);//drop speed percentage on fire
    GO.hp.set(parseFloat(d.health));
    GO.max_hp.set(parseFloat(d.health));
    GO.default_max_hp.set(parseFloat(d.health));
    GO.bounty.set(parseFloat(d.credits_for_kill));
    GO.ammo.set(parseFloat(d.total_shots));
    GO.max_ammo.set(parseFloat(d.total_shots));
    GO.default_max_ammo.set(parseFloat(d.total_shots));
    if (d.marea > 1) { GO.move_radius.set(parseFloat(d.marea)); }
    if (d.marea_max > 1) { GO.move_radius_max.set(parseFloat(d.marea_max)); }
    GO.Obj.ChangeWH(d.w, d.h); // hitbox
    GO.radius.set(d.radius);
    GO._AI.ai_overshoot_factor = parseFloat(d.ai_overshoot_factor);
    GO._AI.ai_check_sec = d.ai_check_sec.split(",");
    GO._AI.ai_check_sec[0] = parseFloat(GO._AI.ai_check_sec[0]);//to floats
    GO._AI.ai_check_sec[1] = parseFloat(GO._AI.ai_check_sec[1]);
    GO._AI.check_cooldown = GO._AI.ai_check_sec[0];
    GO._AI.check_current = GO._AI.ai_check_sec[0] + 1;//fill up already
    GO._AI.ai_need_1 = d.ai_need_1.split(",");
    GO._AI.ai_need_2 = d.ai_need_2.split(",");
    GO._AI.ai_need_3 = d.ai_need_3.split(",");
    GO._AI.ai_need_4 = d.ai_need_4.split(",");
    GO._AI.ai_need_5 = d.ai_need_5.split(",");
    GO._SpawnedShip.name = d.spawned_ship;
    GO._SpawnedShip.time = d.ship_spawn_time;
    GO._SpawnedShip.timeLeft = d.ship_spawn_time;
    GO._SpawnedShip.maxCount = d.max_ships_spawned;
    GO._SpawnedShip.list = [];
    //this.Log(GO._AI.check_cooldown + " " + JSON.stringify(GO._AI.ai_need_1) + " " + JSON.stringify(GO._AI.ai_need_2) + " " + JSON.stringify(GO._AI.ai_need_3) + " " + JSON.stringify(GO._AI.ai_need_4) + " " + JSON.stringify(GO._AI.ai_need_5));
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.JoinFleet = function(socket, d) {
    //this.Log("JoinFleet");
    var GO = socket.VIEW.GO;
    
    if(!d.hasOwnProperty("id")){ this.Log("Invalid JoinFleet (missing id) "); return; }
    d.id = parseInt(d.id);
    if (typeof d.id !== "number") { this.Log("Invalid JoinFleet Id: " + d.id); return; }
    
    //search for friend and use his team and spawn near
    //check only views/ships
    var Views = this.Game.APP.NET.Views;
    for (var v in Views) {
        if (Views.hasOwnProperty(v)) {
            var O = Views[v].GO;//ship (dead or alive)
            if(O.id.get() === d.id){
                var team = O.team.get();
            
                //Check Max
                var count = 0;
                var objs = this.Game.Core.GetGroup(this.Game.Core.goTypes.indexOf("Ship"));
                for (var key in objs) {
                    if (objs.hasOwnProperty(key)) {
                        var O2 = objs[key];
                        if(O2.team.get() === team)
                        {
                            count += 1;
                        }
                    }
                }

            //this.Log("Found Friend Team: " + team + " c: " + count + "/" + this.Game.DATA.fleet_limit);
            
            if (count > this.Game.DATA.fleet_limit) { this.Log("Fleet is Full!"); return; }
            
            GO.team.set(O.team.get());
            GO.x.set(O.x.get());
            GO.y.set(O.y.get());
            return;
            }
        }
    }

    //this.Log("Invalid JoinFleet Id (No Exists): " + d.id);
/*
    var objs = this.Game.Core.GetGroup(this.Game.Core.goTypes.indexOf("Ship"));
    if (objs.hasOwnProperty(d.id)) {
        var O = objs[d.id];
        if (O._AI.active === false) {
            var team = O.team.get();

            //Check Max
            var count = 0;
            for (var key in objs) {
                if (objs.hasOwnProperty(key)) {
                    var O2 = objs[key];
                    if(O2.team.get() == team)
                    {
                        count += 1;
                    }
                }
            }

            this.Log("Found Friend Team: " + team + " c: " + count + "/" + this.Game.DATA.fleet_limit);
            
            if (count > this.Game.DATA.fleet_limit) { this.Log("Fleet is Full!"); return; }
            
            GO.team.set(O.team.get());
            GO.x.set(O.x.get());
            GO.y.set(O.y.get());
        }
        else{
            this.Log("Cannot Joing AI Team: " + O.team.get());
        }
    }
    else
    {
        this.Log("Invalid JoinFleet Id (No Exists): " + d.id);
    }
    */
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.ExitFleet = function(socket, d) {
    var t = this.Game.Core.GetNextTeamGroup();
    //this.Log("ExitFleet " + t);
    socket.VIEW.GO.team.set(t);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.Say = function(socket, d) {
    var GO = socket.VIEW.GO;
    this.Log("Ship " + GO.id.get() + " Say (scope: " + d.scope +"): " + d.text);
    GO.message.set(this.filter.clean(d.text));    
    GO.message_scope.set(d.scope);
    GO.message_timeleft.set(this.Game.DATA.chat_message_duration || 5);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.CashOut = function(socket, d) {
    var GO = socket.VIEW.GO;
    var walletAddress = d.wallet;
    var balance = parseInt(socket.VIEW.playerAccountData.bank.get());
    this.Log("CASH OUT for player " + GO.id.get() + ", balance: " + balance + ", wallet address: " + walletAddress);
    
    /**** WEB3 Ethereum Code ****/

    if (typeof web3 !== 'undefined') {
        this.Log('web3.js: web3 is defined!');
        var web3 = new Web3(web3.currentProvider);
    } else {
        var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
        if (web3.isConnected()) {
            this.Log('web3.js: Connected to web3!');
            web3.eth.defaultAccount = web3.eth.accounts[0];
            this.Log("default web3 account: " + web3.eth.defaultAccount);
        } else {
            this.Log('web3 is not connected');
            // notify client
            this.Send(socket, 0, 'cashout', 'Service is not running'); //result, cmd, data
            return;
        }
    }

    var coin_abi = ''
    this.Log("web3 finished");
    fs.readFile('./refereum.sol', 'utf8', (err,smartContract)=> {
        if (err) return this.Log("Error loading refereum.sol: " + err);   
        //this.Log("file is read");
        /* Get bytecode and abiInterface from our smartContract */
        var output = solc.compile(smartContract, 1);
        //coin_bc = output.contracts[":token"].bytecode;
        var abiLocal =  output.contracts[":token"].interface;
        coin_abi = JSON.parse(abiLocal);
        //this.Log("coin abi: " + abiLocal);


        //creates instance of the smart-contract usin the 
       var MyContract = web3.eth.contract(coin_abi);
       var contractInstance = MyContract.at("0x08FdD0214201AE01C4d3b2fC583609e23cFD16A1");  //the kovan refereum contract address
       //this.Log("contract: " + MyContract + ", contract instance: " + contractInstance);

       // test code for testRPC:
       //var transactionHash = web3.eth.sendTransaction({from: web3.eth.accounts[0], to: walletAddress, value: balance});

       // prod code for parity:
       try {
            web3.personal.unlockAccount(web3.eth.defaultAccount, "Molenko2");
            //remove credits from balance and if successful, transfer to wallet
            balance = parseInt(socket.VIEW.playerAccountData.bank.get());
            this.Game.PLAYFAB.removeCredits(socket.VIEW._pfid, this.Game.PLAYFAB.CURRENCY.CREDITS, balance, () => {
                this.Log("transfering balance (" + balance + ") to wallet " + walletAddress);

                var transactionHash = contractInstance.transfer.sendTransaction(walletAddress, balance, {from: web3.eth.defaultAccount});
                socket.VIEW.playerAccountData.bank.set(0);
            });
       } catch (error) {
           this.Log("An error happened during transaction: " + error.message);
           // notify client
           this.Send(socket, 0, 'cashout', error.message.split("\n")[0]); //result, cmd, data
           return;
       }
    });

    /**** WEB3 Ethereum Code ****/

};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.ResetOwnedShips = function(socket, d) {
    //this.Log("ResetOwnedShips");
    socket.VIEW.playerAccountData.ownedshipslist.set('[]');
    
    // TODO: revoke ships from inventory
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.ResetOwnedTech = function(socket, d) {
    //this.Log("ResetOwnedTech");
    var GO = socket.VIEW.GO;

    if (GO.buffManager){
        GO.buffManager.reset();
        GO.buffManager.save();
        
        // TODO: revoke tech items from inventory
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.AddMissingTech = function(socket, d) {
    this.Log("AddMissingTech");
    
    var GO = socket.VIEW.GO;
    var game = this.Game;

    var currentTech = GO.buffManager._buffs;
    var allTech = game.Stats.Tech;
    var self = this;
    
    var callback = function (itemGrantResults) {
        var grantedTechId = parseInt(itemGrantResults[0].ItemId.substring(4));
        GO.buffManager.addById(grantedTechId, itemGrantResults[0]);
        self.Log("added new tech: " + grantedTechId);
    };
    
    for (var i = 0; i<allTech.length; i++) {
        if (!currentTech.find((e) => (e.config.id === allTech[i].id))) {
            game.PLAYFAB.grantItem(socket.VIEW._pfid, "tech" +  allTech[i].id, callback);
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.CompleteMission = function(socket, d) {
    this.Log("CompleteMission");
    
    var data = socket.VIEW.playerAccountData;
    var GO = socket.VIEW.GO;
    var game = this.Game;
    
    var missionId = data.activeMission.get();
    if (missionId) {
        var mission = game.Stats.getMission(missionId);
        data.missionProgress.set(mission.targetAmount);
        // save to playfab
        GO.saveUserData(false);
    } else {
        this.Log("No active mission to complete!");
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.ClearWave = function(socket, d) {
    this.Log("ClearWave");
    
    this.Game.ClearWave();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.FreeTechPoints = function(socket, d) {
    this.Log("FreeTechPoints");
    var balance = parseInt(socket.VIEW.playerAccountData.techpoints.get());

    // add credits
    var amount = 5000;
    socket.VIEW.playerAccountData.techpoints.set(balance + amount);
    this.Game.PLAYFAB.addCredits(socket.VIEW._pfid, this.Game.PLAYFAB.CURRENCY.TECHPOINTS, amount);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.FreeMoney = function(socket, d) {
    this.Log("FreeMoney");
    var balance = parseInt(socket.VIEW.playerAccountData.bank.get());

    // add credits
    var amount = 100000;
    socket.VIEW.playerAccountData.bank.set(balance + amount);
    this.Game.PLAYFAB.addCredits(socket.VIEW._pfid, this.Game.PLAYFAB.CURRENCY.CREDITS, amount);

}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.RerollBuff = function(socket, d) {
    
    var GO = socket.VIEW.GO;
    var id = d.instanceId;
    //this.Log("RerollBuff:"+id);
    if (GO.buffManager){
        GO.buffManager.reroll(id);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.SellBuff = function(socket, d) {
    
    var GO = socket.VIEW.GO;
    var id = d.instanceId;
    //this.Log("SellBuff:"+id);
    if (GO.buffManager){
        GO.buffManager.sell(id);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.UpgradeBuff = function(socket, d) {
    
    var GO = socket.VIEW.GO;
    var id = d.instanceId;
    //this.Log("UpgradeBuff:"+id);
    if (GO.buffManager){
        GO.buffManager.upgrade(id);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.EquipBuff = function(socket, d) {

    var GO = socket.VIEW.GO;
    var id = d.instanceId;
    //this.Log("EquipBuff:"+id);
    if (GO.buffManager){
        GO.buffManager.equip(id);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.UnequipBuff = function(socket, d) {
    
    var GO = socket.VIEW.GO;
    var id = d.instanceId;
    //this.Log("UnequipBuff:"+id);
    if (GO.buffManager){
        GO.buffManager.unequip(id);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.ActivateSlot = function(socket, d) {
    //this.Log("ActivateSlot");
    var GO = socket.VIEW.GO;
    var slot = parseInt(d.slot);

    if (GO.buffManager){
        GO.buffManager.activate(slot);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//oLogic.prototype.ToggleSlot = function(socket, d) {
//    this.Log("ToggleSlot");
//    var GO = socket.VIEW.GO;
//    var slot = parseInt(d.slot);
//
//    if (GO.buffManager){
//        GO.buffManager.toggle(slot);
//    }
//}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.BuyStation = function(socket, d) {
    var GO = socket.VIEW.GO;
    var stationId = parseInt(d.stationId);
    //this.Log("BuyStation: " + stationId);
    if (GO.stationManager){
        GO.stationManager.buy(stationId);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.EquipStation = function(socket, d) {

    var GO = socket.VIEW.GO;
    var stationId = parseInt(d.stationId);
    //this.Log("EquipStation:"+stationId);
    if (GO.stationManager){
        GO.stationManager.equip(stationId);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.UnequipStation = function(socket, d) {
    
    var GO = socket.VIEW.GO;
    var stationId = parseInt(d.stationId);
    //this.Log("UnequipStation:"+stationId);
    if (GO.stationManager){
        GO.stationManager.unequip();
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.DeployStation = function(socket, d) {
    //this.Log("DeployStation");
    var GO = socket.VIEW.GO;

    if (GO.stationManager){
        // deploy returns 4 for failure by being blocked (1,2,3 are other reasons) (TODO: refactor)
        if (GO.stationManager.deploy() === 4) {
            // notify client so it can show an error message
            this.Send(socket, 0, 'deploystation', 'blocked'); //result, cmd, data
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.SpendCredits = function (socket, cmd, price, callback) {
    //this.Log(cmd + " price: " + price);
    // check user has enough credits
    var data = socket.VIEW.playerAccountData;
    var balance = data.bank.get();
    //this.Log("bank balance: " + balance);
    if (price > 0) {
        if (balance < price) {
            //this.Log("insufficient balance");
            this.Send(socket, 0, cmd, "insufficient");//result, cmd, data
            return;
        }
        // deduct credits
        if (socket.VIEW._pfid) {
            this.Game.PLAYFAB.removeCredits(socket.VIEW._pfid, this.Game.PLAYFAB.CURRENCY.CREDITS, price);
        } 
        // for now, trust the balance data on the server, do not wait for the result of the playfab call for better reaction time
        data.bank.set(data.bank.get() - price);
    }
    if (callback) {
        callback();
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.SpendTechPoints = function (socket, cmd, price, callback) {
    //this.Log(cmd + " TP price: " + price);
    // check user has enough techpoints
    var data = socket.VIEW.playerAccountData;
    var balance = data.techpoints.get();
    //this.Log("TP balance: " + balance);
    if (price > 0) {
        if (balance < price) {
            //this.Log("insufficient balance");
            this.Send(socket, 0, cmd, "insufficient");//result, cmd, data
            return;
        }
        // deduct techpoints
        if (socket.VIEW._pfid) {
            this.Game.PLAYFAB.removeCredits(socket.VIEW._pfid, this.Game.PLAYFAB.CURRENCY.TECHPOINTS, price);
        } 
        // for now, trust the balance data on the server, do not wait for the result of the playfab call for better reaction time
        data.techpoints.set(data.techpoints.get() - price);
    }
    if (callback) {
        callback();
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.GetOwnedShips = function (socket) {
    var ownedships = socket.VIEW.playerAccountData.ownedshipslist.get();
    //this.Log("ownedShips:"+ownedships);
    return JSON.parse(ownedships);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.GetOwnedShip = function (socket, shipName, ownedships) {
    ownedships = ownedships || this.GetOwnedShips(socket);
    var shipIndex = ownedships.findIndex((element)=>(element.ItemId === shipName));
    return shipIndex >= 0 ? ownedships[shipIndex] : null;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.UpdateShipState = function (socket, d) {
    d = d || {};    
    
    if (!socket.VIEW._pfid) {
        // cannot update ship state if not yet logged in to playfab (owned ship list not available, nowhere to save)
        return;
    }
    
    var self = this;
    var ship = d.ship || socket.VIEW.GO;
    var ownedships = d.ownedships || this.GetOwnedShips(socket);
    
    var shipName = ship.subtype.get();
    
    var ownedship = this.GetOwnedShip(socket, shipName, ownedships);
    
    if (!ownedship) {
        this.Log("WARNING: cannot save state: missing ship " + shipName + " from owned ships list! (ship id: " + ship.id.get() + ")");
        return;
    }
    
    // list of equipped tech instance IDs
    var loadoutStr = JSON.stringify(ship.getLoadout());
    // playfab has a limit of 100 characters for invenvtory property values, so we might need to break down the list into multiple parts (one playfab instance ID is 16 characters long)
    var loadout = [];
    do {
        loadout.push(loadoutStr.substring(0,100));
        loadoutStr = loadoutStr.substring(100);
    } while (loadoutStr.length > 0);
    
    ownedship.CustomData = {
        hp: ship.hp.get(),
        //fuel: ship.fuel.get(),
        ammo: ship.ammo.get()
    };
    for (var i = 0; i < loadout.length; i++) {
        ownedship.CustomData["loadout" + ((i > 0) ? (i+1) : "")] = loadout[i];
    }
    
    var ownedshipsstr = JSON.stringify(ownedships);

    socket.VIEW.playerAccountData.ownedshipslist.set(ownedshipsstr);
    
    if (d.save) {
        this.Game.PLAYFAB.updateItemData(socket.VIEW._pfid, ownedship.ItemInstanceId, ownedship.CustomData, function(){
            //self.Log("Saved owned ship state: " + JSON.stringify(ownedship));
        });
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.StartGame = function(socket, d) {
    this.Game.Core.ObjectAdd(socket.VIEW.GO);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.UnlockShip = function(socket, name, d, free, callback) {
    var self = this;
    //this.Log("UnlockShip: " + name);
    if (!socket.VIEW._pfid) {
        this.Log("Unlockship failed: not logged in to playfab");
        //this.Send(socket, 0, 'unlockship', "notloggedin");//result, cmd, data -> unused at client
        return;
    }

    // get price
    var game = this.Game;
    var price = 0;
    var stats = game.Stats.Ships.find((stats) => stats.data_name === name);
    if (stats) {
        if (!free) {
            price = stats.unlock_price_credits;
        }
    } else {
        this.Log("Unlockship failed: no data available for ship with name: '" + name + "'");
        //this.Send(socket, 0, 'unlockship', "notfound");//result, cmd, data -> unused at client
        return;
    }

    // check not already unlocked
    var ownedships = this.GetOwnedShips(socket);
    var ownedShip = this.GetOwnedShip(socket, name, ownedships);
    if (ownedShip && (!ownedShip.CustomData || (ownedShip.CustomData.hp > 0))) {
        //this.Log("already unlocked");
        //this.Send(socket, 0, 'unlockship', "alreadyunlocked");//result, cmd, data -> unused at client
        if (callback) { 
            callback(); 
        }
        return;
    }
    
    this.SpendCredits(socket, 'unlockship', price, function () {
        // if the ship was never unlocked before, grant the new ship item on playfab
        if (!ownedShip) {
            game.PLAYFAB.grantItem(socket.VIEW._pfid, name, function (itemGrantResults) {
                ownedships.push({
                    ItemId: name,
                    ItemInstanceId: itemGrantResults[0].ItemInstanceId
                });
                // sync new owned ship list back to client
                socket.VIEW.playerAccountData.ownedshipslist.set(JSON.stringify(ownedships));
                //self.Log("unlocked: " + name);
                //self.Send(socket, 1, 'unlockship', "OK");//result, cmd, data -> unused at client

                // grant default weapon tech item, if needed
                var defaultWeaponTechId = parseInt(stats.default_weapon.substring(4));
                var currentTechs = socket.VIEW.GO.buffManager._buffs;
                var weapon = currentTechs.find((e) => (e.config.id === defaultWeaponTechId));
                if (!weapon || (weapon._instanceId === undefined)) {
                    game.PLAYFAB.grantItem(socket.VIEW._pfid, stats.default_weapon, function (itemGrantResults) {
                        if (!weapon) {
                            var techId = parseInt(stats.default_weapon.substring(4)); // "tech4" -> 4
                            socket.VIEW.GO.buffManager.addById(techId, itemGrantResults[0], true);
                        } else {
                            weapon.initInstanceData(game, itemGrantResults[0]);
                        }
                        //self.Log("granted weapon tech (default weapon for unlocked ship): " + stats.default_weapon);
                        if (callback) { 
                            callback(); 
                        }
                    });
                } else {
                    //self.Log("player already owns the default weapon for unlocked ship (" + stats.default_weapon + ", instance ID: " + weapon._instanceId + ")");
                    if (callback) { 
                        callback(); 
                    }
                }
            });
        } else {
            // if the player already owned the ship before, reset its stats
            // resetting ship stats
            //self.Log("Resetting stats of already owned, but destroyed ship.");
            ownedShip.CustomData = ownedShip.CustomData || {};
            ownedShip.CustomData.hp = stats.health;
            ownedShip.CustomData.ammo = stats.total_shots;
            // loadout stays the same, so after repurchasing, the player doesn't have to equip everything again
            // save to playfab
            game.PLAYFAB.updateItemData(socket.VIEW._pfid, ownedShip.ItemInstanceId, ownedShip.CustomData, function(){
                //self.Log("Saved owned ship state: " + JSON.stringify(ownedShip));
                // sync new owned ship list back to client
                socket.VIEW.playerAccountData.ownedshipslist.set(JSON.stringify(ownedships));
                //self.Log("unlocked: " + name);
                //self.Send(socket, 1, 'unlockship', "OK");//result, cmd, data -> unused at client
                if (callback) { 
                    callback(); 
                }
            });
        }
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.PickShip = function(socket, name, d) {
    //this.Log("PickShip - " + JSON.stringify(d));
    
    var GO = socket.VIEW.GO;
    
    var ownedships = this.GetOwnedShips(socket);
    
    // if the current ship was destroyed, no state saving (ship state is saved when destroyed)
    if ((!d || !d.dontUpdateCurrent) && (GO.Obj._remove !== true)) {
        // update stored ship state for current ship
        this.UpdateShipState(socket, {ownedships: ownedships, save: true});
    }

    // block if ship not unlocked
    var ownedship = this.GetOwnedShip(socket, name, ownedships);
    if (!ownedship) {
        this.Log("Pickship failed: ship '" + name +"' not yet unlocked for player " + GO.id.get());
        //this.Send(socket, 0, 'pickship', "locked");//result, cmd, data
        return;
    }

    // load new ship
    if(this.UseShipTemplate(name, GO, (!ownedship.CustomData || !ownedship.CustomData.loadout), null) === true) // if there is no loadout saved for this ship yet, we equip the default weapon
    {
        socket.VIEW.playerAccountData.shipName.set(name);
        if (ownedship.CustomData && ownedship.CustomData.hp) {
            GO.hp.set(Math.min(parseFloat(ownedship.CustomData.hp), GO.max_hp.get()));
            GO.ammo.set(Math.min(parseFloat(ownedship.CustomData.ammo), GO.max_ammo.get()));
            if (ownedship.CustomData) {
                // if needed, assembling the loadout string from multiple parts (one property of CustomData is limited to 100 bytes in playfab)
                var loadoutStr = ownedship.CustomData.loadout || "{}";
                for (var i = 2; ownedship.CustomData["loadout" + i]; i++) {
                    loadoutStr += ownedship.CustomData["loadout" + i];
                }
                GO.setLoadout(JSON.parse(loadoutStr));
            }
        }
        // If the ship was destroyed, readd it and dock to main base
        if (GO.Obj._remove === true) {
            //this.Log("Last ship was destroyed -> resetting ship.");
            this.ResetShip(socket, { spawnAtBase: true, dontFlush: true });
        }
        // save new current ship id to playfab
        GO.saveUserData(false);
        //this.Send(socket, 1, 'pickship', "OK");//result, cmd, data
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.DockShip = function(socket, d) {
    var GO = socket.VIEW.GO;
    var base = this.Game.Core.GetObject("Base", d.baseID);
    if (base) {
    
        var data = socket.VIEW.playerAccountData;
    
        var credits = parseInt(data.credits.get());
        // move ship credits to bank
        socket.VIEW.playerAccountData.bank.set(data.bank.get() + credits);

        // only update playfab if already authenticated
        if (socket.VIEW._pfid) {
            this.Game.PLAYFAB.addCredits(socket.VIEW._pfid, this.Game.PLAYFAB.CURRENCY.CREDITS, credits);
        }

        data.credits.set(0);

        //this.Log("Docking at base " + d.baseID);

        data.baseID.set(d.baseID);
        GO.Obj.docked.set(true);
        GO.Obj.docked_at.set(d.baseID);
        
        GO.x.set(base.x.get());
        GO.y.set(base.y.get());
        GO.vx.set(0);
        GO.vy.set(0);
        GO.Obj.UpdateBounds(); // to correctly do the next ScopeCheck
        //this.Log("Docked at base " + d.baseID + " (" + base.x.get() + ", " + base.y.get() + ")");
        if (!d.dontSave) {
            // save to playfab
            GO.saveUserData(false);
        }
    } else {
        this.Log("Ship " + GO.id.get() + " cannot dock: base " + d.baseID + " not found");
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.UndockShip = function(socket, d) {

    var GO = socket.VIEW.GO;
    //this.Log("Undocked!");
    GO.Obj.docked.set(false);
    // setting docked_at to 0 will allow redocking right away (otherwise it is cleared when the docking area is left, see Ship.js)
    // it is fine, because now a keypress is required from the player for docking
    GO.Obj.docked_at.set(0);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.ResetShip = function(socket, d) {
    d = d || {};
    var L = this;
    var GO = socket.VIEW.GO;
    
    if (!d.dontFlush) {
        //flush the view
        socket.VIEW.FlushView();
    }
    
    // look up last base the player docked at
    var baseID = socket.VIEW.playerAccountData.baseID.get();
    var base = this.Game.Core.GetObject("Base", baseID);
    
    // if there is a last base where it was docked, spawn there
    if (base) {
        L.DockShip(socket, {baseID: baseID, dontSave: true});
    } else if (d.spawnAtBase) {
        // if no last base, dock at main base if docking was asked for in the passed data  (otherwise leave current position)
        if (this.Game.DATA.base2_obj) {
            L.DockShip(socket, {baseID: this.Game.DATA.base2_obj.id.get(), dontSave: true});
        }
    }
    //L.SpawnByBase(GO, base, true);
    
    // reset ship stats
    GO.hp.set(GO.max_hp.get());
    //GO.fuel.set(GO.max_fuel.get()); // fuel unused for now!
    GO.ammo.set(GO.max_ammo.get());
    
    //Reset and ReAdd if dead to Core Objects as this was removed
    if (GO.Obj._remove === true) {
        GO.Obj._remove = false;
        GO._dead = false;
        L.Game.Core.ObjectAdd(GO);
        socket.VIEW.Respawned = false;
        socket.VIEW.RespawnTime = 0;
        //L.Log("readded!")
    }
    
    //L.Log("RESET Player " + GO.id.get() + " hp: " + GO.hp.get());
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.RepairShip = function(socket, d) {

    var GO = socket.VIEW.GO;
    var shipName = GO.subtype.get();
    
    // get price
    var game = this.Game;

    //Find stats which match ship name
    var price = 0;
    var stats = game.Stats.Ships.find((stats) => stats.data_name === shipName);
    if (stats) {
        price = stats.repair_cost;
    } else {
        //this.Send(socket, 0, 'repair', "notfound");//result, cmd, data
        return;
    }
    
    price = Math.ceil(price * (1 - GO.hp.get() / GO.max_hp.get()));
    
    this.SpendCredits(socket, 'repair', price, ()=>{
        // update game object - change will be synced to the client(s), playfab inventory only updated when another ship is picked or client disconnects
        GO.hp.set(GO.max_hp.get());

        //this.Log("repaired to max hp");

        //this.Send(socket, 1, 'repair', "OK");//result, cmd, data
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.RestockShip = function(socket, d) {

    var GO = socket.VIEW.GO;
    var shipName = GO.subtype.get();
    
    // get price
    var game = this.Game;

    //Find stats which match ship name
    var price = 0;
    var stats = game.Stats.Ships.find((stats) => stats.data_name === shipName);
    if (stats) {
        price = stats.restock_cost;
    } else {
        //this.Send(socket, 0, 'restock', "notfound");//result, cmd, data
        return;
    }
    
    price = Math.ceil(price * (1 - GO.ammo.get() / GO.max_ammo.get()));
    
    this.SpendCredits(socket, 'restock', price, ()=>{
        // update game object - change will be synced to the client(s), playfab inventory only updated when another ship is picked or client disconnects
        GO.ammo.set(GO.max_ammo.get());

        //this.Log("restocked ship");

        //this.Send(socket, 1, 'restock', "OK");//result, cmd, data
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.StartMission = function(socket, missionId, d) {
    var GO = socket.VIEW.GO;
    
    //this.Log(JSON.stringify(d));
    
    var missions = JSON.parse(socket.VIEW.playerAccountData.missions.get());
    var missionData = missions.find((e)=>e.id===missionId); // user specific mission data
    var now = Date.now();
    var mission = this.Game.Stats.getMission(missionId); // generic mission data
    // checking if refresh time elapsed
    if (missionData && (now-missionData.start)<mission.refreshTime*1000*60) { // ms - minute conversion
        this.Log("cannot start mission "+ missionId + " refresh time hasn't elapsed yet!");
        //this.Send(socket, 0, 'startmission', "timer");//result, cmd, data
        return;
    }
    
    socket.VIEW.playerAccountData.activeMission.set(missionId);
    socket.VIEW.playerAccountData.missionProgress.set(0);

    // save to playfab
    GO.saveUserData(false);
    
    //this.Log("started mission "+ missionId);
    
    //this.Send(socket, 1, 'startmission', "OK");//result, cmd, data
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.ClaimMission = function(socket, d) {
    //this.Log("Claiming mission reward");
    
    var GO = socket.VIEW.GO;
    var data = socket.VIEW.playerAccountData;
    var game = this.Game;
    var self = this;

    // if mission is completed grant reward tech
    var missionId = data.activeMission.get();
    if (missionId) {
        var mission = game.Stats.getMission(missionId);
        // checking mission completion
        if (data.missionProgress.get() >= mission.targetAmount) {
            // granting reward
            game.PLAYFAB.grantItemFromDropTable(socket.VIEW._pfid, mission.dropTable, function (itemGrantResults) {
                var grantedTechId = parseInt(itemGrantResults[0].ItemId.substring(4));
                //var currentTechs = socket.VIEW.GO.buffManager._buffs;
                //var tech = currentTechs.find((e) => (e.config.id === grantedTechId));
                socket.VIEW.GO.buffManager.addById(grantedTechId, itemGrantResults[0]);
                //self.Log("awarded new tech for mission: " + grantedTechId);
            });
            
            data.activeMission.set(0);
            data.missionProgress.set(0);
            
            // sarting mission timer
            var missions = JSON.parse(data.missions.get());
            
            var missionData = missions.find((e)=>e.id===missionId); // user specific mission data
            var now = Date.now();

            // saving start timestamp
            if (!missionData) {
                missions.push({
                    id: missionId,
                    start: now
                });
            } else {
                missionData.start = now;
            }
            data.missions.set(JSON.stringify(missions));
        } else {
            this.Log("Cannot claim mission reward: mission is not complete!");
            //this.Send(socket, 0, 'claimmission', "incomplete");//result, cmd, data
            return;
        }
    } else {
        this.Log("Cannot claim mission reward: there is no active mission!");
        //this.Send(socket, 0, 'claimmission', "nomission");//result, cmd, data
        return;
    }
    
    // save to playfab
    GO.saveUserData(false);
    
    //this.Log("Successfully claimed reward for mission "+ missionId);
    
    //this.Send(socket, 1, 'claimmission', "OK");//result, cmd, data
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.Auth = function(socket, d) {
    var GO = socket.VIEW.GO;
    var sessionTicket = d.sessionTicket;
    var newlyCreated = d.newlyCreated;
    var self = this;
    this.Log("Got playfab session ticket from client: "+GO.id.get()+" (new: "+newlyCreated+")");
    
    this.Game.PLAYFAB.verifyTicket(sessionTicket,function(pfid){
        //self.Log('verified pfid: '+pfid);
        self.Game.PLAYFAB.getUser(pfid,function(user){
            //self.Log('USERACCOUNT:'+JSON.stringify(user));
            
            var displayName = (user.TitleInfo && user.TitleInfo.DisplayName) || user.Username;
            //var email = user.email;
            if (!displayName) {
                if (user.FacebookInfo){
                    if (user.FacebookInfo.FullName){
                        displayName = user.FacebookInfo.FullName;
                    }
                }
                //displayName = 'GUEST '+pfid.substring(0,5);
            }
            //self.Log('Identified user: '+displayName);
            socket.VIEW._pfid = pfid;

            GO.username.set(displayName);
            
            //self.Log('ownedTech:'+JSON.stringify(user.data.ownedTech));
            
            var i, item;
            
            if (user.data.ownedTech) {
                GO.buffManager.reset();
                for (i=0;i<user.data.ownedTech.length;i++){
                    item = user.data.ownedTech[i];
                    var techId = item.ItemId.substring(4); // "tech4"
                    GO.buffManager.addById(parseInt(techId), {
                        ItemInstanceId: item.ItemInstanceId,
                        CustomData: item.CustomData
                    });
                }
            }
            
            if (user.data.ownedStations) {
                GO.stationManager.reset();
                for (i=0;i<user.data.ownedStations.length;i++){
                    item = user.data.ownedStations[i];
                    var stationId = item.ItemId.substring(7); // "station4"
                    GO.stationManager.addById(parseInt(stationId), {
                        ItemInstanceId: item.ItemInstanceId,
                        CustomData: item.CustomData
                    });
                }
            }
              
            var D = self.Game.DATA;
            user.data.shipName = user.data.shipName || (newlyCreated ? D.starting_ship : "");
            // reset data if it is in the old format
            if (!user.data.ownedShips || (user.data.ownedShips.length <= 0) || (typeof user.data.ownedShips[0] !== "object")) {
                user.data.ownedShips = []; 
                user.data.shipName = (newlyCreated ? D.starting_ship : "");
            }
            socket.VIEW.playerAccountData.username.set(displayName);
            socket.VIEW.playerAccountData.kills.set(parseInt(user.data.kills));
            socket.VIEW.playerAccountData.deaths.set(parseInt(user.data.deaths));
            socket.VIEW.playerAccountData.credits.set(parseInt(user.data.credits));
            socket.VIEW.playerAccountData.techpoints.set(parseInt(user.data.techpoints));
            socket.VIEW.playerAccountData.bank.set(parseInt(user.data.bank));
            socket.VIEW.playerAccountData.shipName.set(user.data.shipName);
            socket.VIEW.playerAccountData.baseID.set(user.data.baseID);
            socket.VIEW.playerAccountData.activeMission.set(parseInt(user.data.activeMission) || 0);
            socket.VIEW.playerAccountData.missionProgress.set(parseInt(user.data.missionProgress) || 0);
            socket.VIEW.playerAccountData.missions.set(user.data.missions || "[]");
            socket.VIEW.playerAccountData.xp.set(parseInt(user.data.xp));
            socket.VIEW.playerAccountData.ownedshipslist.set(JSON.stringify(user.data.ownedShips));
            
            if (!newlyCreated) {
                if (user.data.shipName) {
                    self.ResetShip(socket);
                }
            } else {
                var startCredits = D.starting_credits;
                socket.VIEW.playerAccountData.bank.set(startCredits);
                self.Game.PLAYFAB.addCredits(socket.VIEW._pfid, self.Game.PLAYFAB.CURRENCY.CREDITS, startCredits);
            }
            if (user.data.shipName) {
                self.UnlockShip(socket, user.data.shipName, undefined, true, function () {
                    self.PickShip(socket, user.data.shipName, { dontUpdateCurrent: true });
                });
            } else {
                socket.VIEW.GO._dead = true;
                socket.VIEW.GO.Obj._remove = true;
                self.Game.Respawn(socket.VIEW, 0);
            }
            
            self.Send(socket, 1, 'login', 'OK'); // result, cmd, data
        });
    });

}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.NameChange = function(socket, d) {
    var GO = socket.VIEW.GO;
    GO.username.set(d.displayName);
    socket.VIEW.playerAccountData.username.set(d.displayName);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//Object API - list, createupdate, read, delete
oLogic.prototype.get_db_model = function(obj) {
    if (obj ==="global") { return this.Core.DBModelGlobalConfig; }
    if (obj ==="account") { return this.Core.DBModelAccount; }
    if (obj ==="upgradepath") { return this.Core.DBModelUpgradePath; }
    if (obj ==="upgrade") { return this.Core.DBModelUpgrade; }
    if (obj ==="ships") { return this.Core.DBModelShipTemplate; }
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.api_list = function(socket, obj, d) {
    
    //this.Log(JSON.stringify(d));
    
    if (d.obj === "ships") {
        //this.Log(this.Game.Core.JSON(this.Game.DATA.ShipList))
        this.Send(socket, 1, 'api_list', this.Game.DATA.ShipList);//result, cmd, data
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.api_read = function(socket, obj, name, d) {
    //this.Log("READ SHIP: " + JSON.stringify(d));
    
    var s = this.Game.Stats;
    //this.Log(JSON.stringify(s.Ships))
    for (var sn in s.Ships) {
        if (s.Ships.hasOwnProperty(sn)) {
            var Ship = s.Ships[sn];
            if (name === Ship.data_name) {
                var outd =
                {
                    speed: Ship.max_speed,
                    accelerate: Ship.accel_sec,
                    rot_accelerate: Ship.rotation_speed,
                    fdrop: Ship.fire_drop,
                    fdropr: Ship.fire_drop_rot,
                    health: Ship.health,
                    marea: Ship.move_radius,
                    marea_max: Ship.move_radius_max,
                    hitbox: Ship.hitbox
                    
                }
                this.Send(socket, 1, 'api_read', outd);//result, cmd, data
                //this.Log(JSON.stringify(Ship));
                return;
            }
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.api_cu = function(socket, obj, name, d) {
    //this.Log(JSON.stringify(d));
    if (d.obj === "ships") {
        //this.Log(this.Game.Core.JSON(this.Game.Stats.Ships))
        
        var ships = this.Game.Stats.Ships;
        //this.Log(JSON.stringify(s.Ships))
        for (var s in ships) {
            if (ships.hasOwnProperty(s)) {
                var Ship = ships[s];
                if (name === Ship.data_name) {
                    Ship.max_speed = d.speed;
                    Ship.accel_sec = d.accelerate;
                    Ship.rotation_speed = d.rot_accelerate;
                    Ship.fire_drop = d.fdrop;
                    Ship.fire_drop_rot = d.fdropr;
                    Ship.health = d.health;
                    Ship.move_radius = d.marea;
                    Ship.move_radius_max = d.marea_max;
                    Ship.hitbox = d.hitbox;
                    
                    this.Send(socket, 1, 'api_cu', "UPDATED SHIP! " + name);//result, cmd, data
                    //this.Log(JSON.stringify(Ship));
                    return;
                }
            }
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.api_delete = function(socket, obj, name, d) {
    //this.Log(JSON.stringify(d));
    var model = this.get_db_model(obj);
    if (model == false) { C.Log("NoModel"); return; }

    this.Core.DBDelete(model, name, function(status, d) {
        socket.ClientObject.Core.Send(socket, status, 'api_delete', d);//result, cmd, data
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.Input = function(d) {
    if(C.ValidateFields(d, ['up', 'down', 'left', 'right', 'f1', 'vx', 'vy', 'x', 'y','r'])){
        if (d.up == 0 || d.up == 1) { this.GO.up = d.up; }
        if (d.down == 0 || d.down == 1) { this.GO.down = d.down; }
        if (d.left == 0 || d.left == 1) { this.GO.left = d.left; }
        if (d.right == 0 || d.right == 1) { this.GO.right = d.right; }
        if (d.f1 == 0 || d.f1 == 1) { this.GO.f1 = d.f1; }
        if (typeof d.vx == 'number') { this.GO.mx = parseInt(d.vx); }//mouse vector
        if (typeof d.vy == 'number') { this.GO.my = parseInt(d.vy); }
        if (typeof d.x == 'number') { this.GO.clx = parseInt(d.x); }//clients local coords (client predicted)
        if (typeof d.y == 'number') { this.GO.cly = parseInt(d.y); }//clients local coords (client predicted)
        if (typeof d.r == 'number') { this.GO.clr = parseInt(d.r); }//clients local coords (client predicted)
        
        //C.Log(C.JSON(d));
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.set_global = function(socket, k, v, d) {
    this.Log("Global " + JSON.stringify(d));
    if (this.Game.DATA.hasOwnProperty(k)) {
        this.Game.DATA[k] = v;
        this.Log("Global SET! " + k + "=" + v + " " + (typeof v));
        if (k == 'asteroidCountPerAreaMax'){
            this.Game.clearAsteroids();
            this.Game.spawnAsteroids();
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.get_global = function(socket, k, d) {
    //C.Log("GETGlobal " + JSON.stringify(d));
    if (this.Game.DATA.hasOwnProperty(k)) {
        var data = {key: k, value: this.Game.DATA[k]};
        this.Send(socket, 1, 'getglobal', data);//result, cmd, data
        //this.Log(data.value)
        //C.Log("Global GET! " + k + "=" + this.Core.GD[k]);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLogic.prototype.Tweak = function(d) {
    C.Log(JSON.stringify(d));
    //if(C.ValidateFields(d, ['speed', 'sspeed', 'accelerate', 'rot_accelerate', 'marea', 'fdrop', 'fdropr'])){
    if(C.ValidateFields(d, ['eparea', 'erespawn'])){
        d.eparea = parseFloat(d.eparea);
        d.erespawn = parseFloat(d.erespawn);
        if (
            typeof d.eparea == 'number' &&
            typeof d.erespawn == 'number'
            )
        {
            C.Log("TWEAKING! " + JSON.stringify(d));
            this.Core.GD.MapEnemyTarget = d.eparea;
            this.Core.GD.MapEnemyTime = d.erespawn;
            
//            this.GO.maxspeed = d.speed;
//            this.GO.maxspeedSQ = this.GO.maxspeed * this.GO.maxspeed;
//            this.GO.rot_speed = d.rot_accelerate;
//            this.GO.acceleration_rate = d.accelerate;
//            this.GO.f1s = d.sspeed;//shoot laser speed
//            this.GO.fdrop = d.fdrop / 100;//drop speed percentage on fire
//            this.GO.fdropr = d.fdropr / 100;//drop speed percentage on fire
            
            //if (d.marea > 1) {
              //  this.GO.radius = d.marea;
                //this.GO.movebox = d.box;//accelerate outside this box
                //this.GO.moveboxh = d.box/2;
            //}
            
            //asteroids style
            //this.GO.turnrate = d.turnrate;
            //if(d.mstyle == 0){ this.GO.MoveType = "Controlled";}
            //else{
              //  this.GO.MoveType = "Controlled2";
            //}
            
        }
        else {C.Log("not Number!" + JSON.stringify(d))}
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oLogic;
