//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Bonus
    A passive bonus provided by a tech (instance)
    SERVER SIDE
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function Bonus(config, roll)
{
    //debugging
    this.DEBUG = true;
    
    // instance data
    this.roll = roll; // (float, 0: min - 1: max)
    
    // general data (same for the same type of bonus)
    this.config = config;

}

// actual stat value based on the roll, min, max
Bonus.prototype.getValue = function () {
    return this.config.min + Math.floor((this.config.max - this.config.min) * this.roll); 
}

// for saving instance data on playfab
Bonus.prototype.toInstanceData = function () {
    return this.config.id + "," + this.roll;
}

Bonus.prototype.fromBlob = function(blob){

    this.config = blob;

}

Bonus.prototype.toBlob = function(){

    return this.config;

}

// for sending data to client
Bonus.prototype.toJSON = function(){

    return JSON.stringify({
        roll: this.roll,
        config: this.config // TODO: it would be enough to send the ID, the client can fetch metadata from Stats
    });

}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Bonus.prototype.Log = function(m) { if(this.DEBUG){ console.log("[Bonus] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = Bonus;
