//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Turret on top of stations - SERVER SIDE
    
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var oObject = require('../../Engine/Core/GO');
var Buff = require('../../Custom/Objects/Buff');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oTurret(Game, d, d2)
{
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;

    this._view = null;
    
    //this.Log("Creating turret with subtype " + d.subtype + " (standalone: " + d2.standalone + ")");
    
    var tech = Game.Stats.Tech.find((e)=>(e.id===d.subtype));
    
    //this.Log("Turret tech config: " + JSON.stringify(tech));
    
    this._buff = new Buff(tech);
    
    this._ship = d2.ship; // the ship that deployed this turret
    
    this._duration = d2.duration; // duration until expiry, determining how fast the turret loses health, in seconds
    
    d.damage_range = this._buff.getRange();

    //------------------------------------------------------------------------------
    //Object State (Networked - Changes only)
    //------------------------------------------------------------------------------
    //Custom data needed (for datalist)
    var nd = [
        "buff", this._buff.toJSON(), // buff,
        "standalone", d2.standalone,
        "f1s", this._buff.getCooldown(),//action speeds (seconds) / tweakable
        "f1on", 0//is action on
    ];
    
    //this.Log(JSON.stringify(d))
    
    //Create Object Properly
    this.Obj = new oObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }
    
    //this.Log(JSON.stringify(this.Game.Core.DictKeys(this)))
    //this.Log(this.Obj._dl.getkeys())
    
    //------------------------------------------------------------------------------
    //INTERNAL
    //------------------------------------------------------------------------------
    this._f1=0; this._f2=0; this._f3=0; this._f4=0; this._f5=0; this._f6=0;//Try Fire
    this._f1t=0; this._f2t=0; this._f3t=0; this._f4t=0; this._f5t=0; this._f6t=0;//cooldowns
    this._f1net=0; this._f2net=0; this._f3net=0; this._f4net=0; this._f5net=0; this._f6net=0;//network event
    
    this._AI = {
        active: true,
        target: null
    };
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.Log = function(m) { if(this.DEBUG){ console.log("[TURRET] " + m); }};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// calculate damage output for ship
oTurret.prototype.CalcDamage = function() {
    return this._buff.getValue() * this.f1s.get();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// player team 10+
// TODO: propery test if it is a terran turret
oTurret.prototype.isTerran = function () { return (this.team.get() === this.Game.TERRAN_AI_TEAM) || (this.team.get() > 10); };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.canDamage = function (other) {
    var C = this.Game.Core;
    var pvpRadius = this.Game.DATA.fleet_pvp_radius;
    return (this.id.get() !== other.id.get()) &&
           (this.team.get() !== other.team.get()) &&
           (!other.Obj.docked || !other.Obj.docked.get()) &&
           //fleet logic - no pvp damage in safe area
           // can only damage the other ship if...
           // ...any of the two ships is not terran (alien)
           (!this.isTerran() || !other.isTerran() ||
           //// ...or if both are within the PvP area (outside PvP radius) - only standalone turrets (turret on the base station does not attack)
           (this.standalone.get() &&
            !C.CD.inCircle(this.x.get(),  this.y.get(),  0, 0, this.radius.get(),  pvpRadius) &&
            !C.CD.inCircle(other.x.get(), other.y.get(), 0, 0, other.radius.get(), pvpRadius)));
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.Shoot = function() {
    var C = this.Game.Core;
    var o = this;
    var objs, i, key, O;
    
    // zero fire speed means no weapon equipped
    if (this.f1s.get() <= 0) {
        return;
    }
    
//    if (this.max_ammo.get() > 0) { 
//        if (this.ammo.get() <= 0) {
//            return;
//        }
//        this.ammo.set(this.ammo.get() - 1);
//    }
    
    var radius = o.w.get() / 2;
    var angle = C.MATH.degToRad(o.r.get());
    var projectileWidth = this.Game.DATA.projectile_width;
    var x = o.x.get() + Math.cos(angle) * (radius + projectileWidth);
    var y = o.y.get() + Math.sin(angle) * (radius + projectileWidth);
    
    var rxy = C.MATH.rotate_by_pivot(x, y, this.r.get(), this.damage_range.get() - (radius + projectileWidth), 0);

    var hits = [];
    var nearest = null;
    var nearestlength = 0;
    var length = 0;
    
    //Against Ships (Player or AI)
    objs = C.GetGroup(C.goTypes.indexOf("Ship"));
    for (key in objs) {
        if (objs.hasOwnProperty(key)) {
            O = objs[key];
            if (this.canDamage(O)) {
                if (this.Obj.HitCheck(x, y, rxy, O) === true) {
                    hits.push(O);
                }
            }
        }
    }
    
    if (hits.length > 0) {
        for(i=0; i < hits.length; i++)
        {
            if (i===0) {
                O = hits[i];
                //C.Log("Nearest: " + O.id + " " + O.x + " " + O.y);
                nearestlength = C.MATH.VecLengthSquared(O.x.get() - x, O.y.get() - y);
                nearest = O;
            }
            else{
                O = hits[i];
                length = C.MATH.VecLengthSquared(O.x.get() - x, O.y.get() - y);
                if (length < nearestlength) {
                    //C.Log("NEW Nearest: " + O.id + " " + O.x + " " + O.y);
                    nearest = O;
                    nearestlength = length;
                }
            }
        }
        //Apply Damage to nearest ship
        nearest.Obj.Damage(this.CalcDamage());
                
        if(nearest.Obj._remove === true){
            if (this._ship) {
                this._ship.AwardKill(nearest);
            }
            nearest.Die();
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.Process = function(dt) {
    
    var o = this;//deref for clarity
    var x = o.x.get();
    var y = o.y.get();
    var r = o.r.get();
    
    if (this.standalone.get()) {
        // constant health decrease
        this.Obj.Damage(this.max_hp.get() * dt / this._duration);
    }
    
    if (this.hp.get() <= 0) {
        return;
    }
    
    //---------------------------------------------------------------------------------------------------------
    //AI 
    //---------------------------------------------------------------------------------------------------------
    var range = this.damage_range.get();
    var rangeSQ = range * range;
    if (!this._AI.target) {
        this._AI.target = this.AI_Nearest_Target("Ship", rangeSQ);
    }
    if (this.AI_hasValidTarget()) {
        // turn towards target and fire if facing it (and in range)
        var tx = this._AI.target.x.get();
        var ty = this._AI.target.y.get();
        var tr = this.Game.Core.MATH.XYToDegree(tx - x, ty - y);
        
        this.Obj.Turn(tr, o.rot_max_speed.get(), dt);
  
        var diffa = Math.abs(((this.Game.Core.MATH.NormalizeDegrees(r)) + 180 - tr) % 360 - 180);
        var distSQ = this.Game.Core.MATH.VecLengthSquared(tx - x, ty - y);

        if (diffa < 5) {
            this._f1 = 1;    
        } else {
            this._f1 = 0;
        }

        //out of range
        if(distSQ > rangeSQ) { 
            this._AI.target = null;
            this._f1 = 0; 
        }
    // when there is no valid target
    } else { 
        this._AI.target = null; 
        this._f1 = 0; 
        // rotate around slowly
        o.r.set(r + o.rot_max_speed.get() * 0.5 * dt);
    }
    
    // process firing action
    if (this._f1) {
        if (this._f1t === 0) { 
            this.Shoot(dt); 
            this._f1t += this.f1s.get(); 
        }
    }
    if (this._f1t > 0) { 
        this._f1t -= dt; 
        if (this._f1t < 0) { 
            this._f1t = 0; 
        } 
    }
    
    //tracking firing
    if(o.f1on.get() !== o._f1) { o.f1on.set(o._f1); }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.AI_hasValidTarget = function () {
    var target = this._AI.target;
    return target && ((target.Obj._remove === false) && (target.hp.get() > 0) && (target.Obj.docked && !target.Obj.docked.get()));
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.AI_Nearest_Target = function(groupname, rangeSQ) {
    var objs, nearest, O, distSQ, cdistSQ;
    objs = this.Game.Core.GetGroup(this.Game.Core.goTypes.indexOf(groupname));
    nearest = null;
    for (var key in objs) {
        if (objs.hasOwnProperty(key)) {
            O = objs[key];
            if((O.Obj._remove === false) && this.canDamage(O)){
                distSQ = this.Game.Core.MATH.VecLengthSquared(O.x.get() - this.x.get(), O.y.get() - this.y.get());
                if( distSQ < rangeSQ) {
                    //C.Log("Enemy in Range!");
                    if (nearest === null) { nearest = O; cdistSQ = distSQ; }//first one in range
                    else{
                        if(distSQ < cdistSQ){ nearest = O; cdistSQ = distSQ; }//closer one
                    }
                    
                }
            }
        }
    }
    return nearest;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oTurret;

