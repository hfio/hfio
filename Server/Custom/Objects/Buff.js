//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Buff (Tech instance) - SERVER SIDE
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var Bonus = require('../../Custom/Objects/Bonus');
function Buff(config)
{
    //debugging
    this.DEBUG = true;
    
    // instance data
    this._instanceId = undefined; // Playfab item instance ID
    this._bonuslist = [];
    this.equipped = false;
    this.level = 0;
    this.active = false;
    this.timeLeft = 0;

    // general data (same for the same type of tech)
    this.config = config;
}

// instance data from Playfab, with id and custom data object
Buff.prototype.initInstanceData = function(game, instanceData) {
    if (instanceData.ItemInstanceId !== undefined) {
        this._instanceId = instanceData.ItemInstanceId;
    }
    if (instanceData.CustomData !== undefined) {
        var data = instanceData.CustomData;
        // in playfab CustomData all properties are strings
        this.level = parseInt(data.level);
        this.equipped = (data.equipped === "true");
        var bl = data.bonuslist && data.bonuslist.split(";"); // data for bonuses separated by ;
        if (bl) {
            for (var i = 0; i < bl.length; i++) {
                var bonusData = bl[i].split(","); // bonus type id and roll
                var newBonus = new Bonus(game.Stats.getBonus(parseInt(bonusData[0])), parseFloat(bonusData[1]));
                this._bonuslist.push(newBonus);
            }
        }
    }
}

Buff.prototype.fromBlob = function(blob){

    this.config = blob.config;
    this._bonuslist = [];
    var bl = blob.bonuslist;
    if (bl) {
        for (var i=0;i<bl.length;i++){
            var newBonus = new Bonus();
            newBonus.fromBlob(bl[i]);
            this._bonuslist.push(newBonus);
        }
    }

}

Buff.prototype.toBlob = function(){

    var a = [];
    for (var i=0;i<this._bonuslist.length;i++){
        a.push(this._bonuslist[i].toBlob());
    }
    return {
        'bonuslist': a,
        'config': this.config
    }

}

// for sending data to client
Buff.prototype.toJSON = function(){

    var a = [];
    for (var i=0;i<this._bonuslist.length;i++){
        a.push(this._bonuslist[i].toJSON());
    }

    return JSON.stringify( {
        instanceId: this._instanceId,
        equipped: this.equipped,
        level: this.level,
        active: this.active,
        timeLeft: this.timeLeft,
        config: this.config, // TODO: it would be enough to send the ID, the client can fetch metadata from Stats
        bonuslist: a
    });

}

// for saving instance data on playfab
Buff.prototype.toPlayfabData = function(){

    var a = [];
    for (var i=0;i<this._bonuslist.length;i++){
        a.push(this._bonuslist[i].toInstanceData());
    }

    // format for the the Tech inventory item on Playfab
    return {
        ItemInstanceId: this._instanceId,
        CustomData: { // properties cannot be more than 100 bytes (as strings) - Playfab limit
            level: this.level,
            equipped: this.equipped,
            bonuslist: a.join(";") // data for bonuses separated by ;
        }
    };

}

Buff.prototype.toString = function() {
    return (this.config && this.config.name) + " (" + this._instanceId + ")";
}

Buff.prototype.getInstanceId = function() {
    return this._instanceId;
}

Buff.prototype.isWeapon = function() {
    return (this.config.slot === 0);
}

Buff.prototype.isFullyUpgraded = function () {
    return this.level >= this.config.levels.length - 1;
}

Buff.prototype.getValue = function() {
    return this.config.levels[this.level];
}

Buff.prototype.getNextLevelValue = function() {
    return this.config.levels[this.level + 1];
}

Buff.prototype.getSellTechPoints = function() {
    return this.config.sellTechPoints[this.level];
}

Buff.prototype.getCooldown = function() {
    return this.config.cooldown[this.level];
}

Buff.prototype.getRange = function() {
    return this.config.range[this.level];
}

Buff.prototype.getDuration = function() {
    return this.config.duration[this.level];
}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Buff.prototype.Log = function(m) { if(this.DEBUG){ console.log("[Buff] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = Buff;
