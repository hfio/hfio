//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Base
    - Movement
    - Collision Detection
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var oGameObject = require('../../Engine/Core/GO');
var oTurret = require('../../Custom/Objects/Turret');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oBase(Game, d, d2, ship)
{
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;

    //------------------------------------------------------------------------------
    //Object State (Networked - Changes only)
    //------------------------------------------------------------------------------
    //Custom data needed (for datalist)
    var nd = [
        "dock_radius", d2.dockRadius,
        "side", 1
    ];
    
    //Create Object Properly
    this.Obj = new oGameObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }
    ///this.Log(JSON.stringify(this.Game.Core.DictKeys(this)))
    
    //------------------------------------------------------------------------------
    //INTERNAL
    //------------------------------------------------------------------------------
    //Inputs (WASD/UP/DOWN/LEFT/RIGHT)
    //this.up=0; this.down=0; this.left=0; this.right=0;
    //this.upv=0; this.downv=0; this.leftv=0; this.rightv=0;
    //this._mx=0; this._my=0;//mouse vector x/y
    //this._lx=0; this._ly=0; this._lz=0; this._lr=0;//steering to location/rotation
    //this._tx=0; this._ty=0; this._tr=0;//target or target direction(diff)
    //this._tvx=0; this._tvy=0; this._tr=0;//target or target direction(diff)
    
    this._duration = d2.duration; // duration until expiry, determining how fast the station loses health, in seconds (0 = infinite, not losing health)
    
    this._rotationRate = d2.rotationRate;
    
    this._ship = ship; // the ship that deployed this station
    
    //AI
    this._AI = { active: false };
    
    if (d2.turret) {
        var data = this.Game.Core.Copy(this.Game.DATA.Object_Defaults);
        data.id = this.Game.Core.GetNextID();
        data.type = this.Game.DATA.CoreData.goTypes.indexOf("Turret");
        data.subtype = d2.turret;
        data.team = d.team;
        data.x = d.x; 
        data.y = d.y;
        data.radius = 20; // projectiles will spawn this much away from center
        data.rot_speed = this.Game.DATA.turret_rotation_rate;
        // same size as station for scoping
        data.w = d.w;
        data.h = d.h;

        this._turret = new oTurret(Game, data, {standalone: false, ship: this._ship});

        this.Game.Core.ObjectAdd(this._turret);
    }

}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBase.prototype.Log = function(m) { if(this.DEBUG){ console.log("[BASE] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// player ships: team 10+
// TODO: propery test if it is a terran ship
oBase.prototype.isTerran = function () { return (this.team.get() === this.Game.TERRAN_AI_TEAM) || (this.team.get() > 10); };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBase.prototype.Process = function(dt) {
    
    //C.Log("P")
    var o = this;//deref for clarity
    var objs, key;
    var type = o.type.get();
    var subtype = o.subtype.get();
    var x = o.x.get();
    var y = o.y.get();
    var vx = o.vx.get();
    var vy = o.vy.get();
    var r = o.r.get();
    var C = this.Game.Core;
    var D = this.Game.DATA;
    
    if (this._duration) {
        // constant health decrease
        this.Obj.Damage(this.max_hp.get() * dt / this._duration);
    }
    
    if (this.hp.get() <= 0) {
        if (this._turret) {
            this._turret.Obj.KILL();
        }
        return;
    }
    
//    var rxy;
//    if (o.team.get() == 1) {
//        rxy = C.MATH.rotate_by_pivot(0,0, D.base_rstep, D.base_radius, 0);
//    }
//    else
//    {
//        rxy = C.MATH.rotate_by_pivot(0,0, D.base_rstep, -D.base_radius, 0);
//    }
//    o.x.set(Math.floor(rxy[0]));
//    o.y.set(Math.floor(rxy[1]));
    
    //o.r.set(o.r.get() +  (5 * dt));
    
    //trim to .00
    o.r.set(Math.round((o.r.get() +  (this._rotationRate * dt)) * 100) / 100);
    
    //o.r += 5 * dt;
    o.Obj.UpdateBounds();
    //o.Core.GD.base_1_loc
    //:0,//degree location on circle
    //base_2_loc:180,
    //base_rstep: 0, //0-359 for current base positioning

    //var rxy = C.Math2D_rotate_by_pivot(this.x, this.y, this.r, this.damagelength, 0)
    //C.Log("oribt");
    
    

    return;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oBase;
