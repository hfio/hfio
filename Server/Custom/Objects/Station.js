//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Station (droppable station instance) - SERVER SIDE
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var oTurret = require('../../Custom/Objects/Turret');
var oBase = require('../../Custom/Objects/Base');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function Station(Game, config)
{  
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;
    
    // instance data
    this._instanceId = undefined; // Playfab item instance ID
    
    this.equipped = false;
    this.timeLeft = 0; // from cooldown
    this.amount = 0; // owned by the player

    // general data (same for the same type of station)
    this.config = config;
}

// instance data from Playfab, with id and custom data object
Station.prototype.initInstanceData = function(game, instanceData) {
    if (instanceData.ItemInstanceId !== undefined) {
        this._instanceId = instanceData.ItemInstanceId;
    }
    if (instanceData.CustomData !== undefined) {
        var data = instanceData.CustomData;
        // in playfab CustomData all properties are strings
        this.equipped = (data.equipped === "true");
        this.amount = parseInt(data.amount);
    }
}

// for sending data to client
Station.prototype.toJSON = function(){

    return JSON.stringify( {
        equipped: this.equipped,
        timeLeft: this.timeLeft,
        amount: this.amount,
        config: this.config // TODO: it would be enough to send the ID, the client can fetch metadata from Stats
    });

}

Station.prototype.buy = function (amount) {
    this.amount += amount;
}

Station.prototype.equip = function() {
    this.equipped = true;
}

Station.prototype.unequip = function() {
    this.equipped = false;
}

// returns 0 for success and 2,3... for various reasons of failure (TODO: refactor)
Station.prototype.deploy = function (go) {
    if (this.timeLeft > 0) {
        this.Log('station cannot be deployed - cooldown in progress '+this.config.name);
        return 2;
    }
    if (this.amount <= 0) {
        this.Log('station cannot be deployed - amount is zero '+this.config.name);
        return 3;
    }
    
    var x = go.x.get();
    var y = go.y.get();
    var baseTypeIndex = this.Game.DATA.CoreData.goTypes.indexOf("Base");
    
    // cannot deploy too close to other stations
    if (this.Game.Core.CD.CheckCollisionByRadius(x, y, this.config.hitbox / 2 + this.Game.DATA.station_deploy_min_distance, this.Game.Core.GetGroup(baseTypeIndex))) {
        return 4;
    }
    
    this.timeLeft = this.config.cooldown;
    this.amount--;
    if (this.amount === 0) {
        this.equipped = false;
    }
    
    var data = this.Game.Core.Copy(this.Game.DATA.Object_Defaults);
    data.id = this.Game.Core.GetNextID();
    data.type = baseTypeIndex;
    data.subtype = this.config.id;
    data.team = go.team.get();
    data.hp = this.config.health;
    data.x = x;
    data.y = y;
    data.w = this.config.hitbox;
    data.h = this.config.hitbox;
    data.radius = this.config.hitbox / 2;
    
    var base = new oBase(this.Game, data, this.config, go);
    base.side.set(go.side.get());
    
    this.Game.Core.ObjectAdd(base);
    
    //this.Log('station deployed: ' + this.config.name);
    return 0;
}

Station.prototype.Process = function (dt) {
    if (this.timeLeft > 0) {
        this.timeLeft = Math.max(0, this.timeLeft - dt);
        return true;
    }
    return false;
}

// for saving instance data on playfab
Station.prototype.toPlayfabData = function(){
    // format for the the Station inventory item on Playfab
    return {
        ItemInstanceId: this._instanceId,
        CustomData: { // properties cannot be more than 100 bytes (as strings) - Playfab limit
            equipped: this.equipped,
            amount: this.amount
        }
    };

}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Station.prototype.Log = function(m) { if(this.DEBUG){ console.log("[Station] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = Station;
