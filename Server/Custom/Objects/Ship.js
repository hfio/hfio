//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Ship - SERVER SIDE

    - Inputs (Mouse, Keyboard)
    - Movement
    - Collision Detection
    - Collecting resources
    - Firing (laser, projectiles)
    - AI
    
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var oObject = require('../../Engine/Core/GO');
var BuffManager = require('../../Custom/Objects/BuffManager');
var StationManager = require('../../Custom/Objects/StationManager');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oShip(Game, d)
{
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;

    this._view = null;
    
    this.buffManager = new BuffManager(this);
    
    this.stationManager = new StationManager(this);

    //------------------------------------------------------------------------------
    //Object State (Networked - Changes only)
    //------------------------------------------------------------------------------
    //Custom data needed (for datalist)
    var nd = [
        "username", "", // player name
        "bounty", 0, // credits_for_kill
        "energy", 0, // ship energy
        "buffs", this.buffManager.toJSON(), // buff manager
        "docked", false, // currently docked
        "docked_at", 0, // the id of the base of which the ship is within docking range (stays after undock to prevent redocking after docking range is left)
        "default_max_hp", 0, // for health bonus calc - this one doesn't change
        "shoot_drop", 0,//acceleration drop factor
        "shoot_drop_r", 0,//rotation drop factor
        "move_radius", 0,//move when mouse vector outside of radius
        "move_radius_max", 0,//move when mouse vector outside of radius
        "level", 0, "xp", 0,//leveling
        "damageboost", 0, // damage boost
        "speedboost", 0, // speed boost
        "turnboost", 0, // rotation boost
        "fuel", 0, // fuel unused for now! (do not delete, or update hardcoded indexes in network communication)
        "max_fuel", 0,
        "default_max_fuel", 0,
        "ammo", 0,
        "max_ammo", 0,
        "default_max_ammo", 0,
        "stations", this.stationManager.toJSON(),
        "message", "", // chat message to display above ship
        "message_scope", 0, // see OBJ_Pixi.CHAT_MODE
        "message_timeleft", 0,
        "f1s", 0, "f2s", 20, "f3s", 0, "f4s", 0, "f5s", 0, "f6s", 0, //action speeds (seconds) / tweakable
        "f1on", 0, "f2on", 0, "f3on", 0, "f4on", 0, "f5on", 0, "f6on", 0,//is action on
        "side", 1
        //"a1", 0, "a2", 0, "a3", 0, "a4", 0, "a5", 0, "a6", 0, "a7", 0, "a8", 0, "a9", 0, "a0", 0,  //actions
    ];
    
    //this.Log(JSON.stringify(d))
    
    //Create Object Properly
    this.Obj = new oObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }
    
    //this.Log(JSON.stringify(this.Game.Core.DictKeys(this)))
    //this.Log(this.Obj._dl.getkeys())
    
    //------------------------------------------------------------------------------
    //INTERNAL
    //------------------------------------------------------------------------------
    //Inputs (WASD/UP/DOWN/LEFT/RIGHT)
    //this.up=0; this.down=0; this.left=0; this.right=0;
    //this.upv=0; this.downv=0; this.leftv=0; this.rightv=0;
    this._speed=0;
    this._mx=0; this._my=0;//mouse vector x/y
    this._f1=0; this._f2=0; this._f3=0; this._f4=0; this._f5=0; this._f6=0;//Try Fire
    this._f1t=0; this._f2t=0; this._f3t=0; this._f4t=0; this._f5t=0; this._f6t=0;//cooldowns
    this._f1a=null; this._f2a=null; this._f3a=null; this._f4a=null; this._f5a=null; this._f6a=null;//Function Trigger
    this._f1net=0; this._f2net=0; this._f3net=0; this._f4net=0; this._f5net=0; this._f6net=0;//network event
    this._lx=0; this._ly=0; this._lz=0; this._lr=0;//steering to location/rotation
    this._tx=0; this._ty=0; this._tr=0;//target or target direction(diff)
    this._tvx=0; this._tvy=0; this._tr=0;//target or target direction(diff)
    
    this._throttle = 0;
    
    this._dead = false;
    
    //client prediction
    this._clx=0; this._cly=0; this._clr=0;
    this._authradiusSQ = 40000;//200 * 200
    
    
    //AI
    this._AI = {
        name:"",
        active: false,
        state : "idle",
        state_changed: false,
        detect_range : 200,
        gather_range : 100,
        check_cooldown: 5,//secs - set from template
        check_current: 6,//filled up already
        ai_check_sec: null,//[][]
        ai_overshoot_factor: 1,
        ai_need_index: 1,
        ai_need_1: "",//set from template
        ai_need_2: "",
        ai_need_3: "",
        ai_need_4: "",
        ai_need_5: "",
        wander_cooldown: 5,//secs
        wander_current: 6,//filled up already
        target:null,
        newTarget:null,
        leader:null,// for follow formation
        followers:[], // for follow formation
        follow_pos:0,
        slowradiusSQ: this.Game.DATA.AI_slowradiusSQ//100 * 100
        //stopradiusSQ: 2500//50 * 50
    };
    
    // carriers/motherships that spawn ship regularly
    this._SpawnedShip = {
        name: "",
        time: 0,
        timeLeft: 0,
        list: []
    };
    // the carrier/mothership that spawned this ship
    this.mothership = null;
    
    // to avoid saving data changes too often, no new saves are allowed until this countdown expires
    // when the countdown expires, data and statistics are saved according to the flags
    this._dataSave = {
        countdown: 0, // time left until next update can be sent, in seconds
        data: false, // whether a data update needs to be sent when the timer expires
        stats: false // whether a statistics update needs to be sent when the timer expires
    }
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.MIN_DATA_SAVE_DELAY = 2; // at least 2 seconds between data updates (playfab limit: 150 updates / 5 minutes)
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.FOLLOW_FORMATION = [
    [100, -75],
    [-100, -75],
    [0, -100]
];
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Log = function(m) { if(this.DEBUG){ console.log("[SHIP:" + this.id.get() + "] " + m); }};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.DistanceFrom = function(go) {
    return this.Game.Core.MATH.VecLength(this.x.get() - go.x.get(), this.y.get() - go.y.get());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.DistanceSquaredFrom = function(go) {
    return this.Game.Core.MATH.VecLengthSquared(this.x.get() - go.x.get(), this.y.get() - go.y.get());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// data optional (for tech that corresponds to a playfab inventory item)
oShip.prototype.EquipWeapon = function(weaponName, weaponData) {
    var techId = parseInt(weaponName.substring(4)); // "tech4" -> 4
    var tech = this.buffManager.addById(techId, weaponData, true);
    if (tech) {
        this.buffManager.equipTech(tech);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// calculate damage output for ship
oShip.prototype.CalcDamage = function() {
    // DPS calculated from tech (weapon + bonuses) * firing rate * global game damage multiplier (increased for alien ships for higher waves)
    return this.buffManager.damageBonus() * this.f1s.get() * this.Game.getDamageMultiply(this.team.get());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Collect = function(amt) {
    var d = this.Obj;
    var level = d.get("level");
    var xp = d.get("xp");
    
    xp+=amt;
    
    if (level >= 60) { return; }

    //did this level up
    if (xp >= this.Core.GD["xp" + (level + 1)]) {
        //this.level++;
        d.set("level", level + 1);
        this.nextlevelxp = this.Core.GD["xp" + (this.level + 1)];
        //apply level bonuses / show options....etc
    }
    var lvlxp = 0;
    if (level > 0) {
        lvlxp = this.Core.GD["xp" + (level)];    
    }
    this.nextlevel = ((xp - lvlxp) / (this.nextlevelxp - lvlxp))  * 100;//percent to next level
    //C.Log((this.nextlevelxp - lvlxp) + " " + (this.xp - lvlxp));
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.ProcessActions = function(dt) {
    
    //if trying, then trigger if possible
    
    if (this._f1 === 1) { if (this._f1t === 0) { if(this._f1a){ this._f1a(dt); } this._f1t += this.f1s.get(); } }
    if (this._f2 === 1) { if (this._f2t === 0) { if(this._f2a){ this._f2a(dt); } this._f2t += this.f2s.get(); } }
    if (this._f3 === 1) { if (this._f3t === 0) { if(this._f3a){ this._f3a(dt); } this._f3t += this.f3s.get(); } }
    if (this._f4 === 1) { if (this._f4t === 0) { if(this._f4a){ this._f4a(dt); } this._f4t += this.f4s.get(); } }
    if (this._f5 === 1) { if (this._f5t === 0) { if(this._f5a){ this._f5a(dt); } this._f5t += this.f5s.get(); } }
    if (this._f6 === 1) { if (this._f6t === 0) { if(this._f6a){ this._f6a(dt); } this._f6t += this.f6s.get(); } }

    //Actions Cooldowns
    if (this._f1t > 0) { this._f1t -= dt; if (this._f1t < 0) { this._f1t = 0; } }
    if (this._f2t > 0) { this._f2t -= dt; if (this._f2t < 0) { this._f2t = 0; } }
    if (this._f3t > 0) { this._f3t -= dt; if (this._f3t < 0) { this._f3t = 0; } }
    if (this._f4t > 0) { this._f4t -= dt; if (this._f4t < 0) { this._f4t = 0; } }
    if (this._f5t > 0) { this._f5t -= dt; if (this._f5t < 0) { this._f5t = 0; } }
    if (this._f6t > 0) { this._f6t -= dt; if (this._f6t < 0) { this._f6t = 0; } }
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.getLoadout = function () {
    return  {
        tech: this.buffManager.getEquippedTechList(),
        station: this.stationManager.getEquippedId()
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.setLoadout = function (value) {
    if (value.tech) {
        this.buffManager.equipTechs(value.tech);
    }
    if (value.station) {
        this.stationManager.equip(value.station);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.saveUserData = function(includeStatistics, instant) {

    //this.Log('SAVE');
    // updates the playfab data associated with the player owning the ship - current ship, current mission, mission progress etc
    if (this._view){
        if (this._view._pfid) {
            if (instant || (this._dataSave.countdown <= 0)) {
                this.Game.PLAYFAB.saveUserData(this._view._pfid, this._view.playerAccountData, includeStatistics);
                this._dataSave.countdown = oShip.MIN_DATA_SAVE_DELAY;
                this._dataSave.data = false;
                this._dataSave.stats = false;
            } else {
                this._dataSave.data = true;
                this._dataSave.stats = this._dataSave.stats || includeStatistics;
            }
        }
    }

};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AddCredits = function(amount) {
    var view = this._view;
    if (!view) {
        return;
    }
    if (this.docked.get()) {
        view.playerAccountData.bank.set(view.playerAccountData.bank.get() + amount);
        if (view._pfid) {
            this.Game.PLAYFAB.addCredits(view._pfid, this.Game.PLAYFAB.CURRENCY.CREDITS, amount);
        }
    } else {
        view.playerAccountData.credits.set(view.playerAccountData.credits.get() + amount);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.ShareCredit = function(credit) {
    var C = this.Game.Core;
    var x = this.x.get();
    var y = this.y.get();
    var team = this.team.get();
    var rangeSq = this.Game.DATA.fleet_credit_share_range_sq;
    C.ExecuteForObjects("Ship", (ship) => {
       if((ship !== this) && (ship.team.get() === team)) {
            if (ship._view && (C.MATH.VecLengthSquared(x - ship.x.get(), y - ship.y.get()) <= rangeSq)) {
                ship.AddCredits(credit);
            }
        } 
    });
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.SpendCredits = function(cmd, amount, callback) {
    this.Game.LOGIC.SpendCredits(this._view.socket, cmd, amount, callback) 
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.SpendTechPoints = function(cmd, amount, callback) {
    this.Game.LOGIC.SpendTechPoints(this._view.socket, cmd, amount, callback) 
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// player ships: team 10+
// TODO: propery test if it is a terran ship
oShip.prototype.isTerran = function () { return (this.team.get() === this.Game.TERRAN_AI_TEAM) || (this.team.get() > 10); };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.canDamage = function (other) {
    var C = this.Game.Core;
    var pvpRadius = this.Game.DATA.fleet_pvp_radius;
    return (this.id.get() !== other.id.get()) &&
           (this.team.get() !== other.team.get()) &&
           (!other.Obj.docked || !other.Obj.docked.get()) &&
           (!other.standalone || other.standalone.get()) && // only standalone turrets can be damaged
           //fleet logic - no pvp damage in safe area
           // can only damage the other ship if...
           // ...any of the two ships is not terran (alien)
           (!this.isTerran() || !other.isTerran() ||
           // ...or if both ships are within the PvP area (outside PvP radius)
           (!C.CD.inCircle(this.x.get(),  this.y.get(),  0, 0, this.radius.get(),  pvpRadius) &&
            !C.CD.inCircle(other.x.get(), other.y.get(), 0, 0, other.radius.get(), pvpRadius)));
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AwardKill = function (target) {
    // award the kill of target to this ship
    
    var bounty = parseInt(target.Obj.bounty.get());
    //this.Log("bounty:" + bounty);
    // increment kills
    if (this._view) {
        this._view.playerAccountData.kills.set(this._view.playerAccountData.kills.get()+1);
        //  xp
        this._view.playerAccountData.xp.set(this._view.playerAccountData.xp.get()+this.Game.DATA.killxp);
        // credits
        this.AddCredits(bounty);
        // mission progress
        var missionId = this._view.playerAccountData.activeMission.get();
        if (missionId) {
            var mission = this.Game.Stats.getMission(missionId);
            if (mission) {
                // if the destroyed ship is of a type that is among the targets of the mission
                if (mission.target.indexOf(target.subtype.get()) >= 0) {
                    this._view.playerAccountData.missionProgress.set(this._view.playerAccountData.missionProgress.get()+1);
                }
            }
        }

        var percs = bounty * (this.Game.DATA.fleet_credit_share_percent / 100);//percentage to share
        this.ShareCredit(percs);

        // we will send this over with the delete command to the appropriate client so it can display the reward
        target._creditsAwardedTo = this;
        target._creditsAwarded = bounty;

        // save updated statistics / mission state to playfab
        this.saveUserData(true);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype._Hit = function(x, y, groupName, filter) {
    var C = this.Game.Core;
    
    var rxy = C.MATH.rotate_by_pivot(x, y, this.r.get(), this.damage_range.get(), 0);
    
    var hits = [];
    var nearest = null;
    var nearestlength = 0;
    var length = 0;
    
    C.ExecuteForObjects(groupName, (obj)=>{
        if (!filter || filter(obj)) {
            if (this.Obj.HitCheck(x, y, rxy, obj) === true) { hits.push(obj); }
        }
    });
    
    var O;
    
    if (hits.length > 0) {
        for(var i=0; i < hits.length; i++)
        {
            if (i===0) {
                O = hits[i];
                nearestlength = C.MATH.VecLengthSquared(O.x.get() - x, O.y.get() - y);
                nearest = O;
            }
            else{
                O = hits[i];
                length = C.MATH.VecLengthSquared(O.x.get() - x, O.y.get() - y);
                if (length < nearestlength) {
                    nearest = O;
                    nearestlength = length;
                }
            }
        }
        //Apply Damage to nearest hit object
        nearest.Obj.Damage(this.CalcDamage());
        
        return nearest;
    }
    return null;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Shoot = function() {
    // zero fire speed means no weapon equipped
    if (this.f1s.get() <= 0) {
        return;
    }
    
    if (this.docked.get()) {
        return;
    }
    
    if (this.max_ammo.get() > 0) { 
        if (this.ammo.get() <= 0) {
            return;
        }
        this.ammo.set(this.ammo.get() - 1);
    }
    
    var o = this;
    var C = this.Game.Core;
    var radius = o.w.get() / 2;
    var angle = C.MATH.degToRad(o.r.get());
    var projectileWidth = this.Game.DATA.projectile_width;
    var x = o.x.get() + Math.cos(angle) * (radius + projectileWidth);
    var y = o.y.get() + Math.sin(angle) * (radius + projectileWidth);

    //Against Asteroids (block)
    if (this._Hit(x, y, "Asteroid")) {
        return;
    }
    //Against standalone station turrets
    if (this._Hit(x, y, "Turret", (obj)=>(obj.standalone.get() && this.canDamage(obj)))) {
        return;
    }
    //Against bases
    if (this._Hit(x, y, "Base", (obj)=>(this.canDamage(obj)))) {
        return;
    }
    //Against Ships (Player or AI)
    var hit = this._Hit(x, y, "Ship", (obj)=>(this.canDamage(obj)));
    if (hit) {
        if(hit.Obj._remove === true){
            this.AwardKill(hit);
            hit.Die();
            //this.Collect(this.Game.DATA.killxp); // seems broken (old code?)
        }
        return;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Die = function () {
    if (this._dead) {
        return;
    }
    this._dead = true;
    var o = this;
    if (o.hp.get() < 0) {
        o.hp.set(0);
    }
    if (o._view) {
        o._view.playerAccountData.deaths.set(o._view.playerAccountData.deaths.get() + 1);
        // remove unbanked credits
        o._view.playerAccountData.credits.set(0);
        // fail current mission
        o._view.playerAccountData.activeMission.set(0);
        o._view.playerAccountData.missionProgress.set(0);
        // remove reference to ship and last docked station
        o._view.playerAccountData.shipName.set("");
        o._view.playerAccountData.baseID.set(0);
        
        // save 0 HP state to playfab and owned ships list
        o.Game.LOGIC.UpdateShipState(o._view.socket, {save: true});
        
        // update statistics on playfab
        o.saveUserData(true, true);
        // adding respawn timer
        // ships are no longer respawned, player needs to buy them again
        // this.Game.DATA.DeadPlayers[o.id.get()] = {ship: o, time: 0};
    }
};
//*********************************************************************************************************************************************
// Calculates and returns the target rotation based on mouse input (received from client)
//*********************************************************************************************************************************************
oShip.prototype.getTargetRotation = function () {
    return this.Game.Core.MATH.XYToDegree(this._mx, this._my);
}
//*********************************************************************************************************************************************
// Calculates and returns the throttle based on mouse input (received from client)
//*********************************************************************************************************************************************
oShip.prototype.getThrottle = function () {
    var o = this;//deref for clarity
    var C = this.Game.Core;
    var x = o.x.get();
    var y = o.y.get();
    
    // calculate throttle
    var minMoveRadius = o.move_radius.get();
    var maxMoveRadius = o.move_radius_max.get();
    if (C.CD.pointCircle(x, y, x + o._mx, y + o._my, minMoveRadius)) {
        // no throttle within minimum move circle
        return 0;
    } else if (C.CD.pointCircle(x, y, x + o._mx, y + o._my, maxMoveRadius)) {
        // calculated throttle between min and max circles
        var moveDist = Math.sqrt(o._mx * o._mx + o._my * o._my) - minMoveRadius;
        var moveDiff = maxMoveRadius - minMoveRadius;

        return (moveDist / moveDiff);
    } else { 
        // max throttle outside of max circle
        return 1;
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Process = function(dt) {
    
    var o = this;//deref for clarity
    var C = this.Game.Core;
    var i;
    
    // clean up follow state
    if (this._AI.leader && (this._AI.leader.Obj._remove)) {
        this._AI.leader = null;
    }
    for (i = 0; i < this._AI.followers.length; i++) {
        if (this._AI.followers[i].Obj._remove) {
            this._AI.followers.splice(i, 1);
            i--;
        } else {
            this._AI.followers[i]._AI.follow_pos = i;
        }
    }
    
    if (o.Obj._remove === true) {
        this.Die();
        return;
    }
    
    //---------------------------------------------------------------------------------------------------------
    //AI 
    //---------------------------------------------------------------------------------------------------------
    if (o._AI.active === true) {
        o.AI_Think(dt);
    }
    
    var x = o.x.get();
    var y = o.y.get();
    var vx = o.vx.get();
    var vy = o.vy.get();
    var ox = x;
    var oy = y;
    var ovx = vx;
    var ovy = vy;
    
    //-----------------------
    // undocking
    if (o.docked_at.get()) {
        var base = C.GetObject("Base", o.docked_at.get());
        if (base) {
            if (o.docked.get()) {
               o.x.set(base.x.get()); 
               o.y.set(base.y.get()); 
               o.vx.set(0);
               o.vy.set(0);
           } else if (o.DistanceFrom(base) > base.dock_radius.get()) {
               // mark if we left the base docking area
               this.Log("Left docking area of base " + base.id.get());
               o.docked_at.set(0);
           }
        } else {
            // undock ifthe base doesn't exist anymore
            this.Log("Base " + o.docked_at.get() + ", where ship was docked, does not exist anymore, undocking");
            o.docked_at.set(0);
        }    
    } 
    // update docking state if we undocked
    if (!o.docked_at.get() && o.docked.get()) {
        //this.Log("Undocked!");
        o.docked.set(false);
    }
    //---------------------------------------------------------------------------------------------------------
    //Ship Movement
    //---------------------------------------------------------------------------------------------------------
    if (!o.docked.get()) {

        //-----------------------
        // calculating target speed
        var tr = o._AI.active ? this.AI_GetTargetRotation() : this.getTargetRotation();
        var throttle = o._AI.active ? this.AI_GetThrottle() : this.getThrottle();
        var max_speed = (o.max_speed.get() + o.speedboost.get()) * ((o._f1 === 1) ? o.shoot_drop.get() : 1);
        var target_speed = max_speed * throttle;

        // TODO: shift lock not considered
        //-----------------------
        // applying acceleration to change velocity vector towards target velocity vector (ship thrusters)
        var tvxy = C.MV.MoveAlongCurrentRotation(target_speed, tr); // target velocity
        var acc = max_speed / o.acceleration.get(); // "acceleration" actually holds how many seconds are needed to reach max speed
        var vdiff = C.MATH.VecSub(tvxy[0], tvxy[1], vx, vy);
        var len = C.MATH.VecLength(vdiff[0], vdiff[1]);
        if (len > 0.001) {
            vx += vdiff[0] / len * acc * dt;
            vy += vdiff[1] / len * acc * dt;
        }
        
        var speed = C.MATH.VecLength(vx, vy);

        //-----------------------
        // calculating new position
        x += vx * dt;
        y += vy * dt;

        if (o._AI.active === false) {
            //-----------------------
            // comparing calculated position with prediction based on client position, overwriting if within auth circle
            if (C.MATH.VecLengthSquared(o._clx - x, o._cly - y) < this._authradiusSQ) {
                //Try Predicting next location to catch up to real location since we are 100ms behind
                x = o._clx + ((ovx * dt) * 3);// X 6 seems to work since server updates sim a lot more often then sends
                y = o._cly + ((ovy * dt) * 3);
            }
        } else {
            //-----------------------
            // restricting calculated position to map bounds for AI
            var w = this.Game.DATA.CoreData.MapX / 2;
            var h = this.Game.DATA.CoreData.MapY / 2;
            x = Math.min(Math.max(-w, x), w);
            y = Math.min(Math.max(-h, y), h);
        }

        //-----------------------
        // applying position based mechanics to final, valid position
        o.x.set(x);
        o.y.set(y);
        o.Obj.UpdateBounds();
        o.vx.set(vx);
        o.vy.set(vy);

        // asteroid collision
        var Asteroids = C.GetGroup(C.goTypes.indexOf("Asteroid"));

        var shipRadius = o.Obj.w.get() * 0.5;
        var blocker = C.CD.CheckCollisionByRadius(x, y, shipRadius, Asteroids);
        if (blocker) {
            //backtrack and bump if collided with an asteroid
            var dx = x - blocker.x.get();
            var dy = y - blocker.y.get();
            var dist = C.MATH.VecLength(dx, dy);
            dx /= dist;
            dy /= dist;
            var dot = vx * dx + vy * dy; // dot product
            vx -= 2 * dx * dot * (this.Game.DATA.collision_bumpiness || 1);
            vy -= 2 * dy * dot * (this.Game.DATA.collision_bumpiness || 1);
            o.x.set(ox);
            o.y.set(oy);
            o.Obj.UpdateBounds();
            // adjust speed vector - bump away
            o.vx.set(vx);
            o.vy.set(vy);
            // collision damage
            if (o.Obj._remove === false) {
                o.Obj.Damage(this.Game.DATA.collision_ship_damage * speed);
            }
            blocker.Obj.Damage(this.Game.DATA.collision_asteroid_damage * speed);
            //this.Log("asteroid collision");
        }
            
        // health drain if outside game area
        if (!C.CD.inCircle(o.x.get(), o.y.get(), 0,0, o.radius.get(), this.Game.DATA.base_redzone)) {
            if (o.Obj._remove === false) {
                o.Obj.Damage((this.Game.DATA.base_redzone_drain / 100) * o.max_hp.get() * dt); 
            }
        }
        
        //-----------------------
        // Linear Rotation (180 to -180)
        var rotationBonus = (100 + o.turnboost.get()) * 0.01;
        var turnRate = o.rot_speed.get() * rotationBonus * ((o._f1 === 1) ? o.shoot_drop_r.get() : 1);
        this.Obj.Turn(tr, turnRate, dt);
    }
    
    if (o.Obj._remove === true) {
        this.Die();
    } else {        
        // Handle Active Actions (Generic)
        o.ProcessActions(dt);
        // tracking firing
        if(o.f1on.get() !== o._f1) { o.f1on.set(o._f1); }

        if (this.buffManager) {
            this.buffManager.Process(dt);
        }
        if (this.stationManager) {
            this.stationManager.Process(dt);
        }
        
        var t = this.message_timeleft.get();
        if (t > 0) {
            t = Math.max(0, t - dt);
            this.message_timeleft.set(t);
        }
        
        // motherships/carriers spawn other ships
        if (this._SpawnedShip.name) {
            for (i = 0; i < this._SpawnedShip.list.length; i++) {
                if (this._SpawnedShip.list[i].Obj._remove) {
                    this._SpawnedShip.list.splice(i, 1);
                    i--;
                }
            }
            if (this._SpawnedShip.list.length < this._SpawnedShip.maxCount) {
                if (this._SpawnedShip.timeLeft <= 0) {
                    this._SpawnedShip.timeLeft = this._SpawnedShip.time;
                    var spawnedShip = this.Game.SpawnShip(this._SpawnedShip.name, this.team.get(), this);
                    this._SpawnedShip.list.push(spawnedShip);
                } else {
                    this._SpawnedShip.timeLeft -= dt;
                }
            }
        }
        
        // mark last enemy ships for always transfering to all clients, so they can show arrows pointing to them when they are not visible within their area
        if ((this.Game.WaveShipLeftCount <= this.Game.DATA.show_arrows_ship_count) && (this.team.get() === this.Game.ALIEN_AI_TEAM) && !this.mothership) {
            this.alwaysTransfer = true;
        }
    }
    
    // enforcing a minimum delay between playfab data saves
    if (this._dataSave.countdown > 0) {
        this._dataSave.countdown -= dt;
        // if the delay countdown finished, check if new data / stats save was issued, and send the update if so
        if (this._dataSave.countdown <= 0) {
            if (this._dataSave.data) {
                this.Game.PLAYFAB.saveUserData(this._view._pfid, this._view.playerAccountData, this._dataSave.stats);
            }
            this._dataSave.countdown = oShip.MIN_DATA_SAVE_DELAY;
            this._dataSave.data = false;
            this._dataSave.stats = false;
        }
    }
}
//*********************************************************************************************************************************************
//Survive(40) = When the health is below 40%, the ship flies to closest base
//Attack(400) = When an enemy ship enters 400 range, this ship attacks it
//Follow(50) = When the ship sees another friendly ship within 50 range, it follows
//FollowRevenge(30) = When the ship I'm following endures 30% damage, switch to next state down the line
//SelfRevenge(10) = When I take 30% damage, switch to next state down the line
//Wander(1000) = picks a random point 1000 units away and travels there
//Seek(400) = Move towards the main station, attack it if within 400 range
//*********************************************************************************************************************************************
oShip.prototype.AI_CheckState = function(state, p1) {
    var O;
//    this.Log("[Check State] " + state + " " + p1);
    if (state === "survive") {
        if ((this.hp.get() / this.max_hp.get() < (p1 / 100)) || this.docked.get() ) {
            return true;
        }
    }
    else if (state === "attack") {
        //enemy ship?
        O = this.AI_Nearest_Target("Ship", p1 * p1);
        if (O) {
            //this.Log(O);
            this._AI.newTarget = O;
            return true;
        }
        //enemy turret?
        O = this.AI_Nearest_Target("Turret", p1 * p1);
        if (O) {
            //this.Log(O);
            this._AI.newTarget = O;
            return true;
        }
        //enemy base?
        O = this.AI_Nearest_Target("Base", p1 * p1);
        if (O) {
            //C.Log("Base in Range!");
            this._AI.newTarget = O;
            return true;
        }
    }
    else if (state === "follow") {
        //friendly ship?
        O = this.AI_Nearest_NotFollow("Ship", p1 * p1, this.team.get() === 1 ? 2 : 1);//opposite team
        if (O && (O._AI.followers.length < oShip.FOLLOW_FORMATION.length)) {
            this._AI.newTarget = O;
            return true;
        }
        
    }
    else if (state === "follow_revenge") {
        //friendly ship?
        O = this.AI_Nearest_NotFollow("Ship", p1 * p1, this.team.get() === 1 ? 2 : 1);//opposite team
        if (O) {
            //this.Log("Ally in Range!");
            this._AI.newTarget = O;
            return true;
        }
    }
    else if (state === "self_revenge") {
        return true;
    }
    else if (state === "wander") {
        return true;
    }
    else if (state === "seek") {
        return true;
    }
    return false;
}
//*********************************************************************************************************************************************
// Calculates and returns the target rotation based on AI state
//*********************************************************************************************************************************************
oShip.prototype.AI_GetTargetRotation = function () {
    return this.Game.Core.MATH.XYToDegree(this._tvx - this.x.get(), this._tvy - this.y.get());
}
//*********************************************************************************************************************************************
// Calculates and returns the throttle based on AI state
//*********************************************************************************************************************************************
oShip.prototype.AI_GetThrottle = function () {
    var o = this;//deref for clarity
    var C = this.Game.Core;
    var state = this._AI.state;
    //Stop if close enough and simply rotate towards
    if (state === "wander" || state === "seek" || state === "follow") {
        var distSQ = C.MATH.VecLengthSquared(o._tvx - o.x.get(), o._tvy - o.y.get());
        if(distSQ < o._AI.slowradiusSQ){
            this._throttle = 0;
        } else {
            this._throttle = 1;
        }
    }
    if (state === "attack" || state === "follow" || state === "follow_revenge" ) {
        this._throttle = 1;
    }
    return this._throttle;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AI_EnterState = function(state, p1) {
//    this.Log("[Enter State] " + state + " " + p1);
    var d;
    if (state === "survive") {
        d = 5000;//hard code for now, not available as a parameter
        this._tvx = this.x.get() + Math.floor((Math.random() * d) - (d/2));
        this._tvy = this.y.get() + Math.floor((Math.random() * d) - (d/2));

    }
    if (state === "attack") {
        this._AI.target = this._AI.newTarget;
    }
    if (state === "follow") {
        // take position within follow formation
        this._AI.target = this._AI.newTarget;
        var t = this._AI.target;
        this._AI.leader = t;
        this._AI.follow_pos = t._AI.followers.length;
        t._AI.followers.push(this);
        
    }
    if (state === "follow_revenge") {
        this._AI.target = this._AI.newTarget;
    }
    if (state === "self_revenge") {
    
    }
    if (state === "wander") {
        d = p1 * 2;
        this._tvx = this.x.get() + Math.floor((Math.random() * d) - (d/2));
        this._tvy = this.y.get() + Math.floor((Math.random() * d) - (d/2));
        //this.Log("ENTER: (" + this.team.get() + ") " + this.x.get() + ", " + this.y.get() + " to " + this._tvx + ", " + this._tvy)
        //this.Log(Math.floor(this.x.get()  - this._tvx) + " " + Math.floor(this.y.get()  - this._tvy));    
        
        //this.Log(Math.floor(Math.floor((Math.random() * d) - p1)));
        
        //this._tvx = this.x.get() + Math.floor((Math.random() * 100000) - 50000);
        //this._tvy = this.y.get() + Math.floor((Math.random() * 100000) - 50000);
    }
    if (state === "seek") {
    
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AI_hasValidTarget = function () {
    var target = this._AI.target;
    return ((target.Obj._remove === false) && (target.hp.get() > 0) && (!target.Obj.docked || !target.Obj.docked.get()));
};
//*********************************************************************************************************************************************
// Returns false to force a state check next turn (skipping cooldown)
//*********************************************************************************************************************************************
oShip.prototype.AI_ProcessState = function(dt, state, p1) {
    var distSQ, diffa;
    var C = this.Game.Core;
    //this.Log("Process State: " + state);
    var d;
    //---------------------------------------------------------------------------------------------------------
    //Survive
    //---------------------------------------------------------------------------------------------------------
    if (state === "survive") {
        // terran AI returns to station for repairs when in survive mode
        if (this.team.get() === this.Game.TERRAN_AI_TEAM) {
            var base = this.Game.DATA.base2_obj;
            var max_hp = this.max_hp.get();
            // go towards base and dock when there
            if (!this.docked.get()) {
                if ((this.hp.get() === max_hp) || (base.hp.get() <= 0)) {
                    return false;
                }
                this._tvx = base.x.get();
                this._tvy = base.y.get();
                if (this.DistanceFrom(base) <= base.dock_radius.get()) {
                    this.docked.set(true);
                    this.docked_at.set(base.id.get());
                    this.x.set(this._tvx);
                    this.y.set(this._tvy);
                    this.vx.set(0);
                    this.vy.set(0);
                }
            } else {
                // if docked, repair up and undock when fully repaired
                if (this.hp.get() < max_hp) {
                    this.hp.set(Math.min(max_hp, this.hp.get() + max_hp * dt / this.Game.DATA.docked_ai_repair_time));
                } else {
                    this.docked.set(false);
                    this.docked_at.set(0);
                    // finished repairs
                    return false;
                }
            }
            
            return true;
        }
        
        // for non-terran AI:
        //Same as Wander for now (no main Base)
        //move
        
        distSQ = C.MATH.VecLengthSquared(this._tvx - this.x.get(), this._tvy - this.y.get());
        if (distSQ < 25000) {
            return false;
        }
        
        //no movement
        if(this._throttle === 0)
        {
            d = 5000;//hard code for now, not available as a parameter
            this._tvx = this.x.get() + Math.floor((Math.random() * d) - (d/2));
            this._tvy = this.y.get() + Math.floor((Math.random() * d) - (d/2));
            //this.Log("Stuck getting new coords");
        }

        return true;
    } else {
        // all non-survive states should be always undocked
        if (this.docked.get()) {
            this.docked.set(false);
            this.docked_at.set(0);
        }
    }
    //---------------------------------------------------------------------------------------------------------
    //Attack
    //---------------------------------------------------------------------------------------------------------
    if (state === "attack") {
        if(!this._AI.target) return false;
        if (this.AI_hasValidTarget()) {
            var w = this.w.get();
            var target = this._AI.target;
            var range = w + this.damage_range.get() + target.w.get();
            distSQ = this.DistanceSquaredFrom(target);
            
            var toTargetX = target.x.get() - this.x.get();
            var toTargetY = target.y.get() - this.y.get();
            
            var deg = this.Game.Core.MATH.XYToDegree(toTargetX, toTargetY);
            
            diffa = Math.abs(((this.Game.Core.MATH.NormalizeDegrees(this.r.get())) + 180 - deg) % 360 - 180);
            
            var diffb = Math.abs(((this.Game.Core.MATH.NormalizeDegrees(target.r.get())) + 360 - deg) % 360 - 180);
            
            var dot = toTargetX * target.vx.get() + toTargetY * target.vy.get(); // dot product -> will be positive if target is moving away from us
            
            // setting target destination to make attack passes
            
            var overshootRange = range * this._AI.ai_overshoot_factor;
            // rush towards target if out of overshoot range OR within overshoot range and facing towards it OR target facing us and moving towards us (to avoid chasing)
            if ((distSQ > overshootRange * overshootRange) || (diffa < 60) || ((diffb < 20) && (dot < 0))) {
                this._tvx = target.x.get();
                this._tvy = target.y.get();
            } else {
                // if within overshoot range and facing away (passed the target), continue in current direction until out of range again for another pass
                this._tvx = this.x.get() + this.vx.get() * 100;
                this._tvy = this.y.get() + this.vy.get() * 100;
            }
            
            this._f1 = ((diffa < 30) && (distSQ < range * range) && (distSQ > w * w)) ? 1 : 0;
            
            //out of behavior range, return false to force new AI state check
            if(distSQ > p1 * p1) { 
                return false; 
            }
        }
        else { 
            this._f1 = 0;
            return false;
        }
        return true;
    }
    //---------------------------------------------------------------------------------------------------------
    //Follow
    //---------------------------------------------------------------------------------------------------------
    if (state === "follow") {
        if(!this._AI.target || !this._AI.leader) return false;
        if (this.AI_hasValidTarget()) {
            
            // assume position in formation
            var lx = this._AI.leader.x.get();
            var ly = this._AI.leader.y.get();
            this._AI.follow_pos = this._AI.leader._AI.followers.indexOf(this);
            var px = oShip.FOLLOW_FORMATION[this._AI.follow_pos][0];
            var py = oShip.FOLLOW_FORMATION[this._AI.follow_pos][1];
            var pxy = C.MATH.rotate_by_pivot(lx, ly, this._AI.leader.r.get(), py, px);
            
            this._tvx = pxy[0];
            this._tvy = pxy[1];
            
            //distSQ = this.Game.Core.MATH.VecLengthSquared(this._tvx - this.x.get(), this._tvy - this.y.get());
            //out of range
            //if(distSQ > p1 * p1)
            //{ return false; }
        }
        else { return false; }
        return true;
    }
    //---------------------------------------------------------------------------------------------------------
    //Follow Revenge
    //---------------------------------------------------------------------------------------------------------
    //FollowRevenge(30) = When the ship I'm following endures 30% damage, switch to next state down the line
    if (state === "follow_revenge") {
        if(!this._AI.target) return false;
        if (this.AI_hasValidTarget()) {
            this._tvx = this._AI.target.x.get();
            this._tvy = this._AI.target.y.get();
            
            //this.Log("TARGET " + Math.floor(this._tvx) + ", " + Math.floor(this._tvy));
            distSQ = this.Game.Core.MATH.VecLengthSquared(this._tvx - this.x.get(), this._tvy - this.y.get());
            //this.Log("TARGET " + this.x.get() + " " + this._AI.target.x.get())
            
            //out of range
            if(distSQ > p1 * p1)
            { return false; }
            
            //Exit if leader starts attacking or firing
            if (this._AI.target._AI.state === "attack" || this._AI.target._f1 === 1) {
                return false;
            }
            //if (this._AI.target.hp.get() / this._AI.target.max_hp.get() < (p1 / 100) ) {
              //  return false;
            //}
        }
        else { return false; }
        return true;
    }
    //---------------------------------------------------------------------------------------------------------
    //Self Revenge
    //---------------------------------------------------------------------------------------------------------
    //SelfRevenge(10) = When I take 30% damage, switch to next state down the line
    if (state === "self_revenge") {
        if (this.hp.get() / this.max_hp.get() < (p1 / 100) ) {
            return false;
        }
    
        return true;
    }
    //---------------------------------------------------------------------------------------------------------
    //Wander
    //---------------------------------------------------------------------------------------------------------
    if (state === "wander") {
        //move
        distSQ = C.MATH.VecLengthSquared(this._tvx - this.x.get(), this._tvy - this.y.get());
        if (distSQ < 25000) {
            return false;
        }
        
        //no movement
        if(this._throttle === 0)
        {
            d = p1 * 2;
            this._tvx = this.x.get() + Math.floor((Math.random() * d) - (d/2));
            this._tvy = this.y.get() + Math.floor((Math.random() * d) - (d/2));
            //this.Log("Stuck getting new coords");
        }
    
        return true;
    }
    //---------------------------------------------------------------------------------------------------------
    // Seek
    //---------------------------------------------------------------------------------------------------------
    if (state === "seek") {
        var mainBase = this.Game.DATA.base2_obj;
        if (!mainBase || (mainBase._remove === true)) { return false; } 
        this._tvx = mainBase.x.get();
        this._tvy = mainBase.y.get();
        
        //Only if aiming towards (in angle range)
        diffa = Math.abs(((this.Game.Core.MATH.NormalizeDegrees(this.r.get())) + 180 - this.Game.Core.MATH.XYToDegree(this._tvx - this.x.get(), this._tvy - this.y.get())) % 360 - 180);
        distSQ = this.Game.Core.MATH.VecLengthSquared(this._tvx - this.x.get(), this._tvy - this.y.get());

        this._f1 = (diffa < 30) ? 1 : 0;

        //out of range
        if(distSQ > p1 * p1) { 
            this._f1 = 0; 
            //return false; 
        }
        return true;
    }
    return false;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AI_ExitState = function(state) {
//    this.Log("[Exit State] " + state + " " + p1);
    if (state === "survive") {
        this._tvx = this.x.get();
        this._tvy = this.y.get();
    }
    if (state === "attack") {
        this._f1 = 0;
    }
    if (state === "follow") {
        if (this._AI.leader) {
            var f = this._AI.leader._AI.followers;
            f.splice(f.indexOf(this), 1);
        }
        this._AI.leader = null;
    }
    if (state === "follow_revenge") {
    
    }
    if (state === "self_revenge") {
    
    }
    if (state === "wander") {
        this._tvx = this.x.get();
        this._tvy = this.y.get();
    }
    if (state === "seek") {
        this._f1 = 0;
    }
    this._AI.target = null;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AI_Think = function(dt) {

    var o = this;
    var AI = this._AI;
    
    //Current State
    var s, p1, p2;
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //Idle
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //Check all states and see if one is valid in list every X seconds
    if (AI.check_current >= AI.check_cooldown) {
        for (var i=1; i<6; i++ )
        {
            var ostate = AI.state;
            s = AI["ai_need_" + i];//state, parameter1, parameter2, ...
            AI.state = s[0];
            p1 = parseInt(s[1]);
            p2 = parseInt(s[2]);
            
            //Check for valid state in list, allow random bypass with p2
            var r = Math.floor((Math.random() * 100) + 1);
            if (r > p2) {
                if(o.AI_CheckState(AI.state, p1) === true) {
                    AI.ai_need_index = i;
                    o.AI_ExitState(ostate);
                    o.AI_EnterState(AI.state, p1);
                    AI.check_current = 0;
                    
                    //Next Cooldown (random range)
                    var top = AI.ai_check_sec[1] - AI.ai_check_sec[0];
                    var r2 = Math.floor((Math.random() * top) + AI.ai_check_sec[0]);
                    AI.check_cooldown = r2;
                    this.username.set(this._AI.name + " (" + AI.state + " " + p1 + ") " + r2);
                    
                    return;
                }
            }
        }
        AI.check_current = 0;
    }
    else
    {
        AI.check_current += dt;
        
        s = AI["ai_need_" + AI.ai_need_index];//state, parameter1, parameter2, ...
        AI.state = s[0];
        p1 = parseInt(s[1]);
        p2 = parseInt(s[2]);
        
        if(o.AI_ProcessState(dt, AI.state, p1) === false)
        {
            AI.check_current = AI.check_cooldown;//reset
        }
        
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AI_Nearest = function(groupname, rangeSQ, oteam) {
    var objs, nearest, O, distSQ, cdistSQ;
    objs = this.Game.Core.GetGroup(this.Game.Core.goTypes.indexOf(groupname));
    nearest = null;
    for (var key in objs) {
        if (objs.hasOwnProperty(key)) {
            O = objs[key];
            //if(O.nettype.get() == nettype && this.id != O.id && this.team != O.team && O.remove == false){
            if(this.id.get() !== O.id.get() && oteam !== O.team.get() && O.Obj._remove === false){
                distSQ = this.Game.Core.MATH.VecLengthSquared(O.x.get() - this.x.get(), O.y.get() - this.y.get());
                if( distSQ < rangeSQ) {
                    //C.Log("Enemy in Range!");
                    if (nearest === null) { nearest = O; cdistSQ = distSQ; }//first one in range
                    else{
                        if(distSQ < cdistSQ){ nearest = O; cdistSQ = distSQ; }//closer one
                    }
                    
                }
            }
        }
    }
    return nearest;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AI_Nearest_Target = function(groupname, rangeSQ) {
    var objs, nearest, O, distSQ, cdistSQ;
    objs = this.Game.Core.GetGroup(this.Game.Core.goTypes.indexOf(groupname));
    nearest = null;
    for (var key in objs) {
        if (objs.hasOwnProperty(key)) {
            O = objs[key];
            if((O.Obj._remove === false) && this.canDamage(O)){
                distSQ = this.Game.Core.MATH.VecLengthSquared(O.x.get() - this.x.get(), O.y.get() - this.y.get());
                if( distSQ < rangeSQ) {
                    //C.Log("Enemy in Range!");
                    if (nearest === null) { nearest = O; cdistSQ = distSQ; }//first one in range
                    else{
                        if(distSQ < cdistSQ){ nearest = O; cdistSQ = distSQ; }//closer one
                    }
                    
                }
            }
        }
    }
    return nearest;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.AI_Nearest_NotFollow = function(groupname, rangeSQ, oteam) {
    var objs, nearest, O, distSQ, cdistSQ;
    objs = this.Game.Core.GetGroup(this.Game.Core.goTypes.indexOf(groupname));
    nearest = null;
    for (var key in objs) {
        if (objs.hasOwnProperty(key)) {
            O = objs[key];
            if(this.id.get() !== O.id.get() && oteam !== O.team.get() && O.Obj._remove === false && O._AI.state !== "follow" && O._AI.state !== "follow_revenge"){
                distSQ = this.DistanceSquaredFrom(O);
                if( distSQ < rangeSQ) {
                    if ((nearest === null) || (distSQ < cdistSQ)) { // first one or closer one
                        nearest = O; 
                        cdistSQ = distSQ; 
                    }
                }
            }
        }
    }
    return nearest;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oShip;
