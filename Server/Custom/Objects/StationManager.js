//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    StationManager - SERVER SIDE
    Manages the droppable stations owned by a player
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var Station = require('../../Custom/Objects/Station');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationManager(ship)
{
    //debugging
    this.DEBUG = true;
    /** @type oShip */
    this._ship = ship;

    this._stations = null;
    
    this._equippedStation = null;

    this.reset();
}

StationManager.prototype.reset = function(){
    
    this._stations = [];
    this._equippedStation = null;

}

StationManager.prototype.getEquippedId = function(){
    return this._equippedStation ? this._equippedStation.config.id : 0;
}

// if replace = true and a station with the same Id already exists, its data will replaced (otherwise always a new station is added)
StationManager.prototype.addById = function(stationId, stationData, increaseAmount){

    var game = this._ship.Game;
    var stats = game.Stats;
    var stations = stats.Stations;

    for (var t in stations) {
        if (stations.hasOwnProperty(t)) {
            var config=stations[t];
            if (stationId === config.id) {
                // check if we have station with the same id
                var station = this._stations.find((b)=>b.config.id === stationId);
                if (!station) {
                    // add new tech
                    station = new Station(game, config);
                    this._stations.push(station);
                }
                if (stationData) {
                    station.initInstanceData(game, stationData);
                } 
                if (increaseAmount) {
                    station.buy(increaseAmount);
                }
                
                if (station.equipped) {
                    if (!this._equippedStation) {
                        this._equippedStation = station;
                    } else if (this._equippedStation !== station) {
                        station.unequip(); // cannot have multiple stations equipped at the same time
                    }
                }
                
                // update datalist for sync to client
                this.save();
                
                return station;
            }
        }
    }
    this.log('addById failed: no station with such id: ' + stationId);
    return null;
}

// for sending to client - includes instance data and config
StationManager.prototype.toJSON = function(){

    var a = [];
    for (var i=0;i<this._stations.length;i++){
        a.push(this._stations[i].toJSON());
    }

    return JSON.stringify(a);

}

// for saving to playfab - includes instance data only!
StationManager.prototype.toPlayfabData = function(){

    var a = [];
    for (var i=0;i<this._stations.length;i++){
        a.push(this._stations[i].toPlayfabData());
    }

    return a;
}

StationManager.prototype.findById = function (stationId) {
    var i;
    for (i = 0; i < this._stations.length; i++){
        if (this._stations[i].config.id === stationId){
            return this._stations[i];
        }
    }
    return null;
}

StationManager.prototype._add = function (stationId) {
    var self = this;
    var game = this._ship.Game;
    var view = this._ship._view;
    if (!view._pfid) {
        this.log("Cannot add station: not logged in to playfab!");
        return;
    }
    if (!this.findById(stationId)) {
        game.PLAYFAB.grantItem(view._pfid, "station" +  stationId, function (itemGrantResults) {
                    var grantedItemId = parseInt(itemGrantResults[0].ItemId.substring(7));
                    self.addById(grantedItemId, itemGrantResults[0], 1);
                    //self.log("added new station: " + grantedItemId);
                });
    } else {
        this.addById(stationId, null, 1);
    }
}

StationManager.prototype.buy = function(stationId) {
    
    if (!this._ship._view._pfid) {
        this.log("station buy failed: not logged in to playfab!");
        return;
    }
    
    var game = this._ship.Game;
    var config = game.Stats.getStation(stationId);

    var price = config.cost;
    
    var station = this.findById(stationId);
    
    if (station && (station.amount >= config.maxCount)) {
        this.log('station buy failed: already at max count for stationId:'+stationId+" ("+config.maxCount +")");
        return;
    }
    
    this._ship.SpendCredits('buystation', price, ()=>{
        this._add(stationId);
    });
}


StationManager.prototype.equip = function(stationId){

    // do we own it
    var found = this.findById(stationId);
        
    if (found === null){
        this.log('station equip failed not found stationId:'+stationId);
        return;
    }
    
    if (this._equippedStation) {
        this._equippedStation.unequip();
    }
    
    found.equip();
    this._equippedStation = found;

    //this.log('station equipped '+fc.name);
    this.save();

}

StationManager.prototype.unequip = function() {
    if (this._equippedStation) {
        this._equippedStation.unequip();
        this._equippedStation = null;
        this.save();
    }
}

// returns 0 for success and 1,2,3... for various reasons of failure (TODO: refactor)
StationManager.prototype.deploy = function() {
    var result;
    if (this._equippedStation) {
        //this.log('deploying station');
        result = this._equippedStation.deploy(this._ship);
        if (!this._equippedStation.equipped) {
            this._equippedStation = null;
        }
        this.save();
        return result;
    } else {
        this.log('station cannot be deployed: no equipped station');
        return 1;
    }
}

// dt in seconds
StationManager.prototype.Process = function (dt) {
    var changed = false;
    for(var i = 0; i < this._stations.length; i++) {
        changed = changed || this._stations[i].Process(dt);
    }
    if (changed) {
        this.save();
    }
}

StationManager.prototype.save = function() {
    // send to the client with the update of the ship object
    this._ship.stations.set(this.toJSON());
}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationManager.prototype.log = function(m) { if(this.DEBUG){ console.log("[StationManager:" + (this._ship && this._ship.id.get()) + "] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = StationManager;
