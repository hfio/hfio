//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    BuffManager - SERVER SIDE
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var Buff = require('../../Custom/Objects/Buff');
var Bonus = require('../../Custom/Objects/Bonus');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function BuffManager(ship)
{
    //debugging
    this.DEBUG = true;
    /** @type oShip */
    this._ship = ship;

    this._buffs = [];

    this._totalDamageBonus = 0;
    this._totalSpeedBonus = 0;
    this._totalHealthBonus = 0;
    this._totalTurnBonus = 0;
    this._totalResistanceBonus = 0;
    //this._totalFuelBonus = 0;
    this._totalAmmoBonus = 0;

    this.reset();
}

BuffManager.prototype.reset = function(){
    

    this._buffs=[];

//    var game = this._ship.Game;
//    var stats = game.Stats;
//    var tech = stats.Tech;

}

BuffManager.prototype.getEquippedTechList = function() {
    var result = [];
    for (var i = 0; i < this._buffs.length; i++) {
        if (this._buffs[i].equipped) {
            result.push(this._buffs[i].getInstanceId());
        }
    }
    return result;
}

// if replace = true and a buff with the same Id already exists, its data will replaced (otherwise always a new tech is added)
BuffManager.prototype.addById = function(techId, techData, replace){

    //this.log('addById:'+techId);
    var game = this._ship.Game;
    var stats = game.Stats;
    var tech = stats.Tech;

    for (var t in tech) {
        if (tech.hasOwnProperty(t)) {
            var titem=tech[t];
            if (techId === titem.id) {
                // check if we have tech with the same id
                var buff = this._buffs.find((b)=>b.config.id === techId);
                if (!buff || !replace) {
                    // add new tech
                    buff = new Buff(titem);
                    this._buffs.push(buff);
                }
                if (techData) {
                    buff.initInstanceData(game, techData);
                }
                // equipped weapon
                if (buff.equipped && buff.config.slot === 0) {
                    // set fire speed, range
                    this._ship.f1s.set(buff.getCooldown());
                    this._ship.damage_range.set(buff.getRange());
                }
                
                // update datalist for sync to client
                this.save();
                
                return buff;
            }
        }
    }
    this.log('addById failed: no tech with such id: ' + techId);
    return null;
}

// for sending to client - includes instance data and config
BuffManager.prototype.toJSON = function(){

    var a = [];
    for (var i=0;i<this._buffs.length;i++){
        a.push(this._buffs[i].toJSON());
    }

    return JSON.stringify(a);

}

// for saving to playfab - includes instance data only!
BuffManager.prototype.toPlayfabData = function(){

    var a = [];
    for (var i=0;i<this._buffs.length;i++){
        a.push(this._buffs[i].toPlayfabData());
    }

    return a;
}

BuffManager.prototype.findByInstanceId = function(instanceId) {
    for (var s = 0; s < this._buffs.length; s++) {
        if (this._buffs[s].getInstanceId() === instanceId) {
            return this._buffs[s];
        }
    }
    this.log('buff not found instanceId: '+instanceId);
    return null;
}

BuffManager.prototype.equipTech = function(tech, noSave) {
    var fc = tech.config;

    // anything equipped in this slot already unequip it
    for (var s = 0; s < this._buffs.length; s++) {
        var b = this._buffs[s];
        var bc = b.config;
        if (b.equipped && bc.slot === fc.slot){
            this.unequip(b.getInstanceId());
        }
    }

    tech.equipped = true;
    if (fc.slot === 0) {
        // if equipped weapon, set fire speed, range
        //this.log("setting f1s to " + found.getCooldown());
        this._ship.f1s.set(tech.getCooldown());
        this._ship.damage_range.set(tech.getRange());
    }
    if (!this._ship._AI.active) {
        //this.log('buff equipped: ' + tech);
    }
    if (!noSave) {
        this.save();
    }
}


BuffManager.prototype.equip = function(instanceId, noSave){
    var found = this.findByInstanceId(instanceId);
    if (found) {
        this.equipTech(found, noSave);
    }
}

BuffManager.prototype.equipTechs = function(list) {
    for (var i = 0; i < list.length; i++) {
        this.equip(list[i], true);
    }
    this.save();
}

BuffManager.prototype.unequipTech = function(tech, noSave) {
    if (tech.equipped) {
        var bc = tech.config;
        tech.equipped = false;
        tech.active = false;
        // if unequipped weapon, set fire speed, range
        if (bc.slot === 0) {
            this._ship.f1s.set(0);
            this._ship.damage_range.set(0);
        }
        //this.log('buff unequipped: '+ tech);
        if (!noSave) {
            this.save();
        }
    }
}

BuffManager.prototype.unequip = function(instanceId, noSave) {
    var found = this.findByInstanceId(instanceId);
    if (found) {
        this.unequipTech(found, noSave);
    }
}

BuffManager.prototype.unequipAll = function(noSave) {
    for (var i = 0; i < this._buffs.length; i++) {
        this.unequipTech(this._buffs[i], true);
    }
    if (!noSave) {
        this.save();
    }
}

BuffManager.prototype.sell = function(instanceId) {
    var buff = this.findByInstanceId(instanceId);
    if (buff) {
        var accountData = this._ship._view.playerAccountData;
        //this.log('tp: ' + accountData.techpoints.get());
        var selltp = buff.getSellTechPoints();
        //this.log('buff sellTechPoints: ' + selltp);
        var playfab = this._ship.Game.PLAYFAB;
        var pfid = this._ship._view._pfid;
        playfab.revokeItem(pfid, instanceId, ()=>{
            this._buffs.splice(this._buffs.indexOf(buff), 1);
            if (selltp > 0) {
                playfab.addCredits(pfid, playfab.CURRENCY.TECHPOINTS, selltp);
                accountData.techpoints.set(accountData.techpoints.get() + selltp);
            } 
            //this.log('buff sold '+ buff);
            this.save();
        });
    }
}

BuffManager.prototype.upgrade = function(instanceId) {
    var b = this.findByInstanceId(instanceId);
    if (b) {
        var bc = b.config;
        var current = b.level;
        if (b.isFullyUpgraded()) {
            this.log('buff upgrade failed already max level: '+ b);
            return;
        }
        var bank = parseInt(this._ship._view.playerAccountData.bank.get());
        //this.log('bank: '+bank);
        var cost = bc.upgradeCosts[current];
        var givesBonus = bc.upgradeGivesBonus[current];
        //this.log('buff upgrade cost '+cost);
        if (cost > bank){
            //this.log('buff upgrade failed insufficient credits: '+ b + ' '+ cost + ' required');
            return;
        }
        this._ship.SpendCredits('upgradebuff', cost, ()=>{
            b.level++;
        
            if (b.equipped && (bc.slot === 0)) {
                // if upgraded the equipped weapon, update fire speed, range
                this._ship.f1s.set(b.getCooldown());
                this._ship.damage_range.set(b.getRange());
            }

            if (givesBonus){
                //this.log('rolling bonus');
                this.giveRandomBonus(b);
            }

            //this.log('buff level: '+(b.level+1));
            this.save();
            //this.log('buff upgraded '+ b);
        });
    }
}

BuffManager.prototype.reroll = function(instanceId) {
    var buff = this.findByInstanceId(instanceId);
    if (buff) {
        var rerollBonus = null;
        if (buff._bonuslist.length < 1){
            this.log('buff reroll failed no bonus to reroll: ' + buff);
            return;
        } else {
            rerollBonus = buff._bonuslist[buff._bonuslist.length-1];
        }
        
        var rerollCost = 500;
        
        this._ship.SpendTechPoints('rerollbuff', rerollCost, ()=>{
            buff._bonuslist.splice(buff._bonuslist.length-1,1);
            this.giveRandomBonus(buff);
            this.save();
            //this.log('buff rerolled ' + buff);
        });
    }
}

BuffManager.prototype.giveRandomBonus = function(buff){

    var game = this._ship.Game;
    var bonuses = game.Stats.Bonuses;
    
    var count= bonuses.length;
    
    //this.log('bonus count '+count);
    var bonusIndex = Math.floor(Math.random()*count);
    var b = bonuses[bonusIndex];
    
    var rolled = Math.round(Math.random() * 100) / 100;

    var newBonus = new Bonus(b, rolled);
    buff._bonuslist.push(newBonus);

}

BuffManager.prototype.activate = function(index){
    //this.log('activate slot: '+index);
    
    var buff = null;
    for (var s=0;s<this._buffs.length;s++){
        
        var b = this._buffs[s];
        if (b.equipped && (b.config.slot === index)) {
            buff = b;
        }
    }
    if (buff === null) return; // no buff in slot
    
    if (buff.active){
        //this.log('buff already active '+buff);
        return;
    }
    
    if (buff.timeLeft > 0) {
        //this.log('buff cannot be activated - cooldown in progress '+buff);
        return;
    }
    
    buff.active = true;
    buff.timeLeft = buff.getDuration();
    //this.log('buff activated '+buff+" for "+buff.timeLeft+" sec");
    this.save();
}

// dt in seconds
BuffManager.prototype.Process = function (dt) {
    var changed = false;
    for(var s=0; s<this._buffs.length; s++) {
        var buff = this._buffs[s];
        if (buff.timeLeft > 0) {
            buff.timeLeft = Math.max(0, buff.timeLeft - dt);
            if (buff.active && (buff.timeLeft <= 0)) {
                buff.active = false;
                buff.timeLeft = buff.getCooldown();
                //this.log('buff expired '+buff+", cooldown started: "+buff.timeLeft+" sec");
            }
            changed = true;
        }
    }
    if (changed) {
        this.save();
    }
}

// old code: techs could be toggled on-off, subtracting a constant amount from an energy pool while used
//BuffManager.prototype.toggle = function(index){
//
//    this.log('toggle slot: '+index);
//
//    var buff = null;
//    for (var s=0;s<this._buffs.length;s++){
//        var bc = this._buffs[s].config;
//        if (bc.equipped && bc.slot == index){
//            buff = bc;
//        }
//    }
//    if (buff == null) return; // no buff in slot
//
//    // total energy in use
//    var used = 0;
//    for (var s=0;s<this._buffs.length;s++){
//        var bc = this._buffs[s].config;
//        if (bc.active){
//            used+=bc.energyCost;
//        }
//    }
//
//    if (buff.active){
//        buff.active = false;
//        this.log('buff deactivated '+buff.name);
//        this.save();
//        return;
//    }
//
//    var shipName = this._ship.subtype.get();
//    var stats = this._ship.Game.Stats.Ships.find((ship) => ship.data_name === shipName);
//    var energy = 0;
//    if(stats) { 
//        energy = stats.energy;
//    }
//    this.log('ship energy used: '+used + ' / ' +energy);
//
//    var remaining = energy - used;
//
//    if (buff.energyCost <= remaining){
//        buff.active = true;
//        this.log('buff activated '+buff.name);
//        this.save();
//    } else {
//        this.log('insufficient remaining energy to activate '+buff.name);
//    }
//
//}

BuffManager._updateShipStat = function (stat, max_stat, default_max_stat, statBonus) {
    // check if ship max health has changed due to bonus
    var currentMax = parseInt(max_stat.get());
    var defaultMax = parseInt(default_max_stat.get());
    var boostedMax = defaultMax + statBonus;
    if (currentMax !== boostedMax){
        // health changed due to (de)activation of a buff
        var ratio =  boostedMax / currentMax;
        var current = parseInt(stat.get());
        current = Math.ceil(current * ratio);
        currentMax = boostedMax;
        if (current > currentMax) current = currentMax;
        max_stat.set(currentMax);
        stat.set(current);
    }
};

BuffManager.prototype.save = function() {
    
    var damageBonus = 0;
    var speedBonus = 0;
    var healthBonus = 0;
    var turnBonus = 0;
    var resistanceBonus = 0;
    //var fuelBonus = 0;
    var ammoBonus = 0;
    // tech
    for (var s=0;s<this._buffs.length;s++){
        /**@type Buff */
        var buff = this._buffs[s];
        var bc = buff.config;
        if (buff.equipped) {
            if (buff.isWeapon()) {
                damageBonus += buff.getValue();
            } else if (buff.active) {
                var amount = buff.getValue();
                if (bc.effects === 'damage'){
                    damageBonus+=amount;
                }
                if (bc.effects === 'speed'){
                    speedBonus+=amount;
                }
                if (bc.effects === 'health'){
                    healthBonus+=amount;
                }
                if (bc.effects === 'turn'){
                    turnBonus+=amount;
                }
                if (bc.effects === 'shield'){
                    resistanceBonus+=amount;
                }
    //            if (bc.effects === 'fuel'){
    //                fuelBonus+=amount;
    //            }
                if (bc.effects === 'ammo'){
                    ammoBonus+=amount;
                }
            }
            // bonuses
            var bl = buff._bonuslist;
            for (var b=0;b<bl.length;b++){
                var effects = bl[b].config.stat;
                var rolled = bl[b].getValue();
                if (effects === 'damage') {
                    damageBonus+=rolled;
                }
                if (effects === 'speed') {
                    speedBonus+=rolled;
                }
                if (effects === 'health') {
                    healthBonus+=rolled;
                }
                if (effects === 'turn') {
                    turnBonus+=rolled;
                }
                if (effects === 'shield') {
                    resistanceBonus+=rolled;
                }
//                if (effects === 'fuel') {
//                    fuelBonus+=rolled;
//                }
                if (effects === 'ammo') {
                    ammoBonus+=rolled;
                }
            }

        }
    }

    this._totalDamageBonus = damageBonus;
    this._totalSpeedBonus = speedBonus;
    this._totalHealthBonus = healthBonus;
    this._totalTurnBonus = turnBonus;
    this._totalResistanceBonus = resistanceBonus;
    //this._totalFuelBonus = fuelBonus;
    this._totalAmmoBonus = ammoBonus;
    this._ship.damageboost.set(damageBonus);
    this._ship.speedboost.set(speedBonus);
    this._ship.turnboost.set(turnBonus);

    BuffManager._updateShipStat(this._ship.hp, this._ship.max_hp, this._ship.default_max_hp, healthBonus);
    //BuffManager._updateShipStat(this._ship.fuel, this._ship.max_fuel, this._ship.default_max_fuel, fuelBonus);
    BuffManager._updateShipStat(this._ship.ammo, this._ship.max_ammo, this._ship.default_max_ammo, ammoBonus);

    // sent to the client with the update of the ship object
    this._ship.buffs.set(this.toJSON());
}

BuffManager.prototype.damageBonus = function() {
    return this._totalDamageBonus;
}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BuffManager.prototype.log = function(m) { if(this.DEBUG){ console.log("[BuffManager:" + (this._ship && this._ship.id.get()) + "] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = BuffManager;
