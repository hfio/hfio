echo "Stopping server if it is running..."
forever stop staging
echo "Starting server..."
# starts the script and keeps running, logging both to file and to console, to easily see the output when running through docker
forever --uid staging Server.js 2>&1 | tee LOG.txt
