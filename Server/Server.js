//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Flying Turtle SERVER
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//Modules
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var ObjectId = require('mongodb').ObjectID;
var mongoose = require('mongoose');
require('array.prototype.find').shim();
//Custom
var oMongoDB = require('./MongoDB');
var oSocketIO = require('./SocketIO');
var oGame = require('./Custom/Game');
var oStats = require('./Custom/Stats');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oApp() {

    //debugging
    this.DEBUG = true;

    this.Log("****************************************************************************************")
    this.Log("Flying Turtle Server - Developed by Ice Turtle Studios");
    this.Log("****************************************************************************************")
    
    //Modules
    this.DB = new oMongoDB(this);//MongoDB Connection (Persistant Data
    this.NET = new oSocketIO();//Socketio Networking (io)
   
    //server settings
    this.DBHost = 'localhost';
    this.DBName = 'hyperfleet';
    this.Domainallowed = '*';//examples 'http://localhost:8080' 'http://itsdev001.westus.cloudapp.azure.com' 
    this.Port = 80;
    this.HeartBeatTime = 20000;//secs / 1000
    this.HeartBeatTimeSecs = this.HeartBeatTime / 1000;
   
    //Main Engine and Custom Game Objects
    this.Game = new oGame(this);
    
    //Fetch game stats such as ships, level requirements, abilities, etc. from the internet
    this.Game.Stats = new oStats(() => {
        var s = this.Game.Stats;
        this.Log("Google Data Loaded - Setting Globals ");
        this.Log("GLOBAL STATS");
        this.Log(JSON.stringify(s.Global));

        this.Game.DATA.MapEnemyTarget = s.Global.enemies_per_map;
        this.Game.DATA.MapEnemyTime = s.Global.enemies_respawn_per_sec;
        this.Game.DATA.MapFriendTarget = s.Global.wave_friends_per_map;
        this.Game.DATA.killxp = s.Global.xp_for_kill;
        
        this.Game.DATA.base_radius = s.Global.base_radius;
        this.Game.DATA.base_redzone = s.Global.base_redzone;

        this.Game.DATA.winxp = s.Global.credits_for_win;
        this.Game.DATA.losexp = s.Global.credits_for_lose;
        
        this.Game.DATA.AsteroidRespawnTime = s.Global.asteroid_respawn_time;
        this.Game.DATA.asteroidCountPerAreaMin = s.Global.asteroid_count_per_area_min;
        this.Game.DATA.asteroidCountPerAreaMax = s.Global.asteroid_count_per_area_max;
        this.Game.DATA.asteroidRadiusMin = s.Global.asteroid_radius_min;
        this.Game.DATA.asteroidRadiusMax = s.Global.asteroid_radius_max;
        this.Game.DATA.asteroidHealthMin = s.Global.asteroid_health_min;
        this.Game.DATA.asteroidHealthMax = s.Global.asteroid_health_max;
        this.Game.DATA.asteroidDamagePercentList = [
            s.Global.asteroid_damage_percent_minor,
            s.Global.asteroid_damage_percent_major,
            s.Global.asteroid_damage_percent_critical
        ];
        
        //this.Log("SlowRadiusSQ " + s.Global.ai_slowradius_squared)
        this.Game.DATA.AI_slowradiusSQ = s.Global.ai_slowradius_squared;
        
        this.Game.DATA.AI_spawn_ratio = s.Global.ai_spawn_ratio_team1;
        this.Log("AI Spawn Team1 Ratio: " + this.Game.DATA.AI_spawn_ratio);
        
        this.Game.DATA.fleet_limit = s.Global.fleet_limit;
        this.Game.DATA.fleet_credit_share_percent = s.Global.fleet_credit_share_percent;
        this.Game.DATA.fleet_credit_share_range = s.Global.fleet_credit_share_range;
        this.Game.DATA.fleet_credit_share_range_sq = this.Game.DATA.fleet_credit_share_range * this.Game.DATA.fleet_credit_share_range;
        this.Game.DATA.fleet_pvp_radius = s.Global.fleet_pvp_radius;
        this.Log("Fleet limit " + this.Game.DATA.fleet_limit + " share " + this.Game.DATA.fleet_credit_share_percent + "%, radius " + this.Game.DATA.fleet_pvp_radius );
        
        this.Game.DATA.starting_credits = s.Global.starting_credits;
        this.Log("Starting credits: " + this.Game.DATA.starting_credits);
        
        this.Game.DATA.starting_ship = s.Global.starting_ship;
        this.Log("Starting ship: " + this.Game.DATA.starting_ship);
        
        this.Game.DATA.collision_ship_damage = s.Global.collision_ship_damage;
        this.Game.DATA.collision_asteroid_damage = s.Global.collision_asteroid_damage;
        this.Game.DATA.collision_bumpiness = s.Global.collision_bumpiness;
        
        this.Game.DATA.main_station_id = s.Global.main_station_id;
        
        this.Game.DATA.projectile_width = s.Global.projectile_width;
        
        this.Game.DATA.chat_message_duration = s.Global.chat_message_duration;
        
        this.Game.DATA.turret_rotation_rate = s.Global.turret_rotation_rate;
        
        this.Game.DATA.player_spawn_distance_min = s.Global.player_spawn_distance_min || 0;
        this.Game.DATA.player_spawn_distance_max = s.Global.player_spawn_distance_max || 100;
        this.Game.DATA.ai_spawn_distance_min = s.Global.ai_spawn_distance_min || 0;
        
        this.Game.DATA.docked_ai_repair_time = s.Global.docked_ai_repair_time || 1;
        
        this.Game.DATA.wave_restart_time = s.Global.wave_restart_time || 1;
        this.Game.DATA.player_respawn_time = s.Global.player_respawn_time || 1;
        
        this.Game.DATA.show_arrows_ship_count = s.Global.show_arrows_ship_count || 1;
        
        this.Game.DATA.asteroid_spawn_min_distance = s.Global.asteroid_spawn_min_distance || 0;
        
        this.Game.DATA.station_deploy_min_distance = s.Global.station_deploy_min_distance || 0;
        
        //Get Ship Listing
        this.Log("SHIPS");
        for (var sn in s.Ships) {
            if (s.Ships.hasOwnProperty(sn)) {
                var Ship = s.Ships[sn];
                this.Game.DATA.ShipList.push(Ship.data_name);
                this.Game.DATA.SpawnerTeamAlien[Ship.data_name] = Ship.spawn_chance_alien;
                this.Game.DATA.SpawnerTeamTerran[Ship.data_name] = Ship.spawn_chance_terran;
                this.Log(Ship.data_name + " = " + JSON.stringify(this.Game.DATA.SpawnerTeamAlien[Ship.data_name] + " || " + this.Game.DATA.SpawnerTeamTerran[Ship.data_name]));
            }
        }
        this.Log(JSON.stringify(this.Game.DATA.SpawnerTeamAlien))
        this.Log(JSON.stringify(this.Game.DATA.SpawnerTeamTerran))
        this.Log(JSON.stringify(this.Game.DATA.ShipList)) 
        
        //Test DB Connection
        this.DBInit(this.DBReady);
    });

    return this;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oApp.prototype.Log = function(m) { if(this.DEBUG){ console.log("[APP] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oApp.prototype.DBReady = function() {
    this.Log("DB Connected!");
    
    //Valid connection - Inits
    this.Init();
    
    //this.Core.Init();
    this.Game.Init();
    
    //ready to use modules
    this.NET.Init(this.Game);
    this.DB.Init(this.Game);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oApp.prototype.DBFailed = function() {
    this.Log("DB Connection Failed - Local MongoDB Required!");
    process.exit(1);    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oApp.prototype.DBInit = function(onConnectCallback) {
    
    // Use native promises (http://mongoosejs.com/docs/promises.html)
    mongoose.Promise = global.Promise;
    //assert.equal(query.exec().constructor, global.Promise);
    
    var mdb = this;//ref
    var c = 'mongodb://' + this.DBHost + '/' + this.DBName;
    this.db = mongoose.connect(c);
    var dbc = mongoose.connection;
    dbc.on('error', function(){ mdb.DBFailed(); });
    dbc.once('open', function(){ mdb.DBReady(); });

    this.Log("DB Trying: " + c);

}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oApp.prototype.Init = function() {
    var server = this;//ref

    //Networking (Socket.io) and crossdomain setup
    app.use(this.allowCrossDomain);
    io.on('connection', function(socket){ server.NET.Connection(socket); });

    // status route to allow server up test via http request
    app.get('/status', function (req, res) {
      res.send('OK');
    });

    app.get('/pcount', function (req, res) {
        res.send(Object.keys(server.NET.Views).length.toString());
    });
    
    this.Log("-------------------------------------------------------------------------------")
    this.Log('Starting Network Port/Domain Allowed: ' + this.Port + "/" +  this.Domainallowed);
    this.Log("-------------------------------------------------------------------------------")
    http.listen(this.Port, function(){ server.Log("Networking Started"); });
    
    //Hearbeat
    setInterval(this.HeartBeat, this.HeartBeatTime, this);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oApp.prototype.HeartBeat = function(app) 
{
    //var m = "";
    app.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HEARTBEAT ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    //m = "Players [" + C.CountDict(GD.Clients)
    //+ "] pGOs [" + C.CountDict(GD.Objects.Player)
    //+ "] Units [" + C.CountDict(GD.Objects.Unit)
    //+ "] Projectiles [" + C.CountDict(GD.Objects.Projectile)
    //+ "] Statics [" + C.CountDict(GD.Objects.Static) + "]"
    //Log(m);
//    m = "Loops(approx): " + Math.floor(app.Core.GM.GameCounter / app.HeartBeatTimeSecs)
//    + " Nets(approx): " + Math.floor(app.Core.GM.GameNetCounter / app.HeartBeatTimeSecs)
//    + " ProcDelta: " + app.Core.GM.Process_Delta + " NetDelta: " + app.Core.GM.NetUpdate_Delta
    //app.Log(m);
    //GD.Core.AO.CountLog();
    //app.Log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    
    //reset timers
    app.Game.Core.GameCounter = 0;
    app.Game.Core.NetCounter = 0;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oApp.prototype.allowCrossDomain = function(req, res, next) {//Allow CrossDomain communication or lock down
    res.header('Access-Control-Allow-Origin', this.Domainallowed);
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var Server = new oApp();
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************

