//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    DataList - tracks dirty changes on key/values, used for networking only want changes
    
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oDataList(kv)
{
    //debugging
    this.DEBUG = true;
    
    var i,f;
    var ODL = this;
    
	//Internal Data - dont make a field named this!!
	this._idx_ = {
		key: [],//keys
		val: [],//values
		dirty: [],//dirty (1)true or (0)false
		map: {}//key to index
	}
    
    //parse key/values out from list
	for(i = 0; i < kv.length; i+=2) {
        this._idx_.key.push( kv[i] );
        this._idx_.val.push( kv[i+1] );
        this._idx_.dirty.push(0);
        this._idx_.map[ kv[i] ] = i / 2;
        f = {
            d: this,
            i: i / 2,
            name: kv[i],
            set: function(v){ this.d.iset(this.i, v); },
            get: function(){ return this.d.iget(this.i); }
        };
        this[ kv[i] ] = f; //function(v){ if(v != null) { ODL } } //function(v){ ODL.iset(this.index, v); this.name }
	}	
}
const SOME_VALUE = "Your string";
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oDataList.prototype.Log = function(m) { if(this.DEBUG){ console.log("[DATALIST] " + m); }}
oDataList.prototype.getkeys = function(){ return this._idx_.key; }
oDataList.prototype.set = function(k,v){ var i = this._idx_.map[k]; this.iset(i,v); return v; }//Set value and mark dirty flag
oDataList.prototype.get = function(k){ return this._idx_.val[ this._idx_.map[k] ]; }
oDataList.prototype.iset = function(i,v){ this._idx_.val[i] = v; this._idx_.dirty[i] = 1; }//Set value and mark dirty flag
oDataList.prototype.iget = function(i){ return this._idx_.val[i]; }
oDataList.prototype.getClean = function(i){ this._idx_.dirty[i] = 0; return this._idx_.val[i]; }//clear dirty flag
oDataList.prototype.getall = function(i){ return this._idx_.val; }
oDataList.prototype.getallClean = function(i){ this.CleanAll(); return this._idx_.val; }
oDataList.prototype.CleanAll = function(){ for(var i = 0; i < this._idx_.key.length; i++) { this._idx_.dirty[i] = 0; } }
oDataList.prototype.DirtyAll = function(){ for(var i = 0; i < this._idx_.key.length; i++) { this._idx_.dirty[i] = 1; } }
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oDataList.prototype.GetDirtyWithKeysAndClean = function(){
	var out = [];
    for(var i = 0; i < this._idx_.key.length; i++) {
        if (this._idx_.dirty[i] == 1) {
            out.push( this._idx_.key[i] );//key first
            out.push( this.getClean(i) );//value next
        }
	}
    return out;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oDataList.prototype.GetDirty = function(){
	var out = [];
    for(var i = 0; i < this._idx_.key.length; i++) {
        if (this._idx_.dirty[i] == 1) {
            out.push(i);//index first
            out.push( this.iget(i) );//value next
        }
	}
    return out;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oDataList.prototype.GetDirtyAndClean = function(){
	var out = [];
    for(var i = 0; i < this._idx_.key.length; i++) {
        if (this._idx_.dirty[i] == 1) {
            out.push(i);//index first
            out.push( this.getClean(i) );//value next
        }
	}
    return out;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//TESTING
function TEST(args) {
    var o = new oDataList(
        [
            "x", 100,
            "y", 50,
            "z", 70,
        ]
    );
    
    o.Log("TEST ---------------------------");
    //slower for lookup
    //o.iset(0, 500);
    //C.Log(o.iget(0));
    //C.Log("x = " + o.get("x"));
    //o.set("z", 300);
    
	//faster to set/get (no hash lookup)
    o.Log("y = " + o.y.get());
    this.x = o.x;
    var x = o.x;
    var y = o.y;
    var z = o.z;
	this.x.set(700);
	y.set(999);
	z.set(9);
	o.Log("y = " + y.get());
    o.Log("y = " + y);
	
    o.Log(JSON.stringify(o._idx_))
    o.Log("dirty")
    o.Log(JSON.stringify(o.GetDirty()))
    o.Log("dirty then clean")
    o.Log(JSON.stringify(o.GetDirtyAndClean()))
    o.Log("dirty")
    o.Log(JSON.stringify(o.GetDirty()))
    o.Log("END TEST ---------------------------");

}
//TEST();
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oDataList;
