//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Server View (See All) - Sync out Client Limited Views

    mostly general code, but has some game (Hyper Fleet) specific parts (e.g. saving data on disconnect, checking
    for ships teams when deciding whether to send object to clients)
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var oDatalist = require('./DataList');
function oView(Game, socket)
{
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;
    this.socket = socket;
    this.NETID = Game.Core.GetNextID();
    socket.VIEW = this;//ref
    
    //view rectangle
    this.x=0; this.y=0;
    this.w = Game.DATA.CoreData.ClientViewWH; this.h = Game.DATA.CoreData.ClientViewWH;//adjust as needed
    this.mw = this.w/2; this.mh = this.h / 2;
    this.x1 = 0; this.y1 = 0; this.x2 = 0; this.y2 = 0;
    this.UpdateBounds();
    
    //Scoped Objects
    this.NewObjects = {};//id = Object - that is new to view
    this.UpdatedObjects = {};//id = Object - exists in view but needs updates sent out
    this.DeletedObjects = {};//id = Object - needs to be deleted from view
    this.OutOfScopeObjects = {};//id = Object - needs to be deleted from view, because it is out of scope
    
    //My Controlled Object
    this.GO = null;

    // Player account data
    var playerAccountDataDefaults = [
        "username", "",
        "xp", 0,
        "kills", 0,
        "deaths", 0,
        "credits", 0,
        "techpoints", 0,
        "bank", 0,
        "shipName", "_default",
        "activeMission", 0, // id of current mission
        "missionProgress", 0, // progress counter of current mission
        "missions", "[]", // user-specific data for missions (JSON) - i.e. timestamps when missions were last started
        "baseID", "", // base the player last docked at
        "ownedshipslist", "[]"
    ];
    this.playerAccountData = new oDatalist(playerAccountDataDefaults);
    // send all initially
    this.playerAccountData.DirtyAll();

    //custom setup
    this.Game.NewView(this);
    
    //currently resetting the view (short time for cleanup of networked objects)
    this.isResetting = false;
    
    // when the player ship is destroyed, it is lost, but the camera stays at the scene of the explosion for some time, and returns to the
    // station to equip a new ship after some time, this is what we track with the respawn variables
    this.RespawnTime = 0; // time elapsed since destroyed, in seconds
    this.RespawnTimeSent = 0;
    this.Respawned = false; 
    
    //New, Update, Delete Objects
    
    //SCOPE OBJECTS to Client Limited VIEW
    
    //SEND CHANGES ONLY to Connected Clients

    //Only SEND CHANGES / SCOPED
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// 4K definition
oView.MAX_SCOPE_WIDTH = 3840;
oView.MAX_SCOPE_HEIGHT = 2160;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.Log = function(m) { if(this.DEBUG){ console.log("[VIEW] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.UpdateWH = function(w, h) { 
    this.w = Math.min(oView.MAX_SCOPE_WIDTH, w); 
    this.h = Math.min(oView.MAX_SCOPE_HEIGHT, h); 
    this.mw = this.w/2; 
    this.mh = this.h / 2; 
    this.UpdateBounds(); 
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.inBounds = function(tx1, ty1, tx2, ty2) {
    if ((this.x1 <= tx2 && this.x2 >= tx1) && (this.y1 <= ty2 && this.y2 >= ty1)) { return true; }
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.UpdateBounds = function() {
    this.x1 = Math.floor(this.x - this.mw);
    this.y1 = Math.floor(this.y - this.mh);
    this.x2 = Math.floor(this.x + this.mw);
    this.y2 = Math.floor(this.y + this.mh);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.MoveView = function(x, y) {
    this.x = x; this.y = y;
    this.UpdateBounds();    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.ResetView = function() {
    this.NewObjects = {};
    this.UpdatedObjects = {};
    this.DeletedObjects = {};
    this.OutOfScopeObjects = {};
}
//*********************************************************************************************************************************************
// When an object is to be deleted from the view, this data needs to be sent to the client (id is the id of the go game object)
//*********************************************************************************************************************************************
oView.prototype._GetDeleteData = function(id, go, creditsAwarded) {
    return (creditsAwarded !== undefined) ? 
        [parseInt(id), go.type.get(), go.hp.get() > 0 ? 1 : 0, go._AI.active == true ? 1 : 0, creditsAwarded] :
        [parseInt(id), go.type.get(), go.hp.get() > 0 ? 1 : 0, go._AI.active == true ? 1 : 0];
}
//*********************************************************************************************************************************************
// Delete all existing (notify)
//*********************************************************************************************************************************************
oView.prototype.FlushView = function() {
    var O, da, id;
    var n = [];
    var u = [];
    var d = [];
    var oos = [];
    
    // add all to deletes
    
    //Changes
    for (id in this.UpdatedObjects) {
        if (this.UpdatedObjects.hasOwnProperty(id)) {
            O = this.UpdatedObjects[id];
            d.push(this._GetDeleteData(id, O));
        }
    }
    //New
    for (id in this.NewObjects) {
        if (this.NewObjects.hasOwnProperty(id)) {
            O = this.NewObjects[id];
            d.push(this._GetDeleteData(id, O));
        }
    }
    //Delete
    for (id in this.DeletedObjects) {
        if (this.DeletedObjects.hasOwnProperty(id)) {
            O = this.DeletedObjects[id];
            d.push(this._GetDeleteData(id, O));
        }
    }
    //Out of scope
    for (id in this.OutOfScopeObjects) {
        if (this.OutOfScopeObjects.hasOwnProperty(id)) {
            O = this.OutOfScopeObjects[id];
            d.push(this._GetDeleteData(id, O));
        }
    }

    //this.Log("FLUSH View " + this.socket.id);
    da = [n,u,d,oos,true];//[[...],[...],[...],[...],true] -> final true marks it's flush
    this.Game.APP.NET.Send(this.socket, 1, "view", da);//result, cmd, data
    
    //Clear all
    this.ResetView()
    
}
//*********************************************************************************************************************************************
//Validate if this object is still in my view bounds
//*********************************************************************************************************************************************
oView.prototype.ScopeCheck = function(CObj) {
    
    var id = CObj.id.get();
    
    //if flagged for removal - straight to delete if we currently see it
    if(CObj.Obj._remove == true)
    {
        if (this.NewObjects.hasOwnProperty(id) == true) {
            this.DeletedObjects[id] = CObj;
            delete this.NewObjects[id];
        }
        if (this.UpdatedObjects.hasOwnProperty(id) == true) {
            this.DeletedObjects[id] = CObj;
            delete this.UpdatedObjects[id];
        }
        return;
    }
       
    var w = CObj.w.get() * 0.5;
    var h = CObj.h.get() * 0.5;
    var team = this.GO ? this.GO.team.get() : 0;
    //in our current view (with some leeway (half the object size), so that objects which have visual parts outside of their set size are not cut off at the screen edges)
    // always transfer objects that were marked for that (i.e. main station, last remaining enemy ships)
    // always transfer ships that are on the same team (so they will show up in the team list in the top right)
    if ((CObj.alwaysTransfer === true) || (CObj.team.get() === team) || (this.inBounds(CObj.Obj._x1 - w, CObj.Obj._y1 - h, CObj.Obj._x2 + w, CObj.Obj._y2 + h) === true)) {
        //if in new or update list we are good
        //but if not we need to see it as NEW
        if (this.NewObjects.hasOwnProperty(id) == false && this.UpdatedObjects.hasOwnProperty(id) == false ) {
            this.NewObjects[id] = CObj;
        }
    }
    else
    {
        //if in new or update list we need to delete it
        if (this.NewObjects.hasOwnProperty(id) == true) {
            this.OutOfScopeObjects[id] = CObj;
            delete this.NewObjects[id];
        }
        if (this.UpdatedObjects.hasOwnProperty(id) == true) {
            this.OutOfScopeObjects[id] = CObj;
            delete this.UpdatedObjects[id];
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.SendView = function() {
    
    var O, da, id;
    var n = [];
    var u = [];
    var d = [];
    var oos = [];
    
    //Changes - dirty data
    for (id in this.UpdatedObjects) {
        if (this.UpdatedObjects.hasOwnProperty(id)) {
            //use datalist on custom objects (game object)
            O = this.UpdatedObjects[id];
            da = O.Obj._dl.GetDirty();//[index, value, index, value, ...]
            //u.push([id, da]);//[[id,data], [id,data], ...]
            
            if (da.length > 0) {
                //this.Log(JSON.stringify(O.Obj._dl.GetDirty()))
                //this.Log(JSON.stringify(O.Obj._dl._idx_.dirty))
                u.push([parseInt(id), O.type.get(), da]);//[[id,type,data], [id,data], ...]    
            }
            
        }
    }
    //New - send all data then add to Update for next time
    for (id in this.NewObjects) {
        if (this.NewObjects.hasOwnProperty(id)) {
            //use datalist on custom objects (game object)
            O = this.NewObjects[id];
            da = O.Obj._dl.getall();//[..] values only list, client will unmap
            n.push(da);//[[data], [data], ...] - has id and type in it
            
            //move
            this.UpdatedObjects[id] = O;
            delete this.NewObjects[id];
        }
    }
    //Delete
    for (id in this.DeletedObjects) {
        if (this.DeletedObjects.hasOwnProperty(id)) {
            O = this.DeletedObjects[id];
            var creditsAwarded = (O._creditsAwardedTo === this.GO) ? O._creditsAwarded : 0;
            d.push(this._GetDeleteData(id, O, creditsAwarded));
        }
    }
    this.DeletedObjects = {}//clear after send
    //Out of scope
    for (id in this.OutOfScopeObjects) {
        if (this.OutOfScopeObjects.hasOwnProperty(id)) {
            O = this.OutOfScopeObjects[id];
            var creditsAwarded = (O._creditsAwardedTo === this.GO) ? O._creditsAwarded : 0;
            oos.push(this._GetDeleteData(id, O, creditsAwarded));
        }
    }
    this.OutOfScopeObjects = {}//clear after send
        
    //this.Log("Sending Network View " + this.socket.id);
    da = [n,u,d,oos];//[[...],[...],[...],[...]]
    this.Game.APP.NET.Send(this.socket, 1, "view", da);//result, cmd, data

    // send changed playerAccountData if any
    var dirtyData = this.playerAccountData.GetDirtyWithKeysAndClean();
    if (dirtyData.length > 0) {
        this.Game.APP.NET.Send(this.socket, 1, "account", dirtyData);
    }

    //Update to my GO Position
    if (this.GO && !this.GO.Obj._remove) {
        this.MoveView(this.GO.x.get(), this.GO.y.get());
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.Message = function(d) {
    this.Game.API.Call(this.socket, d);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.Connect = function() {
    //this.Log('Connect: ' + this.socket.id);
    // sending game state
    this.Game.APP.NET.Send(this.socket, 1, 'wave', this.Game.GetWaveData());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oView.prototype.Disconnect = function() {
    var id = this.GO.id.get(), self = this;
    this.Log('Disconnect: ' + id + " (players: " + this.Game.APP.NET.ViewCount + ")");
    
    if (!this.GO.Obj._remove) {
        // update stored ship state
        this.Game.LOGIC.UpdateShipState(this.socket, {save: true});
    }
    
    this.GO.Obj._remove = true;
    
    // save tech and stations state if logged in to playfab
    if (this._pfid) {
    
        // save tech state
        var techData = this.GO.buffManager.toPlayfabData();

        for (var i = 0; i < techData.length; i++) {
            this.Game.PLAYFAB.updateItemData(this._pfid, techData[i].ItemInstanceId, techData[i].CustomData, function(){
                //self.Log("Saved owned tech state: " + JSON.stringify(techData[i]));
            });
        }

        // save station (droppable turret) state
        var stationData = this.GO.stationManager.toPlayfabData();

        for (var i = 0; i < stationData.length; i++) {
            this.Game.PLAYFAB.updateItemData(this._pfid, stationData[i].ItemInstanceId, stationData[i].CustomData, function(){
                //self.Log("Saved owned station state: " + JSON.stringify(stationData[i]));
            });
        }
    
    }
    
    // make sure it is not respawned
    // ships are no longer respawned, player needs to buy them again
//    if (this.Game.DATA.DeadPlayers[id]) {
//        delete this.Game.DATA.DeadPlayers[id];
//    }
    this.GO._view = null;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oView;
