//*********************************************************************************************************************************************
//Engine Core Manages a Local or Remote game (client or server)
// Minimal game engine
//  Data (GD)
//  Objects
//  Timing
//  Simulation / Processing
//  Math2D
//  Collision
//  Movement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var oTimer = require('./Core/Timer');
var oMath2D = require('./Core/Math2D');
var oCollision = require('./Core/Collision');
var oMovement = require('./Core/Movement');
var oArea = require('./Core/Area');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oCore(Game, d) {

    //debugging
    this.DEBUG = true;
    
    //Game
    this.Game = Game;
    
    //Modules
    this.TIME = new oTimer(this);
    this.MATH = new oMath2D(this);
    this.CD = new oCollision(this);
    this.MV = new oMovement(this);
    this.AREA = new oArea(this, d.MapX, d.MapY, d.MapSize);
    
    this.Name = "CORE";
    
    //Setup Data
    this.data = d;//any data
    this.goTypes = d.goTypes;//types list [...]
    this.Objects = {};//object tracking by group (ordered by list)
    for(var i = 0; i < d.goTypes.length; i++)
    {
        this.Objects[i] = {};//new group
    }
    this.ObjectsRemovals = {};//object tracking by group (ordered by list)
    for(i = 0; i < d.goTypes.length; i++)
    {
        this.ObjectsRemovals[i] = {};//new group
    }
    
    //Internal Data
    this.LoopTime = d.LT;//1000/30;//30 times a sec (simulation)
    this.LoopCounter = 0;//for update loops counting
    this.NextID = 0;//ids for objects (incremented)
    this.Process_Delta = 0;//time tracking
    this.NextID = 0;//ids for objects (incremented)
    this.NextTEAM = 1;//teams 1-2
    this.NextTEAMGROUP = 11;//continuous (11+)
    this.NextTEAM_MAX = 2;//teams max
    
    //Networking if used
    this.NetUpdateTime = d.NT;// times a sec (network updates)
    this.NetCounter = 0;//for Net update loops ran since heartbeat
    this.NetUpdate_Delta = 0;
    this.NetClientNextUpdate = 0;
    
    //debug info
    this.Log("Object Types: " + this.JSON(d.goTypes));
    this.Log("Objects: " + this.JSON(this.Objects));
    this.Log("LoopTime: " + (d.LT / 100).toFixed(2) + " per sec");
    this.Log("NetworkTime: " + d.NT + " per sec");
    //this.Log("CP: " + CP);
    
    return this;
}
//*********************************************************************************************************************************************
//Helpers
//*********************************************************************************************************************************************
oCore.prototype.Log = function(m) { if(this.DEBUG){ console.log("[CORE] " + m); }};
oCore.prototype.JSON = function(d) { return JSON.stringify(d); };
oCore.prototype.Copy = function(d) { return JSON.parse(JSON.stringify(d)); };//simple copy
oCore.prototype.CountDict = function(d) { return Object.keys(d).length; };
oCore.prototype.DictKeys = function(d) { return Object.keys(d); };
//oCore.prototype.ObjectCreate = function() { this.Objects[GO.type][GO.id] = GO; }
oCore.prototype.ObjectAdd = function(GO) { this.Objects[GO.type.get()][GO.id.get()] = GO; };
oCore.prototype.GetNextID = function() { this.NextID++; return this.NextID; };
oCore.prototype.GetNextTeam = function() { var t = this.NextTEAM; this.NextTEAM++; if(this.NextTEAM > this.NextTEAM_MAX){this.NextTEAM=1;} return t; };
oCore.prototype.GetNextTeamGroup = function() { var t = this.NextTEAMGROUP; this.NextTEAMGROUP++; return t; };
oCore.prototype.GetGroup = function(g){ return this.Objects[g]; };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.GetObject = function(objectTypeName, objectId) { 
    var group = this.GetGroup(this.goTypes.indexOf(objectTypeName));
    return group[objectId];
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.ExecuteForObjects = function(objectTypeName, callback) { 
    var group = this.GetGroup(this.goTypes.indexOf(objectTypeName));
    var keys = Object.keys(group);
    for (var i = 0; i < keys.length; i++) {
        callback(group[keys[i]]);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.Init = function() {
    this.Log("Init");
    
    setInterval(this.Loop, this.LoopTime, this);
    return this;
};
//*********************************************************************************************************************************************
//Run Main Loop - manage simulation
//*********************************************************************************************************************************************
oCore.prototype.Loop = function(C) {
    C.GameCounter++; 
    C.TIME.UpdateTimer();
    C.TIME.TrackStart();//track time start
    C.Process(C.TIME.Delta);
    C.Process_Delta = C.TIME.TrackEnd();//track time end
    C.CheckNetUpdate(C);
};
//*********************************************************************************************************************************************
//Check if we need to send a network update to players
//*********************************************************************************************************************************************
oCore.prototype.CheckNetUpdate = function(C) {
    C.NetClientNextUpdate += C.TIME.Delta;
    if(C.NetClientNextUpdate >= C.NetUpdateTime){ C.RunNetUpdate(C); }
};
//*********************************************************************************************************************************************
//Yes send network update!
//*********************************************************************************************************************************************
oCore.prototype.RunNetUpdate = function(C) {
    C.TIME.TrackStart();//track time start
    C.NetUpdate();
    C.NetUpdate_Delta = C.TIME.TrackEnd();//track time end
    C.NetClientNextUpdate = 0; C.NetCounter++;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.Process = function(dt) 
{
    var id, objs;
    
    //this.Log(this.JSON(this.DictKeys(this.Objects)));
    
    //process all groups and objects
    for(var i = 0; i < this.goTypes.length; i++)
    {
        objs = this.Objects[i];//keys are numbers
        for (id in objs) {
            if (objs.hasOwnProperty(id)) {
                O = objs[id];
                O.Process(dt);
                
                //flag for removal if needed
                if (O.Obj._remove === true) {
                    this.ObjectsRemovals[i][id] = O;
                }
                
                //update scoping to client views
                if (this.Game.Server === true) {
                    this.Game.ScopeProcess(O);
                }
            }
        }
    }
    
    //Custom Processing
    if (this.Game) {
        this.Game.Process(dt);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.CleanProcess = function() 
{
    var id, objs;
    
    //this.Log(this.JSON(this.DictKeys(this.Objects)));
    
    //process all groups and objects
    for(var i = 0; i < this.goTypes.length; i++)
    {
        objs = this.Objects[i];//keys are numbers
        for (id in objs) {
            if (objs.hasOwnProperty(id)) {
                objs[id].Obj.new.set(false); // mark all objects as no longer newly created
                objs[id].Obj._dl.CleanAll();
            }
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.NetUpdate = function() 
{
    var id, objs;
    //this.Log("NetUpdate");
    
    //Network Processing (Server Only)
    if (this.Game.Server === true) {
        this.Game.NetProcess();
    }
    
    //Remove Dead Objects (Only after all have synced out)
    //this.Log("Deads")
    for(var i = 0; i < this.goTypes.length; i++)
    {
        objs = this.ObjectsRemovals[i];//keys are numbers
        for (id in objs) {
            if (objs.hasOwnProperty(id)) {
                delete this.Objects[i][id];
                //this.Log("Removed: [" + this.goTypes[i] + "] " + id);
            }
        }
        //clear it now
        this.ObjectsRemovals[i] = {};
    }
    
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oCore;
