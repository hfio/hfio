echo "Stopping server if it is running..."
forever stop staging
echo "Starting server..."
# starts the script, redirects all logging to file, exits script
forever --uid staging Server.js > LOG.txt 2>&1 &
sleep 5
