//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    MongoDB - Persistant Data / CRUD
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
var ObjectId = require('mongodb').ObjectID;
var mongoose = require('mongoose');
//Custom
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oMongoDB(App) {
    //debugging
    this.DEBUG = true;
    
    this.APP = App;

    this.Config = {};
    this.Model = {};
    return this;    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMongoDB.prototype.Log = function(m) { if(this.DEBUG){ console.log("[MONGODB] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMongoDB.prototype.Init = function(Game) {
    this.Log("Init!")
    
    this.Game = Game;
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMongoDB.prototype.MakeSchema = function(name) {
    //Creates standard document - Name, Properties
    this.Config[name] = this.APP.db.Schema( { name: { type:String, required: true }, Properties: this.APP.db.Schema.Types.Mixed} );
    this.Model[name] = mongoose.model(name, this.Config[name]);
    this.Log("CreatedSchema: " + name);
    
    var mdb = this;
    this.List(name, function(status, d){
        //mdb.Log(status + JSON.stringify(d));
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMongoDB.prototype.List = function(ModelName, callback) {
    var model = this.Model[ModelName];
    model.find({}, function(err, d) {
        if (err) { callback(0, 'ERROR'); return; }
        callback(1, d);
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMongoDB.prototype.CreateUpdate = function(ModelName, iname, properties, callback) {
    var model = this.Model[ModelName];
    var mdb = this;
    //save it (must be unique by owner/name)
    model.findOne({ Name: iname }, function(err, d) {
        if (err) {
            callback(0, 'Invalid Data');
            return;
        }
        else {
            //L.Log(record);
            if (d != null) {
                //update and overwrite it
                L.Log('DB Updating : ' + iname);
                d.Properties  = properties;
                d.save(function(err) {
                    if (err) { callback(0, 'Could NOT save'); return; }
                    else { callback(1,'OK'); }
                });
            }
            else
            {
                L.Log('DB Creating : ' + iname);
                var obj = new model({ Name: iname, Properties: properties});
                obj.save(function(err) {
                    if (err) { callback(0, 'Could NOT save'); return; }
                    else { callback(1,'OK'); }
                });
            }
        }
    });
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMongoDB.prototype.Read = function(ModelName, iname, callback) {
    var model = this.Model[ModelName];
    model.findOne({ name: iname }, function(err, d) {
        if (err) { callback(0, "Invalid Data"); return; }
        if (d == null) { callback(0, 'no db object exists'); return; }
        
        //Return
        callback(1, d);
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMongoDB.prototype.Delete = function(ModelName, iname, callback ) {
    var model = this.Model[ModelName];
    model.findOneAndRemove({Name: iname}, function(err){
        if (err) {
            callback(0, "Invalid Data");
            return;
        }
        callback(1, 'OK');
    });
   
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
module.exports = oMongoDB;
