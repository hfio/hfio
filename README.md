Build process (client)
======================

All client source files (code, assets) are in the `Client/src` folder. Development related files (scripts, metadata such as package.json etc)
are outside of this folder, directly in `Client`.

The build process requires **npm** and **java** to be installed, and uses **gulp**. There is a [docker image](https://hub.docker.com/r/nkrisztian89/java-npm-gulp/) 
on Docker hub that has the prerequisites installed so that it can be used in the client update Buddy pipelines ([staging](https://app.buddy.works/dylankjones/hfio/pipelines/pipeline/51687/actions)
and [production](https://app.buddy.works/dylankjones/hfio/pipelines/pipeline/49339/actions))
to automatically run the build process after each push, before uploading the resulting files to hyperfleet.io ("Terminal" action type).
The dockerfile used to create this image can be found in the `Client/Buddy` folder.

After running the build process, the production ready game files will be in the `Client/dist` folder, so the `index.php` needs to be opened 
from there. (and that folder needs to be copied over to the server in the build pipeline)

The build process **differs for staging (development) and production**. The production build process does not create source maps for the 
compiled code and has the debug functions disabled (the GLOBAL_DEBUG variable is set to false through a Closure define). It doesn't include
the GUI for the debug functions either.


Gulp tasks
----------

The gulp tasks related to the build process:

```
gulp build
gulp build-prod
```

Process all files in the `src` folder (minifying HTML/CSS, compiling js files with **Closure**, optimizing images etc), and output the
resulting files to the `dist` folder (within `Client`)
The `build` task is for staging (development), and the `build-prod` task is for production.

```
gulp clean
```

Deletes the `dist` folder.

```
gulp watch
```

Starts watching for changes in the subfolders of `src/` folder, and runs the appropriate part of the staging (development) build process again if a file changes.

Shell scripts
-------------

There are two shell scrips in the `Client` folder to make life easier:

- `setup_build.sh` installs gulp and all dependencies to run the gulp tasks
- `build.sh` runs the _clean_ and then the _build_ task
- `build-prod.sh` runs the _clean_ and then the _build-prod_ task

Testing the client (frontend) locally
=====================================

Dependencies:

- an HTTP server (e.g. Apache)
- PHP, PHP-curl

Regardless of the method below, the game client will first attempt to connect to the locally running server, then (if that doesn't exist or full) 
the server running in local Docker, and then (if that doesn't exist or full) the global production server.

Running directly
----------------

- install the dependencies
- run the client build process outlined above
- place (or link) the created game client files (`Client/dist` folder) into the serving folder of your HTTP server 
(for example for Apache: `/var/www/html/` on Linux and `/Library/WebServer/Documents` on Mac by default)
- redirect your HTTP server from the default port 80 to another (for example 8080), otherwise later it will clash with the game backend if you want to test that too locally, as that needs port 80
- if you placed/linked the game files in the `hfio` folder within your serving folder, you can access the game at `http://localhost:8080/hfio/?r=99x0` 
(substitute `hfio` for the folder and `8080` for the port you used)

Using Docker
------------

- install Docker
- run the client build process outlined above
- open the command line and go to the `Client` folder
- run `docker_build_client.sh` (you might need `sudo` on linux)
- run `docker_run_client.sh` (you might need `sudo` on linux)
- you can access the game client at `http://localhost:8080/hfio/?r=99x0`
- every time you make a new client build, you need to run `docker_stop_client.sh`, `docker_build_client.sh` and `docker_run_client.sh` again


Testing the server (backend) locally
====================================

Dependencies:

- [npm](https://www.npmjs.com/)
- [forever](https://github.com/foreverjs/forever)
- [mongodb](https://www.mongodb.com/)

Make sure you have both the **80 and 3010 ports free**, as the game server uses these. (you might need to redirect your webserver to use another
port than the default 80, for example for Apache: in `/etc/apache2/ports.conf`, change the port as `Listen 8080`, then go 
to `/etc/apache2/sites-enabled/000-default.conf` and change the first line as
`<VirtualHost *: 8080>`. Now restart: `sudo service apache2 restart`, to use port 8080 instead.)

Running directly
----------------

On linux, you can install the dependencies locally on your machine, go to the `Server` folder, issue `npm install` and then `sudo sh run.sh`.
On the first run, you will see an error message like `error:   Forever cannot find process with id: staging`. This is normal, as the script will
first try to stop the server if it is already running.

Using Docker
------------

- install Docker
- open the command line and go to the project root folder
- run `docker_build_server.sh` (you might need `sudo` on linux)
- make sure your port 80 is free! The game backend needs it, and it cannot be changed!
- run `docker_run_server.sh` (you might need `sudo` on linux)
- if the server is running, you will see its logs in the terminal, and you can check by opening `http://localhost/pcount`, which should display the player count
- every time you make changes to the server code, you need to run `docker_stop_server.sh`, `docker_build_server.sh` and `docker_run_server.sh` again