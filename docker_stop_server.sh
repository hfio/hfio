# stop docker containers running the game backend

docker rm $(docker stop $(docker ps -a -q --filter ancestor=hf-server --format="{{.ID}}"))
