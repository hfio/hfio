<?php
//CRUD Available Hosts Lists
ini_set('display_errors',1);
error_reporting(E_ALL);
//**********************************************************************************************************************************************************
// Configuration
//**********************************************************************************************************************************************************
$APIKey = 'hx25fnEZxh4CqR86nsDvK';
$cmd = '';
//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
function Done($result, $scope, $data)
{
    $r = json_encode([$result, $scope, $data], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE |  JSON_PRETTY_PRINT);
    die($r);
}
//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
if(isset($_POST['cmd'])){ $cmd = $_POST['cmd']; }

//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
// get contents of a file into a string
$filename = "hosts.json";
$handle = fopen($filename, "r");
$HostsFile = fread($handle, filesize($filename));
fclose($handle);
$HostsFile = json_decode($HostsFile, true);
//var_dump();
//var_dump(json_decode($HostsFile, true));
//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
if($cmd == 'read')
{
    //Get Current Counts
    global $HostsFile;
    foreach ($HostsFile as $Index => &$Arr) {
        $ch = curl_init("http://" . $Arr[0] . "/pcount");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,2);//2 seconds max timeout per server
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $c = curl_exec($ch);
        
        // Check HTTP status code
        $output = 1000000;//DEFAULT NOT RESPONDING as WAY OVER MAX (This means instance is down)
        if (!curl_errno($ch)) {
          switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
            case 200:  # OK
              $output = intval($c);
              break;
            default:
                $Arr[3] = 'Error: ' . $http_code;//save error issues
              //die( 'Unexpected HTTP code: ' . $http_code . " " .$Arr[0] . "\n");
          }
        }
        
        curl_close($ch);
        $Arr[1] = $output;
    }
    
    Done(1,$cmd, $HostsFile);
}
//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
if($cmd == 'createupdate')
{
    //Authorize
    if(!isset($_POST['key'])){ Done(0,"none", "no key"); }
    if($_POST['key'] != $APIKey ){ Done(0,"none", "bad key:" . $_POST['key']); }
    if(!isset($_POST['index'])){ Done(0,$cmd, "no index field set"); }
    if(!isset($_POST['host'])){ Done(0,$cmd, "no host set"); }
    if(!isset($_POST['max'])){ Done(0,$cmd, "no max set"); }
    if($_POST['index'] == '' || $_POST['host'] == '' || $_POST['max'] == ''){ Done(0,$cmd, "no index or host or max set"); }

    $Index = $_POST['index'];
    $Host = $_POST['host'];
    $Max = intval($_POST['max']);
    $HostsFile[$Index] = [$Host, 0, $Max, ""];

    $handle = fopen($filename, "w");
    fwrite($handle, json_encode($HostsFile, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    fclose($handle);
    
    Done(1,$cmd, $HostsFile);
    
}
//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
if($cmd == 'delete')
{
    //Authorize
    if(!isset($_POST['key'])){ Done(0,"none", "no key"); }
    if($_POST['key'] != $APIKey ){ Done(0,"none", "bad key:" . $_POST['key']); }
    if(!isset($_POST['index'])){ Done(0,$cmd, "no index field set"); }
    if($_POST['index'] == ''){ Done(0,$cmd, "no index set"); }

    $Index = $_POST['index'];
    
    if (array_key_exists($Index, $HostsFile)) {
        unset($HostsFile[$Index]);
        $handle = fopen($filename, "w");
        fwrite($handle, json_encode($HostsFile, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        fclose($handle);
        Done(1,$cmd, $HostsFile);
    }
    else{
        Done(1,$cmd, 'Index does not exist');
    }
}
//**********************************************************************************************************************************************************
//**********************************************************************************************************************************************************
//nothing requested
//Done(0,"none", "Hello");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body style="padding: 50px;">
    <form action="./hosts.php" method="post">
        <table>
            <tr><td>Command: </td><td>
                <select name="cmd" id="file">
                 <option value="read">Read</option>
                 <option value="createupdate">CreateUpdate</option>
                 <option value="delete">Delete</option>
               </select>
            </td></tr>
            <tr><td>Index: </td><td><input type="text" name="index"></td></tr>
            <tr><td>Host: </td><td><input type="text" name="host"></td></tr>
            <tr><td>MaxPlayers: </td><td><input type="text" name="max" value="100"></td></tr>
            <tr><td>Key: </td><td><input type="text" name="key"></td></tr>
        </table>
        <br>
        <br>
        <input type="submit" value="Submit" name="submit">
    </form>
</body>
</html>