<?php
    $RefID = '-1';
    if(isset($_GET["r"])) {
        $RefID = $_GET["r"];
    }
?>
<!doctype html>
<html lang="us">
<head>
    <meta charset="utf-8">
    <title>HyperFleet v0.1)</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>    
    
    <!-- GameAnalytics -->
    <script>
    (function(w,d,a,m){var s='script';var g='GameAnalytics';w[g]=w[g]||function(){(w[g].q=w[g].q||[]).push(arguments)},a=d.createElement(s),m=d.getElementsByTagName(s)[0];a.async=1;a.src='http://download.gameanalytics.com/js/GameAnalytics-2.1.0.min.js';m.parentNode.insertBefore(a,m)})(window,document);

    GameAnalytics("setEnabledInfoLog", true);
    GameAnalytics("initialize", "b99b52d1b66d0d2b20d943d6e1ef918b", "774ed4e1ec0df4298cc1050d412eaad1a1707b53");
    </script>
    <!-- End GameAnalytics -->

    <!-- frameworks -->
    <link rel="stylesheet" href="framework/bootstrap/bootstrap.min.css">
    <script type="text/javascript" src="framework/jquery/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="framework/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="framework/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="framework/angular/angular.min.js"></script>
    <script type="text/javascript" src="framework/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="framework/jqwidgets/jqx-all.js"></script>
    <script type="text/javascript" src="framework/jqwidgets/jqxangular.js"></script>
    <script type="text/javascript" src="framework/socketio/socket.io.js"></script>
    <script type="text/javascript" src="framework/socketio/socket.io-stream.js"></script>
    <script type="text/javascript" src="framework/shatter.js"></script>
    <script type="text/javascript" src="framework/clipboard.min.js"></script>

    <!-- fetch polyfill (https://github.com/github/fetch) -->
    <script type="text/javascript" src="framework/fetch.js"></script>

    <!-- Playfab -->
    <script type="text/javascript" src="https://download.playfab.com/PlayFabClientApi.js"></script>

    <!-- Pixi Engine -->
    <script id="pixi-script" src="framework/pixi/pixi.js"></script>
    <script type="text/javascript" src="framework/pixi/pixi-particles.js"></script>
    
    <script type="text/javascript" src="framework/PixiTextInput.js"></script>

    <!-- Web Fonts for Pixi -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script type="text/javascript">
    // https://github.com/typekit/webfontloader
    fontsLoaded = false;
    WebFont.load({
        custom: {
            families: ['Noto Sans', 'BlenderPro Medium'],
            urls: ['css/fonts.css', 'http://fonts.googleapis.com/css?family=Noto+Sans']
        },
        active: function() { fontsLoaded = true; splash_ready(); },
        inactive: function() { throw new Error('Font(s) did not load'); }
    });

    </script>

    <!-- font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <!-- add one of the jQWidgets styles -->
    <link rel="stylesheet" href="framework/jqwidgets/styles/jqx.base.css" type="text/css" />

    <!-- Components -->
    <link href="css/tool.css" rel="stylesheet">

    <!-- The custom engine and game javascript source files, found in the js folder, are compiled by Closure, which creates this single final
         JS file as a result, and so the individual source files need not be referenced here. (if a new file is added, Closure will include 
         it automatically in the final file the next time it is built (if needed)) Sourcemaps are also created by Closure for debugging.-->
    <script type="text/javascript" src="js/dist.js?v=<?php echo date('mdHi'); ?>"></script>

    <script>
    var NGE;
    var noCacheTime = performance.now();
    // !! the line below is replaced by the build script to include build timestamp !! - do not modify
    var _buildVersion = "";
    // !! the line above is replaced by the build script to include build timestamp !! - do not modify
    var noCache = function (url) {
        return url + (_buildVersion ? "?v=" + _buildVersion : "?t=" + noCacheTime);
    }    
        
    //Preload images
    var imagesLoaded = false;
    PIXI.loader
        .add(noCache("img/ui/button1.png"))
        .add(noCache("img/ui/black-hexagon.png"))
        .add(noCache("img/ui/energy-bar-angle-fill.png"))
        .add(noCache("img/ui/energy-bolt.png"))
        .add(noCache("img/ui/currency.png"))
        .add(noCache("img/ui/currency-purple.png"))
        .add(noCache("img/ui/dice.png"))
        .add(noCache("img/ui/battery.png"))
        .add(noCache("img/ui/battery-black.png"))
        .add(noCache("img/ui/nine-patch-glow-box.png"))
        .add(noCache("img/ui/text-button-blue-up.png"))
        .add(noCache("img/ui/text-button-blue-down.png"))
        .add(noCache("img/ui/text-button-blue-hover.png"))
        .add(noCache("img/ui/text-button-orange-up.png"))
        .add(noCache("img/ui/text-button-orange-down.png"))
        .add(noCache("img/ui/text-button-orange-hover.png"))
        .add(noCache("img/ui/text-button-green-up.png"))
        .add(noCache("img/ui/text-button-green-down.png"))
        .add(noCache("img/ui/text-button-green-hover.png"))
        .add(noCache("img/ui/text-button-purple-up.png"))
        .add(noCache("img/ui/text-button-purple-down.png"))
        .add(noCache("img/ui/text-button-purple-hover.png"))
        .add(noCache("img/ui/text-button-red-up.png"))
        .add(noCache("img/ui/text-button-red-down.png"))
        .add(noCache("img/ui/text-button-red-hover.png"))
        .add(noCache("img/ui/text-button-grey-up.png"))
        .add(noCache("img/ui/text-button-grey-down.png"))
        .add(noCache("img/ui/text-button-grey-hover.png"))
        .add(noCache("img/ui/timer-icon.png"))
        .add(noCache("img/ui/orange-arrow.png"))
        .add(noCache("img/asteroid.png"))
        .add(noCache("img/asteroid_minor.png"))
        .add(noCache("img/asteroid_major.png"))
        .add(noCache("img/asteroid_critical.png"))
        .add(noCache("img/tileset/explosionAnim.json"))
        .add(noCache("img/effect/ring.png"))
        .load(function() {
            imagesLoaded = true;
            splash_ready();
        });

    var RefIDIn = '<?php echo $RefID; ?>';
    
    function boot() {
        //if(fontsLoaded && imagesLoaded) {
        var userselect = document.getElementById("userselect");
        var index = userselect.selectedIndex;
        var userID = (index > 0) ? userselect.getElementsByTagName("option")[index].textContent : null;
        NGE = new oGame('<?php echo $RefID; ?>', userID);
            //NGE.Init(); //Will Init after server resolve
        //}
    }
    //---------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------
	</script>
    
</head>
<body>
<?php include_once("analyticstracking.php") ?>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '374813232882642',
      xfbml      : false,
      version    : 'v2.9'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<!-- ************************************************************************************************************************* -->
<!-- ************************************************************************************************************************* -->
<div id="Splash" class="gbscr noselect" style="display: none;">
    <div style="margin: 0 auto; text-align: center; padding: 20px;">
        <img src="img/logo.png" style="width: 300px; border: solid 4px #555555;" />
    </div>
    <div style="margin: 0 auto; text-align: center;">
        <h1 style="width:100%; color: white; text-align: center; ">Welcome!</h1>
        <h3 style="width:100%; color: white; text-align: center; ">Select user: <select style="color: black;" id="userselect"><option>new user</option></select></h3>
        <h3 style="width:100%; color: white; text-align: center; "><button id="clearusers">Clear user list</button></h3>
        <h1 style="width:100%; color: white; text-align: center; "><button id="splashready">Play</button></h1>
        <h1 style="width:100%; color: white; text-align: center; " id="RefIDInput">(No Referral Link)</h1>
        
    </div>
</div>
<div id="Loading" class="gbscr noselect">
    <div style="margin: 0 auto; text-align: center; padding: 20px;">
        <img src="img/logo.png" style="width: 300px; border: solid 4px #555555;" />
    </div>
    <div style="margin: 0 auto; text-align: center;">
        <h1 style="width:100%; color: white; text-align: center; ">Loading...</h1>
    </div>
</div>
<div id="Disconnected" class="gbscr noselect">
    <div style="margin: 0 auto; text-align: center; padding: 20px;">
        <img src="img/logo.png" style="width: 300px; border: solid 4px #555555;" />
    </div>
    <div style="margin: 0 auto; text-align: center;">
        <h1 style="width:100%; color: white; text-align: center; ">Disconnected</h1>
        <h3 style="width:100%; color: white; text-align: center; "><a id="reconnectLink">Click to reconnect</a></h3>
    </div>
</div>
<div id="Wait" class="gbscr noselect">
    <div style="margin: 0 auto; text-align: center; padding: 20px;">
        <img src="img/logo.png" style="width: 600px; border: solid 4px #555555;" />
    </div>
    <div style="margin: 0 auto; text-align: center;">
        <h3 style="width:100%; color: white; text-align: center; ">Please Wait...<br>(Refresh if more than a minute)</br></h3>
    </div>
</div>
<div id="Play" class="gbscr noselect"></div>
<div id="Admin" class="" caption="Admin" style="display:none"></div>
<style>
    div#urlfleet{
       position: fixed; 
       background: rgba(0,0,0,0); 
       color: #00dbff;
       transition: color 0.3s;
       user-select: none;
       -moz-user-select: none;
       -webkit-user-select: none;
       -ms-user-select: none;
    }
    div#urlfleet:hover {
       color: #ffffff;
       transition: color 0.3s;
       cursor: pointer;
    }
</style>
<div id="urlfleet" class="game" hidden></div>
<div id="twitterbutton" class="game" style="position: fixed;" hidden><a href="https://twitter.com/tdj" class="twitter-follow-button" data-show-count="false">Follow @tdj</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></div>
<style>
    a.discord {
       display: inline-block;
       background: #7289DA; 
       color: white;
       border-radius: 3px;
       padding: 3px 4px 1px 4px;
       text-decoration: none;
       font-size: 11px;
       font-family: "Helvetica Neue", Arial, sans-serif;
       font-weight: 500;
    }
    a.discord:link, a.discord:hover, a.discord:visited, a.discord:active {
        color: white;
        text-decoration: none;
    }
    a.iogames {
       display: inline-block;
       background: #337ab7;
       color: white;
       border-radius: 3px;
       padding: 3px 4px 2px 4px;
       text-decoration: none;
       font-size: 11px;
       font-family: "Helvetica Neue", Arial, sans-serif;
       font-weight: 500;
    }
    a.iogames:link, a.iogames:hover, a.iogames:visited, a.iogames:active {
        color: white;
        text-decoration: none;
    }
    a.iogames:hover {
        background: #286090;
    }
</style>
<div id="discordbutton" class="game" style="position: fixed;" hidden><a class="discord" target="_blank" href="https://discordapp.com/invite/QaqfckU"><img src="img/ui/discord-logo-16px.png">Join on Discord</a></div>
<div id="iogamesbutton" class="game" style="position: fixed;" hidden><a class="iogames" target="_blank" href="http://iogames.space">More IO Games</a></div>
<!-- ************************************************************************************************************************* -->
<!-- ************************************************************************************************************************* -->
</body>
</html>
