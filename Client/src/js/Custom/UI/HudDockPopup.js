//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudDockPopup() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 200;
    this.size.y = 15;
    
    this.dockText = new PIXI.Text("PRESS SPACE TO ENTER STATION", { fontFamily: 'BlenderPro Medium', fontSize: 22, fill: 0xffffff, align: "center" });
    
    this.container.addChild(this.dockText);
}
HudDockPopup.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudDockPopup.prototype.update = function(dt) {
    if (NGE.PIXI.myShip && !NGE.PIXI.myShip.docked.get() && NGE.PIXI.baseToDockAt && (NGE.PIXI.chatMode === OBJ_Pixi.CHAT_MODE.OFF)) {
        this.dockText.x = Math.floor(this.size.x/2 - this.dockText.width/2);
        this.dockText.visible = NGE.PIXI.TutorialFinished;
    } else {
        this.dockText.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************