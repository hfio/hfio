//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationTechCard() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 188;
    this.size.y = 297;

    /**@type Buff */
    this.tech = null;
    this.displayedTech = null;

    // Level Tab
    {
        var tabWidth = 92;
        var tabHeight = 11;
        this.tab = new PIXI.Graphics();
        // Fill
        this.tab.beginFill(0x000000);
        this.tab.fillAlpha = 0.75;
        this.tab.drawPolygon([
            0, tabHeight,
            tabHeight, 0,
            tabWidth - tabHeight, 0,
            tabWidth, tabHeight
        ]);
        this.tab.endFill();
        // Stroke
        this.tab.lineStyle(1, 0x00dbff); 
        this.tab.moveTo(0, tabHeight);
        this.tab.lineTo(tabHeight, 0);
        this.tab.lineTo(tabWidth - tabHeight, 0);
        this.tab.lineTo(tabWidth, tabHeight);
        this.tab.y = -this.tab.height;
        this.tab.x = this.size.x - this.tab.width;
        // Text
        this.levelText = new PIXI.Text('LVL 3', { fontFamily: 'BlenderPro Medium', fontSize: 11, fill: 0x00dbff });
        this.levelText.x = this.tab.x + Math.floor(this.tab.width/2) - Math.floor(this.levelText.width/2);
        this.levelText.y = this.tab.y + Math.floor(this.tab.height/2) - Math.floor(this.levelText.height/2);
    }

    this.background = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/tech-card.png"));

    // overlay, to make image look darker
    this.overlay = new PIXI.Graphics();
    this.overlay.beginFill(0x000000);
    this.overlay.fillAlpha = 0.5;
    this.overlay.drawRect(0, 0, this.size.x, this.size.y);
    this.overlay.endFill();

    this.border = new PIXI.Graphics();
    this.border.lineStyle(1, 0x00dbff); 
    this.border.moveTo(0, 0);
    this.border.lineTo(0, this.size.y);
    this.border.lineTo(this.size.x, this.size.y);
    this.border.lineTo(this.size.x, 0);
    this.border.lineTo(0, 0);

    this.techNameBackground = new PIXI.Graphics();
    this.techNameBackground.beginFill(0x00dbff);
    this.techNameBackground.drawRect(0, 0, this.size.x, 25);
    this.techNameBackground.endFill();
    this.techNameBackground.y = 5;

    // Slot Number
    this.hexagon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/black-hexagon.png"));
    this.hexagon.x = 4;
    this.hexagon.y = Math.floor(this.techNameBackground.y + this.techNameBackground.height/2 - this.hexagon.height/2);
    this.slotNumber = new PIXI.Text('2', { fontFamily: 'BlenderPro Medium', fontSize: 16, fill: 0xffffff });
    this.slotNumber.x = Math.floor(this.hexagon.x + this.hexagon.width/2 - this.slotNumber.width/2) + 1;
    this.slotNumber.y = Math.floor(this.hexagon.y + this.hexagon.height/2 - this.slotNumber.height/2);

    this.techNameText = new PIXI.Text('Tech name', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.techNameText.x = 32;
    this.techNameText.y = this.techNameBackground.y + Math.floor(this.techNameBackground.height/2 - this.techNameText.height/2) - 1;

    this.descriptionText = new PIXI.Text("Tech description", 
        { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff, wordWrap: true, wordWrapWidth: this.size.x - 2 * 17 }
    );
    this.descriptionText.x = 17;
    this.descriptionText.y = 37;
    
    var textStyle = { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff };
    
    this.stat1Text = new PIXI.Text('', textStyle); // fire rate for weapons, duration for other tech
    this.stat1Text.x = 17;
    this.stat1Text.y = 57;
    
    this.stat2Text = new PIXI.Text('', textStyle); // range for weapons, cooldown for other tech
    this.stat2Text.x = 17;
    this.stat2Text.y = 74;

    this.upgradeButton = new ActionWithCostButton({
        style: "green",
        actionText: NGE.i18n.MenuStatic.MS25.english,
        cost: 50,
        marginLeft: 10,
        marginRight: 10,
        action: ()=>{
            NGE.buffManager.upgrade(this.tech);
        }
    });
    this.upgradeButton.pos.x = Math.floor(this.size.x/2 - this.upgradeButton.size.x/2);
    this.upgradeButton.pos.y = 97;

    this.statModifierText = new PIXI.Text(".2 > .3 RELOAD", { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xf2f2f2 });
    this.statModifierText.x = Math.floor(this.size.x/2 - this.statModifierText.width/2);
    this.statModifierText.y = 125;

    this.bonusShipStatText = new PIXI.Text("+"+NGE.i18n.MenuStatic.MS26.english, { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0x00daff });
    this.bonusShipStatText.x = Math.floor(this.size.x/2 - this.bonusShipStatText.width/2);
    this.bonusShipStatText.y = 140;

    this.horizontalRule = new PIXI.Graphics();
    this.horizontalRule.lineStyle(1, 0x7b7d7b);
    this.horizontalRule.moveTo(0, 0);
    this.horizontalRule.lineTo(Math.floor(this.size.x * 0.8617), 0);
    this.horizontalRule.x = Math.floor(this.size.x/2 - this.horizontalRule.width/2);
    this.horizontalRule.y = 185;

    // Create bonus text line items (which look something like "SPD +30/50")
    var bonuses = [
        {name: "HULL", numerator: 5, denominator: 10},
        {name: "SPD", numerator: 30, denominator: 50},
        {name: "?"},
        {name: "?"}
    ];
    this.bonusesText = bonuses.map((bonus) => {
        var container = new PIXI.Container();
        var a = new PIXI.Text("", { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xf2f2f2 })
        var b = new PIXI.Text("", { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xf2f2f2 });
        if(bonus.name === "?") {
            a.text = "?+?";
        } else {
            a.text = bonus.name + " +" + bonus.numerator + "/";
            b.text = bonus.denominator;
            b.x = a.width;
        }
        b.alpha = 0.5;
        container.addChild(a);
        container.addChild(b);  
        return container;      
    });

    // Position bonus text line items
    var y = StationTechCard.BONUS_START_Y;
    this.bonusesText.forEach((item) => {
        item.x = 17;
        item.y = y;
        y += StationTechCard.BONUS_OFFSET_Y;
    });

    this.rerollButton = new ActionWithCostButton({
        style: "purple",
        actionText: "",
        width: 64, 
        height: 16,
        cost: 500,
        costCurrency: "TP",
        marginLeft: 4,
        marginRight: 4,
        icon: "img/ui/dice.png",
        action: ()=>{
            NGE.buffManager.reroll(this.tech);
        }
    });
    
    this.rerollButton.pos.x = 110;
    this.rerollButton.pos.y = 209;
    
    this.sellButton = new ActionWithCostButton({
        style: "orange",
        actionText: NGE.i18n.MenuStatic.MS28.english,
        cost: 50,
        costCurrency: "TP",
        sell: true,
        marginLeft: 10,
        marginRight: 10,
        action: ()=>{
            NGE.buffManager.sell(this.tech);
        }
    });
    this.sellButton.pos.x = Math.floor(this.size.x/2 - this.sellButton.size.x/2);
    this.sellButton.pos.y = 236;

    this.equipButton = new TextButton({text: NGE.i18n.MenuStatic.MS23.english, style: "orange"});
    this.equipButton.pos.x = Math.floor(this.size.x/2 - this.equipButton.size.x/2);
    this.equipButton.pos.y = 266;
    this.unequipButton = new TextButton({text: NGE.i18n.MenuStatic.MS24.english, style: "orange"});
    this.unequipButton.pos.x = Math.floor(this.size.x/2 - this.unequipButton.size.x/2);
    this.unequipButton.pos.y = 266;
    
    var buttonCenterX = this.equipButton.pos.x + this.equipButton.size.x / 2;
    var buttonCenterY = this.equipButton.pos.y + this.equipButton.size.y / 2;
    
    this.equippedText = new PIXI.Text('EQUIPPED', { fontFamily: 'BlenderPro Medium', fontSize: 17, fill: 0xff9700 });
    this.equippedText.x = Math.floor(buttonCenterX - this.equippedText.width / 2);
    this.equippedText.y = Math.floor(buttonCenterY - this.equippedText.height / 2);

    this.container.addChild(this.tab);
    this.container.addChild(this.levelText);
    this.container.addChild(this.background);
    this.container.addChild(this.overlay);
    this.container.addChild(this.techNameBackground);
    this.container.addChild(this.hexagon);
    this.container.addChild(this.slotNumber);
    this.container.addChild(this.techNameText);
    this.container.addChild(this.descriptionText);
    this.container.addChild(this.stat1Text);
    this.container.addChild(this.stat2Text);
    this.container.addChild(this.statModifierText);
    this.container.addChild(this.bonusShipStatText);
    this.container.addChild(this.horizontalRule);
    this.container.addChild(this.equippedText);
    this.bonusesText.forEach((item) => this.container.addChild(item));
    this.addChild(this.upgradeButton);
    this.addChild(this.rerollButton);
    this.addChild(this.sellButton);
    this.addChild(this.equipButton);
    this.addChild(this.unequipButton);
    this.container.addChild(this.border);
}
StationTechCard.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationTechCard.BONUS_START_Y = 162;
StationTechCard.BONUS_OFFSET_Y = 17;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationTechCard.prototype.update = function() {
    if(NGE.PIXI.Input.MOUSE_RELEASED) {
        if(this.equipButton.mouseOver && this.equipButton.container.visible) {
            NGE.buffManager.equip(this.tech);
        }
        if(this.unequipButton.mouseOver && this.unequipButton.container.visible) {
            NGE.buffManager.unequip(this.tech);
        }
    }
    if(this.tech) {
        var tech = this.tech; // for cleaner code
        
        var weapon = tech.isWeapon();
        
        this.sellButton.container.visible = !weapon || !tech.equipped;
        if (this.sellButton.container.visible) {
            this.sellButton.cost = tech.getSellTechPoints();
        }
        
        // Show/hide the relevant equip/unequip buttons
        this.equipButton.container.visible = !tech.equipped;
        this.unequipButton.container.visible = (tech.equipped && !weapon);
        this.equippedText.visible = (tech.equipped && weapon);

        if ((this.displayedTech !== tech) && tech.config.cardImage) {
            this.background.texture = NGE.PIXI.TextureFromImage("img/ui/" + tech.config.cardImage);
        }

        this.descriptionText.text = tech.config.description;
        this.techNameText.text = tech.config.name;

        // weapons vs non-weapon tech
        if (weapon) {
            this.stat1Text.text = 'Fire rate: ' + Math.round(100 / tech.getCooldown()) / 100;
            this.stat2Text.text = 'Range: ' + tech.getRange();
        } else {
            this.stat1Text.text = 'Duration: ' + tech.getDuration();
            this.stat2Text.text = 'Cooldown: ' + tech.getCooldown();
        }
        
        var fullyUpgraded = tech.isFullyUpgraded();

        // Display "bonus ship stat" text only if bonus is active
        this.bonusShipStatText.visible = !fullyUpgraded && tech.config.upgradeGivesBonus[tech.level];

        // Hide upgrade button once fully upgraded
        this.upgradeButton.container.visible = !fullyUpgraded;

        // Update upgrade cost text if visible
        if(this.upgradeButton.container.visible) {
            var upgradeCost = tech.config.upgradeCosts[tech.level];
            this.upgradeButton.cost = upgradeCost;
        }

        // Update stat preview text
        {
            let category = tech.config.effects.toUpperCase();
            if(fullyUpgraded) {
                let stat = tech.getValue();
                this.statModifierText.text = "" + stat + " " + category;
            } else {
                let before = tech.getValue();
                let after = tech.getNextLevelValue();
                this.statModifierText.text = "" + before + " > " + after + " " + category;
            }
            this.statModifierText.x = Math.floor(this.size.x/2 - this.statModifierText.width/2);
        }

        // Update level text
        this.levelText.text = "LVL " + (tech.level + 1); 
        this.levelText.x = this.tab.x + Math.floor(this.tab.width/2) - Math.floor(this.levelText.width/2);

        // Update slot number text
        this.slotNumber.text = tech.config.slot;
        this.slotNumber.x = Math.floor(this.hexagon.x + this.hexagon.width/2 - this.slotNumber.width/2) + 1;

        // Update bonus stat text
        for(var i=0; i<this.bonusesText.length; i++) {
            if(i < tech._bonuslist.length) {
                var bonus = tech._bonuslist[i];
                this.bonusesText[i].children[0].text = bonus.config.stat.toUpperCase() + " +" + bonus.getValue() + "/";
                this.bonusesText[i].children[1].text = bonus.config.max;
                this.bonusesText[i].children[1].x = this.bonusesText[i].children[0].width;
            } else {
                this.bonusesText[i].children[0].text = "?+?";
                this.bonusesText[i].children[1].text = "";
            }
        }

        if (tech._bonuslist.length > 0) {
            this.rerollButton.pos.y = StationTechCard.BONUS_START_Y + ((tech._bonuslist.length-1) * StationTechCard.BONUS_OFFSET_Y);
            this.rerollButton.visible = true;
        } else {
            this.rerollButton.visible = false;
        }
    }
    this.displayedTech = this.tech;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
