//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationSortButtons() { // extends BaseGuiElement
    BaseGuiElement.call(this);
    
    this.padding = 3;

    var text = [
        NGE.i18n.MenuStatic.MS10.english,
        NGE.i18n.MenuStatic.MS14.english,
        NGE.i18n.MenuStatic.MS13.english,
        NGE.i18n.MenuStatic.MS15.english
    ];
    for(var i=0; i<text.length; i++) {
        var button = new StationSortButton();
        button.text.text = text[i];
        this.addChild(button);

        // hide background of all but first button to make it appear "selected"
        if(i>0) { button.background.alpha = 0; }
    }
}
StationSortButtons.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationSortButtons.prototype.update = function() {
    var x = 0;
    for(var i=0; i<this.children.length; i++) {
        var button = this.children[i];
        button.pos.x = x;
        x += button.size.x + this.padding;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************