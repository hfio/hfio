//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudErrorPopup() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 200;
    this.size.y = 15;

    this._text = new PIXI.Text("", { fontFamily: 'BlenderPro Medium', fontSize: 22, fill: 0xff6622, align: "center" });
    
    this._background = new PIXI.Graphics();
    this._background.beginFill(0x000000);
    this._background.alpha = 0.5;
    this._background.drawRect(0, 0, 8, 8);
    this._background.endFill();
    
    this.container.addChild(this._background);
    this.container.addChild(this._text);
    
    this._timeLeft = 0;
}
HudErrorPopup.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudErrorPopup.FADEOUT_DURATION = 0.2;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudErrorPopup.prototype.update = function(dt) {
    if (this._timeLeft > 0) {
        this._timeLeft = Math.max(0, this._timeLeft - dt);
    }
    if (this._timeLeft > HudErrorPopup.FADEOUT_DURATION) {
        this._text.alpha = 1;
    } else {
        this._text.alpha = this._timeLeft / HudErrorPopup.FADEOUT_DURATION;
    }
    if ((this._text.alpha > 0) && NGE.PIXI.TutorialFinished) {
        this._text.x = Math.floor(this.size.x / 2 - this._text.width / 2);
        this._text.y = Math.floor(this.size.y / 2 - this._text.height / 2);
        this._text.visible = true;
        this._background.visible = true;
        this._background.width = this._text.width + 16;
        this._background.height = this._text.height + 16;
        this._background.x = Math.floor(this.size.x / 2 - this._background.width / 2);
        this._background.y = Math.floor(this.size.y / 2 - this._background.height / 2);
        this._background.visible = true;
        this._background.alpha = this._text.alpha * 0.65;
    } else {
        this._text.visible = false;
        this._background.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudErrorPopup.prototype.setText = function(text, duration) {
    this._text.text = text;
    this._timeLeft = duration || HudErrorPopup.FADEOUT_DURATION;
};
