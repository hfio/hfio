//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudEnergyBar() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 604;
    this.size.y = 14;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, this.size.x - 2, this.size.y - 2);
    this.background.endFill();
    this.background.x = 1;
    this.background.y = 1;

    var stroke = new PIXI.Graphics();
    stroke.lineStyle(1, 0x00dbff); 
    stroke.moveTo(0, 0);
    stroke.lineTo(0, this.size.y);
    stroke.lineTo(this.size.x, this.size.y);
    stroke.lineTo(this.size.x, 0);
    stroke.lineTo(0, 0);

    this.orangeBar = new PIXI.Graphics();
    this.orangeBar.beginFill(0xff9700);
    this.orangeBar.drawRect(0, 0, 8, 8);
    this.orangeBar.endFill();
    this.orangeBar.width = 473;
    this.orangeBar.height = 3;
    this.orangeBar.x = 6;
    this.orangeBar.y = 6;

    this.blackBar = new PIXI.Graphics();
    this.blackBar.beginFill(0x000000);
    this.blackBar.drawRect(0, 0, 8, 8);
    this.blackBar.endFill();
    this.blackBar.width = 119;
    this.blackBar.height = 3;
    this.blackBar.x = 479;
    this.blackBar.y = 6;

    this.angleFill = new oNinePatch('img/ui/energy-bar-angle-fill.png', { 
        width: 53,
        height: 13,
        left: 0,
        top: 11,
        right: 12,
        bottom: 0
    });
    this.angleFill.container.y = 1;

    this.bolt = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/energy-bolt.png"));
    this.bolt.x = 3;
    this.bolt.y = 2;

    this.text = new PIXI.Text('200', { fontFamily: 'BlenderPro Medium', fontSize: 9, fill: 0x00dbff });
    this.text.x = 14;
    this.text.y = 3;

    this.container.addChild(this.background);
    this.container.addChild(this.orangeBar);
    this.container.addChild(this.blackBar);
    this.container.addChild(this.angleFill.container);
    this.container.addChild(stroke);    
    this.container.addChild(this.bolt);
    this.container.addChild(this.text);
}
HudEnergyBar.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudEnergyBar.prototype.update = function() {
    
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudEnergyBar.prototype.setValue = function(ratio) {
    // expect ratio between 0 and 1
    var fullWidth = (this.size.x - this.angleFill.width - 12);
    var barWidth = fullWidth * ratio;
    var barX = this.angleFill.width;
    this.orangeBar.x = barX;
    this.orangeBar.width = barWidth;
    this.blackBar.width = fullWidth * (1-ratio);
    this.blackBar.x = barX + barWidth;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************