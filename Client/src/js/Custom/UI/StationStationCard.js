//*********************************************************************************************************************************************
// Card for droppable mini stations with turrets
//*********************************************************************************************************************************************
function StationStationCard(station) { // extends BaseGuiElement
    BaseGuiElement.call(this);
    
    this.station = null; // a reference to the station stats item belonging to the selected station

    this.size.x = 188;
    this.size.y = 297;

    this.background = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/station-card.png"));

    // overlay, to make image look darker
    this.overlay = new PIXI.Graphics();
    this.overlay.beginFill(0x000000);
    this.overlay.fillAlpha = 0.5;
    this.overlay.drawRect(0, 0, this.size.x, this.size.y);
    this.overlay.endFill();

    this.border = new PIXI.Graphics();
    this.border.lineStyle(1, 0x00dbff); 
    this.border.moveTo(0, 0);
    this.border.lineTo(0, this.size.y);
    this.border.lineTo(this.size.x, this.size.y);
    this.border.lineTo(this.size.x, 0);
    this.border.lineTo(0, 0);

    this.stationNameBackground = new PIXI.Graphics();
    this.stationNameBackground.beginFill(0x00dbff);
    this.stationNameBackground.drawRect(0, 0, this.size.x, 25);
    this.stationNameBackground.endFill();
    this.stationNameBackground.y = 5;

    this.stationNameText = new PIXI.Text("STATION NAME", { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0x000000 });
    this.stationNameText.x = 7;
    this.stationNameText.y = this.stationNameBackground.y + Math.floor(this.stationNameBackground.height/2 - this.stationNameText.height/2) - 1;

    this.descriptionText = new PIXI.Text(
            "Description",
        { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff, wordWrap: true, wordWrapWidth: this.size.x - 2 * 17 }
    );
    this.descriptionText.x = 17;
    this.descriptionText.y = 37;
    
    var textStyle = { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff };

    this.damageText = new PIXI.Text('Damage: -', textStyle);
    this.damageText.x = 17;
    this.damageText.y = 143;

    this.rangeText = new PIXI.Text('Range: -', textStyle);
    this.rangeText.x = 17;
    this.rangeText.y = 159;

    this.healthText = new PIXI.Text('Health: -', textStyle);
    this.healthText.x = 17;
    this.healthText.y = 175;
    
    this.durationText = new PIXI.Text('Duration: -', textStyle);
    this.durationText.x = 17;
    this.durationText.y = 192;

    this.statusBackground = new PIXI.Graphics();
    this.statusBackground.beginFill(0x000000);
    this.statusBackground.fillAlpha = 0.5;
    this.statusBackground.drawRect(0, 0, this.size.x, 25);
    this.statusBackground.endFill();
    this.statusBackground.y = 209;

    this.statusText = new PIXI.Text('NOT PURCHASED', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.statusText.x = Math.floor(this.size.x/2 - this.statusText.width/2);
    this.statusText.y = this.statusBackground.y + Math.floor(this.statusBackground.height/2 - this.statusText.height/2);    
    
    this.purchaseButton = new ActionWithCostButton({
        style: "orange",
        actionText: NGE.i18n.MenuStatic.MS22.english,
        cost: 50,
        marginLeft: 10,
        marginRight: 10,
        action: ()=>{
            NGE.stationManager.buy(this.station.id);
        }
    });
    this.purchaseButton.pos.x = Math.floor(this.size.x/2 - this.purchaseButton.size.x/2);
    this.purchaseButton.pos.y = 236;

    this.equipButton = new TextButton({text: NGE.i18n.MenuStatic.MS23.english, style: "orange"});
    this.equipButton.pos.x = Math.floor(this.size.x/2 - this.equipButton.size.x/2);
    this.equipButton.pos.y = 266;
    
    this.unequipButton = new TextButton({text: NGE.i18n.MenuStatic.MS24.english, style: "orange"});
    this.unequipButton.pos.x = Math.floor(this.size.x/2 - this.unequipButton.size.x/2);
    this.unequipButton.pos.y = 266;
    
    if (station) {
        this.setStation(station);
    }

    this.container.addChild(this.background);
    this.container.addChild(this.overlay);
    this.container.addChild(this.stationNameBackground);
    this.container.addChild(this.stationNameText);
    this.container.addChild(this.descriptionText);
    this.container.addChild(this.damageText);
    this.container.addChild(this.rangeText);
    this.container.addChild(this.healthText);
    this.container.addChild(this.durationText);
    this.container.addChild(this.statusBackground);
    this.container.addChild(this.statusText);
    this.addChild(this.purchaseButton);
    this.addChild(this.equipButton);
    this.addChild(this.unequipButton);
    this.container.addChild(this.border);
}
StationStationCard.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationStationCard.prototype.setStation = function (station) {
    if (this.station !== station) {
        this.station = station;
        if (this.station) {
            var turret = NGE.Stats.getStationTurret(station.id);

            this.background.texture = NGE.PIXI.TextureFromImage("img/ui/"+station.cardImage);
            this.stationNameText.text = station.name;
            this.descriptionText.text = station.description;
            if (turret) {
                this.damageText.text = 'Damage: ' + turret.levels[0]; // DPS
                this.rangeText.text = 'Range: ' + turret.range[0];
                this.damageText.visible = true;
                this.rangeText.visible = true;
            } else {
                this.damageText.visible = false;
                this.rangeText.visible = false;
            }
            this.healthText.text = 'Health: ' + station.health;
            this.durationText.text = 'Duration: ' + station.duration;
            this.purchaseButton.cost = station.cost;
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationStationCard.prototype.update = function() {
    if(NGE.PIXI.Input.MOUSE_RELEASED) {
        if(this.equipButton.mouseOver && this.equipButton.container.visible) {
            NGE.stationManager.equip(this.station.id);
        }
        if(this.unequipButton.mouseOver && this.unequipButton.container.visible) {
            NGE.stationManager.unequip();
        }
    }
    
    var owned = false;
    var equipped = false;
    var station = null;
    
    if (NGE.stationManager) {
        station = NGE.stationManager.findById(this.station.id);
        if (station) {
            owned = station.amount > 0;
            equipped = owned && station.equipped;
        } else {
            owned = false;
        }
    }
    this.purchaseButton.container.visible = !owned || (station.amount < this.station.maxCount) && NGE.WS.isLoggedIn;
    this.equipButton.container.visible = owned && !equipped;
    this.unequipButton.container.visible = owned && equipped;
    this.statusText.text = owned ? (station.amount + "/" + this.station.maxCount + (equipped ? ' EQUIPPED' : ' IN CARGO')) : 'NOT PURCHASED';
    this.statusText.x = Math.round(this.size.x / 2 - this.statusText.width / 2);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************