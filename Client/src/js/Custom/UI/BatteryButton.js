//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function BatteryButton(iconName) { // extends BaseGuiElement
	BaseGuiElement.call(this);

	this.size.x = 112;
	this.size.y = 26;

	Object.defineProperty(this, 'selected', {get: this._getSelected, set: this._setSelected});
	this._selected = false;	

	this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, this.size.x, this.size.y);
    this.background.endFill();

    this.glowBox = new oNinePatch('img/ui/nine-patch-glow-box.png', { 
		width: 128,
		height: 128,
		left: 18,
		top: 19,
		right: 19,
		bottom: 19
	});
	this.glowBox.container.visible = false; // must be selected to be seen

    this.highlight = new PIXI.Graphics();
    this.highlight.lineStyle(1, 0x00dbff); 
    this.highlight.moveTo(0, this.size.y);
    this.highlight.lineTo(0, this.size.y - 6);

        iconName = iconName || "battery";
	this.batteryTexture = NGE.PIXI.TextureFromImage("img/ui/" + iconName + ".png");
	this.batterySelectedTexture = NGE.PIXI.TextureFromImage("img/ui/" + iconName + "-black.png");
	this.battery = new PIXI.Sprite(this.batteryTexture);

	this.container.addChild(this.background);
	this.container.addChild(this.glowBox.container);
	this.container.addChild(this.highlight);
	this.container.addChild(this.battery);
}
BatteryButton.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BatteryButton.prototype.update = function() {
	this.background.width = this.size.x;
	this.highlight.x = this.size.x - 1;
	this.battery.x = Math.floor(this.size.x/2 - this.battery.width/2);
        this.battery.y = Math.floor(this.size.y/2 - this.battery.height/2);
	
	this.glowBox.width = this.size.x + this.glowBox._left + this.glowBox._right;
	this.glowBox.height = this.size.y + this.glowBox._top + this.glowBox._bottom;
	this.glowBox.container.x = -this.glowBox._left;
	this.glowBox.container.y = -this.glowBox._top;

	if(NGE.PIXI.Input.MOUSE_RELEASED && this.mouseOver && typeof(this.onclick) === 'function') {
        this.onclick();
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BatteryButton.prototype._getSelected = function() {
	return this._selected;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BatteryButton.prototype._setSelected = function(bool) {
	if(this._selected !== bool) {
		this._selected = bool;
		this.glowBox.container.visible = this._selected;
		this.background.visible = !this._selected;
		this.battery.texture = (this._selected ? this.batterySelectedTexture : this.batteryTexture);
	}
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************