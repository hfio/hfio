//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationSideBar() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 154;

    this.fill = new PIXI.Graphics();
    this.fill.beginFill(0x000000);
    this.fill.alpha = 0.5;
    this.fill.drawRect(0, 0, this.size.x, 8);
    this.fill.endFill();

    this.galaxyNameText = new PIXI.Text(NGE.i18n.MenuStatic.MS20.english, { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.galaxyNameText.x = 7;
    this.galaxyNameText.y = 42;

    this.stationNameText = new PIXI.Text(NGE.i18n.MenuStatic.MS21.english, { fontFamily: 'BlenderPro Medium', fontSize: 22, fill: 0xffffff });
    this.stationNameText.x = 7;
    this.stationNameText.y = 57;

    this.resourceRestock = new StationSideBarResource(()=>{
        if (this.resourceRestock.enabled) {
            NGE.WS.Send('restock', {});
        }
    });
    this.resourceRestock.nameText.text = NGE.i18n.MenuStatic.MS8.english;
    this.resourceRestock.pos.y = 124;

    this.resourceRepair = new StationSideBarResource(()=>{
        if (this.resourceRepair.enabled) {
            NGE.WS.Send('repair', {});
        }
    });
    this.resourceRepair.nameText.text = NGE.i18n.MenuStatic.MS6.english;
    this.resourceRepair.pos.y = 167; //209;

    this.launchButton = new TextButton({text: NGE.i18n.MenuStatic.MS9.english, style: "orange", width: 154, height: 50, textX: 7});
    this.launchButton.pos.y = 325;

    this.container.addChild(this.fill);
    this.container.addChild(this.galaxyNameText);
    this.container.addChild(this.stationNameText);
    this.addChild(this.resourceRestock);
    this.addChild(this.resourceRepair);
    this.addChild(this.launchButton);
}
StationSideBar.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationSideBar.prototype.update = function() {
    this.size.y = window.innerHeight;
    this.fill.height = this.size.y;
    
    if (NGE.PIXI.myShip) {
        var o = NGE.PIXI.myShip;
        this.resourceRestock.updateForResourceValue(o.ammo.get() / o.max_ammo.get(), NGE.Stats.getShipStat(o, "restock_cost"));
        this.resourceRepair.updateForResourceValue(o.hp.get() / o.max_hp.get(), NGE.Stats.getShipStat(o, "repair_cost"));
        
        this.launchButton.container.alpha = 1;
    } else {
        this.resourceRestock.updateForResourceValue(1, 1);
        this.resourceRepair.updateForResourceValue(1, 1);
        
        this.launchButton.container.alpha = 0.2;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************