// Closure
goog.require('baseguielement'); // later in the alphabet, so we need to require it manually to ensure closure loads that file first
goog.provide('basebutton');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function BaseButton(options) { // extends BaseGuiElement
    BaseGuiElement.call(this);
    
    // default options:
    options = options || {};
    options.style = options.style || 'orange';
    options.width = options.width || 155;
    options.height = options.height || 27;
    
    var backgroundPrefix = BaseButton.BACKGROUND_NINEPATCH_URL_PREFIX + options.style;
    
    // Ninepatches used for button states
    this.up = new oNinePatch(backgroundPrefix + '-up.png', {width: options.width, height: options.height, left: 3, top: 3, right: 3, bottom: 3});
    this.down = new oNinePatch(backgroundPrefix + '-down.png', {width: options.width, height: options.height, left: 3, top: 3, right: 3, bottom: 3 });
    this.hover = new oNinePatch(backgroundPrefix + '-hover.png', {width: options.width, height: options.height, left: 3, top: 3, right: 3, bottom: 3});

    // Container to hold ninepatches
    this.backgroundContainer = new PIXI.Container();
    
    this.container.addChild(this.backgroundContainer);
    this.backgroundContainer.addChild(this.up.container);
    this.backgroundContainer.addChild(this.down.container);
    this.backgroundContainer.addChild(this.hover.container);
    
    this.size.x = options.width;
    this.size.y = options.height;
}
BaseButton.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
BaseButton.prototype.constructor = BaseButton;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseButton.BACKGROUND_NINEPATCH_URL_PREFIX = "img/ui/text-button-";
BaseButton.COLORS = {
    orange: 0xff9700,
    purple: 0xcc00d2,
    green: 0x00d200,
    blue: 0x00dbff,
    grey: 0x4a4a4a,
    red: 0xd20000
};
Object.freeze(BaseButton.COLORS);
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseButton.prototype.update = function() {
    if(NGE.PIXI.Input.MOUSE_STATE && this.mouseOver) {
        this.up.container.visible = false;
        this.down.container.visible = true;
        this.hover.container.visible = false;
    } else if(this.mouseOver) {
        this.up.container.visible = false;
        this.down.container.visible = false;
        this.hover.container.visible = true;
    } else {
        this.up.container.visible = true;
        this.down.container.visible = false;
        this.hover.container.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseButton.prototype._setWidth = function(width) {
    BaseGuiElement.prototype._setWidth.call(this, width);
    if(this.up) { this.up.width = width; }
    if(this.down) { this.down.width = width; }
    if(this.hover) { this.hover.width = width; }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseButton.prototype._setHeight = function(height) {
    BaseGuiElement.prototype._setHeight.call(this, height);
    if(this.up) { this.up.height = height; }
    if(this.down) { this.down.height = height; }
    if(this.hover) { this.hover.height = height; }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseButton.prototype.updateLayout = function() {
    this.size.x = this.size.x;
    this.size.y = this.size.y;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Object.defineProperty(BaseButton.prototype, "visible", {
    get: function () {
        return this.container.visible;
    },
    set: function (v) {
        this.container.visible = v;
    }
});