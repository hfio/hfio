//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HealthBar(object) { 
    this._size = object.w.get();
    this._width = Math.floor(this._size * 1.2);
    this._height = (this._width > 200) ? 3 : 2;
    this._y = Math.floor(this._size * 0.8);
    this._background = new PIXI.Graphics();
    this._background.beginFill(0x000000);
    this._background.drawRect(0, 0, this._width, this._height);
    this._foreground = new PIXI.Graphics();
    this._foreground.beginFill((object.team.get() === NGE.ALIEN_AI_TEAM) ? 0xffff00 : 0x00dbff);
    this._foreground.drawRect(0, 0, this._width, this._height);
    this._container = new PIXI.Container();
    this._container.addChild(this._background);
    this._container.addChild(this._foreground);
    this._container.pivot.set(this._width/2, this._height/2);
    NGE.PIXI.PixiPlayerOverlayGroup.addChild(this._container);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HealthBar.prototype.updateSize = function(size) {
    if (this._size === size) {
        return;
    }
    this._size = size;
    this._width = Math.floor(this._size * 1.2);
    this._height = (this._width > 200) ? 3 : 2;
    this._y = Math.floor(this._size * 0.8);
    this._background.width = this._width;
    this._background.height = this._height;
    this._foreground.height = this._height;
    this._container.pivot.set(this._width/2, this._height/2);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HealthBar.prototype.update = function(x, y, hp) {
    if (!NGE.PIXI.testStationMenu.container.visible) {
        this._container.x = x;
        this._container.y = y - this._y;
        this._foreground.visible = hp > 0;
        this._foreground.width = Math.ceil(hp * this._width);
        this._container.visible = true;
    } else {
        this._container.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HealthBar.prototype.destroy = function() {
    this._container.destroy();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Object.defineProperty(HealthBar.prototype, 'visible', {
    get: function() { return this._container.visible; },
    set: function(value) { this._container.visible = value; }
});
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************