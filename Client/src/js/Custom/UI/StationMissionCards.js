//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationMissionCards() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.cardPadding = 12;
    this.cardIndex = 0;
    this.cardsAdded = 0;
    this.cardsPerPage = 0;

    this.cards = [];
    
    this.shipType = null; // the cards for this ship's missions are displayed
}
StationMissionCards.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMissionCards.prototype.update = function() {
    var i;
    // checking if we have the right set of cards (missions for the current ship type)
    var newShip = false;
    if (NGE.PIXI.myShip && (NGE.PIXI.myShip.subtype.get() !== this.shipType)) {
        // changing the cards to the right ones - adding / updating / removing
        var missionIds = NGE.Stats.getShipStat(NGE.PIXI.myShip, "missions");
        for(i=0; i<missionIds.length; i++) {
            var mission = NGE.Stats.getMission(missionIds[i]);
            if (this.cards.length <= i) {
                this.cards.push(new StationMissionCard(mission));
            } else {
                this.cards[i].setMission(mission);
            }
        }
        while (i < this.cards.length) {
            this.removeChild(this.cards[i]);
            this.cards.splice(i, 1);
        }
        this.shipType = NGE.PIXI.myShip.subtype.get();
        newShip = true;
    }
    
    this.cardsPerPage = Math.floor(this.size.x / (this.cards[0].size.x + this.cardPadding));

    if(newShip || this.cardsAdded < this.cardsPerPage || this.cardsAdded > this.cardsPerPage) {
        this.reflowCards();
    }

    // update card positions
    for(i=0; i<this.children.length; i++) {
        var card = this.children[i];
        card.pos.x = this.cardPadding + i * (card.size.x + this.cardPadding);
        card.pos.y = 13;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMissionCards.prototype.reflowCards = function() {
    var i;
    // remove all cards
    this.cardsAdded = 0;
    for(i=0; i<this.cards.length; i++) {
        this.removeChild(this.cards[i]);
    }
    // only a subset of cards gets added
    for(i=this.cardIndex; i<Math.min(this.cardIndex + this.cardsPerPage, this.cards.length); i++) {
        this.addChild(this.cards[i]);
        this.cardsAdded++;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMissionCards.prototype.previousPage = function() {
    this.cardIndex = Math.max(0, this.cardIndex - this.cardsPerPage);
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMissionCards.prototype.nextPage = function() {
    this.cardIndex = Math.max(0, Math.min(this.cards.length - this.cardsPerPage, this.cardIndex + this.cardsPerPage));
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************