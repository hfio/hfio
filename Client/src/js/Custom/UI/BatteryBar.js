//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function BatteryBar() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 798;
    this.size.y = 26;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    this.highlights = new PIXI.Graphics();
    this.highlights.lineStyle(1, 0x00dbff); 
    this.highlights.moveTo(0, 0);
    this.highlights.lineTo(16, 0);
    this.highlights.moveTo(0, this.size.y);
    this.highlights.lineTo(16, this.size.y);

    this.buttons = new BatteryButtons();

    this.container.addChild(this.background);
    this.addChild(this.buttons);
    this.container.addChild(this.highlights);
}
BatteryBar.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BatteryBar.prototype.update = function() {
    this.background.width = this.size.x;
    this.highlights.width = this.size.x;
    this.buttons.size.x = this.size.x;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************