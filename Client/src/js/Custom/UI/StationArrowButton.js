//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationArrowButton() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 40;
    this.size.y = 40;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.fillAlpha = 0.5;
    this.background.drawRect(0, 0, this.size.x - 2, this.size.y - 2);
    this.background.endFill();
    this.background.x = 1;
    this.background.y = 1;
    
    this.blackFill = new PIXI.Graphics();
    this.blackFill.beginFill(0x000000);
    this.blackFill.drawRect(0, 0, this.size.x - 6, this.size.y - 6);
    this.blackFill.endFill();
    this.blackFill.x = 3;
    this.blackFill.y = 3;

    var highlightWidth = 10;
    this.highlight = new PIXI.Graphics();
    this.highlight.lineStyle(1, 0xff9700); 
    this.highlight.moveTo(highlightWidth, 0);
    this.highlight.lineTo(0, 0);
    this.highlight.lineTo(0, this.size.y);
    this.highlight.lineTo(highlightWidth, this.size.y);
    this.highlight.moveTo(this.size.x - highlightWidth, 0);
    this.highlight.lineTo(this.size.x, 0);
    this.highlight.lineTo(this.size.x, this.size.y);
    this.highlight.lineTo(this.size.x - highlightWidth, this.size.y);

    this.arrow = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/orange-arrow.png"));
    this.arrow.pivot.x = Math.floor(this.arrow.width/2);
    this.arrow.pivot.y = Math.floor(this.arrow.height/2);
    this.arrow.x = Math.floor(this.size.x/2);
    this.arrow.y = Math.floor(this.size.y/2);

    this.container.addChild(this.background);
    this.container.addChild(this.blackFill);
    this.container.addChild(this.highlight);
    this.container.addChild(this.arrow);
}
StationArrowButton.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************