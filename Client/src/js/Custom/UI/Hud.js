//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function Hud() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.usernameButton = new HudUsernameButton();

    this.wallet = new HudWallet();
    
    this.fleetPanel = new HudFleetPanel();

    this.missionText = new HudMissionText();
    this.compass = new HudCompass();
    this.compass.pos.y = 16;
    
    this.stationArrow = new HudStationArrow();
    
    this.infoPopup = new HudInfoPopup();
    this.dockPopup = new HudDockPopup();
    this.errorPopup = new HudErrorPopup();

    this.bottomBar = new HudBottomBar();
    
    this.shipStatusPanel = new HudShipStatusPanel();
    
    this.chatEditPanel = new HudChatEditPanel();
    this.chatPanel = new HudChatPanel();
    
    // reusing texture objects for the skill button icons
    this.techTextures = {};
    this.stationTextures = {};
    
    this.techCooldownTexture = NGE.PIXI.TextureFromImage("img/ui/tech-icon-cooldown.png");
    this.techActiveTexture = NGE.PIXI.TextureFromImage("img/ui/tech-icon-active.png");

    this.addChild(this.usernameButton);
    this.addChild(this.wallet);
    this.addChild(this.fleetPanel);
    this.addChild(this.missionText);
    this.addChild(this.compass);
    this.addChild(this.stationArrow);
    this.addChild(this.infoPopup);
    this.addChild(this.dockPopup);
    this.addChild(this.errorPopup);
    this.addChild(this.bottomBar);
    this.addChild(this.shipStatusPanel);
    this.addChild(this.chatEditPanel);
    this.addChild(this.chatPanel);
}
Hud.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Hud._getStatColor = function (relativeValue) {
    relativeValue = Math.min(Math.max(0, relativeValue), 1);
    // red - yellow
    if (relativeValue <= 0.5) {
        // red + add green component
        return 0xff0000 + ((0xff * relativeValue * 2) << 8);
    }
    // yellow - green
    relativeValue = 1 - relativeValue;
    // green + add red component
    return 0x00ff00 + ((0xff * relativeValue * 2) << 16);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Hud._getCountdowntext = function(timeLeft) {
    return timeLeft.toFixed(1);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Hud.prototype.showError = function(message, duration) {
    return this.errorPopup.setText(message, duration);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Hud.prototype.update = function() {
    this.wallet.pos.x = window.innerWidth - this.wallet.size.x;
    
    this.fleetPanel.pos.x = window.innerWidth - this.fleetPanel.size.x;
    this.fleetPanel.pos.y = this.wallet.size.y;
    
    this.missionText.pos.x = Math.floor(window.innerWidth/2 - this.missionText.size.x/2);
    this.compass.pos.x = Math.floor(window.innerWidth/2 - this.compass.size.x/2);
    this.compass.pos.y = this.missionText.pos.y + this.missionText.size.y;
    
    this.stationArrow.pos.x = Math.floor(window.innerWidth/2 - this.stationArrow.size.x/2);
    this.stationArrow.pos.y = this.compass.pos.y + this.compass.size.y;
    
    this.infoPopup.pos.x = Math.floor(window.innerWidth/2 - this.infoPopup.size.x/2);
    this.infoPopup.pos.y = 120;
    
    this.dockPopup.pos.x = Math.floor(window.innerWidth/2 - this.dockPopup.size.x/2);
    this.dockPopup.pos.y = window.innerHeight - this.bottomBar.size.y - 45;
    
    this.errorPopup.pos.x = Math.floor(window.innerWidth/2 - this.errorPopup.size.x/2);
    this.errorPopup.pos.y = this.infoPopup.pos.y + 140;
    
    this.bottomBar.pos.x = Math.floor(window.innerWidth/2 - this.bottomBar.size.x/2);
    this.bottomBar.pos.y = window.innerHeight - this.bottomBar.size.y - 5;
    
    this.chatPanel.pos.x = window.innerWidth - this.chatPanel.size.x;
    this.chatPanel.pos.y = window.innerHeight - this.chatPanel.size.y - 5;
    
    // caching myShip stats
    
    var stats;
    var myShip = NGE.PIXI.myShip;
    
    if (myShip) {
        // chat edit panel
        if (myShip.docked.get()) {
            NGE.PIXI.chatMode = OBJ_Pixi.CHAT_MODE.OFF;
        }
        var chv = this.chatEditPanel.container.visible;
        this.chatEditPanel.container.visible = NGE.PIXI.chatMode !== OBJ_Pixi.CHAT_MODE.OFF;
        if (!chv && this.chatEditPanel.container.visible) {
            this.chatEditPanel.input.text = "";
            this.chatEditPanel.input.focus();
        } else if (chv && !this.chatEditPanel.container.visible) {
            this.chatEditPanel.input.blur();
        }
        this.chatEditPanel.pos.x = this.chatPanel.pos.x + this.chatPanel.size.x - this.chatEditPanel.size.x - 5;
        this.chatEditPanel.pos.y = this.chatPanel.pos.y + this.chatPanel.size.y - this.chatEditPanel.size.y - 5;
        
        
        var shipName = myShip.subtype.get();
        stats = NGE.Stats.Ships.find((ship) => ship.data_name === shipName);
    } else {
        NGE.PIXI.chatMode = OBJ_Pixi.CHAT_MODE.OFF;
        this.chatEditPanel.container.visible = false;
    }
    
    // BOTTOM BAR (SKILL BUTTONS)
    
    this.bottomBar.skillButtons.setButtonCount(stats ? (stats.slots || 0) + 2 : 0); // primary weapon + deploy station + slots
    var btn;
    
    if (NGE.buffManager) {
        var usedSlots = new Array(10);
        for (var s = 0; s< NGE.buffManager._buffs.length;s++){
            var buff = NGE.buffManager._buffs[s];
            var bc = buff.config;
            if (buff.equipped) {
                var slotIndex = parseInt(bc.slot);
                usedSlots[slotIndex] = true;
                btn = this.bottomBar.skillButtons.buttons[slotIndex+1];
                if (buff.active) {
                    btn.setStateTexture(this.techActiveTexture);
                    btn.countdownText.visible = false;
                } else {
                    if (buff.timeLeft > 0) {
                        btn.setStateTexture(this.techCooldownTexture);
                        btn.countdownText.text = Hud._getCountdowntext(buff.timeLeft);
                        btn.countdownText.visible = true;
                    } else {
                        btn.setStateTexture(null);
                        btn.countdownText.visible = false;
                    }
                }
                this.techTextures[bc.id] = this.techTextures[bc.id] || NGE.PIXI.TextureFromImage("img/ui/tech-icon-" + bc.id + ".png");
                btn.setIcon(this.techTextures[bc.id]);
                btn.buffIndex = s;
                btn.amountText.visible = false;
            }
        }
        for (var i=0;i<10;i++){
            if (!usedSlots[i]){
                btn = this.bottomBar.skillButtons.buttons[i+1];
                btn.setIcon(null);
                btn.setStateTexture(null);
                btn.countdownText.text = '';
                btn.buffIndex = -1;
                btn.amountText.visible = false;
            }
        }

//        var energy = 0;
//        if (stats) {
//            energy = stats.energy;
//        }
//        
//        var used = 0;//NGE.buffManager.getEnergyUsed();
//        this.bottomBar.energyBar.text.text=used+'/'+energy;
//        var ratio = 0;
//        if (energy > 0) {
//            ratio = used/energy;
//        }
//        if (ratio < 0) ratio = 0;
//        if (ratio > 1) ratio = 1;
//        this.bottomBar.energyBar.setValue(1-ratio);
    }
    
    // station deploy button
    btn = this.bottomBar.skillButtons.buttons[0];
    var station = NGE.stationManager && NGE.stationManager.getEquippedStation();
    if (station) {
        var id = station.config.id;
        this.stationTextures[id] = this.stationTextures[id] || NGE.PIXI.TextureFromImage("img/ui/station-icon-" + id + ".png");
        btn.setIcon(this.stationTextures[id]);
        if (station.timeLeft > 0) {
            btn.setStateTexture(this.techCooldownTexture);
            btn.countdownText.text = Hud._getCountdowntext(station.timeLeft);
            btn.countdownText.visible = true;
        } else {
            btn.setStateTexture(null);
            btn.countdownText.visible = false;
        }
        btn.amountText.text = station.amount;
        btn.amountText.visible = true;
    } else {
        btn.setIcon(null);
        btn.setStateTexture(null);
        btn.countdownText.visible = false;
        btn.amountText.visible = false;
    }
    
    // bottom bar visibility
    if (NGE.PIXI.myShip && (this.bottomBar.skillButtons.children.length > 0)) {
        this.bottomBar.container.visible = true;
    } else {
        this.bottomBar.container.visible = false;
    }
    
    this.shipStatusPanel.pos.x = 0;
    this.shipStatusPanel.pos.y = window.innerHeight - this.shipStatusPanel.size.y - 5;
    this.shipStatusPanel.container.visible = !!NGE.PIXI.myShip;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************