//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationCardSection(suffix, cards) { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.y = 324;
    
    this.suffix = suffix; // to be displayed above the section in the tab bar, after the equipped ship's name

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.fillAlpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    this.highlight = new PIXI.Graphics();
    this.highlight.lineStyle(1, 0x00dbff); 
    this.highlight.moveTo(0, 0);
    this.highlight.lineTo(8, 0);

    this.cards = cards;

    this.leftButton = new StationArrowButton();
    this.leftButton.pos.y = 137;
    this.leftButton.arrow.rotation = Math.PI;

    this.rightButton = new StationArrowButton();
    this.rightButton.pos.y = 137;

    this.scrollBar = new StationCardScrollBar();
    this.scrollBar.pos.y = this.size.y - this.scrollBar.size.y;

    this.container.addChild(this.background);
    this.container.addChild(this.highlight);
    this.addChild(this.cards);
    this.addChild(this.scrollBar);
    this.addChild(this.leftButton);
    this.addChild(this.rightButton);
}
StationCardSection.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationCardSection.prototype.update = function() {
    this.background.width = this.size.x;
    this.highlight.width = this.size.x;
    this.cards.size.x = this.size.x;
    this.scrollBar.size.x = this.size.x;
    if (this.cards.cards.length > this.cards.cardsPerPage) {
        this.rightButton.pos.x = this.size.x - this.rightButton.size.x;
        this.leftButton.container.visible = true;
        this.rightButton.container.visible = true;
    } else {
        this.leftButton.container.visible = false;
        this.rightButton.container.visible = false;
    }
    

    if(NGE.PIXI.Input.MOUSE_RELEASED) {
        if(this.leftButton.mouseOver) {
            this.cards.previousPage();
        }
        if(this.rightButton.mouseOver) {
            this.cards.nextPage();
        }
    }

    this.scrollBar.scroll = this.cards.cardIndex / (this.cards.cards.length - this.cards.cardsPerPage);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************