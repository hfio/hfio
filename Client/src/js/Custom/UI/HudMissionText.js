//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudMissionText() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.y = 15;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    this.waveText = new PIXI.Text("", { fontFamily: 'BlenderPro Medium', fontSize: 15, fill: 0xffffff });
    this.missionText = new PIXI.Text("", { fontFamily: 'BlenderPro Medium', fontSize: 15, fill: 0xffffff });

    this.container.addChild(this.background);
    this.container.addChild(this.waveText);
    this.container.addChild(this.missionText);
}
HudMissionText.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudMissionText.prototype.update = function() {
    if (NGE.WaveIndex > 0) {
        this.waveText.text = NGE.i18n.get("MS32") + " " + NGE.WaveIndex;
        this.waveText.visible = true;
    } else {
        this.waveText.text = "";
        this.waveText.visible = false;
    }
    
    if (NGE.accountData.activeMission) {
        var mission = NGE.Stats.getMission(NGE.accountData.activeMission);
        var progress = NGE.accountData.missionProgress;
        if (progress >= mission.targetAmount) {
            this.missionText.text = NGE.i18n.get("MS34") + ": " + NGE.i18n.get("MS35");
        } else {
            var text = NGE.i18n.get("MS34") + ": Kill ";
            if (progress > 0) {
                text += (mission.targetAmount - NGE.accountData.missionProgress) + "/";
            }
            text += mission.targetAmount;
            var target = NGE.Stats.getShipStatById(mission.target[0], "real_name");
            text += " " + target;
            for (var i = 1; i < mission.target.length; i++) {
                target = NGE.Stats.getShipStatById(mission.target[i], "real_name");
                text += " or " + target;
            }
            this.missionText.text = text;
        }
        this.missionText.visible = true;
    } else {
        this.missionText.text = "";
        this.missionText.visible = false;
    }
    
    this.waveText.y = (this.missionText.visible ? 15 : 0);
    
    this.size.x = Math.max(200, this.waveText.width + 136, this.missionText.width + 36);
    this.size.y = (this.waveText.visible ? 15 : 0) + (this.missionText.visible ? 15 : 0) + 3;
    this.waveText.x = Math.floor(this.size.x/2 - this.waveText.width/2);
    this.missionText.x = Math.floor(this.size.x/2 - this.missionText.width/2);
    this.background.width = this.size.x;
    this.background.height = this.size.y;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************