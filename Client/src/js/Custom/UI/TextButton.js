//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function TextButton(options) { // extends BaseButton
    BaseButton.call(this, options);
    // default options
    options = options || {};
    options.text = options.text || "Lorem Ipsum";
    options.fontFamily = options.fontFamily || 'BlenderPro Medium';
    options.fontSize = options.fontSize || 17;
    var color = BaseButton.COLORS[options.style];
    options.textColor = (options.textColor !== undefined) ? options.textColor : ((color !== undefined) ? color : 0xffffff); // can be zero = black
    options.textX = (options.textX !== undefined) ? options.textX : -1; // default: -1 -> align text to center

    this.text = new PIXI.Text(options.text, { fontFamily: options.fontFamily, fontSize: options.fontSize, fill: options.textColor });
    this.textX = options.textX;
    
    if (options.leftIcon) {
        this.leftIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage(options.leftIcon.image));
        this.leftIconMargin = options.leftIcon.margin;
    }
    if (options.rightIcon) {
        this.rightIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage(options.rightIcon.image));
        this.rightIconMargin = options.rightIcon.margin;
    }
    
    if (this.leftIcon) {
        this.container.addChild(this.leftIcon);
    }
    this.container.addChild(this.text);
    if (this.rightIcon) {
        this.container.addChild(this.rightIcon);
    }
    
    // update layout for size
    this.updateLayout();
}
TextButton.prototype = Object.create(BaseButton.prototype); // inherit from BaseButton
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
TextButton.prototype._setWidth = function(width) {
    BaseButton.prototype._setWidth.call(this, width);
    if (this.text) { this.text.x = (this.textX >= 0) ? this.textX : Math.floor(this.size.x/2 - this.text.width/2); }
    if (this.leftIcon) {
        this.leftIcon.x = this.text.x - this.leftIcon.width - this.leftIconMargin;
    }
    if (this.rightIcon) {
        this.rightIcon.x = this.text.x +this.text.width + this.rightIconMargin;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
TextButton.prototype._setHeight = function(height) {
    BaseButton.prototype._setHeight.call(this, height);
    if (this.text) { this.text.y = Math.floor(this.size.y/2 - this.text.height/2); }
    if (this.leftIcon) {
        this.leftIcon.y = Math.floor(this.size.y/2 - this.leftIcon.height/2);
    }
    if (this.rightIcon) {
        this.rightIcon.y = Math.floor(this.size.y/2 - this.rightIcon.height/2);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
