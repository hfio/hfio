//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationSideBarResource(action) { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 154;
    this.size.y = 38;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.fillAlpha = 0.5;
    this.background.drawRect(0, 0, this.size.x, this.size.y);
    this.background.endFill();

    this.highlight = new PIXI.Graphics();
    this.highlight.lineStyle(1, 0x00dbff); 
    this.highlight.moveTo(0, this.size.y);
    this.highlight.lineTo(this.size.x, this.size.y);
    this.highlight.lineTo(this.size.x, this.size.y - 7);

    this.nameText = new PIXI.Text('RESOURCE', { fontFamily: 'BlenderPro Medium', fontSize: 17, fill: 0x00dbff });
    this.nameText.x = 8;
    this.nameText.y = 4;

    this.numberText = new PIXI.Text('0', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.numberText.x = this.size.x - this.numberText.width - 21;
    this.numberText.y = 6;

    this.currencyIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/currency.png"));
    this.currencyIcon.x = this.size.x - this.currencyIcon.width - 5;
    this.currencyIcon.y = 7;

    this.barBackground = new PIXI.Graphics();
    this.barBackground.beginFill(0x000000);
    this.barBackground.drawRect(0, 0, this.size.x - 10, 3);
    this.barBackground.endFill();
    this.barBackground.x = 5;
    this.barBackground.y = 28;

    this.barForeground = new PIXI.Graphics();
    this.barForeground.beginFill(0x00dbff);
    this.barForeground.drawRect(0, 0, Math.floor(Math.random() * (this.size.x - 10)), 3);
    this.barForeground.endFill();
    this.barForeground.x = 5;
    this.barForeground.y = 28;
    
    this._warnTime = 0;
    this._action = action;
    this._cost = 0;
    
    this.enabled = true;

    this.container.addChild(this.background);
    this.container.addChild(this.highlight);
    this.container.addChild(this.nameText);
    this.container.addChild(this.numberText);
    this.container.addChild(this.currencyIcon);
    this.container.addChild(this.barBackground);
    this.container.addChild(this.barForeground);
}
StationSideBarResource.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationSideBarResource._WARN_DURATION = 0.3;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationSideBarResource.prototype.update = function(dt) {
    // this is the same functionality as in ActionWithCostButton, could be refactored later to DRY
    if (NGE.PIXI.Input.MOUSE_RELEASED && this.mouseOver) {
        var balance = NGE.accountData.bank;
        if (balance < this._cost) {
            this._warnTime = StationSideBarResource._WARN_DURATION;
            return;
        }
        if (this._action) {
            this._action();
        }
    }
    this.numberText.style.fill = (this._warnTime > 0) ? 0xff0000 : 0xffffff;
    this._warnTime = Math.max(0, this._warnTime - dt);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationSideBarResource.prototype.updateForResourceValue = function(value, cost) {
    var missingValue = 1 - value;
    
    var oldCost = this._cost;
    
    this._cost = Math.ceil(cost * missingValue);
    
    if (this._cost !== oldCost) {
        this.numberText.text = this._cost.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    this.numberText.x = this.size.x - this.numberText.width - 21;
    
    this.container.removeChild(this.barForeground);
    this.barForeground = new PIXI.Graphics();
    this.barForeground.beginFill(0x00dbff);
    this.barForeground.drawRect(0, 0, Math.floor(missingValue * (this.size.x - 10)), 3);
    this.barForeground.endFill();
    this.barForeground.x = 5;
    this.barForeground.y = 28;
    this.container.addChild(this.barForeground);
    
    if (value < 1) {
        this.container.alpha = 1;
        this.enabled = true;
    } else {
        this.container.alpha = 0.2;
        this.enabled = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************