// Closure
goog.provide('baseguielement');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function BaseGuiElement() {
    var container = this.container = new PIXI.Container();

    // BaseGuiElement.size
    this.size = {};
    this._width = 0;
    this._height = 0;
    Object.defineProperty(this.size, 'x', {
        get: this._getWidth.bind(this),
        set: this._setWidth.bind(this)
    });
    Object.defineProperty(this.size, 'y', {
        get: this._getHeight.bind(this),
        set: this._setHeight.bind(this)
    });

    // BaseGuiElement.pos
    this.pos = {};
    Object.defineProperty(this.pos, 'x', {
        get: function() { return container.x; },
        set: function(x) { container.x = x; }
    });
    Object.defineProperty(this.pos, 'y', {
        get: function() { return container.y; },
        set: function(y) { container.y = y; }
    });

    this.pivot = {x: 0, y: 0};
    this.align = {x: 'LEFT', y: 'TOP'};
    this.children = [];
    this.htmlElements = [];
    this.zIndex = 0;
    this.mouseOver = false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseGuiElement.prototype.update = function() {};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseGuiElement.prototype.addChild = function(element) {
    this.children.push(element);
    this.container.addChild(element.container);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseGuiElement.prototype.removeChild = function(element) {
    var index = this.children.indexOf(element);
    if(index >= 0) {
        this.children.splice(index, 1);
    }
    this.container.removeChild(element.container);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseGuiElement.prototype.addHTMLElement = function(element) {
    this.htmlElements.push(element);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BaseGuiElement.prototype._getWidth = function() { return this._width;};
BaseGuiElement.prototype._setWidth = function(width) { this._width = width; };
BaseGuiElement.prototype._getHeight = function() { return this._height; };
BaseGuiElement.prototype._setHeight = function(height) { this._height = height; };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************