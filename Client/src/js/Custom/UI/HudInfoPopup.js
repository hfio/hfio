//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudInfoPopup() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 200;
    this.size.y = 15;

    this.text = new PIXI.Text("", { fontFamily: 'BlenderPro Medium', fontSize: 28, fill: 0xffffff, align: "center" });
    
    this.container.addChild(this.text);
    
    this._timeLeft = 0;
}
HudInfoPopup.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudInfoPopup.FADEOUT_DURATION = 1;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudInfoPopup.prototype.update = function(dt) {
    if (this._timeLeft > 0) {
        this._timeLeft = Math.max(0, this._timeLeft - dt);
    }
    if (!NGE.PIXI.myShip || !NGE.PIXI.myShip.docked.get()) {
        if (this._timeLeft > HudInfoPopup.FADEOUT_DURATION) {
            this.text.alpha = 1;
        } else {
            this.text.alpha = this._timeLeft / HudInfoPopup.FADEOUT_DURATION;
        }
        this.text.x = Math.floor(this.size.x/2 - this.text.width/2);
        this.text.visible = NGE.PIXI.TutorialFinished;
    } else {
        this.text.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudInfoPopup.prototype.setText = function(text, duration) {
    this.text.text = text;
    this._timeLeft = duration || HudInfoPopup.FADEOUT_DURATION;
};
