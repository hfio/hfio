//*********************************************************************************************************************************************
// Floating credit marker that appears where an enemy is destroyed
//*********************************************************************************************************************************************
function CreditMarker(x, y, amount) { // extends BaseGuiElement
    BaseGuiElement.call(this);
    
    //debugging
    this.DEBUG = true;
    
    this.Log("CreditMarker create "+ x + "," + y + "," + amount);
    
    this._text = new PIXI.Text("+" + amount, { fontFamily: 'BlenderPro Medium', fontSize: 22, fontWeight: 'bold', fill: 0xffffff });
    this._moneyIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/currency.png"));
    
    this.size.x = this._text.width + this._moneyIcon.width + CreditMarker.PADDING;
    this.size.y = Math.max(this._text.height, this._moneyIcon.height);
    
    this._text.x = -this.size.x / 2;
    this._text.y = Math.round(-this._text.height/2);
    
    this._moneyIcon.x = this.size.x / 2 - this._moneyIcon.width;
    this._moneyIcon.y = Math.round(-this._moneyIcon.height/2);

    this.container.addChild(this._text);
    this.container.addChild(this._moneyIcon);
    
    this._timeLeft = CreditMarker.DURATION;
    
    this._worldX = x;
    this._worldY = y;
    
    this.pos.x = window.innerWidth / 2 + x - NGE.PIXI.Camera.x;
    this.pos.y = window.innerHeight / 2 + y - NGE.PIXI.Camera.y;
}
CreditMarker.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
CreditMarker.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[CREDIT_MARKER] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
CreditMarker.PADDING = 6;
CreditMarker.DURATION = 1;
CreditMarker.END_OFFSET = 100;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
CreditMarker.prototype.update = function(dt) {
    if (this._timeLeft <= 0) {
        this._text.destroy();
        this._moneyIcon.destroy();
        if (this.parent) {
            this.parent.removeChild(this);
        }
        return;
    }
    this._worldY -= dt * CreditMarker.END_OFFSET / CreditMarker.DURATION;
    this.pos.x = window.innerWidth / 2 + this._worldX - NGE.PIXI.Camera.x;
    this.pos.y = window.innerHeight / 2 + this._worldY - NGE.PIXI.Camera.y;
    this._timeLeft -= dt;
    this.container.alpha = this._timeLeft / CreditMarker.DURATION;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************