//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationShipCards() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.cardPadding = 12;
    this.cardIndex = 0;
    this.cardsAdded = 0;
    this.cardsPerPage = 0;

    this.cards = [];
}
StationShipCards.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCards.prototype.setShipList = function(list) {
    this.cards.forEach((card) => this.removeChild(card));
    this.cards = [];
    list.forEach((shipName) => {
            var stats = NGE.Stats.Ships.find((ship) => ship.data_name === shipName);
            if(!stats) { 
                throw new Error('StationShipCards.setShipList: could not find stats for ship'); 
            }
            if (stats.card_image) {
                var card = new StationShipCard();
                card.shipId = shipName;
                card.shipNameText.text = stats.real_name;
                card.image = stats.card_image;
                card.purchaseButton.cost = stats.unlock_price_credits;
                this.cards.push(card);
            }
    });
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCards.prototype.update = function() {
    if(this.cards.length === 0) { return; }

    this.cardsPerPage = Math.floor(this.size.x / (this.cards[0].size.x + this.cardPadding));

    if(this.cardsAdded < this.cardsPerPage || this.cardsAdded > this.cardsPerPage) {
        this.reflowCards();
    }

    // update card positions
    for(var i=0; i<this.children.length; i++) {
        var card = this.children[i];
        card.pos.x = this.cardPadding + i * (card.size.x + this.cardPadding);
        card.pos.y = 13;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCards.prototype.reflowCards = function() {
    // remove all cards
    this.cardsAdded = 0;
    for(var i=0; i<this.cards.length; i++) {
        this.removeChild(this.cards[i]);
    }
    // only a subset of cards gets added
    for(var i=this.cardIndex; i<Math.min(this.cardIndex + this.cardsPerPage, this.cards.length); i++) {
        this.addChild(this.cards[i]);
        this.cardsAdded++;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCards.prototype.previousPage = function() {
    this.cardIndex = Math.max(0, this.cardIndex - this.cardsPerPage);
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCards.prototype.nextPage = function() {
    this.cardIndex = Math.max(0, Math.min(this.cards.length - this.cardsPerPage, this.cardIndex + this.cardsPerPage));
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************