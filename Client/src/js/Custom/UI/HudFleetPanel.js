//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudFleetPanel() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 180;
    this.size.y = 160;
    
    this.rightPadding = 5;
    
    var textStyle = { fontFamily: 'BlenderPro Medium', fontSize: 14, fontWeight: 'normal', fill: 0xffffff, align: 'right' };
    var grayTextStyle = { fontFamily: 'BlenderPro Medium', fontSize: 14, fontWeight: 'normal', fill: 0xaaaaaa, align: 'right' };
    

    this.copyLinkText = document.getElementById("urlfleet");
    
    this.copyLinkText.textContent = "COPY FLEET LINK";
    this.copyLinkText.style.fontFamily = textStyle.fontFamily;
    this.copyLinkText.style.fontSize = textStyle.fontSize + "px";
    this.copyLinkText.style.fontWeight = textStyle.fontWeight;
    
    this._displayedBonus = undefined;
    
    this.fleetBonusText = new PIXI.Text("", textStyle);
    this.fleetBonusText.x = 0;
    this.fleetBonusText.y = 23;
    
    this.shipNamesText = new PIXI.Text("", textStyle);
    this.shipNamesText.x = 0;
    this.shipNamesText.y = 39;
    
    this.outOfRangeShipNamesText = new PIXI.Text("", grayTextStyle);
    this.outOfRangeShipNamesText.x = 0;
    this.outOfRangeShipNamesText.y = 39;
    
    this.addHTMLElement(this.copyLinkText);
    
    this.container.addChild(this.fleetBonusText);
    this.container.addChild(this.shipNamesText);
    this.container.addChild(this.outOfRangeShipNamesText);
}
HudFleetPanel.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudFleetPanel.prototype.update = function() {
    this.copyLinkText.style.left = Math.round(this.pos.x + this.size.x - this.copyLinkText.offsetWidth - this.rightPadding) + "px";
    this.copyLinkText.style.top = Math.round(this.pos.y + 3) + "px";
    
    if (NGE.PIXI.myShip) {
        if (this._displayedBonus !== NGE.DATA.fleet_credit_share_percent) {
            this._displayedBonus = NGE.DATA.fleet_credit_share_percent;
            this.fleetBonusText.text = "+" + (this._displayedBonus || "?") + "% FROM FLEET:";
            this.fleetBonusText.x = this.size.x - this.fleetBonusText.width - this.rightPadding;
        }
        
        var C = NGE.Core;
        var myShip = NGE.PIXI.myShip;
        var x = myShip.x.get();
        var y = myShip.y.get();
        var myTeam = myShip.team.get();
        var rangeSq = NGE.DATA.fleet_credit_share_range_sq;
        var inRangeShipNames = [];
        var outOfRangeShipNames = [];
        C.ExecuteForObjects("Ship", (ship) => {
            if ((ship !== myShip) && (ship.team.get() === myTeam)) { 
                if (C.MATH.VecLengthSquared(x - ship.x.get(), y - ship.y.get()) <= rangeSq) {
                    inRangeShipNames.push(ship.username.get() || HudUsernameButton.DEFAULT_TEXT);
                } else {
                    outOfRangeShipNames.push(ship.username.get() || HudUsernameButton.DEFAULT_TEXT);
                }
            }
        });
        this.shipNamesText.text = inRangeShipNames.join("\n");
        this.outOfRangeShipNamesText.text = outOfRangeShipNames.join("\n");
        
        this.shipNamesText.x = this.size.x - this.shipNamesText.width - this.rightPadding;
        
        this.outOfRangeShipNamesText.x = Math.floor(this.size.x - this.outOfRangeShipNamesText.width - this.rightPadding);
        this.outOfRangeShipNamesText.y = Math.floor(this.shipNamesText.y + ((inRangeShipNames.length > 0) ? this.shipNamesText.height : 0));
        
        if ((inRangeShipNames.length > 0) || (outOfRangeShipNames.length > 0)) {
            this.fleetBonusText.visible = true;
            this.shipNamesText.visible = true;
            this.outOfRangeShipNamesText.visible = true;
        } else {
            this.fleetBonusText.visible = false;
            this.shipNamesText.visible = false;
            this.outOfRangeShipNamesText.visible = false;
        }
    } else {
        this.fleetBonusText.visible = false;
        this.shipNamesText.visible = false;
        this.outOfRangeShipNamesText.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************