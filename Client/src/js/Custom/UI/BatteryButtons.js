//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function BatteryButtons() { // extends BaseGuiElement
    BaseGuiElement.call(this);
    
    this.padding = 3;

    ['SHIPS', 'TECH', 'STATIONS', 'MISSIONS'].forEach((id) => {
        var button = new BatteryButton("tab_" + id.toLowerCase());
        button.id = id;
        this.addChild(button);
    });
}
BatteryButtons.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BatteryButtons.prototype.update = function() {
    var buttonWidth = (this.size.x - (this.children.length - 1) * this.padding) / this.children.length;
    for(var i=0; i<this.children.length; i++) {
        var button = this.children[i];
        button.pos.x = Math.round(i * (buttonWidth + this.padding));
        button.size.x = Math.round(buttonWidth);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************