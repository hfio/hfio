//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oNinePatch(path, settings) {
    Object.defineProperty(this, 'width', {get: this._getWidth, set: this._setWidth});
    Object.defineProperty(this, 'height', {get: this._getHeight, set: this._setHeight});

    this._width = settings.width || 0;
    this._height = settings.height || 0;
    this._left = settings.left || 0;
    this._top = settings.top || 0;
    this._right = settings.right || 0;
    this._bottom = settings.bottom || 0;
    
    path = noCache(path);

    if(!(path in PIXI.loader.resources)) {
        throw new Error('oNinePatch: requires preloaded image ' + path);
    }
    var baseTexture = new PIXI.BaseTexture(PIXI.loader.resources[path].data, PIXI.SCALE_MODES.NEAREST)
    this._topLeft = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(0, 0, this._left, this._top)));
    this._topCenter = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(this._left, 0, baseTexture.width - this._right - this._left, this._top)));
    this._topRight = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(baseTexture.width - this._right, 0, this._right, this._top)));
    this._centerLeft = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(0, this._top, this._left, baseTexture.height - this._bottom - this._top)));
    this._centerCenter = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(this._left, this._top, baseTexture.width - this._right - this._left, baseTexture.height - this._bottom - this._top)));
    this._centerRight = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(baseTexture.width - this._right, this._top, this._right, baseTexture.height - this._bottom - this._top)));
    this._bottomLeft = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(0, baseTexture.height - this._bottom, this._left, this._bottom)));
    this._bottomCenter = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(this._left, baseTexture.height - this._bottom, baseTexture.width - this._right - this._left, this._bottom)));
    this._bottomRight = new PIXI.Sprite(new PIXI.Texture(baseTexture, new PIXI.Rectangle(baseTexture.width - this._right, baseTexture.height - this._bottom, this._right, this._bottom)));
    
    this._assemble();

    this.container = new PIXI.Container();
    this.container.addChild(this._topLeft);
    this.container.addChild(this._topCenter);
    this.container.addChild(this._topRight);
    this.container.addChild(this._centerLeft);
    this.container.addChild(this._centerCenter);
    this.container.addChild(this._centerRight);
    this.container.addChild(this._bottomLeft);
    this.container.addChild(this._bottomCenter);
    this.container.addChild(this._bottomRight);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oNinePatch.prototype._assemble = function() {
    this._topCenter.x = this._left;
    this._topCenter.width = this._width - this._left - this._right;
    this._topRight.x = this._width - this._right;
    this._centerLeft.y = this._top;
    this._centerLeft.height = this._height - this._top - this._bottom;
    this._centerCenter.x = this._left;
    this._centerCenter.y = this._top;
    this._centerCenter.width = this._width - this._left - this._right;
    this._centerCenter.height = this._height - this._top - this._bottom;
    this._centerRight.x = this._width - this._right;
    this._centerRight.y = this._top;
    this._centerRight.height = this._height - this._top - this._bottom;
    this._bottomLeft.y = this._height - this._bottom;
    this._bottomCenter.x = this._left;
    this._bottomCenter.y = this._height - this._bottom;
    this._bottomCenter.width = this._width - this._left - this._right;
    this._bottomRight.x = this._width - this._right;
    this._bottomRight.y = this._height - this._bottom;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oNinePatch.prototype._getWidth = function() { return this._width; };
oNinePatch.prototype._setWidth = function(width) { this._width = width; this._assemble(); };
oNinePatch.prototype._getHeight = function() { return this._height; };
oNinePatch.prototype._setHeight = function(height) { this._height = height; this._assemble(); };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************