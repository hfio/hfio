//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function GuiManager() {
    this.children = [];
    this.container = new PIXI.Container();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
GuiManager.prototype.update = function(dt) {
    this.children.sort(function(a, b) {
        return a.zIndex - b.zIndex;
    });
    for(var i = 0; i < this.children.length; i++) {
        this.updateRecursive(this.children[i], this.children[i].children, this.children[i].htmlElements, 0, 0, dt);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
GuiManager.prototype.updateRecursive = function(parent, children, htmlElements, x, y, dt) {
    var i;
    if(parent.container.visible) {
        var newX = x + parent.pos.x;
        var newY = y + parent.pos.y;
        parent.mouseOver = (NGE.PIXI.Input.MouseX >= newX && NGE.PIXI.Input.MouseX < newX + parent.size.x && NGE.PIXI.Input.MouseY >= newY && NGE.PIXI.Input.MouseY < newY + parent.size.y);
        parent.update(dt);
        for(i = 0; i < children.length; i++) {
            this.updateRecursive(children[i], children[i].children, children[i].htmlElements, newX, newY, dt);
        }
        for (i = 0; i < htmlElements.length; i++) {
            htmlElements[i].hidden = false;
        }
    } else {
        this.hideHTML(children, htmlElements);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
GuiManager.prototype.hideHTML = function(children, htmlElements) {
    var i;
    for (i = 0; i < htmlElements.length; i++) {
        htmlElements[i].hidden = true;
    }
    for (i = 0; i < children.length; i++) {
        this.hideHTML(children[i].children, children[i].htmlElements);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************