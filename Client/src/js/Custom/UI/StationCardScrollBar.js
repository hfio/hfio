//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationCardScrollBar() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.y = 7;
    this.scroll = 0; // position between 0, left and 1, right
    this._scroll = 0;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.fillAlpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    // OK, this border business would be cleaner if it were a nine-patch...
    this.borderLeft = new PIXI.Graphics();
    this.borderLeft.lineStyle(1, 0x00dbff); 
    this.borderLeft.moveTo(0, 0);
    this.borderLeft.lineTo(0, this.size.y);
    this.borderRight = new PIXI.Graphics();
    this.borderRight.lineStyle(1, 0x00dbff); 
    this.borderRight.moveTo(0, 0);
    this.borderRight.lineTo(0, this.size.y);
    this.borderTopAndBottom = new PIXI.Graphics();
    this.borderTopAndBottom.lineStyle(1, 0x00dbff); 
    this.borderTopAndBottom.moveTo(0, 0);
    this.borderTopAndBottom.lineTo(8, 0);
    this.borderTopAndBottom.moveTo(0, this.size.y);
    this.borderTopAndBottom.lineTo(8, this.size.y);

    this.bar = new PIXI.Graphics();
    this.bar.beginFill(0x00dbff);
    this.bar.drawRect(0, 0, 86, this.size.y);
    this.bar.endFill();

    this.container.addChild(this.background);
    this.container.addChild(this.borderLeft);
    this.container.addChild(this.borderRight);
    this.container.addChild(this.borderTopAndBottom);
    this.container.addChild(this.bar);
}
StationCardScrollBar.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationCardScrollBar.prototype.update = function() {
    this.background.width = this.size.x;
    this.borderRight.x = this.size.x - 1;
    this.borderTopAndBottom.width = this.size.x;
    
    this._scroll += (this.scroll - this._scroll) * NGE.Core.TIME.Delta * 12;
    this.bar.x = Math.round(this._scroll * (this.size.x - this.bar.width));
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************