// Closure
goog.require('basebutton'); // later in the alphabet, so we need to require it manually to ensure closure loads that file first
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function ActionWithCostButton(options) { // extends BaseButton
    BaseButton.call(this, options);
    // default options:
    options = options || {};
    // action
    options.actionText = (typeof options.actionText === "string") ? options.actionText : "Action";
    options.actionFontFamily = options.actionFontFamily || 'BlenderPro Medium';
    options.actionFontSize = options.actionFontSize || 17;
    var color = BaseButton.COLORS[options.style];
    options.actionTextColor = (options.actionTextColor !== undefined) ? options.actionTextColor : ((color !== undefined) ? color : 0xffffff); // can be zero = black
    options.action = options.action || function () { constole.warn("Buy/sell action not implemented!"); };
    options.sell = options.sell || false;
    // cost
    options.cost = options.cost || 500;
    options.costFontFamily = options.costFontFamily || 'BlenderPro Medium';
    options.costFontSize = options.costFontSize || 14;
    options.costTextColor = (options.costTextColor !== undefined) ? options.actionTextColor : 0xffffff; // can be zero = black
    options.costCurrency = options.costCurrency || 'CR';
    // general
    options.marginLeft = options.marginLeft || 0;
    options.marginRight = options.marginRight || 0;
    options.marginCurrency = options.marginCurrency || 4; // space between the cost text and currency icon

    // initializing properties
    
    this._sell = options.sell;
    this._cost = options.cost;
    this._currency = options.costCurrency;
    this._action = options.action;
    this._costTextColor = options.costTextColor;

    this._actionText = new PIXI.Text(options.actionText, {fontFamily: options.actionFontFamily, fontSize: options.actionFontSize, fill: options.actionTextColor});
    this._costText = new PIXI.Text(ActionWithCostButton._getCostText(this._cost), {fontFamily: options.costFontFamily, fontSize: options.costFontSize, fill: this._costTextColor});

    // alternative to text (positioned the same way -> icon + text layout not implemented)
    if (options.icon) {
        this._icon = new PIXI.Sprite(NGE.PIXI.TextureFromImage(options.icon));
    }

    this._moneyIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage(this._getCurrencyInfo().ICON));
    
    this._warnTime = 0;

    this.marginLeft = options.marginLeft;
    this.marginRight = options.marginRight;
    this.marginCurrency = options.marginCurrency;

    this.container.addChild(this._actionText);
    this.container.addChild(this._costText);
    this.container.addChild(this._moneyIcon);
    if (this._icon) {
        this.container.addChild(this._icon);
    }

    // update layout for size
    this.updateLayout();
}
ActionWithCostButton.prototype = Object.create(BaseButton.prototype); // inherit from BaseButton
ActionWithCostButton.prototype.constructor = ActionWithCostButton;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
ActionWithCostButton._getCostText = function (cost) {
    return cost.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
ActionWithCostButton._WARN_DURATION = 0.3;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
ActionWithCostButton._CURRENCY_INFO = {
    CR: {
        ICON: "img/ui/currency.png",
        BALANCE: "bank"
    },
    TP: {
        ICON: "img/ui/currency-purple.png",
        BALANCE: "techpoints"
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
ActionWithCostButton.prototype._getCurrencyInfo = function() {
    return ActionWithCostButton._CURRENCY_INFO[this._currency];
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
ActionWithCostButton.prototype._setWidth = function (width) {
    BaseButton.prototype._setWidth.call(this, width);
    if (this._actionText) { this._actionText.x = this.marginLeft; }
    if (this._icon) { this._icon.x = this.marginLeft; }
    if (this._costText) { this._costText.x = width - this.marginRight - this._moneyIcon.width - this.marginCurrency - this._costText.width; }
    if (this._moneyIcon) { this._moneyIcon.x = width - this.marginRight - this._moneyIcon.width; }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
ActionWithCostButton.prototype._setHeight = function (height) {
    BaseButton.prototype._setHeight.call(this, height);
    if (this._actionText) { this._actionText.y = Math.floor(this.size.y / 2 - this._actionText.height / 2); }
    if (this._icon) { this._icon.y = Math.floor(this.size.y / 2 - this._icon.height / 2); }
    if (this._costText) { this._costText.y = Math.floor(this.size.y / 2 - this._costText.height / 2); }
    if (this._moneyIcon) { this._moneyIcon.y = Math.floor(this.size.y / 2 - this._moneyIcon.height / 2); }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
ActionWithCostButton.prototype.update = function (dt) {
    BaseButton.prototype.update.call(this, dt);
    if (NGE.PIXI.Input.MOUSE_RELEASED && this.mouseOver) {
        if (!this._sell && (this._cost > 0)) {
            var balance = NGE.accountData[this._getCurrencyInfo().BALANCE];
            if (balance < this._cost) {
                this._warnTime = ActionWithCostButton._WARN_DURATION;
                return;
            }
        }
        if (this._action) {
            this._action();
        }
    }
    this._costText.style.fill = (this._warnTime > 0) ? 0xff0000 : this._costTextColor;
    this._warnTime = Math.max(0, this._warnTime - dt);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Object.defineProperty(ActionWithCostButton.prototype, 'actionText', {
    get: function() { return this._actionText.text; },
    set: function(value) { this._actionText.text = value; this.updateLayout(); }
});
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Object.defineProperty(ActionWithCostButton.prototype, 'cost', {
    get: function() { return this._cost; },
    set: function(value) { 
        this._cost = value; 
        this._costText.text = ActionWithCostButton._getCostText(value); 
        this.updateLayout(); 
    }
});