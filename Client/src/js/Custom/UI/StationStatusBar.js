//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationStatusBar() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.y = 28;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.fillAlpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    this.highlight = new PIXI.Graphics();
    this.highlight.lineStyle(1, 0x00dbff); 
    this.highlight.moveTo(0, this.size.y);
    this.highlight.lineTo(16, this.size.y);
    
    this.newsText = new PIXI.Text("STATION NEWS", { fontFamily: 'BlenderPro Medium', fontSize: 15, fill: 0xffffff});
    this.newsText.y = Math.floor(this.size.y / 2 - this.newsText.height / 2);
    this.newsText.x = 12;

    this.container.addChild(this.background);
    this.container.addChild(this.highlight);
    this.container.addChild(this.newsText);
    
    this.twitterButton = document.getElementById("twitterbutton");
    this.addHTMLElement(this.twitterButton);
    
    this.discordButton = document.getElementById("discordbutton");
    this.addHTMLElement(this.discordButton);
    
    this.ioGamesButton = document.getElementById("iogamesbutton");
    this.addHTMLElement(this.ioGamesButton);
}
StationStatusBar.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationStatusBar.prototype.update = function() {
    this.background.width = this.size.x;
    this.highlight.width = this.size.x;
    
    var news = NGE.Stats.Global.station_news;
    if (this.newsText.text !== news) {
        this.newsText.text = news;
    }
    
    this.twitterButton.style.left = Math.round(this.pos.x + this.newsText.x + this.newsText.width + 12) + "px";
    this.twitterButton.style.top = Math.round(this.pos.y + this.size.y / 2 - this.twitterButton.offsetHeight / 2 + 1) + "px";
    
    this.discordButton.style.left = this.pos.x + this.newsText.x + this.newsText.width + 12 + this.twitterButton.offsetWidth + 12 + "px";
    this.discordButton.style.top = this.twitterButton.style.top;
    
    this.ioGamesButton.style.left = this.pos.x + this.newsText.x + this.newsText.width + 12 + this.twitterButton.offsetWidth + 12 + this.discordButton.offsetWidth + 12 + "px";
    this.ioGamesButton.style.top = this.twitterButton.style.top;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************