//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudWallet() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 128;
    this.size.y = 42;

    var backgroundHeight = 40;
    var background = new PIXI.Graphics();
    background.beginFill(0x000000);
    background.fillAlpha = 0.5;
    background.drawPolygon([
        1, 0,
        1, 27,
        15, backgroundHeight,
        this.size.x, backgroundHeight,
        this.size.x, 0
    ]);
    background.endFill();

    var highlights = new PIXI.Graphics();
    // corner:
    highlights.lineStyle(1, 0x00dbff);
    highlights.moveTo(0, 22);
    highlights.lineTo(0, 26);
    highlights.lineTo(14, backgroundHeight + 1);
    highlights.lineTo(19, backgroundHeight + 1);
    // fat chunk:
    highlights.lineStyle(0, 0x000000);
    highlights.beginFill(0x00dbff);
    highlights.moveTo(this.size.x - 45, backgroundHeight);
    highlights.lineTo(this.size.x - 45 + 2, backgroundHeight + 2);
    highlights.lineTo(this.size.x, backgroundHeight + 2);
    highlights.lineTo(this.size.x, backgroundHeight);
    highlights.endFill();

    this.bankBalance = new PIXI.Text('50,000', { fontFamily: 'BlenderPro Medium', fontSize: 12, fill: 0xffffff });
    this.bankBalance.y = 3;

    var inBank = new PIXI.Text(NGE.i18n.MenuStatic.MS3.english, { fontFamily: 'BlenderPro Medium', fontSize: 12, fill: 0xffffff });
    inBank.x = 82;
    inBank.y = 3;

    var bankIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/currency.png"));
    bankIcon.x = 65;
    bankIcon.y = 2;

    this.shipBalance = new PIXI.Text('50,000', { fontFamily: 'BlenderPro Medium', fontSize: 12, fill: 0xffffff });
    this.shipBalance.y = 21;

    var onShip = new PIXI.Text(NGE.i18n.MenuStatic.MS4.english, { fontFamily: 'BlenderPro Medium', fontSize: 12, fill: 0xffffff });
    onShip.x = 82;
    onShip.y = 21;

    var shipIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/currency.png"));
    shipIcon.x = 65;
    shipIcon.y = 20;
    
    this.shipGroup = new PIXI.Container();
    
    this.tpBalance = new PIXI.Text('50,000', { fontFamily: 'BlenderPro Medium', fontSize: 12, fill: 0xffffff });
    this.tpBalance.y = 21;
    
    var techPoints = new PIXI.Text("TECH", { fontFamily: 'BlenderPro Medium', fontSize: 12, fill: 0xffffff });
    techPoints.x = 82;
    techPoints.y = 21;

    var techPointsIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/currency-purple.png"));
    techPointsIcon.x = 65;
    techPointsIcon.y = 20;
    
    this.tpGroup = new PIXI.Container();
    
    this._cashOutButton = new TextButton({text: "CASH OUT", style: "blue"});
    this._cashOutButton.pos.x = -this._cashOutButton.size.x;
    
    this._cashOutPopup = document.getElementById("cashOutPopup");
    this._walletAddress = document.getElementById("walletAddress");
    this._sendButton =  document.getElementById("sendToWallet");
    this._cancelButton =  document.getElementById("cancelCashOut");
    
    this._walletAddress.value = "";
    this._sendButton.disabled = true;
    
    this._walletAddress.onkeyup = (e)=>{
        this._sendButton.disabled = this._walletAddress.value === "";
        if (e.keyCode === 13) {
            this.cashOut();
        } else if (e.keyCode === 27) {
            this.cancelEditing();
        }
    };
    
    this._sendButton.onclick = ()=>{
        this.cashOut();
    };
    
    this._cancelButton.onclick = ()=>{
        this.cancelEditing();
    };
    
    // animation when increasing
    this._bankAnim = HudWallet._createAnimationData();
    this._shipAnim = HudWallet._createAnimationData();
    this._tpAnim = HudWallet._createAnimationData();
    // animation when switching to showing TP
    this._tpSwitchAnim = HudWallet._createAnimationData(true);

    this.container.addChild(background);
    this.container.addChild(highlights);
    
    this.container.addChild(this.bankBalance);
    this.container.addChild(inBank);
    this.container.addChild(bankIcon);
    
    this.shipGroup.addChild(this.shipBalance);
    this.shipGroup.addChild(onShip);
    this.shipGroup.addChild(shipIcon);
    this.container.addChild(this.shipGroup);
    
    this.tpGroup.addChild(this.tpBalance);
    this.tpGroup.addChild(techPoints);
    this.tpGroup.addChild(techPointsIcon);
    this.container.addChild(this.tpGroup);
    
    this.addChild(this._cashOutButton);
    
    this.tpGroup.alpha = 0;
}
HudWallet.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
////*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.GROW_DURATION = 1;
HudWallet.GROW_SCALE = 1.5;
HudWallet.SWITCH_DURATION = 1;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet._createAnimationData = function(float) {
    return {
        startValue: 0,
        endValue: 0,
        currentValue: 0,
        timeLeft: 0,
        float: float || false
    };
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet._startAnimation = function(animData, value) {
    if (value !== animData.endValue) {
        animData.endValue = value;
        animData.startValue = animData.currentValue;
        animData.timeLeft = HudWallet.GROW_DURATION;
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet._animate = function(animData, duration, dt) {
    var ratio = animData.timeLeft / duration;
    animData.currentValue = animData.endValue + ratio * (animData.startValue - animData.endValue);
    if (!animData.float) {
        animData.currentValue = Math.max(0, Math.floor(animData.currentValue));
    }
    if (animData.timeLeft > 0) {
        animData.timeLeft -= dt;
    } else {
        animData.timeLeft = 0;
    }
    return ratio;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet._animateGrow = function(text, animData, dt) {
    var ratio = HudWallet._animate(animData, HudWallet.GROW_DURATION, dt);
    var scale = 1 + ratio * (HudWallet.GROW_SCALE - 1);
    text.scale.x = scale;
    text.scale.y = scale;
    if (text.visible) {
        text.text = animData.currentValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        text.x = 60 - text.width;
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet._animateSwitch = function(group1, group2, animData, dt) {
    HudWallet._animate(animData, HudWallet.SWITCH_DURATION, dt);
    group1.alpha = Math.max(0, 1 - 2 * animData.currentValue);
    group2.alpha = Math.max(0, 2 * animData.currentValue - 1);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.prototype.isEditing = function() {
    return this._cashOutPopup && !this._cashOutPopup.hidden;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.prototype.cashOut = function() {
    if (this._walletAddress.value) {
        NGE.WS.Send('cashout', {wallet: this._walletAddress.value});
        this._cashOutPopup.hidden = true;
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.prototype.cancelEditing = function() {
    this._cashOutPopup.hidden = true;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.prototype.update = function(dt) {
    this._cashOutButton.container.visible = NGE.WS.isLoggedIn;
    var hasMoney = (this._bankAnim.endValue > 0) && (this._bankAnim.currentValue > 0);
    if (this._cashOutButton.container.visible) {
        this._cashOutButton.container.alpha = (hasMoney && NGE.PIXI.TutorialFinished) ? 1 : 0.2;
    }
    if(NGE.PIXI.Input.MOUSE_RELEASED) {
        if(this._cashOutButton.container.visible && this._cashOutButton.mouseOver && hasMoney && NGE.PIXI.TutorialFinished) {
            NGE.PIXI.chatMode = OBJ_Pixi.CHAT_MODE.OFF;
            NGE.PIXI.HUD.usernameButton.cancelEditing();
            this._cashOutPopup.hidden = false;
            this._walletAddress.focus();
        }
    }
    if (NGE.PIXI.myShip && NGE.PIXI.myShip.docked.get()) {
        if (this._shipAnim.currentValue === 0) {
            HudWallet._startAnimation(this._tpSwitchAnim, 1);
        }
    } else {
        HudWallet._startAnimation(this._tpSwitchAnim, 0);
    }
    HudWallet._animateSwitch(this.shipGroup, this.tpGroup, this._tpSwitchAnim, dt);
    // change animation
    HudWallet._animateGrow(this.bankBalance, this._bankAnim, dt);
    HudWallet._animateGrow(this.shipBalance, this._shipAnim, dt);
    HudWallet._animateGrow(this.tpBalance, this._tpAnim, dt);
    
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.prototype.setBankBalance = function(value) {
    HudWallet._startAnimation(this._bankAnim, value);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.prototype.setShipBalance = function(value) {
    HudWallet._startAnimation(this._shipAnim, value);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudWallet.prototype.setTPBalance = function(value) {
    HudWallet._startAnimation(this._tpAnim, value);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************