//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationSortButton() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.y = 26;
    this.padding = 12;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    this.text = new PIXI.Text('BUTTON', { fontFamily: 'BlenderPro Medium', fontSize: 15, fill: 0x00dbff });
    this.text.y = Math.floor(this.size.y/2 - this.text.height/2);

    this.container.addChild(this.background);
    this.container.addChild(this.text);
}
StationSortButton.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationSortButton.prototype.update = function() {
    this.size.x = this.text.width + this.padding * 2;
    this.background.width = this.size.x;
    this.text.x = Math.floor(this.size.x/2 - this.text.width/2);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************