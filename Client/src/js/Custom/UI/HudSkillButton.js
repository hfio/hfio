//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudSkillButton() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 50;
    this.size.y = 67;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, this.size.x - 2, this.size.y - 1);
    this.background.endFill();
    this.background.x = 1;
    
    this.iconBackground = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/tech-icon-background.png"));
    this.iconBackground.x = 4;
    this.iconBackground.y = 22;

    this._stateImage = new PIXI.Sprite();
    this._stateImage.x = 4;
    this._stateImage.y = 22;
    this._stateImage.visible = false;

    this.icon = new PIXI.Sprite();
    this.icon.x = 4;
    this.icon.y = 22;
    this.icon.visible = false;

    var highlightWidth = 12;
    var highlight = new PIXI.Graphics();
    highlight.lineStyle(1, 0x00dbff);
    highlight.moveTo(highlightWidth, 18);
    highlight.lineTo(0, 18);
    highlight.lineTo(0, this.size.y);
    highlight.lineTo(highlightWidth, this.size.y);
    highlight.moveTo(this.size.x - highlightWidth, 18);
    highlight.lineTo(this.size.x, 18);
    highlight.lineTo(this.size.x, this.size.y);
    highlight.lineTo(this.size.x - highlightWidth, this.size.y);

    this.slotText = new PIXI.Text('#', { fontFamily: 'BlenderPro Medium', fontSize: 16, fill: 0x00dbff });
    this.slotText.y = 2;
    
    this.countdownText = new PIXI.Text('#', { fontFamily: 'BlenderPro Medium', fontSize: 16, fill: 0xffffff });
    
    this.amountText = new PIXI.Text('#', { fontFamily: 'BlenderPro Medium', fontSize: 16, fill: 0xffffff });
    
    this.amountBackground = new PIXI.Graphics();
    this.amountBackground.beginFill(0x000000);
    this.amountBackground.alpha = 0.5;
    this.amountBackground.drawRect(0, 0, 1, 1);
    this.amountBackground.endFill();
    this.amountBackground.x = 1;

    this.container.addChild(this.background);
    this.container.addChild(this.iconBackground);
    this.container.addChild(this.icon);
    this.container.addChild(this.amountBackground);
    this.container.addChild(this._stateImage);
    this.container.addChild(highlight);
    this.container.addChild(this.slotText);
    this.container.addChild(this.countdownText);
    this.container.addChild(this.amountText);
}
HudSkillButton.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButton.prototype.update = function() {
    this.slotText.x = Math.floor(this.size.x/2 - this.slotText.width/2);
    this.countdownText.x = Math.floor(this.iconBackground.x + this.iconBackground.width / 2 - this.countdownText.width / 2);
    this.countdownText.y = Math.floor(this.iconBackground.y + this.iconBackground.height / 2 - this.countdownText.height / 2);
    if (this.amountText.visible) {
        this.amountText.x = this.iconBackground.x + 3;
        this.amountText.y = this.iconBackground.y + this.iconBackground.height - this.amountText.height - 2;
        this.amountBackground.x = this.iconBackground.x + 1;
        this.amountBackground.y = this.amountText.y - 2;
        this.amountBackground.width = this.amountText.width + 3;
        this.amountBackground.height = this.amountText.height + 3;
        this.amountBackground.visible = true;
    } else {
        this.amountBackground.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButton.prototype.setIcon = function (texture) {
    if (texture) {
        this.icon.texture = texture;
        this.icon.visible = true;
        this.icon.x = this.iconBackground.x + this.iconBackground.width / 2 - this.icon.width / 2;
        this.icon.y = this.iconBackground.y + this.iconBackground.height / 2 - this.icon.height / 2;
    } else {
        this.icon.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButton.prototype.setStateTexture = function (texture) {
    if (texture) {
        this._stateImage.texture = texture;
        this._stateImage.visible = true;
    } else {
        this._stateImage.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************