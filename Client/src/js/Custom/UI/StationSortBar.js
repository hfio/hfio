//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationSortBar() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.y = 26;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    this.highlights = new PIXI.Graphics();
    this.highlights.lineStyle(1, 0x00dbff); 
    this.highlights.moveTo(0, this.size.y);
    this.highlights.lineTo(16, this.size.y);

    this.text = new PIXI.Text(NGE.i18n.MenuStatic.MS12.english + ':', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.text.y = Math.floor(this.size.y / 2 - this.text.height / 2);
    this.text.x = 12;

    //this.buttons = new StationSortButtons();
    //this.buttons.pos.x = 74;

    this.container.addChild(this.background);
    this.container.addChild(this.text);
    //this.addChild(this.buttons);
    this.container.addChild(this.highlights);
}
StationSortBar.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationSortBar.prototype.update = function() {
    this.background.width = this.size.x;
    this.highlights.width = this.size.x;
    
    if (NGE.PIXI.myShip) {
        var text = NGE.Stats.getShipStat(NGE.PIXI.myShip, "real_name").toUpperCase();
        if (this.suffix) {
            text += this.suffix;
        }
        this.text.text = text;
    } else {
        this.text.text = "NO SHIP";
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************