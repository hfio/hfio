//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudShipStatusPanel() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 180;
    this.size.y = 160;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.drawPolygon([
        0, 0,
        0, this.size.y,
        this.size.x, this.size.y,
        this.size.x, 5,
        this.size.x-5, 0
    ]);
    this.background.endFill();
    this.background.alpha = 0.5;

    this.highlight = new PIXI.Graphics();
    this.highlight.lineStyle(1, 0x00dbff);
    this.highlight.moveTo(this.size.x - 10, 0);
    this.highlight.lineTo(this.size.x -5, 0);
    this.highlight.lineTo(this.size.x, 5);
    this.highlight.lineTo(this.size.x, 10);
    
    this.container.addChild(this.background);
    this.container.addChild(this.highlight);
    
    this._textStyle = { fontFamily: 'BlenderPro Medium', fontSize: 14, fontWeight: 'normal', fill: 0xffffff };
    var warningTextStyle = { fontFamily: 'BlenderPro Medium', fontSize: 14, fontWeight: 'normal', fill: 0xff0000 };
    
    this._currentY = 10;

    this.attackText = this._addText('ATTACK');
    this.speedText = this._addText('SPEED');
    this.turnText = this._addText('TURN');
    this.thrustText = this._addText('THRUST (SHIFT): OFF');
    this.throttleText = this._addText('THRUST');
    
    // HULL
    
    this.hullText = this._addText('100');
    
    this.maxHullText = new PIXI.Text('/100 HULL', this._textStyle);
    this.maxHullText.x = 10;
    this.maxHullText.y = this.hullText.y;
    
    this.hullWarningText = new PIXI.Text('!', warningTextStyle);
    this.hullWarningText.x = 10;
    this.hullWarningText.y = this.hullText.y;
    this.hullWarningText.visible = false;
    
    this._hullWarning = false;
    
    // AMMO
    
    this.ammoText = this._addText('100');
    
    this.maxAmmoText = new PIXI.Text('/100 AMMO', this._textStyle);
    this.maxAmmoText.x = 10;
    this.maxAmmoText.y = this.ammoText.y;
    
    this.ammoWarningText = new PIXI.Text('!', warningTextStyle);
    this.ammoWarningText.x = 10;
    this.ammoWarningText.y = this.ammoText.y;
    this.ammoWarningText.visible = false;
    
    this._ammoWarning = false;

    this.container.addChild(this.maxHullText);
    this.container.addChild(this.hullWarningText);
    this.container.addChild(this.maxAmmoText);
    this.container.addChild(this.ammoWarningText);
    
    this._warnState = false;
    
    this._warn = function () {
        this._warnState = !this._warnState;
        this.hullWarningText.visible = this._hullWarning ? this._warnState : false;
        this.ammoWarningText.visible = this._ammoWarning ? this._warnState : false;
    }.bind(this);
    
    this._warnInterval = null;
}
HudShipStatusPanel.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudShipStatusPanel.prototype._addText = function (initialText) {
    var text = new PIXI.Text(initialText, this._textStyle);
    text.x = 10;
    text.y = this._currentY;
    this.container.addChild(text);
    this._currentY += 20;
    return text;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudShipStatusPanel.prototype.setWarnings = function (hull, ammo) {
    this._hullWarning = hull;
    this._ammoWarning = ammo;
    if (hull || ammo) {
        if (this._warnInterval === null) {
            this._warnState = true;
            this._warnInterval = setInterval(this._warn, 200);
        }
    } else {
        if (this._warnInterval !== null) {
            clearInterval(this._warnInterval);
            this._warnInterval = null;
            this.hullWarningText.visible = false;
            this.ammoWarningText.visible = false;
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudShipStatusPanel.prototype.update = function() {
    if (NGE.PIXI.testStationMenu.container.visible) {
        this.background.visible = false;
        this.highlight.visible = false;
    } else {
        this.background.visible = true;
        this.highlight.visible = true;
    }
    
    var myShip = NGE.PIXI.myShip;
    
    if (myShip) {
        var docked = myShip.docked.get();
        
        this.attackText.text = Math.round(myShip.damage.get() + myShip.damageboost.get()).toString() + " ATTACK";
        this.speedText.text = Math.round(myShip.max_speed.get() + myShip.speedboost.get()).toString() + " SPEED";
        this.turnText.text = Math.round(myShip.rot_speed.get() * (1 + 0.01 * myShip.turnboost.get())).toString() + " TURN";
        
        this.thrustText.text = "THRUST (SHIFT): " + (docked ? "-" : (NGE.PIXI.Input.SHIFT ? "ON": "OFF"));
        
        this.throttleText.text = "THRUST: " + (docked? "-" : (NGE.PIXI.Input.SHIFT ? "LOCKED" : (Math.round(myShip._throttle * 100) + "%")));
        
        var hullRatio = myShip.hp.get()/myShip.max_hp.get();
        this.hullText.text = Math.round(myShip.hp.get()).toString();
        this.hullText.style.fill = Hud._getStatColor(hullRatio);
        this.maxHullText.text = "/" + Math.round(myShip.max_hp.get()).toString() + " HULL";
        
        var ammoRatio = myShip.ammo.get()/myShip.max_ammo.get();
        this.ammoText.text = Math.round(myShip.ammo.get()).toString();
        this.ammoText.style.fill = Hud._getStatColor(ammoRatio);
        this.maxAmmoText.text = "/" + Math.round(myShip.max_ammo.get()).toString() + " AMMO";
        
        this.setWarnings(hullRatio <= 0.1, ammoRatio <= 0.1);
    } else {
        this.attackText.text = "- ATTACK";
        this.speedText.text = "- SPEED";
        this.turnText.text = "- TURN";
        
        this.thrustText.text = "THRUST (SHIFT): -";
        this.throttleText.text = "THRUST: -";
        
        this.hullText.text = "0";
        this.hullText.style.fill = Hud._getStatColor(0);
        this.ammoText.text = "0";
        this.ammoText.style.fill = Hud._getStatColor(0);
        
        this.setWarnings(false, false, false);
    }
    
    
    this.maxHullText.x = this.hullText.x + this.hullText.width;
    this.hullWarningText.x = this.maxHullText.x + this.maxHullText.width;
    this.maxAmmoText.x = this.ammoText.x + this.ammoText.width;
    this.ammoWarningText.x = this.maxAmmoText.x + this.maxAmmoText.width;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************