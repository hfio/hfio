//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudBottomBar() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 604;
    this.size.y = 87;

    this.skillButtons = new HudSkillButtons();
    this.skillButtons.pos.x = 12;
    while(this.skillButtons.children.length < this.skillButtons.maxButtons) {
        this.skillButtons.addButton();
    }

    this.highlights = new PIXI.Graphics();
    this._drawHighlights();

//    var energyBar = this.energyBar = new HudEnergyBar();
//    energyBar.pos.x = 0;
//    energyBar.pos.y = this.size.y - energyBar.size.y;

    this.container.addChild(this.highlights);
    this.addChild(this.skillButtons);
//    this.addChild(energyBar);
}
HudBottomBar.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudBottomBar.prototype._drawHighlights = function() {
    this.highlights.lineStyle(1, 0x00dbff);
    this.highlights.moveTo(8, 29);
    this.highlights.lineTo(0, 29);
    this.highlights.lineTo(0, this.size.y);
    this.highlights.moveTo(8, 60);
    this.highlights.lineTo(0, 60);
    this.highlights.moveTo(this.size.x - 8, 29);
    this.highlights.lineTo(this.size.x, 29);
    this.highlights.lineTo(this.size.x, this.size.y);
    this.highlights.moveTo(this.size.x - 8, 60);
    this.highlights.lineTo(this.size.x, 60);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudBottomBar.prototype.update = function() {
    var size = this.skillButtons.size.x + this.skillButtons.pos.x * 2;
    if (size !== this.size.x) {
        this.size.x = size;
        this.highlights.clear();
        this._drawHighlights();
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************