//*********************************************************************************************************************************************
// Panel in bottom left displaying chat status / what key to press to start chatting
//*********************************************************************************************************************************************
function HudChatPanel() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 180;
    this.size.y = 20;
    
    this.rightPadding = 5;
    
    var textStyle = { fontFamily: 'BlenderPro Medium', fontSize: 14, fontWeight: 'normal', fill: 0xffffff, align: 'right' };
    
    this.text = new PIXI.Text("", textStyle);
    
    this.container.addChild(this.text);
}
HudChatPanel.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudChatPanel.prototype.update = function() {
    var myShip = NGE.PIXI.myShip;
    if (myShip && !myShip.docked.get() && !NGE.PIXI.HUD.usernameButton.isEditing() && (NGE.PIXI.chatMode === OBJ_Pixi.CHAT_MODE.OFF)) {
        if (myShip.message_timeleft.get() <= 0) {
            this.text.text = NGE.i18n.get("MS30");
        } else {
            this.text.text = NGE.i18n.get("MS31");
        }
        this.text.x = Math.floor(this.size.x - this.text.width - this.rightPadding);
        this.text.visible = true;
    } else {
        this.text.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************