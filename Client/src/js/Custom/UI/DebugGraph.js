//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function DebugGraph() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size = { x: 490, y: 88 };
    
    this.clocks = {};
    this.marks = [];
    this.markY = 0;
    
    this.scaleY = 2;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.fillAlpha = 0.5;
    this.background.drawRect(0, 0, this.size.x, this.size.y);
    this.background.endFill();

    this.line1 = new PIXI.Graphics();
    this.line1.beginFill(0xffffff);
    this.line1.fillAlpha = 0.5;
    this.line1.drawRect(0, 0, this.size.x, 1);
    this.line1.endFill();
    this.line1.y = this.size.y - this.line1.height - 16 * this.scaleY;
    this.line1Text = new PIXI.Text('16 ms', { fontFamily: 'BlenderPro Medium', fontSize: 8, fill: 0xffffff });
    this.line1Text.x = this.size.x - this.line1Text.width - 4;
    this.line1Text.y = this.line1.y - this.line1Text.height;

    this.line2 = new PIXI.Graphics();
    this.line2.beginFill(0xffffff);
    this.line2.fillAlpha = 0.5;
    this.line2.drawRect(0, 0, this.size.x, 1);
    this.line2.endFill();
    this.line2.y = this.size.y - this.line2.height - 33 * this.scaleY;
    this.line2Text = new PIXI.Text('33 ms', { fontFamily: 'BlenderPro Medium', fontSize: 8, fill: 0xffffff });
    this.line2Text.x = this.size.x - this.line2Text.width - 4;
    this.line2Text.y = this.line2.y - this.line2Text.height;

    this.graphTexture = PIXI.RenderTexture.create(this.size.x, this.size.y);
    this.graphSprite = new PIXI.Sprite(this.graphTexture);

    this.hiddenGraphTexture = PIXI.RenderTexture.create(this.size.x, this.size.y);
    this.hiddenGraphSprite = new PIXI.Sprite(this.hiddenGraphTexture);
    this.hiddenGraphSprite.x = -1;
    this.hiddenGraphContainer = new PIXI.Container();
    this.hiddenGraphContainer.addChild(this.hiddenGraphSprite);

    this.marksContainer = new PIXI.Container();

    this.container.addChild(this.background);
    this.container.addChild(this.line1);
    this.container.addChild(this.line2);
    this.container.addChild(this.graphSprite);
    this.container.addChild(this.marksContainer);

    this.addClock('render', 0x13baff);
    this.addClock('objects_process', 0xbb0fff);
    this.addClock('objects_remove', 0xa2e908);
    this.addClock('lag', 0xf26900);

    // position clock name text
    var x = 6;
    for(var name in this.clocks) {
        if(!this.clocks.hasOwnProperty(name)) { continue; }
        this.clocks[name].text.x = x;
        x += this.clocks[name].text.width + 60;
    }

    this.container.addChild(this.line1Text);
    this.container.addChild(this.line2Text);
}
DebugGraph.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
DebugGraph.prototype.addClock = function(name, color) {
    var fill = new PIXI.Graphics();
    fill.beginFill(color);
    fill.drawRect(0, 0, 1, 1);
    fill.endFill();
    fill.x = this.size.x - 1;
    this.hiddenGraphContainer.addChild(fill);

    var text = new PIXI.Text(name, { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: color });
    text.y = 2;
    this.container.addChild(text);

    this.clocks[name] = {
        color: color,
        current: 0,
        start: performance.now(),
        average: 0,
        fill: fill,
        text: text
    };
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
DebugGraph.prototype.beginFrame = function() {
    this.endClock('lag');
    this.frameStartTime = performance.now();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
DebugGraph.prototype.endFrame = function() { 
    // shift graph 1px left, works because hiddenGraphSprite.x == -1
    NGE.PIXI.Canvas.render(this.graphSprite, this.hiddenGraphTexture);
    NGE.PIXI.Canvas.render(this.hiddenGraphContainer, this.graphTexture);

    // graph current time
    var y = this.size.y;
    for(var name in this.clocks) {
        if(!this.clocks.hasOwnProperty(name)) { continue; }
        var height = Math.round(this.clocks[name].current * this.scaleY);
        this.clocks[name].fill.height = height;
        y -= height;
        this.clocks[name].fill.y = y;
    }

    // update text
    for(var name in this.clocks) {
        if(!this.clocks.hasOwnProperty(name)) { continue; }
        this.clocks[name].text.text = name + ' (' + this.clocks[name].average.toFixed(1) + ' ms)';
    }

    // shift/clean-up old marks
    for(var i=0; i<this.marksContainer.children.length; i++) {
        var child = this.marksContainer.children[i];
        child.x -= 1;
        if((child instanceof PIXI.Text && child.x < 0) || (child instanceof PIXI.Graphics && child.x + child.width < 0)) {
            child.destroy();
        }
    }

    // add new marks
    if(this.marks.length > 0) {
        var markLine = new PIXI.Graphics();
        markLine.beginFill(0xffffff);;
        markLine.drawRect(0, 0, 1, this.size.y);
        markLine.endFill();
        markLine.x = this.size.x - 1;
        this.marksContainer.addChild(markLine);
    }
    for(var i=0; i<this.marks.length; i++) {
        var message = this.marks[i];
        var markText = new PIXI.Text(message, { fontFamily: 'BlenderPro Medium', fontSize: 8, fill: 0xffffff });
        markText.x = this.size.x - markText.width - 1;
        markText.y = 21 + this.markY;
        this.markY = (this.markY+8)%32;
        this.marksContainer.addChild(markText);
    }
    this.marks = [];

    var frameTime = performance.now() - this.frameStartTime;
    var nextFrameDue = (1000/60) - frameTime;
    this.beginClock('lag', Math.max(nextFrameDue, 0));
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
DebugGraph.prototype.beginClock = function(name, offset) {
    this.clocks[name].start = performance.now() + (offset || 0);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
DebugGraph.prototype.endClock = function(name) {
    var c = this.clocks[name];
    c.current = Math.max(performance.now() - c.start, 0);
    c.average = c.average * 0.8 + c.current * 0.2;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
DebugGraph.prototype.mark = function(message) {
    this.marks.push(message);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
DebugGraph.prototype.update = function() {
    this.pos.x = window.innerWidth - this.size.x;
    this.pos.y = window.innerHeight - this.size.y - 5;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************