//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudCompass() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 122;
    this.size.y = 16;

    var background = new PIXI.Graphics();
    background.beginFill(0x000000);
    background.drawPolygon([
        0, 0,
        0, this.size.y - 5,
        5, this.size.y,
        this.size.x - 5, this.size.y,
        this.size.x, this.size.y - 5,
        this.size.x, 0
    ]);
    background.endFill();
    background.alpha = 0.5;

    var highlightWidth = 164;
    var highlightHeight = 2;
    var mx = this.size.x / 2;
    this.stationBarBackground = new PIXI.Graphics();
    this.stationBarBackground.beginFill(0x000000);
    this.stationBarBackground.drawRect(0, 0, highlightWidth, highlightHeight);
    this.stationBarForeground = new PIXI.Graphics();
    this.stationBarForeground.beginFill(0xffffff);
    this.stationBarForeground.drawRect(0, 0, highlightWidth, highlightHeight);
    
    this._lastHealth = NGE.PIXI.mainStation ? NGE.PIXI.mainStation.hp.get() : 0;
    
    this._flashTimeLeft = 0;

    this.text = new PIXI.Text('4M', { fontFamily: 'BlenderPro Medium', fontSize: 13, fill: 0xffffff });
    this.text.x = Math.floor(this.size.x/2 - this.text.width/2);
    this.text.y = 2;

    this.container.addChild(background);
    this.container.addChild(this.stationBarBackground);
    this.container.addChild(this.stationBarForeground);
    this.container.addChild(this.text);
}
HudCompass.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudCompass.FLASH_DURATION = 0.5;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudCompass.prototype.update = function(dt) {
    if (NGE.WaveIndex > 0) {
        this.text.text = NGE.WaveShipLeftCount +  "/" + NGE.WaveShipCount + " " + NGE.i18n.get("MS33");
        this.text.visible = true;
    } else {
        this.text.text = "";
        this.text.visible = false;
    }
    
    this.stationBarBackground.x = Math.floor(this.size.x/2 - this.stationBarBackground.width/2);
    this.stationBarForeground.x = this.stationBarBackground.x;
    var hp = 0;
    if (NGE.PIXI.mainStation) {
        hp = NGE.PIXI.mainStation.hp.get() / NGE.PIXI.mainStation.max_hp.get();
        // flashing with red tint when main station HP decreases
        if (hp < this._lastHealth) {
            this._flashTimeLeft = HudCompass.FLASH_DURATION;
        } else {
            this._flashTimeLeft = Math.max(0, this._flashTimeLeft - dt);
        }
        this._lastHealth = hp;
        var ratio = 1 - this._flashTimeLeft / HudCompass.FLASH_DURATION;
        this.stationBarForeground.tint = (((1-ratio) * 0xff)<<16) + ((ratio * 0xdb)<<8) + (ratio * 0xff);
    }
    this.stationBarForeground.width = Math.ceil(hp * this.stationBarBackground.width);
    
    this.text.x = Math.floor(this.size.x/2 - this.text.width/2);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************