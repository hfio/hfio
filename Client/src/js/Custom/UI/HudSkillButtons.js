//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudSkillButtons() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 579;

    this.minButtons = 2;
    this.maxButtons = 11;

    // create buttons
    var i;
    this.buttons = [];
    for (i = 0; i < this.maxButtons; i++) {
        var button = new HudSkillButton();
        button.buffIndex = -1; // index of equipped tech (if any) in the buff list - updated in Hud.js
        button.warning = false;
        button.warningTimeout = 0;
        // index 0: deploy turret (always visible)
        // index 1: primary fire (left mouse button) (always visible)
        // index > 1: slots
        if (i === 0) {
             button.onclick = this.activateButton.bind(this, 0);
             button.slotText.text = "0";
        } else if (i === 1) {
             button.slotText.text = "CLICK";
        } else {
            button.onclick = this.activateButton.bind(this, i-1);
            button.slotText.text = (i-1).toString().substr(-1);
        }
        this.buttons.push(button);
    }
    
    NGE.PIXI.addNumberKeyHandler(this.activateButton.bind(this));

    // activate a minimum number of buttons
    for (i = 0; i < this.minButtons; i++) {
        this.addButton();
    }
    
    this.padding = (this.size.x - this.buttons[0].size.x * this.maxButtons) / (this.maxButtons - 1);
}
HudSkillButtons.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButtons.prototype.update = function() {
    var width = this.buttons[0].size.x * this.children.length + this.padding * (this.children.length - 1);
    this.size.x = width;
    for(var i=0; i<this.children.length; i++) {
        var button = this.children[i];
        // index 0 (deploy station) goes to the last position
        var posIndex = (i > 0) ? (i - 1) : (this.children.length - 1);
        button.pos.x = Math.round(posIndex * (button.size.x + this.padding));

        // Handle button click
        if(NGE.PIXI.Input.MOUSE_RELEASED && button.mouseOver && typeof button.onclick === "function") {
            button.onclick();
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButtons.prototype.addButton = function() {
    if(this.children.length < this.maxButtons) {
        this.addChild(this.buttons[this.children.length]);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButtons.prototype.removeButton = function() {
    if(this.children.length > this.minButtons) {
        this.removeChild(this.buttons[this.children.length-1]);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButtons.prototype.setButtonCount = function(count) {
    count = Math.min(Math.max(this.minButtons, count), this.maxButtons);
    while(this.children.length > count) {
        this.removeChild(this.buttons[this.children.length-1]);
    }
    while(this.children.length < count) {
        this.addChild(this.buttons[this.children.length]);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudSkillButtons.prototype.activateButton = function (slotIndex) {
    if ((slotIndex < 0) || (slotIndex >= this.buttons.length)) {
        return;
    }
    if (slotIndex === 0) {
        if (NGE.PIXI.myShip && NGE.stationManager && !NGE.PIXI.myShip.docked.get()) {
            NGE.stationManager.deploy();
        }
        return;
    }
    var button = this.buttons[slotIndex + 1];
    if (NGE.PIXI.myShip) {
//        var shipName = NGE.PIXI.myShip.subtype.get();
//        var stats = NGE.Stats.Ships.find((ship) => ship.data_name === shipName);

//        if (stats && NGE.buffManager) {
        if (NGE.buffManager) {
            //var energy = stats.energy;
            //var used = 0;//NGE.buffManager.getEnergyUsed();
            var buff; //, bc;//, cost;
            // check if button refers to an equipped tech
            if (button.buffIndex >= 0) {
                buff = NGE.buffManager._buffs[button.buffIndex];
                //bc = buff.config;
                //cost = 0;//bc.energyCost;
            }

            if (buff && !buff.active && (buff.timeLeft <= 0)) {
                NGE.buffManager.activate(slotIndex);
            }
            // old energy-based code:
//            // can toggle if equipped tech is either active (so we toggle off) or can be afforded (to toggle on)
//            if (buff && (buff.active || (cost <= energy - used))) {
//                NGE.buffManager.activate(slotIndex);
//            } else {
//                // otherwise mark the button with a warning tag
//                if (button.warning) {
//                    clearTimeout(button.warningTimeout);
//                }
//                button.warning = true;
//                button.warningTimeout = setTimeout(function () {
//                    button.warning = false;
//                }.bind(button), 300);
//            }
        }
    }
};