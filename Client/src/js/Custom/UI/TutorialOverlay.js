//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function TutorialOverlay() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 220;
    this.size.y = 50;
    
    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.65;
    this.background.drawRect(0, 0, this.size.x, this.size.y);
    this.background.endFill();
    
    this.playButton = new TextButton({text: "PLAY", fontSize: 22, style: "blue", width: 154, height: 50});
    this.playButton.pos.y = 325;
    
    var textStyle = { fontFamily: 'BlenderPro Medium', fontSize: 18, fill: 0xffffff, align: "center"};
    
    this.welcomeText = new PIXI.Text("Welcome to HYPER FLEET!", { fontFamily: 'BlenderPro Medium', fontSize: 26, fill: 0xffffff});
    this.welcomeText.x = 10;
    this.welcomeText.y = 100;
    
    this.tutorialText = new PIXI.Text(
            "You can change your name in the top left\n" +
            "Press enter to chat with other players\n" + 
            "Use the mouse to turn and shoot\n" +
            "Use the number keys to activate abilities", textStyle);
    this.tutorialText.x = 10;
    this.tutorialText.y = 100;
    
    this.tipsText = new PIXI.Text("Return to the station often to upgrade your ship", textStyle);
    this.tipsText.x = 10;
    this.tipsText.y = 140;
    
    this.moneyText = new PIXI.Text("Earn real money by collecting Refereum!", textStyle);
    this.moneyText.x = 10;
    this.moneyText.y = 70;

    this.container.addChild(this.background);
    this.addChild(this.playButton);
    this.container.addChild(this.playButton.container);
    this.container.addChild(this.welcomeText);
    this.container.addChild(this.tutorialText);
    this.container.addChild(this.tipsText);
    this.container.addChild(this.moneyText);
}
TutorialOverlay.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
TutorialOverlay.prototype.update = function() {
    this.size.x = window.innerWidth;
    this.background.width = this.size.x;
    this.size.y = window.innerHeight;
    this.background.height = this.size.y;
    
    this.playButton.pos.x = Math.floor((this.size.x - this.playButton.size.x) * 0.5);
    this.playButton.pos.y = Math.floor((this.size.y - this.playButton.size.y) * 0.5);
    
    var base = Math.floor(this.playButton.pos.y - 20);
    
    this.welcomeText.x = Math.floor((this.size.x - this.welcomeText.width) * 0.5);
    this.welcomeText.y = base - this.tutorialText.height - this.welcomeText.height - 20;
    
    this.tutorialText.x = Math.floor((this.size.x - this.tutorialText.width) * 0.5);
    this.tutorialText.y = base - this.tutorialText.height;
    this.tipsText.x = Math.floor((this.size.x - this.tipsText.width) * 0.5);
    this.tipsText.y = Math.floor(this.playButton.pos.y + this.playButton.size.y + 20);
    this.moneyText.x = this.size.x - this.moneyText.width - 10;
    this.moneyText.y = NGE.PIXI.HUD.wallet.size.y + 10;
    
    if(NGE.PIXI.Input.MOUSE_RELEASED && this.playButton.mouseOver) {
        this.container.visible = false;
        NGE.PIXI.TutorialFinished = true;
        NGE.WS.Send('startgame', {});
        if (NGE.Respawned) {
            NGE.PIXI.testStationMenu.container.visible = true;
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************