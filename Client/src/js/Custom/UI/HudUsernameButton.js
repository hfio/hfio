//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudUsernameButton() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 154;
    this.size.y = 22;
    
    this.FailedUsername = null;
    this.FailedUsernameState = HudUsernameButton.USERNAME_STATE.GOOD;
    this.FailedUsernameTimeLeft = 0;

    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.fillAlpha = 0.5;
    this.background.drawRect(0, 0, this.size.x, this.size.y);
    this.background.endFill();

    this.highlight = new PIXI.Graphics();
    this.highlight.lineStyle(1, 0x00dbff); 
    this.highlight.moveTo(0, this.size.y);
    this.highlight.lineTo(this.size.x, this.size.y);
    this.highlight.lineTo(this.size.x, this.size.y - 5);

    this.text = new PIXI.Text(NGE.i18n.MenuStatic.MS29.english, { fontFamily: 'BlenderPro Medium', fontSize: 15, fill: 0x00dbff });
    this.text.x = 12;
    this.text.y = Math.floor(this.size.y/2 - this.text.height/2) - 1;

    this.container.addChild(this.background);
    this.container.addChild(this.highlight);
    this.container.addChild(this.text);
    
    this.edit = document.getElementById("usernameEdit");
    
    this._editing = false;
    
    this.edit.onblur = function () {
        if (this._editing) {
            this._editing = false;
            this._updateDisplayName();
        }
    }.bind(this);
    this.edit.onkeyup = function(e) {
        if (this._editing) {
            if (e.keyCode === 13) {
                this._editing = false;
                this._updateDisplayName();
            } else if (e.keyCode === 27) {
                this.cancelEditing();
            }
        }
    }.bind(this);
    
    this.addHTMLElement(this.edit);
}
HudUsernameButton.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudUsernameButton.USERNAME_STATE = {
    GOOD: 0,
    ALREADY_TAKEN : 1,
    WRONG_LENGTH: 2
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudUsernameButton.prototype.isEditing = function() {
    return this._editing;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudUsernameButton.prototype.cancelEditing = function() {
    this._editing = false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudUsernameButton.DEFAULT_TEXT = 'ROOKIE';
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudUsernameButton.prototype._updateDisplayName = function() {
    var newName = this.edit.value;
    if (newName !== NGE.accountData.username) {
        if ((newName.length < 3) || newName.length > 25) {
            this.FailedUsername = newName;
            this.FailedUsernameState = HudUsernameButton.USERNAME_STATE.WRONG_LENGTH;
            this.FailedUsernameTimeLeft = 3;
            return;
        }
        if (GLOBAL_DEBUG) {
            console.log("Changing user display name: " + NGE.accountData.username + " -> " + newName);
        }
        NGE.AUTH.changeDisplayName(newName, function (data) {
            if (data) {
                var displayName = data.DisplayName;
                if (NGE.PIXI.myShip) {
                    NGE.PIXI.myShip.username.set(displayName);
                    this.FailedUsernameTimeLeft = 0;
                }
                NGE.WS.Send('namechange', {displayName: displayName});
            } else {
                this.FailedUsername = newName;
                this.FailedUsernameState = HudUsernameButton.USERNAME_STATE.ALREADY_TAKEN;
                this.FailedUsernameTimeLeft = 3;
            }
        }.bind(this));
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudUsernameButton.prototype.update = function(dt) {
    
    this.edit.style.left = this.pos.x + "px";
    this.edit.style.top = this.pos.y + "px";
    this.edit.style.width = this.size.x + "px";
    this.edit.style.height = this.size.y + "px";
    
    if (this.container.visible) {
        if (this.FailedUsernameTimeLeft > 0) {
            this.FailedUsernameTimeLeft -= dt;
            switch (this.FailedUsernameState) {
                case HudUsernameButton.USERNAME_STATE.ALREADY_TAKEN:
                    this.text.text = "NAME TAKEN!";
                    break;
                case HudUsernameButton.USERNAME_STATE.WRONG_LENGTH:
                    this.text.text = "WRONG NAME LENGTH!";
                    break;
                default:
                    this.text.text = "WRONG NAME!";
                    break;
            }
            this.text.style.fill = 0xff0000;
        } else {
            this.text.text = NGE.accountData.username || NGE.i18n.MenuStatic.MS29.english;
            this.text.style.fill = 0x00dbff;
        }

        if(NGE.PIXI.Input.MOUSE_RELEASED && this.mouseOver) {
            NGE.PIXI.chatMode = OBJ_Pixi.CHAT_MODE.OFF;
            NGE.PIXI.HUD.wallet.cancelEditing();
            this._editing = true;
            this.edit.value = NGE.accountData.username || HudUsernameButton.DEFAULT_TEXT;
            this.edit.style.display = "inline";
            this.edit.focus();
        }
        if (this._editing) {
            this.edit.style.display = "inline";
        } else {
            this.edit.style.display = "none";
        }
    } else {
        this.edit.style.display = "none";
    }
}
