//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationShipCard() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 188;
    this.size.y = 297;

    this.background = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/ship0.png"));

    this.border = new PIXI.Graphics();
    this.border.lineStyle(1, 0x00dbff); 
    this.border.moveTo(0, 0);
    this.border.lineTo(0, this.size.y);
    this.border.lineTo(this.size.x, this.size.y);
    this.border.lineTo(this.size.x, 0);
    this.border.lineTo(0, 0);

    this.shipNameBackground = new PIXI.Graphics();
    this.shipNameBackground.beginFill(0x000000);
    this.shipNameBackground.drawRect(0, 0, this.size.x, 25);
    this.shipNameBackground.endFill();
    this.shipNameBackground.y = 5;
    
    this.shipId = "";

    this.shipNameText = new PIXI.Text('Ship name', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.shipNameText.x = 7;
    this.shipNameText.y = this.shipNameBackground.y + Math.floor(this.shipNameBackground.height/2 - this.shipNameText.height/2) - 1;

    this.purchaseButton = new ActionWithCostButton({
        style: "orange",
        actionText: NGE.i18n.MenuStatic.MS22.english,
        cost: 50,
        marginLeft: 10,
        marginRight: 10,
        action: ()=>{
            var d = { name: this.shipId };
            if (GLOBAL_DEBUG) {
                console.log('sending unlockship:', JSON.stringify(d));
            }
            NGE.WS.Send('unlockship', d);
        }
    });
    this.purchaseButton.pos.x = Math.floor(this.size.x/2 - this.purchaseButton.size.x/2);
    this.purchaseButton.pos.y = 266;

    this.equipButton = new TextButton({text: NGE.i18n.MenuStatic.MS23.english, style: "orange"});
    this.equipButton.pos.x = Math.floor(this.size.x/2 - this.equipButton.size.x/2);
    this.equipButton.pos.y = 266;

    this.container.addChild(this.background);
    this.container.addChild(this.shipNameBackground);
    this.container.addChild(this.shipNameText);
    this.addChild(this.purchaseButton);
    this.addChild(this.equipButton);
    this.container.addChild(this.border);

    this._state = null;
    Object.defineProperty(this, 'state', {get: this._getState, set: this._setState});
    this.state = StationShipCard.STATE.DEFAULT;

    Object.defineProperty(this, 'image', {set: (filename) => {
        this.background.texture = NGE.PIXI.TextureFromImage("img/ui/" + filename);
    }});
}
StationShipCard.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCard.prototype.update = function() {
    var ship = NGE.accountData.ownedshipslist.find((element)=>(element.ItemId === this.shipId));
    var unlocked = (ship && (!ship.CustomData || (ship.CustomData.hp > 0)));
    var equipped = (NGE.PIXI.myShip && NGE.PIXI.myShip.subtype.get() === this.shipId);
    if(!unlocked) {
        this.state = NGE.WS.isLoggedIn ? StationShipCard.STATE.DEFAULT : StationShipCard.STATE.NOTLOGGEDIN;
    } else if(unlocked && !equipped) {
        this.state = StationShipCard.STATE.PURCHASED;
    } else if(unlocked && equipped) {
        this.state = StationShipCard.STATE.EQUIPPED;
    }

    if(NGE.PIXI.Input.MOUSE_RELEASED) {
        if(this.equipButton.container.visible && this.equipButton.mouseOver) {
            var d = { obj: "ships", name: this.shipId }; // FIXME: why do we send a (redundant?) "ships" string with this "pickship" event?
            if (GLOBAL_DEBUG) {
                console.log("sending pickship:", JSON.stringify(d));
            }
            NGE.WS.Send('pickship', d);
        }
    }
};
StationShipCard.prototype._getState = function() {
    return this._state;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCard.prototype._setState = function(state) {
    if(state !== this._state) {
        this._state = state;
        this.purchaseButton.container.visible = (state === StationShipCard.STATE.DEFAULT);
        this.equipButton.container.visible = (state === StationShipCard.STATE.PURCHASED);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationShipCard.STATE = {
    DEFAULT: 0,
    PURCHASED: 1,
    EQUIPPED: 2,
    NOTLOGGEDIN: 3
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
