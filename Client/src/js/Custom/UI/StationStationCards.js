//*********************************************************************************************************************************************
// Cards for droppable mini stations that can be purchased and deployed on the battlefield to shoot at enemies
//*********************************************************************************************************************************************
function StationStationCards() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.cardPadding = 12;
    this.cardIndex = 0;
    this.cardsAdded = 0;
    this.cardsPerPage = 0;

    this.cards = [];
    
    this.shipType = null; // the cards for this ship's missions are displayed
}
StationStationCards.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationStationCards.prototype.update = function() {
    var i;
    // checking if we have the right set of cards (missions for the current ship type)
    var newShip = false;
    if (NGE.PIXI.myShip && (NGE.PIXI.myShip.subtype.get() !== this.shipType)) {
        // changing the cards to the right ones - adding / updating / removing
        var stationIds = NGE.Stats.getShipStat(NGE.PIXI.myShip, "stations");
        for(i=0; i<stationIds.length; i++) {
            var station = NGE.Stats.getStation(stationIds[i]);
            if (this.cards.length <= i) {
                this.cards.push(new StationStationCard(station));
            } else {
                this.cards[i].setStation(station);
            }
        }
        while (i < this.cards.length) {
            this.removeChild(this.cards[i]);
            this.cards.splice(i, 1);
        }
        this.shipType = NGE.PIXI.myShip.subtype.get();
        newShip = true;
    }
    
    this.cardsPerPage = Math.floor(this.size.x / (this.cards[0].size.x + this.cardPadding));

    if(newShip || this.cardsAdded < this.cardsPerPage || this.cardsAdded > this.cardsPerPage) {
        this.reflowCards();
    }

    // update card positions
    for(i=0; i<this.children.length; i++) {
        var card = this.children[i];
        card.pos.x = this.cardPadding + i * (card.size.x + this.cardPadding);
        card.pos.y = 13;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationStationCards.prototype.reflowCards = function() {
    var i;
    // remove all cards
    this.cardsAdded = 0;
    for(i=0; i<this.cards.length; i++) {
        this.removeChild(this.cards[i]);
    }
    // only a subset of cards gets added
    for(i=this.cardIndex; i<Math.min(this.cardIndex + this.cardsPerPage, this.cards.length); i++) {
        this.addChild(this.cards[i]);
        this.cardsAdded++;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationStationCards.prototype.previousPage = function() {
    this.cardIndex = Math.max(0, this.cardIndex - this.cardsPerPage);
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationStationCards.prototype.nextPage = function() {
    this.cardIndex = Math.max(0, Math.min(this.cards.length - this.cardsPerPage, this.cardIndex + this.cardsPerPage));
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************