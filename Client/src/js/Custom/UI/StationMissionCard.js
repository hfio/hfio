//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationMissionCard(mission) { // extends BaseGuiElement
    BaseGuiElement.call(this);
    
    this.mission = null;

    this.size.x = 188;
    this.size.y = 297;

    this.background = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/mission-card.png"));

    // overlay, to make image look darker
    this.overlay = new PIXI.Graphics();
    this.overlay.beginFill(0x000000);
    this.overlay.fillAlpha = 0.5;
    this.overlay.drawRect(0, 0, this.size.x, this.size.y);
    this.overlay.endFill();

    this.border = new PIXI.Graphics();
    this.border.lineStyle(1, 0x00dbff); 
    this.border.moveTo(0, 0);
    this.border.lineTo(0, this.size.y);
    this.border.lineTo(this.size.x, this.size.y);
    this.border.lineTo(this.size.x, 0);
    this.border.lineTo(0, 0);

    this.missionNameBackground = new PIXI.Graphics();
    this.missionNameBackground.beginFill(0x00dbff);
    this.missionNameBackground.drawRect(0, 0, this.size.x, 25);
    this.missionNameBackground.endFill();
    this.missionNameBackground.y = 5;

    this.missionNameText = new PIXI.Text("MISSION NAME", { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0x000000 });
    this.missionNameText.x = 7;
    this.missionNameText.y = this.missionNameBackground.y + Math.floor(this.missionNameBackground.height/2 - this.missionNameText.height/2) - 1;

    this.goalText = new PIXI.Text("MISSION TYPE", { fontFamily: 'BlenderPro Medium', fontSize: 17, fill: 0x00dbff });
    this.goalText.x = 12;
    this.goalText.y = 42;

    this.descriptionText = new PIXI.Text(
            "Description",
        { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff, wordWrap: true, wordWrapWidth: this.size.x - 2 * 17 }
    );
    this.descriptionText.x = 17;
    this.descriptionText.y = 63;

    this.timerIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/timer-icon.png"));
    this.timerIcon.x = 17;
    this.timerIcon.y = 180;

    this.timeText = new PIXI.Text('0m', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.timeText.x = 17 + this.timerIcon.width + 4;
    this.timeText.y = 181;

    this.expiresText = new PIXI.Text(NGE.i18n.MenuStatic.MS16.english + ': 3h 53m', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.expiresText.x = 17;
    this.expiresText.y = 197;

    this.galaxyText = new PIXI.Text(NGE.i18n.MenuStatic.MS17.english + ': 12ly', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this.galaxyText.x = 17;
    this.galaxyText.y = 215;

    this.rewardBackground = new PIXI.Graphics();
    this.rewardBackground.beginFill(0x000000);
    this.rewardBackground.fillAlpha = 0.5;
    this.rewardBackground.drawRect(0, 0, this.size.x, 25);
    this.rewardBackground.endFill();
    this.rewardBackground.y = 239;

    //var iconPad = 0;
    //this.currencyIcon = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/currency.png"));
    this.rewardText = new PIXI.Text(NGE.i18n.MenuStatic.MS19.english + ': EARN SHIP TECH', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    //this.rewardText.x = Math.floor(this.size.x/2 - (this.rewardText.width + iconPad + this.currencyIcon.width)/2);
    this.rewardText.x = Math.floor(this.size.x/2 - this.rewardText.width/2);
    this.rewardText.y = this.rewardBackground.y + Math.floor(this.rewardBackground.height/2 - this.rewardText.height/2);    
    //this.currencyIcon.x = this.rewardText.x + this.rewardText.width + iconPad;
    //this.currencyIcon.y = this.rewardBackground.y + Math.floor(this.rewardBackground.height/2 - this.currencyIcon.height/2);

    this.acceptButton = new TextButton({text: NGE.i18n.MenuStatic.MS11.english, style: "orange"});
    this.acceptButton.pos.x = Math.floor(this.size.x/2 - this.acceptButton.size.x/2);
    this.acceptButton.pos.y = 266;
    
    this.claimButton = new TextButton({text: NGE.i18n.MenuStatic.MS27.english, style: "orange"});
    this.claimButton.pos.x = Math.floor(this.size.x/2 - this.claimButton.size.x/2);
    this.claimButton.pos.y = 266;
    this.claimButton.container.visible = false;
    
    this._buttonCenterX = this.acceptButton.pos.x + this.acceptButton.size.x / 2;
    this._buttonCenterY = this.acceptButton.pos.y + this.acceptButton.size.y / 2;
    
    this.statusText = new PIXI.Text('', { fontFamily: 'BlenderPro Medium', fontSize: 17, fill: 0xff9700 });
    this.statusText.x = Math.floor(this._buttonCenterX - this.statusText.width / 2);
    this.statusText.y = Math.floor(this._buttonCenterY - this.statusText.height / 2);
    
    if (mission) {
        this.setMission(mission);
    }

    this.container.addChild(this.background);
    this.container.addChild(this.overlay);
    this.container.addChild(this.missionNameBackground);
    this.container.addChild(this.missionNameText);
    this.container.addChild(this.goalText);
    this.container.addChild(this.descriptionText);
    this.container.addChild(this.timerIcon);
    this.container.addChild(this.timeText);
    this.container.addChild(this.expiresText);
    this.container.addChild(this.galaxyText);
    this.container.addChild(this.rewardBackground);
    this.container.addChild(this.rewardText);
    //this.container.addChild(this.currencyIcon);
    this.addChild(this.acceptButton);
    this.addChild(this.claimButton);
    this.container.addChild(this.statusText);
    this.container.addChild(this.border);
}
StationMissionCard.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMissionCard.prototype.setMission = function (mission) {
    if (this.mission !== mission) {
        this.mission = mission;

        this.background.texture = NGE.PIXI.TextureFromImage("img/ui/"+mission.cardImage);
        this.missionNameText.text = mission.name;
        this.goalText.text = mission.type;
        this.descriptionText.text = mission.description;
        this.timeText.text = mission.refreshTime+'m';
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMissionCard.prototype.update = function() {
    
    var missions = JSON.parse(NGE.accountData.missions);
    var missionData = (this.mission ? missions.find((e)=>e.id===this.mission.id) : null); // user specific mission data
    
    var expiring = false;
    
    if (missionData) {
        var now = Date.now();
        // checking if refresh time elapsed
        var elapsed = now-missionData.start;
        if (missionData && elapsed<this.mission.refreshTime*1000*60) { // ms - minute conversion
            expiring = true;
            var left = Math.ceil(this.mission.refreshTime*60 - elapsed/1000);
            var leftSec = Math.floor(left) % 60;
            var leftMin = Math.floor(left/(60)) % 60;
            var leftHr = Math.floor(left/(60*60));
            this.expiresText.text = NGE.i18n.get("MS16") + ': ' + ((leftHr > 0) ? (leftHr + 'h ') : '') + ((leftMin > 0) ? (leftMin + 'm ') : '') + leftSec + 's',
            this.expiresText.visible = true;
        } else {
            this.expiresText.visible = false;
        }
    } else {
        this.expiresText.visible = false;
    }
    
    if (this.mission && (NGE.accountData.activeMission === this.mission.id)) {
        this.acceptButton.container.visible = false;
        if (NGE.accountData.missionProgress >= this.mission.targetAmount) {
            this.claimButton.container.visible = true;
            this.statusText.visible = false;
        } else {
            this.claimButton.container.visible = false;
            this.statusText.text = NGE.i18n.get("MS36") + ": " + Math.round(100 * NGE.accountData.missionProgress / this.mission.targetAmount) + "%";
            this.statusText.x = Math.floor(this._buttonCenterX - this.statusText.width / 2);
            this.statusText.visible = true;
        }
    } else {
        this.claimButton.container.visible = false;
        if (expiring) {
            this.statusText.text = this.expiresText.text;
            this.statusText.x = Math.floor(this._buttonCenterX - this.statusText.width / 2);
            this.statusText.visible = true;
            this.acceptButton.container.visible = false;
        } else {
            this.statusText.visible = false;
            this.acceptButton.container.visible = true;
        }
    }
    
    if(NGE.PIXI.Input.MOUSE_RELEASED) {
        if(this.acceptButton.mouseOver && this.acceptButton.container.visible) {
            NGE.WS.Send('startmission',{ missionId: this.mission.id });
        }
        if(this.claimButton.mouseOver && this.claimButton.container.visible) {
            NGE.WS.Send('claimmission', {});
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************