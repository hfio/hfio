//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudStationArrow() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 122;
    this.size.y = 16;
    
    this.arrow = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/arrow.png"));
    this.arrow.anchor.set(0.5, 0.5);
    this.arrow.x = Math.floor(this.size.x / 2);
    this.arrow.y = 15;
    
    this._lastHealth = NGE.PIXI.mainStation ? NGE.PIXI.mainStation.hp.get() : 0;
    
    this._flashTimeLeft = 0;

    this.container.addChild(this.arrow);
}
HudStationArrow.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudStationArrow.FLASH_DURATION = 0.5;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudStationArrow.prototype.update = function(dt) {
    if (NGE.PIXI.mainStation  && NGE.PIXI.myShip && !NGE.PIXI.myShip.docked.get()) {
        this.arrow.visible = true;
        this.arrow.rotation = Math.atan2(NGE.PIXI.mainStation.y.get() - NGE.PIXI.myShip.y.get(), NGE.PIXI.mainStation.x.get() - NGE.PIXI.myShip.x.get());
        
        // flashing with red tint when main station HP decreases
        var hp = NGE.PIXI.mainStation.hp.get();
        if (hp < this._lastHealth) {
            this._flashTimeLeft = HudStationArrow.FLASH_DURATION;
        } else {
            this._flashTimeLeft = Math.max(0, this._flashTimeLeft - dt);
        }
        this._lastHealth = hp;
        var ratio = 1 - this._flashTimeLeft / HudStationArrow.FLASH_DURATION;
        this.arrow.tint = 0xff0000 + ((ratio * 0xff)<<8) + (ratio * 0xff);
    } else {
        this.arrow.visible = false;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************