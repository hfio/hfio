//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationTechCards() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.cardPadding = 12;
    this.cardIndex = 0;
    this.cardsAdded = 0;
    this.cardsPerPage = 0;

    this.cards = [];
}
StationTechCards.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationTechCards.prototype.update = function() {
    if (!NGE.buffManager || !NGE.PIXI.myShip) {
        return;
    }
    
    var shipTechIds = NGE.Stats.getShipStat(NGE.PIXI.myShip, "tech");
    
    // filtering by ship and ordering by slot and id
    var techs = NGE.buffManager._buffs
            .filter((e)=>shipTechIds.indexOf(e.config.id)>=0)
            .sort((a,b)=>(a.config.slot*10000+a.config.id)-(b.config.slot*10000+b.config.id));
    
    var changed = false;
    
    // add new cards if the player has new tech
    for (var l = 0; l < techs.length; l++) {
        if (l < this.cards.length) {
            this.cards[l].tech = techs[l];
        } else {
            var card = new StationTechCard();
            card.tech = techs[l];
            this.cards.push(card);
            changed = true;
        }
    }
    // removing cards if we have more cards than tech
    while (l < this.cards.length) {
        this.removeChild(this.cards[l]);
        this.cards.splice(l, 1);
        changed = true;
    }
    
    if (this.cards.length > 0) {
        this.cardsPerPage = Math.floor(this.size.x / (this.cards[0].size.x + this.cardPadding));

        if(changed || (this.cardsAdded < this.cardsPerPage) || (this.cardsAdded > this.cardsPerPage)) {
            this.reflowCards();
        }

        // update card positions
        for(var i=0; i<this.children.length; i++) {
            var card = this.children[i];
            card.pos.x = this.cardPadding + i * (card.size.x + this.cardPadding);
            card.pos.y = 13;
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationTechCards.prototype.reflowCards = function() {
    // remove all cards
    this.cardsAdded = 0;
    for(var i=0; i<this.cards.length; i++) {
        this.removeChild(this.cards[i]);
    }
    // only a subset of cards gets added
    for(var i=this.cardIndex; i<Math.min(this.cardIndex + this.cardsPerPage, this.cards.length); i++) {
        this.addChild(this.cards[i]);
        this.cardsAdded++;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationTechCards.prototype.previousPage = function() {
    this.cardIndex = Math.max(0, this.cardIndex - this.cardsPerPage);
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationTechCards.prototype.nextPage = function() {
    this.cardIndex = Math.max(0, Math.min(this.cards.length - this.cardsPerPage, this.cardIndex + this.cardsPerPage));
    this.reflowCards();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************