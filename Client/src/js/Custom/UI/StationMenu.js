//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationMenu() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.sideBar = new StationSideBar();

    this.batteryBar = new BatteryBar();
    this.batteryBar.pos.x = 162;
    this.batteryBar.pos.y = 50;

    this.sortBar = new StationSortBar();
    this.sortBar.pos.x = 162;
    this.sortBar.pos.y = 81;
    
    this.statusBar = new StationStatusBar();
    this.statusBar.pos.x = 162;
    this.statusBar.pos.y = 438;
    
    this.addChild(this.sideBar);
    this.addChild(this.batteryBar);
    this.addChild(this.sortBar);
    this.addChild(this.statusBar);
    
    this._sections = [];

    this.shipSection = new StationCardSection(" EQUIPPED", new StationShipCards());
    this._addSection('SHIPS', this.shipSection);
    
    this.missionSection = new StationCardSection(" MISSIONS", new StationMissionCards());
    this._addSection('MISSIONS', this.missionSection);
    
    this.techSection = new StationCardSection(" TECH", new StationTechCards());
    this._addSection('TECH', this.techSection);
    
    this.stationSection = new StationCardSection(" STATIONS", new StationStationCards());
    this._addSection('STATIONS', this.stationSection);
    
    // default section
    this._selectSection(this.shipSection);
}
StationMenu.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMenu.prototype._addSection = function(buttonId, section) {
    section.pos.x = 162;
    section.pos.y = 111;
    section.container.visible = false;
    var button = this.batteryBar.buttons.children.find((button) => button.id === buttonId);
    section.selectorButton = button;
    button.onclick = () => {
        if (NGE.PIXI.myShip && NGE.WS.isLoggedIn) {
            this._selectSection(section);
        }
    };
    this.addChild(section);
    this._sections.push(section);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMenu.prototype._selectSection = function(section) {
    for (var i = 0; i < this._sections.length; i++) {
        var s = this._sections[i];
        s.container.visible = s === section;
        s.selectorButton.selected = s === section;
    }
    this.sortBar.suffix = section.suffix;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationMenu.prototype.update = function() {
    this.batteryBar.size.x = window.innerWidth - this.batteryBar.pos.x;
    this.sortBar.size.x = window.innerWidth - this.sortBar.pos.x;
    this.statusBar.size.x = window.innerWidth - this.statusBar.pos.x;
    
    for (var i = 0; i < this._sections.length; i++) {
        var s = this._sections[i];
        s.size.x = window.innerWidth - s.pos.x;
        if (s !== this.shipSection) {
            s.selectorButton.container.alpha = (NGE.PIXI.myShip && NGE.WS.isLoggedIn) ? 1 : 0.2;
        }
    }
    
    if(NGE.PIXI.Input.MOUSE_RELEASED && this.sideBar.launchButton.mouseOver) {
        if (NGE.PIXI.myShip) {
            NGE.WS.Send('undock', {});
        }
    }
    
    if (!NGE.PIXI.myShip && !this.shipSection.container.visible) {
        this._selectSection(this.shipSection);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************