//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function HudChatEditPanel() { // extends BaseGuiElement
    BaseGuiElement.call(this);

    this.size.x = 220;
    this.size.y = 50;
    
    this.background = new PIXI.Graphics();
    this.background.beginFill(0x000000);
    this.background.alpha = 0.5;
    this.background.drawRect(0, 0, 8, this.size.y);
    this.background.endFill();

    this.label = new PIXI.Text("Say to all:", { fontFamily: 'BlenderPro Medium', fontSize: 15, fill: 0x00dbff });
    this.label.x = 10;
    this.label.y = 5;
    
    this.input = new PixiTextInput("I don't have a background", { fontFamily: 'BlenderPro Medium', fontSize: 18, fill: 0xffffff });
    this.input.width = 200;
    this.input.background = false;
    this.input.caretColor = 0xffffff;
    this.input.position.x = 10;
    this.input.position.y = 26;
    this.input.maxLength = 30;

    this.container.addChild(this.background);
    this.container.addChild(this.label);
    this.container.addChild(this.input);
}
HudChatEditPanel.prototype = Object.create(BaseGuiElement.prototype); // inherit from BaseGuiElement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
HudChatEditPanel.prototype.update = function() {
    this.background.width = this.size.x;
    this.label.text = (NGE.PIXI.chatMode === OBJ_Pixi.CHAT_MODE.ALL) ? "Say to all:" : "Say to team:";
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************