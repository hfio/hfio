//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oLocalization(callback) {
    //debugging
    this.DEBUG = true;
    
    this.Language = "english";
    
    var Localization = this;
    var loading = 0;

    var onSuccess = function() {
        loading--;
        if(loading === 0) {
            callback();
        }
    };
    
    var self = this;

    var onFailure = function(err) {
        self.Log(err);
    };

    //Localization.MenuStatic
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1y02_BsmViLL2iFlDgfHrNWZ7Pj_oBAwh5GRQIYG5XmA/1/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            this.Log('Successfully fetched Localization.MenuStatic');
            Localization.MenuStatic = {};
            json.feed.entry.forEach((entry) => {
                Localization.MenuStatic[entry.gsx$stringid.$t] = {
                    english: entry.gsx$english.$t,
                    french: entry.gsx$french.$t,
                    spanish: entry.gsx$spanish.$t
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLocalization.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[LOCALIZATION] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oLocalization.prototype.get = function(key) {
    return this.MenuStatic[key] ? this.MenuStatic[key][this.Language] : key;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************