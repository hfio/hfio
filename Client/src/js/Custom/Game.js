//*********************************************************************************************************************************************
//Game Specific Custom Code (Runs on top of Core)
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//Custom Modules
//var oData = require('./sData');
//var oLogic = require('./sLogic');
//var oAPI = require('./sAPI');
//var oAI = require('./sAI');
//var oClient = require('./sClient');

//Custom Objects
//var oBase = require('./Objects/sBase');
//var oShip = require('./Objects/sShip');
//var oAsteroid = require('./Objects/sAsteroid');
//var oCoin = require('./Objects/sCoin');
//var oProjectile = require('./Objects/sProjectile');
//Core Engine
//var oCore = require('../Engine/sCore');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oGame(ref, userID) {
    //debugging
    this.DEBUG = true;
    
    this.RefID = ref;
    this.RefServer = null;
    this.RefShipID = null;
    this.RefUsed = false; //flag if we need to setup with buddy ship and pvp team
    this.Hosts = null;
    this.HostsShipCounts = {};
    
    if(ref !== "-1")
    {
        var RefV = RefIDIn.split("x");
        if(RefV.length === 2)
        {
            this.RefServer = RefV[0];
            this.RefShipID = RefV[1];
            this.Log("ServerID: " + RefV[0] + " ShipID: " + RefV[1]);
        }
        else{
            this.Log("No Referral Link!");    
        }
    }
    else { this.Log("No Referral Link!"); }
    
    //debugging
    this.DEBUG = true;

    this.Core = null;
    
    //Modules
    this.AUTH = new OBJ_Auth(userID);//User authentication
    this.DATA = new oData(this);
    this.LOGIC = new oLogic(this);
    this.WS = new OBJ_SocketIO();
    this.PIXI = new OBJ_Pixi();
    this.GUI = new OBJ_GUI();

    // player account data
    this.accountData = {};

    this.Name = "HF";
    this.Server = false;
    
    this.LastMoveTime = performance.now();
    
    this.WaveIndex = 0;
    this.WaveShipCount = 0;
    this.WaveShipLeftCount = 0;
    
    // marks if after player ship destruction, the game stte returned to chosing a new ship at the station
    this.Respawned = false;
    
    //Begin Server Resolution
    this.StartResolveServer();
    
    return this;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[GAME] " + m); }};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.ALIEN_AI_TEAM = 1;
oGame.prototype.TERRAN_AI_TEAM = 2;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.GetFleetPath = function (shipId) {
    return location.pathname + "?r=" + ((this.RefServer !== null) ? this.RefServer : 1) + "x" + shipId;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.GetFleetUrl = function (shipId) {
    return "http://" + document.location.host + this.GetFleetPath(shipId);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.StartResolveServer = function() {
    var G = this;
    
    // Disable caching of AJAX responses
    $.ajaxSetup ({ cache: false });
    
    $.ajax({
        method: "POST",
        url: "hosts.php",
        data: { cmd: "read"}
    })
    .done(function( d ) {//success
        G.Hosts = JSON.parse(d)[2];
        G.Log(JSON.stringify(G.Hosts));

        /*
        //get player ship counts
        for (var h in G.Hosts) {
            if (G.Hosts.hasOwnProperty(h)) {
                var hurl = "http://" + G.Hosts[h] + ":3010/pcount";//target url
                G.HostsShipCounts[h] = 0;//placeholder
                console.log("checking " + hurl);
                var PReq = $.ajax({
                    method: "GET",
                    url: hurl
                    //data: { cmd: "read"}
                });
                PReq.HURL = hurl;
                
                PReq.done(function( d ) {//success
                    console.log("response: " + d + " " + this.HURL);
                    
                });
                
            }
        }
        */
        
        
        G.ResolveServer();
    });
    
    
    
    
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.ResolveServer = function() {
    
    //default (1 must exist always)
    //console.log(this.Hosts);
    if(this.Hosts.s1 === undefined){ this.Log("NO DEFAULT SERVER (s1) Defined!! see ./hosts.php"); return; }
    var h = this.Hosts.s1;
    var host = h[0];
    var pcount = h[1];
    var pmax = h[2];
    var FULL = false;
    if(pcount >= pmax)
    {
        this.Log("Default Server FULL");
        FULL = true;
    }

    
    this.WS.Server = "http://" + host + ":80";
    if(this.RefServer !== null){
        if(this.Hosts["s" + this.RefServer] !== undefined)
        {
            h = this.Hosts["s" + this.RefServer];
            host = h[0];
            pcount = h[1];
            pmax = h[2];
            
            if(pcount >= pmax)
            {
                this.Log("Referred Server FULL");
                FULL = true;
            }
            else {
                this.WS.Server = "http://" + host + ":80";
                this.Log("Selected Server: " + this.WS.Server);
                FULL = false;
            }
        }
        else
        {
            this.Log("Trying Default Server: " + this.WS.Server);
        }
    }
    else
    {
        //this.Log("Selected Default Server: " + this.WS.Server);
    }
    
    //Try to find an open server
    if(FULL === true){
        this.WS.Server = null;
        for (var hh in this.Hosts) {
            if (this.Hosts.hasOwnProperty(hh)) {
                h = this.Hosts[hh];
                host = h[0];
                pcount = h[1];
                pmax = h[2];
                if(pcount < pmax)
                {
                    this.WS.Server = "http://" + host + ":80";
                    this.Log("FOUND OPEN Server: " + this.WS.Server);
                }
            }
        }
    }
    
    if(this.WS.Server){
        this.Init();    
    }
    else{
        this.Log("ALL SERVERS FULL!!!");
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Init = function() {
    this.Log("Custom Init!");

    //force the user to confirm before leaving or refreshing the page
    window.onbeforeunload = function () {
        var promptBeforeLeavingPage = true;
        if (promptBeforeLeavingPage) {
            return true;
        }
        return undefined;
    };
    
    this.Core = new oCore(this, this.DATA.CoreData);
    
    var self = this;

    //Load game stats from Google spreadsheets
    this.Stats = new oStats(() => {
        //Load localization
        this.i18n = new oLocalization(function() {
            //Setup old GUI
            NGE.GUI.Init(function() {
                //Connect to server
                NGE.WS.Init();

                //Get latest ship list
                self.Log("SEND: " + JSON.stringify({ obj: "ships" }));
                NGE.WS.Send('api_list',{ obj: "ships" });

                NGE.GUI.ViewChange("Play");
                NGE.PIXI.Init();
            });
        });
    });
	
    //Start Core
    this.Core.Init();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Process = function(dt) {
    
    if (NGE.WS.connected) {
        //this.Log("CustomProc " + dt + " " + this.Name);

        NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.beginClock('render');
        NGE.PIXI.render(dt);
        NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.endClock('render');

        var now = performance.now();

        if (NGE.Stats && NGE.Stats.Global) {
            if (NGE.PIXI.Input.Active) {
                this.LastMoveTime = now;
                NGE.PIXI.Input.Active = false;
            } else if ((now - this.LastMoveTime) > NGE.Stats.Global.game_timeout_time * 1000) { // convert sec to ms
                this.Log("Timeout due to inactivity for more than "+NGE.Stats.Global.game_timeout_time+" seconds!");
                NGE.WS.Disconnect();
            }
        } else {
            this.LastMoveTime = now;
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.NetProcess = function() {
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.ScopeProcess = function(CObj) {
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGame.prototype.Mode_Play = function()
{
    //Get Global settings
    NGE.GUI.RequestGLOBALS();
    
    this.Log("Mode_Play");
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = oGame;
// Closure export:
window['oGame'] = oGame; // jshint ignore:line