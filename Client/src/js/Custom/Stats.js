//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oStats(callback) {
    //debugging
    this.DEBUG = true;
    
    var Stats = this;
    var loading = 0;

    var onSuccess = function() {
        loading--;
        if(loading === 0) {
            callback();
        }
    };

    var onFailure = function(err) {
        Stats.Log(err);
    };

    //Stats.Ships, an array of objects like { move_radius: _, accel_sec: _, hitbox: _, etc... }
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/1/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            Stats.Log('Successfully fetched stats: Ships');
            Stats.Ships = json.feed.entry.map((entry) => {
                return {
                    accel_sec: parseFloat(entry.gsx$accelsec.$t),
                    data_name: entry.gsx$dataname.$t,
                    fire_drop: parseFloat(entry.gsx$firedrop.$t),
                    fire_drop_rot: parseFloat(entry.gsx$firedroprot.$t),
                    slots: parseInt(entry.gsx$slots.$t),
                    health: parseFloat(entry.gsx$health.$t),
                    total_shots: parseInt(entry.gsx$totalshots.$t),
                    hitbox: parseFloat(entry.gsx$hitbox.$t),
                    card_image: entry.gsx$cardimage.$t,
                    max_speed: parseFloat(entry.gsx$maxspeed.$t),
                    move_radius: parseFloat(entry.gsx$moveradius.$t),
                    real_name: entry.gsx$realname.$t,
                    rotation_speed: parseFloat(entry.gsx$rotationspeed.$t),
                    default_weapon: entry.gsx$defaultweapon.$t,
                    credits_for_kill: parseFloat(entry.gsx$creditsforkill.$t),
                    unlock_price_credits: parseFloat(entry.gsx$unlockpricecredits.$t),
                    repair_cost: entry.gsx$repaircost.$t,
                    restock_cost: entry.gsx$restockcost.$t,
                    missions: entry.gsx$missions.$t && entry.gsx$missions.$t.split(",").map((e)=>parseInt(e)) || [],
                    tech: entry.gsx$tech.$t && entry.gsx$tech.$t.split(",").map((e)=>parseInt(e)) || [],
                    stations: entry.gsx$stations.$t && entry.gsx$stations.$t.split(",").map((e)=>parseInt(e)) || []
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);

    // Tech
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/3/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            Stats.Log('Successfully fetched stats: Tech');
            Stats.Tech = [];
            var lastId = -1;
            for (var i = 0; i < json.feed.entry.length; i++) {
                var entry = json.feed.entry[i];
                var id = parseInt(entry.gsx$id.$t);
                var tech;
                if (id === lastId) {
                    tech = Stats.Tech[Stats.Tech.length - 1];
                } else {
                    tech = {id: id};
                    Stats.Tech.push(tech);
                }
                var lvl = parseInt(entry.gsx$lvl.$t);
                if (lvl === 1) {
                    tech.cardImage = entry.gsx$techcardimage.$t;
                    tech.name = entry.gsx$name.$t;
                    tech.description = entry.gsx$description.$t;
                    tech.slot = parseInt(entry.gsx$slot.$t);
                    tech.effects = entry.gsx$effects.$t;
                    tech.sellTechPoints = [parseInt(entry.gsx$selltechpoints.$t)];
                    tech.cooldown = [parseFloat(entry.gsx$cooldown.$t)];
                    tech.range = [parseFloat(entry.gsx$range.$t)];
                    tech.duration = [parseFloat(entry.gsx$duration.$t)];
                    tech.levels = [parseFloat(entry.gsx$levels.$t)];
                    tech.upgradeGivesBonus = [oStats.parseBool(entry.gsx$upgradegivesbonus.$t)];
                    tech.upgradeCosts = [parseFloat(entry.gsx$upgradecosts.$t)];
                    tech.localizeStringId = entry.gsx$localizestringid.$t;
                } else {
                    //console.log("Setting level " + lvl + " of " + JSON.stringify(tech));
                    tech.sellTechPoints[lvl - 1] = parseInt(entry.gsx$selltechpoints.$t);
                    tech.cooldown[lvl - 1] = parseFloat(entry.gsx$cooldown.$t);
                    tech.range[lvl - 1] = parseFloat(entry.gsx$range.$t);
                    tech.duration[lvl - 1] = parseFloat(entry.gsx$duration.$t);
                    tech.levels[lvl - 1] = parseFloat(entry.gsx$levels.$t);
                    tech.upgradeGivesBonus[lvl - 1] = oStats.parseBool(entry.gsx$upgradegivesbonus.$t);
                    tech.upgradeCosts[lvl - 1] = parseFloat(entry.gsx$upgradecosts.$t);
                }
                lastId = id;
            }
        })
        .then(onSuccess)
        .catch(onFailure);

    // Bonuses
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/4/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            Stats.Log('Successfully fetched stats: Bonuses');
            Stats.Bonuses = json.feed.entry.map((entry) => {
                return {
                    id: parseInt(entry.gsx$id.$t),
                    stat: entry.gsx$stat.$t,
                    localizeStringId: entry.gsx$localizestringid.$t,
                    min: parseInt(entry.gsx$statmin.$t),
                    max: parseInt(entry.gsx$statmax.$t),
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);

    // Missions
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/5/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            Stats.Log('Successfully fetched stats: Missions');
            Stats.Missions = json.feed.entry.map((entry) => {
                return {
                    id: parseInt(entry.gsx$missionid.$t),
                    cardImage: entry.gsx$image.$t,
                    type: entry.gsx$missiontype.$t,
                    name: entry.gsx$name.$t,
                    description: entry.gsx$description.$t,
                    target: entry.gsx$target.$t && entry.gsx$target.$t.split(",") || [],
                    targetAmount: parseInt(entry.gsx$targetamount.$t),
                    refreshTime: parseInt(entry.gsx$minutesuntilrefresh.$t)
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);

    // Stations (droppable and main)
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/6/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            Stats.Log('Successfully fetched stats: Stations');
            Stats.Stations = json.feed.entry.map((entry) => {
                return {
                    id: parseInt(entry.gsx$id.$t),
                    cardImage: entry.gsx$cardimage.$t,
                    image: entry.gsx$image.$t,
                    name: entry.gsx$name.$t,
                    description: entry.gsx$description.$t,
                    duration: parseFloat(entry.gsx$duration.$t),
                    maxCount: parseInt(entry.gsx$maxcount.$t),
                    cooldown: parseFloat(entry.gsx$cooldown.$t),
                    cost: parseInt(entry.gsx$cost.$t),
                    health: parseFloat(entry.gsx$health.$t),
                    turret: parseInt(entry.gsx$turret.$t),
                    hitbox: parseFloat(entry.gsx$hitbox.$t),
                    dockRadius: parseFloat(entry.gsx$dockradius.$t),
                    rotationRate: parseFloat(entry.gsx$rotationrate.$t),
                    anchor: entry.gsx$anchor.$t
                };
            });
        })
        .then(onSuccess)
        .catch(onFailure);    

    //Stats.Global[<key>] returns corresponding value (with correct type)
    loading++;
    fetch("https://spreadsheets.google.com/feeds/list/1heeVs-F8lpo-KmImzbRaSScC6lrRXCPS358cwzs3AS0/2/public/values?alt=json")
        .then((res) => res.json())
        .then((json) => {
            Stats.Log('Successfully fetched stats: Global');
            Stats.Global = {};
            json.feed.entry.forEach((entry) => {
                var key = entry.gsx$key.$t;
                var value = entry.gsx$value.$t;
                var type = entry.gsx$type.$t;
                if(type === 'number') {
                    value = parseFloat(value);
                } else if(type === 'boolean') {
                    value = (value == 'true');
                }
                Stats.Global[key] = value;
            });
        })
        .then(onSuccess)
        .catch(onFailure);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[STATS] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.parseBool = function(boolString){
    return boolString.toLowerCase() === "true";
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.parseCSV = function(csv){
    if (csv == '') return null;
    return JSON.parse(csv);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getShipStat = function(ship, statName) {
    //Find stats which match ship name
    var shipName = ship.subtype.get()
    var stats = this.Ships.find((stats) => stats.data_name === shipName);
    if (stats) {
        return stats[statName];
    } else {
        return;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getShipStatById = function(shipId, statName) {
    //Find stats which match ship name
    var stats = this.Ships.find((stats) => stats.data_name === shipId);
    if (stats) {
        return stats[statName];
    } else {
        return;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getMission = function(missionId) {
    var stats = this.Missions.find((stats) => stats.id === missionId);
    if (stats) {
        return stats;
    } else {
        return null;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getStation = function(stationId) {
    var stats = this.Stations.find((stats) => stats.id === stationId);
    if (stats) {
        return stats;
    } else {
        return null;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oStats.prototype.getStationTurret = function(stationId) {
    var station = this.Stations.find((stats) => stats.id === stationId);
    if (station) {
        var turret = this.Tech.find((stats) => stats.id === station.turret);
        return turret || null;
    } else {
        return null;
    }
};