//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Asteroid
    - Movement
    - Collision Detection
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var oGameObject = require('../../Engine/Core/sGO');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oAsteroid(Game, d)
{
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;

    //------------------------------------------------------------------------------
    //Object State (Networked - Changes only)
    //------------------------------------------------------------------------------
    //Custom data needed (for datalist)
    var nd = [ ]
    
    //Create Object Properly
    this.Obj = new oGameObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }
    //L.Log(JSON.stringify(this.Obj))
    
    //------------------------------------------------------------------------------
    //INTERNAL
    //------------------------------------------------------------------------------
    this.Obj._blocking = true;
    //Inputs (WASD/UP/DOWN/LEFT/RIGHT)
    //this.up=0; this.down=0; this.left=0; this.right=0;
    //this.upv=0; this.downv=0; this.leftv=0; this.rightv=0;
    //this._mx=0; this._my=0;//mouse vector x/y
    //this._lx=0; this._ly=0; this._lz=0; this._lr=0;//steering to location/rotation
    //this._tx=0; this._ty=0; this._tr=0;//target or target direction(diff)
    //this._tvx=0; this._tvy=0; this._tr=0;//target or target direction(diff)

    // pixi
    this._sprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/asteroid.png"));
    this._sprite.anchor.set(0.5);
    this._sprite.x = this.x.get();
    this._sprite.y = this.y.get();
    this._spriteSize = this.Obj.radius.get() * 2.6;
    this._sprite.width = this._spriteSize;
    this._sprite.height = this._spriteSize;
    this._sprite.rotation = NGE.Core.MATH.degToRad(this.r.get());
    NGE.PIXI.PixiAsteroidGroup.addChild(this._sprite);

    // for debug - remove for performance
    if (GLOBAL_DEBUG) {
        this._infoText = new PIXI.Text('', { fontFamily: 'Noto Sans', fontSize: 14, fill: 0xffffff });
        this._infoText.anchor.set(0.5,0.5);
        this._infoText.x = this.x.get();
        this._infoText.y = this.y.get();
        NGE.PIXI.PixiAsteroidGroup.addChild(this._infoText);
    }
    // for debug - remove for performance

    this._createTimeLeft = d.new ? oAsteroid.CREATE_ANIMATION_DURATION : 0;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oAsteroid.CREATE_ANIMATION_DURATION = 0.5;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oAsteroid.prototype.Remove = function(silent) {
    if(!silent) {
        var scale = (this.radius.get() * 2) / 64;
        //NGE.Core.ObjectAdd(new oShattered(this._sprite,"Asteroid",scale)); //shatter into pieces
        // explosion
        // adjust scale for asteroid radius
        var preset = JSON.parse(JSON.stringify(Explosion.PRESET.ASTEROID_DESTROYED));
        preset.config.scale.x *= scale;
        preset.config.scale.y *= scale;
        new Explosion(this.x.get(), this.y.get(), preset);
    }
    if (GLOBAL_DEBUG) {
        this._infoText.destroy(); // for debug - remove for performance
    }
    this._sprite.destroy();
    this.Obj.KILL();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oAsteroid.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[ASTEROID] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oAsteroid.prototype.Process = function(dt) {

    if (this.Obj._remove === true) { return; }
    
    //this.Log("Process Asteroid " + this.Obj.w.get())
    var o = this;//deref for clarity
    var x = o.x.get();
    var y = o.y.get();
    var C = this.Game.Core;

//    this._sprite.position.set(this.x.get(), this.y.get());
//    this._sprite.rotation = this.r.get() * (Math.PI/180);

    // refactorme
    var damagedPercent = 100 - (100 * (this.hp.get() / this.max_hp.get()));
    if (damagedPercent > this.Game.DATA.asteroidDamagePercentList[2]){
        this._sprite.texture = NGE.PIXI.TextureFromImage("img/asteroid_critical.png");
    } else {
        if (damagedPercent > this.Game.DATA.asteroidDamagePercentList[1]){
            this._sprite.texture = NGE.PIXI.TextureFromImage("img/asteroid_major.png");
        } else {
            if (damagedPercent > this.Game.DATA.asteroidDamagePercentList[0]){
                this._sprite.texture = NGE.PIXI.TextureFromImage("img/asteroid_minor.png");
            }
        }
    }
    // refactorme

    // for debug 
    if (GLOBAL_DEBUG) { 
        if (NGE.PIXI.DebugP.Active){
            this._infoText.text = Math.round(this.hp.get()) + "/" + this.max_hp.get();
            this._infoText.visible = true;
        } else {
            this._infoText.visible = false;
        }
    }

    var rotation;
    if (NGE.PIXI.WasTabbedAway || (dt >= 1000)) {
        x = this.x.get();
        y = this.y.get();
        rotation = C.MATH.degToRad(this.r.get());
    } else {
        x = C.MV.Lerp(this._sprite.position.x, this.x.get(), this.Game.DATA.LERPRATE);
        y = C.MV.Lerp(this._sprite.position.y, this.y.get(), this.Game.DATA.LERPRATE);
        rotation = C.MV.Lerp(this._sprite.rotation, C.MATH.degToRad(this.r.get()), this.Game.DATA.LERPRATEROT);
    }

    this._sprite.position.set(x, y);
    this._sprite.rotation = rotation;
    
    var createAnimProgress = 1 - this._createTimeLeft / oAsteroid.CREATE_ANIMATION_DURATION;
    
    this._sprite.width = createAnimProgress * this._spriteSize;
    this._sprite.height = createAnimProgress * this._spriteSize;
    
    if (this._createTimeLeft > 0) {
        this._createTimeLeft = Math.max(0, this._createTimeLeft - dt);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = oAsteroid;
