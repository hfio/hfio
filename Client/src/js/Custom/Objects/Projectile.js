//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oProjectile(Game, d, source, pvp, skipAsteroids) {
    this.Game = Game;

    //Generate local ID
    d.id = oProjectile.IDCounter++;

    //Set type automatically
    d.type = NGE.Core.goTypes.indexOf("Projectile"); // FIXME: use enum instead of hardcoding?

    var nd = [
        "initial_x", d.x,
        "initial_y", d.y
    ];
    
    this.terran = !!(source && source.isTerran());
    this.pvp = !!pvp;
    this.skipAsteroids = !!skipAsteroids;

    //Create Object Properly
    this.Obj = new oGameObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }

    //Load texture
    var imagePath = 'img/bullet_friend.png';
    var myShip = NGE.PIXI.myShip;
    if (myShip) {
        if (source === myShip) {
            imagePath = 'img/bullet_me.png';
        } else if (this.canDamage(myShip, true)) {
            imagePath = 'img/bullet_enemy.png';
        }
    } else if (!this.terran) {
        imagePath = 'img/bullet_enemy.png';
    }
    if (!NGE.PIXI.imgs.hasOwnProperty(imagePath)) {
        NGE.PIXI.imgs[imagePath] = NGE.PIXI.TextureFromImage(imagePath);
    }

    //Create sprite
    this.sprite = new PIXI.Sprite(NGE.PIXI.imgs[imagePath]);
    this.sprite.position.x = this.x.get();
    this.sprite.position.y = this.y.get();
    this.sprite.rotation = this.r.get();
    this.sprite.anchor.set(0.9, 0.5);

    //Add sprite to stage
    NGE.PIXI.PixiProjectileGroup.addChild(this.sprite);

    this.skipProcessNeeded = true;
    
    //NGE.PIXI.DebugP.createLine(this.id.get(), this);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oProjectile.prototype.HitCheck = function(x, y, rxy, O) { // checks if the projectile has hit a GameObject (rectangle check)
    var C = this.Game.Core;
    if(C.MATH.isLineIntersect(x, y, rxy[0], rxy[1], O.Obj._x1, O.Obj._y1, O.Obj._x2, O.Obj._y1, this._myship)){ return true; }
    if(C.MATH.isLineIntersect(x, y, rxy[0], rxy[1], O.Obj._x2, O.Obj._y1, O.Obj._x2, O.Obj._y2, this._myship)){ return true; }
    if(C.MATH.isLineIntersect(x, y, rxy[0], rxy[1], O.Obj._x2, O.Obj._y2, O.Obj._x1, O.Obj._y2, this._myship)){ return true; }
    if(C.MATH.isLineIntersect(x, y, rxy[0], rxy[1], O.Obj._x1, O.Obj._y2, O.Obj._x1, O.Obj._y1, this._myship)){ return true; }
    // if both points are inside the hitbox, the intersects will not catch it, but it is a hit
    if((rxy[0] > O.Obj._x1) && (rxy[0] < O.Obj._x2) && (rxy[1] > O.Obj._y1) && (rxy[1] < O.Obj._y2)) { return true; }
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oProjectile.prototype.canDamage = function (ship, disregardDock) {
    var C = this.Game.Core;
    var pvpRadius = this.Game.DATA.fleet_pvp_radius;
    return (this.team.get() !== ship.team.get()) &&
           (disregardDock || !(ship.Obj.docked && ship.Obj.docked.get())) && 
           //fleet logic - no pvp damage in safe area
           // can only damage the ship if...
           // ...any of the two ships is not terran (alien)
           (!this.terran || !ship.isTerran() ||
           // ...or if both ships are within the PvP area (outside PvP radius)
           (this.pvp &&
            !C.CD.inCircle(ship.x.get(), ship.y.get(), 0, 0, ship.radius.get(), pvpRadius)));
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oProjectile.prototype._Hit = function(groupName, filter, explosionPreset) {
    var rxy = [this.x.get(), this.y.get()];
    var objs = NGE.Core.GetGroup(NGE.Core.goTypes.indexOf(groupName));
    for (var id in objs) {
        if (objs.hasOwnProperty(id) && (!filter || filter(objs[id]))) {
            var obj = objs[id];
            if (this.HitCheck(this.initial_x.get(), this.initial_y.get(), rxy, obj))
            {
                new Explosion(this.x.get(), this.y.get(), explosionPreset);
                this.Remove();
                return true;
            }
        }
    }
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oProjectile.prototype.Process = function(dt) {
    //This is a hack to prevent the projectile from being moved on the frame it was created.
    //Without this, the projectile appears to spawn further away from its origin.
    if(this.skipProcessNeeded) {
        this.skipProcessNeeded = false;
        return;
    }

    if (this.Obj._remove === true) { return; }
    
    //Move
    this.x.set(this.x.get() + this.vx.get() * dt);
    this.y.set(this.y.get() + this.vy.get() * dt);
    
    

    // blocked by asteroids
    if (!this.skipAsteroids) {
        var Asteroids = NGE.Core.GetGroup(NGE.Core.goTypes.indexOf("Asteroid"));
        if(NGE.Core.CD.CheckCollisionByRadius(this.x.get(), this.y.get(), 1, Asteroids))
        {
            new Explosion(this.x.get(), this.y.get(), Explosion.PRESET.PROJECTILE_ASTEROID);        
            this.Remove();
            return;
        }
    }
    
    // blocked by standalone turrets (stations)
    if (this._Hit("Turret", (obj)=>(obj.standalone.get() && this.canDamage(obj)), Explosion.PRESET.SHIP_DAMAGED)) {
        return;
    }
    // blocked by bases
    if (this._Hit("Base", (obj)=>(this.canDamage(obj)), Explosion.PRESET.SHIP_DAMAGED)) {
        return;
    }
    // blocked by ships
    if (this._Hit("Ship", (obj)=>(this.canDamage(obj)), Explosion.PRESET.SHIP_DAMAGED)) {
        return;
    }

    //Max distance travelled?
    var p1 = { x: this.initial_x.get(), y: this.initial_y.get() };
    var p2 = { x: this.x.get(), y: this.y.get() };
    var diff =  this.Game.Core.MATH.VecSub(p1.x, p1.y, p2.x, p2.y);
    var distance = this.Game.Core.MATH.VecLength(diff[0], diff[1]);
    if(distance >= this.damage_range.get()) {
        // explosion
        var x = this.initial_x.get() + Math.cos(this.r.get()) * this.damage_range.get();
        var y = this.initial_y.get() + Math.sin(this.r.get()) * this.damage_range.get();
        new Explosion(x, y, Explosion.PRESET.PROJECTILE_REMOVED);
        this.Remove();
        return;
    }

    this.sprite.position.set(this.x.get(), this.y.get());
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oProjectile.prototype.Remove = function(m) {
    this.sprite.destroy();
    //NGE.PIXI.DebugP.removeLine(this.id.get());
    this.Obj.KILL();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oProjectile.IDCounter = 0;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************