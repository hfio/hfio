//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    StationManager - CLIENT SIDE
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function StationManager(ship)
{
    //debugging
    this.DEBUG = true;
    this._ship = ship;

    this._stations = [];
}

// receiving data from server
StationManager.prototype.fromJSON = function(json){

    this._stations = [];
    var list = JSON.parse(json);
    for (var i = 0; i < list.length; i++) {
        var newStation = new Station();
        newStation.fromJSON(list[i]);
        this._stations.push(newStation);
    }
}

StationManager.prototype.findById = function (stationId) {
    var i;
    for (i = 0; i < this._stations.length; i++){
        if (this._stations[i].config.id === stationId){
            return this._stations[i];
        }
    }
    return null;
}

StationManager.prototype.getEquippedStation = function () {
    var i;
    for (i = 0; i < this._stations.length; i++){
        if (this._stations[i].equipped){
            return this._stations[i];
        }
    }
    return null;
}

StationManager.prototype.buy = function(stationId) {
    this.log('buy station: ' + stationId);
    NGE.WS.Send('buystation',{ stationId: stationId });
}

StationManager.prototype.deploy = function() {
    var station = this.getEquippedStation();
    if (station && station.canBeDeployed()) {
        // check for blockage by other stations already on client side (server also checks if this goes through)
        if (NGE.Core.CD.CheckCollisionByRadius(this._ship.x.get(), this._ship.y.get(), station.config.hitbox / 2 + NGE.Stats.Global.station_deploy_min_distance, NGE.Core.GetGroup(NGE.Core.goTypes.indexOf("Base")))) {
            this.log('deploy station fail: blocked by other station');
            this._ship.ShowErrorMessage("Cannot deploy on top of other stations!", 2.5);
            return;
        }
        this.log('deploy station');
        NGE.WS.Send('deploystation',{ });
    }
}

StationManager.prototype.equip = function(stationId){

    this.log('equip station:'+stationId);
    NGE.WS.Send('equipstation',{ stationId: stationId });
}

StationManager.prototype.unequip = function(){

    this.log('unequip station');
    NGE.WS.Send('unequipstation',{ });
}

//StationManager.prototype.sell = function(buffId){
//
//    this.log('sell buff:'+buffId);
//    NGE.WS.Send('sellbuff',{ buffId: buffId });
//}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
StationManager.prototype.log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[StationManager] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
