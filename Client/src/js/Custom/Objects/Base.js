//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Base
    - Movement
    - Collision Detection
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var oGameObject = require('../../Engine/Core/sGO');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oBase(Game, d, d2, isMainStation)
{
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;

    //------------------------------------------------------------------------------
    //Object State (Networked - Changes only)
    //------------------------------------------------------------------------------
    //Custom data needed (for datalist)
    var nd = [
        "dock_radius", d2.dockRadius,
        "side", 1
    ];
    
    //Create Object Properly
    this.Obj = new oGameObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }
    ///this.Log(JSON.stringify(this.Game.Core.DictKeys(this)))
    
    //------------------------------------------------------------------------------
    //INTERNAL
    //------------------------------------------------------------------------------
    //Inputs (WASD/UP/DOWN/LEFT/RIGHT)
    //this.up=0; this.down=0; this.left=0; this.right=0;
    //this.upv=0; this.downv=0; this.leftv=0; this.rightv=0;
    //this._mx=0; this._my=0;//mouse vector x/y
    //this._lx=0; this._ly=0; this._lz=0; this._lr=0;//steering to location/rotation
    //this._tx=0; this._ty=0; this._tr=0;//target or target direction(diff)
    //this._tvx=0; this._tvy=0; this._tr=0;//target or target direction(diff)
    
    // pixi
//    if (this.team.get() == 1) { this.sprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/base.png")); }
//    else{ this.sprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/base_e.png")); }
    var imgBase = "img/" + d2.image;

    this.sprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imgBase + "_top.png"));
    this.bottomSprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imgBase + "_bottom.png"));
    if (d2.dockRadius > 0) {
        this.inviteSprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imgBase + "_invite.png"));
    }
    
    this._inviteFadingIn = true;
    
    var anchor = d2.anchor ? d2.anchor.split(",").map(parseFloat) : [0.5, 0.5];

    this.sprite.anchor.set(anchor[0],anchor[1]);
    this.bottomSprite.anchor.set(anchor[0],anchor[1]);
    if (this.inviteSprite) {
        this.inviteSprite.alpha = 0;
        this.inviteSprite.anchor.set(anchor[0],anchor[1]);
    }
    
    //health bar
    this._healthBar = null;
    if (!isMainStation) {
        this._healthBar = new HealthBar(this);
    }
    
    this.sprite.x = this.x.get();
    this.sprite.y = this.y.get();
    this.sprite.rotation = this.Game.Core.MATH.degToRad(this.r.get());
    
    NGE.PIXI.PixiBaseTopGroup.addChild(this.sprite);
    if (this.inviteSprite) {
        NGE.PIXI.PixiBaseBottomGroup.addChild(this.inviteSprite);
    }
    NGE.PIXI.PixiBaseBottomGroup.addChild(this.bottomSprite);
    
//    this.Log("Created base type " + this.subtype.get() + " at " + this.x.get() + "," + this.y.get() + " config: " + JSON.stringify(d2));
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBase.prototype.Remove = function(silent, hpflag) {
    this.Log("Deleting station " + this.id.get());
    // only add effects if no silent flag (i.e. game is not tabbed away)
    if(!silent) {
        if (!hpflag) {
            // removal because the turret was destroyed -> explosion effect
            //Spawn ejecta
            NGE.PIXI.spawnEjecta(this.x.get(), this.y.get()); 
            
            //Death explosion
            var scale = this.w.get() / 64;
            var preset = JSON.parse(JSON.stringify(Explosion.PRESET.TURRET_DESTROYED));
            preset.config.scale.x *= scale;
            preset.config.scale.y *= scale;
            new Explosion(this.x.get(), this.y.get(), preset);
        } 
    }
    this.sprite.destroy();
    this.bottomSprite.destroy();
    if (this.inviteSprite) {
        this.inviteSprite.destroy();
    }
    if (this._healthBar) {
        this._healthBar.destroy();
    }
    this.Obj.KILL();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBase.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[BASE] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBase.INVITE_FADE_DURATION = 1;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// AI terran ships: team 2; player ships: team 10+
// TODO: propery test if it is a terran ship
oBase.prototype.isTerran = function () { return (this.team.get() === 2) || (this.team.get() > 10); };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBase.prototype.Process = function(dt) {
    
    if (this.Obj._remove === true) { return; }
    
    if (NGE.PIXI.WasTabbedAway || (dt >= 1000)) {
        this.sprite.x = this.x.get();
        this.sprite.y = this.y.get();
        this.sprite.rotation = this.Game.Core.MATH.degToRad(this.r.get());
    } else {
        this.sprite.x = this.Game.Core.MV.Lerp(this.sprite.x, this.x.get(), this.Game.DATA.LERPRATE);
        this.sprite.y = this.Game.Core.MV.Lerp(this.sprite.y, this.y.get(), this.Game.DATA.LERPRATE);
        this.sprite.rotation = this.Game.Core.MV.Lerp(this.sprite.rotation, this.Game.Core.MATH.degToRad(this.r.get()), this.Game.DATA.LERPRATEROT);
    }
    
    this.bottomSprite.x = this.sprite.x;
    this.bottomSprite.y = this.sprite.y;
    this.bottomSprite.rotation = this.sprite.rotation;
    
    if (this.inviteSprite) {
        this.inviteSprite.x = this.sprite.x;
        this.inviteSprite.y = this.sprite.y;
        this.inviteSprite.rotation = this.sprite.rotation;
        
        if (this._inviteFadingIn) {
            if (this.inviteSprite.alpha < 1) {
                this.inviteSprite.alpha = Math.min(1, this.inviteSprite.alpha + dt / oBase.INVITE_FADE_DURATION);
            } else {
                this._inviteFadingIn = false;
            }
        } else {
            if (this.inviteSprite.alpha > 0) {
                this.inviteSprite.alpha = Math.max(0, this.inviteSprite.alpha - dt / oBase.INVITE_FADE_DURATION);
            } else {
                this._inviteFadingIn = true;
            }
        }
    }
    
    //update health bar
    if (this._healthBar) {
        var visible = this.hp.get() < this.max_hp.get();
        this._healthBar.visible = visible;
        if (visible) {
            this._healthBar.update(this.sprite.x, this.sprite.y, this.hp.get() / this.max_hp.get());
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
