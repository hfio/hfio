//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oBGObject(config) {
    //debugging
    this.DEBUG = true;
    
    var defaults = NGE.Core.Copy(NGE.DATA.Object_Defaults);
    defaults.id = NGE.Core.GetNextID();
    defaults.type = NGE.Core.goTypes.indexOf("BGObject"); // (FIXME: use enum instead of hardcoding?)

    var nd = [];

    //Create Object Properly
    this.Obj = new oGameObject(NGE.Core, defaults, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }

    this._sprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage('img/bgobject_' + config.typeIndex + '.png'));
    this._sprite.position.x = config.position.x || 0;
    this._sprite.position.y = config.position.y || 0;
    this._sprite.scale.set(config.scale);
    this._sprite.anchor.set(0.5);
    this._sprite.rotation = Math.random() * 2 * Math.PI;
    
    this._originalX = this._sprite.position.x;
    this._originalY = this._sprite.position.y;
    
    this._moveRate = config.move;

    //this._rotateSpeed = Math.random() * 0.012;

    NGE.PIXI.PixiBGObjectGroup.addChild(this._sprite);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBGObject.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[BGOBJECT] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBGObject.prototype.Process = function(dt) {
    var camera = NGE.PIXI.Camera;
    
    // parallax effect
    this._sprite.position.x = this._originalX + camera.x * (1 - this._moveRate);
    this._sprite.position.y = this._originalY + camera.y * (1 - this._moveRate);
    
    //_this.sprite.rotation += this._rotateSpeed * dt;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oBGObject.prototype.Remove = function(m) {
    this.Log("removing BGObject " + this.id.get());
    this._sprite.destroy();
    this.Obj.KILL();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************