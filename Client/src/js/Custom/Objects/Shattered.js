//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oShattered(sprite, prefix, scale) {
    this.prefix = prefix;
    
    scale = scale || 1;
    var defaults = NGE.Core.Copy(NGE.DATA.Object_Defaults);
    defaults.id = NGE.Core.GetNextID();
    defaults.type = NGE.Core.goTypes.indexOf("Shattered"); // FIXME: use enum instead of hardcoding?

    var nd = [];

    //Create Object Properly
    this.Obj = new oGameObject(NGE.Core, defaults, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }

    var count = NGE.DATA[prefix+"ShatterPieces"];
    var rotateMin = NGE.DATA[prefix+"ShatterRotateMin"];
    var rotateMax = NGE.DATA[prefix+"ShatterRotateMax"];
    var velocityMin = NGE.DATA[prefix+"ShatterVelocityMin"];
    var velocityMax = NGE.DATA[prefix+"ShatterVelocityMax"];

    var shatter = new Shatter(sprite.texture.baseTexture.source, count);
    this._container = new PIXI.Container();

    for(var i=0; i<shatter.images.length; i++) {
        var baseTexture = new PIXI.BaseTexture(shatter.images[i].image);
        var texture = new PIXI.Texture(baseTexture);
        var fragment = new PIXI.Sprite(texture);
        fragment.position.set(
            shatter.images[i].x + shatter.images[i].image.width/2,
            shatter.images[i].y + shatter.images[i].image.height/2
        );
        fragment.anchor.set(0.5);

        var angle = Math.random() * 2 * Math.PI;
        var velocity = (Math.random() * (velocityMax - velocityMin)) + velocityMin;
        fragment.vx = Math.cos(angle) * velocity;
        fragment.vy = Math.sin(angle) * velocity;

        fragment.rv = (Math.random() * 2 - 1) * ((rotateMax - rotateMin) + rotateMin);
        this._container.addChild(fragment);
        // TODO: build collision from polygons at shatter.images[i].img
    }

    this._createdAt = NGE.Core.TIME.Current;

    this._container.position.set(sprite.x, sprite.y);
    this._container.pivot.set((sprite.width/scale)/2, (sprite.height/scale)/2);
    this._container.rotation = sprite.rotation;
    this._container.scale.set(scale);

    sprite.parent.addChild(this._container);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShattered.prototype.Process = function(dt) {
    var fadeDelay = NGE.DATA[this.prefix + "ShatterFadeDelay"];
    var fadeDuration = NGE.DATA[this.prefix + "ShatterFadeDuration"];

    //fade out over time
    if(NGE.Core.TIME.Current >= this._createdAt + fadeDelay * 1000) {
        this._container.alpha = 1 - (((NGE.Core.TIME.Current - (this._createdAt + fadeDelay * 1000)) / 1000) / fadeDuration);
        if(this._container.alpha <= 0) {
            this.Remove();
            return;
        }
    }

    //move fragments by velocity
    for(var j=0; j<this._container.children.length; j++) {
        var fragment = this._container.children[j];
        fragment.x += fragment.vx * dt;
        fragment.y += fragment.vy * dt;
        fragment.rotation += fragment.rv * dt;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShattered.prototype.Remove = function(m) {
    this._container.destroy();
    this.Obj.KILL();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************