//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Ship - CLIENT SIDE

    - Inputs (Mouse, Keyboard)
    - Movement
    - Collision Detection
    - Collecting resources
    - Firing (laser, projectiles)
    - AI
    
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var oGameObject = require('../../Engine/Core/sGO');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oShip(Game, d, silent)
{
    var i;
    
    //debugging
    this.DEBUG = true;
    
    this.Game = Game;

    this.buffManager = new BuffManager(this);
    this.stationManager = new StationManager(this);
    //------------------------------------------------------------------------------
    //Object State (Networked - Changes only)
    //------------------------------------------------------------------------------
    //Custom data needed (for datalist)
    // if modifies, the hardcoded indexes in ws.js need to be changed!
    var nd = [
        "username", "", // player name
        "bounty", 0, // credits_for_kill
        "energy", 0, // ship energy
        "buffs", null, // buff manager
        "docked", false, // currently docked
        "docked_at", 0, // the id of the base of which the ship is within docking range (stays after undock to prevent redocking after docking range is left)
        "default_max_hp", 0, // for health bonus calc - this one doesn't change
        "shoot_drop", 0,//acceleration drop factor
        "shoot_drop_r", 0,//rotation drop factor
        "move_radius", 0,//move when mouse vector outside of radius
        "move_radius_max", 0,//move when mouse vector outside of radius
        "level", 0, "xp", 0,//leveling
        "damageboost", 0, // damage boost
        "speedboost", 0, // speed boost
        "turnboost", 0, // rotation boost
        "fuel", 0, // fuel is unused for now! (do not delete, or update hardcoded indexes in network communication)
        "max_fuel", 0,
        "default_max_fuel", 0,
        "ammo", 0,
        "max_ammo", 0,
        "default_max_ammo", 0,
        "stations", null,
        "message", "", // chat message to display above ship
        "message_scope", 0, // see OBJ_Pixi.CHAT_MODE
        "message_timeleft", 0,
        "f1s", 0, "f2s", 0, "f3s", 0, "f4s", 0, "f5s", 0, "f6s", 0, //speed (seconds) / tweakable        
        "f1on", 0, "f2on", 0, "f3on", 0, "f4on", 0, "f5on", 0, "f6on", 0,//is action on
        "side", 1
        //"a1", 0, "a2", 0, "a3", 0, "a4", 0, "a5", 0, "a6", 0, "a7", 0, "a8", 0, "a9", 0, "a0", 0,  //actions
    ];
    
    //this.Log(JSON.stringify(d))
    
    //Create Object Properly
    this.Obj = new oGameObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }
    
    //this.Log(JSON.stringify(this.Game.Core.DictKeys(this)))
    //this.Log(this.Obj._dl.getkeys())
    
    //------------------------------------------------------------------------------
    //INTERNAL
    //------------------------------------------------------------------------------
    //Inputs (WASD/UP/DOWN/LEFT/RIGHT)
    //this.up=0; this.down=0; this.left=0; this.right=0;
    //this.upv=0; this.downv=0; this.leftv=0; this.rightv=0;
    this._speed=0;
    this._mx=0; this._my=0;//mouse vector x/y
    this._f1=0; this._f2=0; this._f3=0; this._f4=0; this._f5=0; this._f6=0;//Try Fire
    this._f1t=0; this._f2t=0; this._f3t=0; this._f4t=0; this._f5t=0; this._f6t=0;//cooldowns
    this._f1a=null; this._f2a=null; this._f3a=null; this._f4a=null; this._f5a=null; this._f6a=null;//Function Trigger
    this._f1net=0; this._f2net=0; this._f3net=0; this._f4net=0; this._f5net=0; this._f6net=0;//network event
    this._lx=0; this._ly=0; this._lz=0; this._lr=0;//steering to location/rotation
    this._tx=0; this._ty=0; this._tr=0;//target or target direction(diff)
    this._tvx=0; this._tvy=0; this._tr=0;//target or target direction(diff)
    this._svx=0; this._svy=0; this._svvx=0; this._svvy=0; this._svr=0;//sever x, y, vx, vy, r
    this._myship = false;//is this myship
    this._lerpx=0; this._lerpy=0; this._lerpr=0;
    this._lockedvx=0;
    this._lockedvy=0;
    this._locked=false;
    this._throttle = 0;
    
    //client prediction
    this._clx=0; this._cly=0; this._clr=0;
    this._authradiusSQ = 40000;//200 * 200
    
    this.netdata = null;
    
    // TODO: get this data properly
    this.blinkSpriteCount = 2;
    this.blinkWaitTime = 4;
    this.blinkShowTime = 0.5;
    
    this.blinkTimeElapsed = 0;
    this.blinkIndex = 0;
    
    // TODO: get this data properly
    this.thrustFrameDuration = 0.05; // in sec
    
    this.thrustFrameCount = 3;
    this.thrustAnimationSpeed = 1 / (60 * this.thrustFrameDuration); // speed 1 is 60 FPS
    
    this.rotationState = oShip.RotationState.NONE;
    this.thrustStopTimeElapsed = 0;
    this._lastRotation = 0; // to compare with the current one to determine turning direction
    
    this._pixiGroup = this.isTerran() ? NGE.PIXI.PixiPlayerGroup : NGE.PIXI.PixiAlienShipGroup;

    //hitcircle
    this._hitcircle = new PIXI.Graphics();
    this._hitcircle.beginFill(0xFF0000);
    this._hitcircle.fillAlpha = 0.5;
    this._hitcircle.arc(0, 0, this.w.get()/2, 0, 2 * Math.PI)
    this._hitcircle.endFill();
    this._hitcircle.pivot.set(0.5);
    this._hitcircle.visible = false;
    this._pixiGroup.addChild(this._hitcircle);
    
    //arrow pointing in the ship's direction (for the last remaining enemies)
    this._arrow = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/ui/arrow-red.png"));
    this._arrow.visible = false;
    NGE.PIXI.PixiPlayerOverlayGroup.addChild(this._arrow);

    // sprites
    this._spriteGroup = new PIXI.Container(); // a group containing all ships sprites and groups
    this._lightGroup = new PIXI.Container();
    this._blinkGroup = new PIXI.Container();
    
    this._CreateSprites(this.x.get(), this.y.get(), this.Game.Core.MATH.degToRad(this.r.get()));

    this._fireTimer = 0;
    this._firing = false;

    this._lastX = 0;
    this._lastY = 0;
    this._lastVelocityX = 0;
    this._lastVelocityY = 0;
    
    this._engineOscillateTimer = 0;
    this._lastEngineOn = false;
    
    this._createTimeLeft = silent ? 0 : oShip.CREATE_ANIMATION_DURATION;
    
    this._errorMessage = null;
    this._errorMessageTimeLeft = 0;

    //name text
    this._nameText = new PIXI.Text('', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this._nameText.anchor.set(0.5);
    
    NGE.PIXI.PixiPlayerOverlayGroup.addChild(this._nameText);

    //health bar
    this._healthBar = new HealthBar(this);
    
    this._messageBackground = new PIXI.Graphics();
    this._messageBackground.beginFill(0x000000);
    this._messageBackground.alpha = 0.5;
    this._messageBackground.drawRect(0, 0, 8, 8);
    this._messageBackground.endFill();
    this._messageText = new PIXI.Text('', { fontFamily: 'BlenderPro Medium', fontSize: 14, fill: 0xffffff });
    this._messageText.anchor.set(0.5);
    if (this.message_timeleft.get() <= 0) {
        this._messageText.alpha = 0;
    }
    NGE.PIXI.PixiPlayerOverlayGroup.addChild(this._messageBackground);
    NGE.PIXI.PixiPlayerOverlayGroup.addChild(this._messageText);
    
    if (!silent && !this.docked.get()) {
        // warp in ripple effect
        NGE.PIXI.AddRipple(this._spriteGroup.x, this._spriteGroup.y);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.RotationState = { NONE: 0, LEFT: 1, RIGHT: 2, STOPPING_LEFT: 3, STOPPING_RIGHT: 4 };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.CREATE_ANIMATION_DURATION = 0.5;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.USERNAME_STATE = {
    GOOD: 0,
    ALREADY_TAKEN : 1,
    WRONG_LENGTH: 2
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[SHIP:" + this.id.get() + "] " + m); }};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.DistanceFrom = function(go) {
    return NGE.Core.MATH.VecLength(this.x.get() - go.x.get(), this.y.get() - go.y.get());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.DistanceSquaredFrom = function(go) {
    return NGE.Core.MATH.VecLengthSquared(this.x.get() - go.x.get(), this.y.get() - go.y.get());
}
//*********************************************************************************************************************************************
// Returns true if the velocity vector of the ship points in the direction of the passed game object (= the ship gets closer to it every frame, if it doesn't move)
//*********************************************************************************************************************************************
oShip.prototype.IsMovingTowards = function(go) {
    // dot product of vector pointing to the passed game object and the velocity vector -> positive their is angle less than 90 degrees
    return ((go.x.get() - this.x.get()) * this.vx.get() + (go.y.get() - this.y.get()) * this.vy.get()) > 0;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// AI terran ships: team 2; player ships: team 10+
// TODO: propery test if it is a terran ship
oShip.prototype.isTerran = function () { return (this.team.get() === 2) || (this.team.get() > 10); }
//*********************************************************************************************************************************************
// Get position and rotation of ship visuals - always use these from outside the class
//*********************************************************************************************************************************************
oShip.prototype.GetPosX = function() { return this._spriteGroup.position.x; }
oShip.prototype.GetPosY = function() { return this._spriteGroup.position.y; }
oShip.prototype.GetRotation = function() { return this._spriteGroup.rotation; }
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype._CreateSprites = function(x, y, r) {
    var i, imageBaseUrl = "img/ship_" + this.subtype.get();
    // base sprite
    this._regularTexture = NGE.PIXI.TextureFromImage(imageBaseUrl + ".png");
    this._pvpTexture = null;
    if (this.isTerran()) { 
        this._pvpTexture = NGE.PIXI.TextureFromImage(imageBaseUrl + "_neutral.png");
    }
    this._spriteGroup.x = x;
    this._spriteGroup.y = y;
    this._spriteGroup.rotation = r;
    this._pixiGroup.addChild(this._spriteGroup);
    
    this._sprite = new PIXI.Sprite(this._regularTexture);
    this._sprite.anchor.set(0.5);
    this._spriteGroup.addChild(this._sprite);
    
    // lighting
    this._lightFront = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_front_shadow.png"));
    this._lightFront.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    this._lightFront.anchor.set(0.5);
    this._lightGroup.addChild(this._lightFront);
    
    this._lightBack = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_back_shadow.png"));
    this._lightBack.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    this._lightBack.anchor.set(0.5);
    this._lightGroup.addChild(this._lightBack);
    
    this._lightLeft = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_left_shadow.png"));
    this._lightLeft.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    this._lightLeft.anchor.set(0.5);
    this._lightGroup.addChild(this._lightLeft);
    
    this._lightRight = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_right_shadow.png"));
    this._lightRight.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    this._lightRight.anchor.set(0.5);
    this._lightGroup.addChild(this._lightRight);
    
    this._highlightFront = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_front_highlight.png"));
    this._highlightFront.blendMode = PIXI.BLEND_MODES.ADD;
    this._highlightFront.anchor.set(0.5);
    this._lightGroup.addChild(this._highlightFront);
    
    this._highlightBack = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_back_highlight.png"));
    this._highlightBack.blendMode = PIXI.BLEND_MODES.ADD;
    this._highlightBack.anchor.set(0.5);
    this._lightGroup.addChild(this._highlightBack);
    
    this._highlightLeft = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_left_highlight.png"));
    this._highlightLeft.blendMode = PIXI.BLEND_MODES.ADD;
    this._highlightLeft.anchor.set(0.5);
    this._lightGroup.addChild(this._highlightLeft);
    
    this._highlightRight = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_lighting_right_highlight.png"));
    this._highlightRight.blendMode = PIXI.BLEND_MODES.ADD;
    this._highlightRight.anchor.set(0.5);
    this._lightGroup.addChild(this._highlightRight);
    
    // re-add the groups, so they are right above the ship sprite but below other ships
    if (this._lightGroup.parent) {
        this._spriteGroup.removeChild(this._lightGroup);
    }
    this._spriteGroup.addChild(this._lightGroup);
    
    if (this._blinkGroup.parent) {
        this._spriteGroup.removeChild(this._blinkGroup);
    }
    this._spriteGroup.addChild(this._blinkGroup);
    
    // blinking
    this._blink = [];
    for (i = 0; i < this.blinkSpriteCount; i++) {
        var blink = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_blink_" + (i + 1).toString() + ".png"));
        blink.anchor.set(0.5);
        blink.visible = false;
        this._blinkGroup.addChild(blink);
        this._blink.push(blink);
    }
    
    // turning thrusters
    var frames;
    
    frames = [];
    for (i = 0; i < this.thrustFrameCount; i++) {
        frames.push(NGE.PIXI.TextureFromImage(imageBaseUrl + "_thrust_left_" + (i + 1).toString() + ".png"));
    }
    
    this._thrustLeft = new PIXI.extras.AnimatedSprite(frames);
    this._thrustLeft.anchor.set(0.5);
    this._thrustLeft.animationSpeed = this.thrustAnimationSpeed;
    this._thrustLeft.visible = false;
    
    this._spriteGroup.addChild(this._thrustLeft);
    
    frames = [];
    for (i = 0; i < this.thrustFrameCount; i++) {
        frames.push(NGE.PIXI.TextureFromImage(imageBaseUrl + "_thrust_right_" + (i + 1).toString() + ".png"));
    }
    
    this._thrustRight = new PIXI.extras.AnimatedSprite(frames);
    this._thrustRight.anchor.set(0.5);
    this._thrustRight.animationSpeed = this.thrustAnimationSpeed;
    this._thrustRight.visible = false;
    
    this._spriteGroup.addChild(this._thrustRight);
    
    //engine
    this._engine = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_engine.png"));
    this._engine.anchor.set(0.5);
    this._spriteGroup.addChild(this._engine);
    
    this._engineSmall = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_engine_small.png"));
    this._engineSmall.anchor.set(0.5);
    this._spriteGroup.addChild(this._engineSmall);
    
    // cockpit
    this._cockpit = new PIXI.Sprite(NGE.PIXI.TextureFromImage(imageBaseUrl + "_cockpit.png"));
    this._cockpit.anchor.set(0.5);
    this._spriteGroup.addChild(this._cockpit);
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype._DestroySprites = function() {
    this._sprite.destroy();
    this._lightFront.destroy();
    this._lightBack.destroy();
    this._lightLeft.destroy();
    this._lightRight.destroy();
    this._highlightFront.destroy();
    this._highlightBack.destroy();
    this._highlightLeft.destroy();
    this._highlightRight.destroy();
    var i;
    for (i = 0; i < this._blink.length; i++) {
        this._blink[i].destroy();
    }
    this._thrustLeft.destroy();
    this._thrustRight.destroy();
    this._engine.destroy();
    this._engineSmall.destroy();
    this._cockpit.destroy();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.UpdateImage = function() {
    var st = this.subtype.get();
    //change ship image
    var x = this._spriteGroup.position.x;
    var y = this._spriteGroup.position.y;
    var r = this._spriteGroup.rotation;

    this.Log("img/ship_" + st + ".png" + " " + x + " " + y + " " +  r);

    this._DestroySprites();
    this._CreateSprites(x, y, r);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Remove = function(silent, hpflag, creditsAwarded) {
    // only add effects if no silent flag (i.e. game is not tabbed away)
    if(!silent) {
        if (!hpflag) {
            // removal because the ship was destroyed -> explosion effect
            //Shatter into pieces
            NGE.Core.ObjectAdd(new oShattered(this._sprite,"Ship"));

            //Spawn ejecta
            NGE.PIXI.spawnEjecta(this.x.get(), this.y.get());

            //Death explosion
            var scale = this.w.get() / 64;
            var preset = JSON.parse(JSON.stringify(Explosion.PRESET.SHIP_DESTROYED));
            preset.config.scale.x *= scale;
            preset.config.scale.y *= scale;
            new Explosion(this.x.get(), this.y.get(), preset);

            //Shake screen
            var localShip = {
                x: (NGE.PIXI.myShip ? NGE.PIXI.myShip.x.get() : NGE.PIXI.Camera.x),
                y: (NGE.PIXI.myShip ? NGE.PIXI.myShip.y.get() : NGE.PIXI.Camera.y)
            };
            var shakeMinAmplitude = 0;
            var shakeMinDistance = 0;
            var shakeMaxAmplitude = NGE.DATA.ShipExplodeShakeAmplitude;
            var shakeMaxDistance = NGE.DATA.ShipExplodeShakeRange;
            var diff = NGE.Core.MATH.VecSub(this.x.get(), this.y.get(), localShip.x, localShip.y);
            var distance = NGE.Core.MATH.VecLength(diff[0], diff[1]);
            var shakeAmplitude = NGE.Core.MATH.Map(distance, shakeMinDistance, shakeMaxDistance, shakeMaxAmplitude, shakeMinAmplitude);
            NGE.PIXI.ScreenShakeAmplitude = Math.max(NGE.PIXI.ScreenShakeAmplitude, shakeAmplitude);
            
            // displaying earned credits with a floating number
            if (creditsAwarded) {
                this.Log("credits awarded: " + creditsAwarded);
                NGE.PIXI.HUD.addChild(new CreditMarker(this.x.get(), this.y.get(), creditsAwarded));
            }
        } else {
            // removal with the ship still existing -> warp out ripple effect
            NGE.PIXI.AddRipple(this.x.get(), this.y.get());
        }
    } 

    this._DestroySprites();
    this._lightGroup.destroy();
    this._nameText.destroy();
    this._healthBar.destroy();
    this._hitcircle.destroy();
    this._arrow.destroy();
    this._messageText.destroy();
    this._messageBackground.destroy();
    this.Obj.KILL();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Collect = function(amt) {
    var d = this.Obj;
    var level = d.get("level");
    var xp = d.get("xp");
    
    xp+=amt;
    
    if (level >= 60) { return; }

    //did this level up
    if (xp >= this.Core.GD["xp" + (level + 1)]) {
        //this.level++;
        d.set("level", level + 1);
        this.nextlevelxp = this.Core.GD["xp" + (this.level + 1)];
        //apply level bonuses / show options....etc
    }
    var lvlxp = 0;
    if (level > 0) {
        lvlxp = this.Core.GD["xp" + (level)];    
    }
    this.nextlevel = ((xp - lvlxp) / (this.nextlevelxp - lvlxp))  * 100;//percent to next level
    //C.Log((this.nextlevelxp - lvlxp) + " " + (this.xp - lvlxp));
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.ProcessActions = function(dt) {
    
    //if trying, then trigger if possible
    
    if (this._f1 === 1) { if (this._f1t === 0) { if(this._f1a){ this._f1a(dt); } this._f1t += this.f1s.get(); } }
    if (this._f2 === 1) { if (this._f2t === 0) { if(this._f2a){ this._f2a(dt); } this._f2t += this.f2s.get(); } }
    if (this._f3 === 1) { if (this._f3t === 0) { if(this._f3a){ this._f3a(dt); } this._f3t += this.f3s.get(); } }
    if (this._f4 === 1) { if (this._f4t === 0) { if(this._f4a){ this._f4a(dt); } this._f4t += this.f4s.get(); } }
    if (this._f5 === 1) { if (this._f5t === 0) { if(this._f5a){ this._f5a(dt); } this._f5t += this.f5s.get(); } }
    if (this._f6 === 1) { if (this._f6t === 0) { if(this._f6a){ this._f6a(dt); } this._f6t += this.f6s.get(); } }

    //Actions Cooldowns
    if (this._f1t > 0) { this._f1t -= dt; if (this._f1t < 0) { this._f1t = 0; } }
    if (this._f2t > 0) { this._f2t -= dt; if (this._f2t < 0) { this._f2t = 0; } }
    if (this._f3t > 0) { this._f3t -= dt; if (this._f3t < 0) { this._f3t = 0; } }
    if (this._f4t > 0) { this._f4t -= dt; if (this._f4t < 0) { this._f4t = 0; } }
    if (this._f5t > 0) { this._f5t -= dt; if (this._f5t < 0) { this._f5t = 0; } }
    if (this._f6t > 0) { this._f6t -= dt; if (this._f6t < 0) { this._f6t = 0; } }
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.CanDockAt = function(base) {
    var baseSide = base.side.get();
    var shipSide = this.Obj.side.get();
    if (baseSide === shipSide){
        var dockRadius = base.dock_radius.get();
        if ((dockRadius > 0) && (this.DistanceFrom(base) <= dockRadius)) {
            return true;
        }
    }
    return false;
}
//*********************************************************************************************************************************************
// Calculates and returns the target rotation based on mouse input
//*********************************************************************************************************************************************
oShip.prototype.getTargetRotation = function () {
    return this.Game.Core.MATH.XYToDegree(this._mx, this._my);
}
//*********************************************************************************************************************************************
// Returns the last calculated throttle 
//*********************************************************************************************************************************************
oShip.prototype.getThrottle = function () {
    return this._throttle;
}
//*********************************************************************************************************************************************
// Calculates and returns the throttle based on mouse input
//*********************************************************************************************************************************************
oShip.prototype._calculateThrottle = function () {
    var o = this;//deref for clarity
    var C = this.Game.Core;
    var x = o.x.get();
    var y = o.y.get();
    
    // calculate throttle
    var minMoveRadius = o.move_radius.get();
    var maxMoveRadius = o.move_radius_max.get();
    if (C.CD.pointCircle(x, y, x + o._mx, y + o._my, minMoveRadius)) {
        // no throttle within minimum move circle
        this._throttle = 0;
    } else if (C.CD.pointCircle(x, y, x + o._mx, y + o._my, maxMoveRadius)) {
        // calculated throttle between min and max circles
        var moveDist = Math.sqrt(o._mx * o._mx + o._my * o._my) - minMoveRadius;
        var moveDiff = maxMoveRadius - minMoveRadius;

        this._throttle = (moveDist / moveDiff);
    } else { 
        // max throttle outside of max circle
        this._throttle = 1;
    }
    return this._throttle;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.ShowErrorMessage = function(message, duration) {
    this._errorMessage = message;
    this._errorMessageTimeLeft = duration;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oShip.prototype.Process = function(dt) {
    
    if (this.Obj._remove === true) { return; }
    
    var o = this;//deref for clarity
    var C = this.Game.Core;
    
    // Update username over ship if field set
    if ((o.team.get() > 10) || (GLOBAL_DEBUG && NGE.PIXI.DebugP.Active)) { // players ships are team 10+ - TODO: properly test if it is player ship
        var u = NGE.PIXI.HUD.usernameButton;
        if (u.FailedUsernameTimeLeft > 0) {
            switch (u.FailedUsernameState) {
                case HudUsernameButton.USERNAME_STATE.ALREADY_TAKEN:
                    o._nameText.text = 'Username "' + u.FailedUsername + '" already taken!';
                    break;
                case HudUsernameButton.USERNAME_STATE.WRONG_LENGTH:
                    o._nameText.text = 'Username must be between 3-25 characters long!';
                    break;
                default:
                    o._nameText.text = 'Wrong username "' + u.FailedUsername + '"!';
                    break;
            }
            o._nameText.style.fill = 0xff0000;
        } else if (o.username.get() !== '') { 
            o._nameText.text = o.username.get() || HudUsernameButton.DEFAULT_TEXT;
            o._nameText.style.fill = 0xffffff;
        } else {
            o._nameText.text = '';
        }
    }
    
    //---------------------------------------------------------------------------------------------------------
    //Ship Movement
    //---------------------------------------------------------------------------------------------------------
    
    var x = o.x.get();
    var y = o.y.get();
    var vx = o.vx.get();
    var vy = o.vy.get();
    var ox = x;
    var oy = y;
    
    var max_speed = (o.max_speed.get() + o.speedboost.get()) * ((o._f1 === 1) ? o.shoot_drop.get() : 1);
    
    //---------------------------------------------------------------------------------------------------------
    // Local calculation of movement for myShip (unless game was tabbed away, then update position from server same as with other ships)
    //---------------------------------------------------------------------------------------------------------
    if (o._myship && !NGE.PIXI.WasTabbedAway) {
        // update tech state with the one got from server
        this.buffManager.fromJSON(this.buffs.get());
        this.stationManager.fromJSON(this.stations.get());
        
        NGE.PIXI.myTeam = o.team.get(); // remember team for when the ship is destroyed
        
        if (!o.docked.get()) {
            //-----------------------
            // calculating target speed
            var tr = this.getTargetRotation();
            var throttle = this._calculateThrottle();
            var target_speed = max_speed * throttle;
            
            //SHIFT LOCK
            if(o._locked === false) {
                if(NGE.PIXI.Input.SHIFT === 1) {
                    o._locked = true;
                    //o.Log("LOCKED");
                    o._lockedvx = vx;
                    o._lockedvy = vy;
                }

                //-----------------------
                // applying acceleration to change velocity vector towards target velocity vector (ship thrusters)
                var tvxy = C.MV.MoveAlongCurrentRotation(target_speed, tr); // target velocity
                var acc = max_speed / o.acceleration.get(); // "acceleration" actually holds how many seconds are needed to reach max speed
                var vdiff = C.MATH.VecSub(tvxy[0], tvxy[1], vx, vy);
                var len = C.MATH.VecLength(vdiff[0], vdiff[1]);
                if (len > 0.001) {
                    vx += vdiff[0] / len * acc * dt;
                    vy += vdiff[1] / len * acc * dt;
                }
            } else {
                if(NGE.PIXI.Input.SHIFT === 0) {
                    o._locked = false;
                    //o.Log("UNLOCKED");
                }
                // Move along Current Rotation (SHIFT LOCKED)
                // no change to vx, vy
            }

            //-----------------------
            // calculating new position
            x += vx * dt;
            y += vy * dt;

            //-----------------------
            // applying position based mechanics to final, valid position
            o.x.set(x);
            o.y.set(y);
            o.Obj.UpdateBounds();
            o.vx.set(vx);
            o.vy.set(vy);
            
            // dock with base
            NGE.PIXI.baseToDockAt = null;
            if (!o._dockingAt) {
                // start docking process if a suitable station is found
                // docked_at marks the id of the base within docking range as detected by the server (need to leave the docking range before redocking)
                if (!o.docked_at.get()) {
                    C.ExecuteForObjects("Base", (base) => {
                        if (this.CanDockAt(base)) {
                            NGE.PIXI.baseToDockAt = base;
                        }
                    });
                }
            } else {
                // cancel docking process
                if (!this.CanDockAt(this._dockingAt)) {
                    this._dockingAt = null;
                }
            }

            // asteroid collision
            var Asteroids = C.GetGroup(C.goTypes.indexOf("Asteroid"));

            var shipRadius = o.Obj.w.get() * 0.5;
            var blocker = C.CD.CheckCollisionByRadius(x, y, shipRadius, Asteroids);
            if (blocker) {
                //backtrack and bump if collided with an asteroid
                var dx = x - blocker.x.get();
                var dy = y - blocker.y.get();
                var dist = C.MATH.VecLength(dx, dy);
                dx /= dist;
                dy /= dist;
                var dot = vx * dx + vy * dy; // dot product
                vx -= 2 * dx * dot * (NGE.Stats.Global.collision_bumpiness || 1);
                vy -= 2 * dy * dot * (NGE.Stats.Global.collision_bumpiness || 1);
                o.x.set(ox);
                o.y.set(oy);
                o.Obj.UpdateBounds();
                o.vx.set(vx);
                o.vy.set(vy);
                new Explosion(x - shipRadius * dx, y - shipRadius * dy, Explosion.PRESET.SHIP_ASTEROID);
//                this.Log("asteroid collision");
            }

            //-----------------------
            // Linear Rotation (180 to -180)
            var rotationBonus = (100 + o.turnboost.get()) * 0.01;
            var turnRate = o.rot_speed.get() * rotationBonus * ((o._f1 === 1) ? o.shoot_drop_r.get() : 1);
            this.Obj.Turn(tr, turnRate, dt);
        } else {
            // when docked at base
            o._dockingAt = null;
            var baseId = o.docked_at.get();
            var base = C.GetObject("Base", baseId);
            if (base) {
                o.x.set(base.x.get());
                o.y.set(base.y.get());
                o.vx.set(0);
                o.vy.set(0);
            } else {
                this.Log("Can't find base " + baseId + " where the ship is docked at!");
            }
        }
        //Handle Active Actions (Generic)
        o.ProcessActions(dt);
    } else {
        // non-layer ships:
        // X and Y set during update from server, but internal bounds data needs to be updated
        o.Obj.UpdateBounds();
    }
    
    //if my own ship or coming back from tabbed away state, set instantly
    //otherwise lerp to last update
    var rotation;
    if (o._myship || NGE.PIXI.WasTabbedAway || (dt >= 1000)) {
        x = this.x.get();
        y = this.y.get();
        rotation = C.MATH.degToRad(this.r.get());
        //this.Log("MyShip INSTA Rot: " + rotation + " dropr " + o.shoot_drop_r.get() + " drop " + o.shoot_drop.get());
    }
    else
    {
        x = C.MV.Lerp(this._spriteGroup.position.x, this.x.get(), this.Game.DATA.LERPRATE);
        y = C.MV.Lerp(this._spriteGroup.position.y, this.y.get(), this.Game.DATA.LERPRATE);
        rotation = C.MV.Lerp(this._spriteGroup.rotation, C.MATH.degToRad(this.r.get()), this.Game.DATA.LERPRATEROT);
    }
    
    var radius = this.w.get() / 2;
    var inPvPZone = !C.CD.inCircle(x, y, 0,0, radius, this.Game.DATA.fleet_pvp_radius);
    var engineOn = false;
    
    this._spriteGroup.position.set(x, y);
    
    var createAnimProgress = 1 - this._createTimeLeft / oShip.CREATE_ANIMATION_DURATION;
    
    this._spriteGroup.scale.set(createAnimProgress, createAnimProgress);
    
    if (o.Obj.docked.get()) {
        this._sprite.visible = false;
        this._nameText.visible = false;
        this._thrustLeft.visible = false;
        this._thrustRight.visible = false;
        this._healthBar.visible = false;
        this._lightGroup.visible = false;
        this._blinkGroup.visible = false;
        this._engine.visible = false;
        this._engineSmall.visible = false;
        this._cockpit.visible = false;
        this._messageText.visible = false;
        this._messageBackground.visible = false;
        
        this._errorMessageTimeLeft = 0;
        
        this._firing = false;
    } else {
        this._sprite.visible = true;
        this._nameText.visible = !!this._nameText.text;
        this._healthBar.visible = true;
        this._lightGroup.visible = true;
        this._blinkGroup.visible = true;
        this._cockpit.visible = true;
    
        // updating main sprite

        // different terran ship texture when in PVP area
        if (this._pvpTexture && (o.team.get() !== NGE.PIXI.myTeam) && inPvPZone) {
            this._sprite.texture = this._pvpTexture;
        } else {
            this._sprite.texture = this._regularTexture;
        }

        this._spriteGroup.rotation = rotation;

        var rotatingRight = (rotation - this._lastRotation) > 0.01; // in radians
        var rotatingLeft = (rotation - this._lastRotation) < -0.01;

        // turning thrusters

        if (rotatingLeft) {
            this.rotationState = oShip.RotationState.LEFT;
        } else if (rotatingRight) {
            this.rotationState = oShip.RotationState.RIGHT;
        } else {
            switch (this.rotationState) {
                case oShip.RotationState.LEFT:
                    this.rotationState = oShip.RotationState.STOPPING_LEFT;
                    this.thrustStopTimeElapsed = 0;
                    break;
                case oShip.RotationState.RIGHT:
                    this.rotationState = oShip.RotationState.STOPPING_RIGHT;
                    this.thrustStopTimeElapsed = 0;
                    break;
                case oShip.RotationState.STOPPING_LEFT:
                case oShip.RotationState.STOPPING_RIGHT:
                    this.thrustStopTimeElapsed += dt;
                    if (this.thrustStopTimeElapsed >= this.thrustFrameDuration) {
                        this.rotationState = oShip.RotationState.NONE;
                    }
                    break;
            }
        }
        switch (this.rotationState) {
            case oShip.RotationState.LEFT:
            case oShip.RotationState.STOPPING_RIGHT:
                this._thrustLeft.visible = true;
                this._thrustLeft.play();
                this._thrustRight.visible = false;
                this._thrustRight.stop();
                break;
            case oShip.RotationState.RIGHT:
            case oShip.RotationState.STOPPING_LEFT:
                this._thrustLeft.visible = false;
                this._thrustLeft.stop();
                this._thrustRight.visible = true;
                this._thrustRight.play();
                break;
            default:
                this._thrustLeft.visible = false;
                this._thrustLeft.stop();
                this._thrustRight.visible = false;
                this._thrustRight.stop();
        }

        //update hitcircle
        this._hitcircle.x = x;
        this._hitcircle.y = y;

        //update health bar
        this._healthBar.updateSize(this.w.get());
        this._healthBar.update(x, y, this.hp.get() / this.max_hp.get());
        
        this._nameText.x = x;
        this._nameText.y = y - this.w.get() * 0.8 - this._nameText.height;

        //lighting

        // vector pointing in the direction the ship is facing
        var directionX = Math.cos(this._spriteGroup.rotation);
        var directionY = Math.sin(this._spriteGroup.rotation);

        var light = NGE.PIXI.Light;

        // dot product of direction (d) and light vector (l) -> equals |d|*|l|*cos(angle)
        // d and l are unit vectors, so we get the cos right away, use that for lighting
        var cosFrontAngle = directionX * light.x + directionY * light.y;

        if (cosFrontAngle > 0) {
            this._lightFront.alpha = cosFrontAngle;
            this._lightFront.visible = true;
            this._lightBack.visible = false;
            this._highlightFront.alpha = cosFrontAngle;
            this._highlightFront.visible = true;
            this._highlightBack.visible = false;
        } else {
            this._lightFront.visible = false;
            this._lightBack.alpha = -cosFrontAngle;
            this._lightBack.visible = true;
            this._highlightFront.visible = false;
            this._highlightBack.alpha = -cosFrontAngle;
            this._highlightBack.visible = true;
        }

        // similarly with right / left lighting - right vector is [-dy;dx] (from the direction vector)
        var cosRightAngle = - directionY * light.x + directionX * light.y;

        if (cosRightAngle > 0) {
            this._lightRight.alpha = cosRightAngle;
            this._lightRight.visible = true;
            this._lightLeft.visible = false;
            this._highlightRight.alpha = cosRightAngle;
            this._highlightRight.visible = true;
            this._highlightLeft.visible = false;
        } else {
            this._lightRight.visible = false;
            this._lightLeft.alpha = -cosRightAngle;
            this._lightLeft.visible = true;
            this._highlightRight.visible = false;
            this._highlightLeft.alpha = -cosRightAngle;
            this._highlightLeft.visible = true;
        }

        // blinking
        if (this._blink.length > 0) {
            if (this.blinkTimeElapsed > this.blinkWaitTime) {
                this._blink[this.blinkIndex].visible = true;
            } else {
                this._blink[this.blinkIndex].visible = false;
            }
            this.blinkTimeElapsed += dt;
            if (this.blinkTimeElapsed > (this.blinkWaitTime + this.blinkShowTime)) {
                this.blinkTimeElapsed = 0;
                this._blink[this.blinkIndex].visible = false;
                this.blinkIndex = (this.blinkIndex + 1) % this._blink.length; 
            }
        }

        //Oscillate engine alpha
        var minAlpha = NGE.DATA.ShipEngineMinAlpha;
        //var lastSpeed = NGE.Core.MATH.VecLength(this._lastVelocityX, this._lastVelocityY);
        var currSpeed = NGE.Core.MATH.VecLength(this.vx.get(), this.vy.get());
        // there is a throttle calculated for myShip, but for others, it is calculated only on the server / other clients
        // so we use their speed instead
        engineOn = (o._myship && (this._throttle > 0)) || (!o._myship && (currSpeed > 5));// && currSpeed >= lastSpeed; // show always while moving, not just on acceleration - smoother effect, not restarting on every short deceleration
        var engine, otherEngine;
        if ((o._myship && (this._throttle < 1)) || (!o._myship && (currSpeed < (max_speed - 5)))) {
            engine = this._engineSmall;
            otherEngine = this._engine;
        } else {
            engine = this._engine;
            otherEngine = this._engineSmall;
        }
        engine.visible = true;
        otherEngine.visible = false;
        if(engineOn) {
            var maxAlpha = NGE.DATA.ShipEngineMaxAlpha;
            var hz = NGE.DATA.ShipEngineHz;
            this._engineOscillateTimer = (!this._lastEngineOn ? 0 : this._engineOscillateTimer + dt);
            engine.alpha = minAlpha + (maxAlpha - minAlpha) * Math.cos(this._engineOscillateTimer * hz * Math.PI * 2);
        } else {
            //Tween alpha to 0
            engine.alpha -= engine.alpha * dt * 6;
        }
        
        // update message text
        var errorShown = false;
        // use it for error messages
        if (this._errorMessage && (this._errorMessageTimeLeft > 0)) {
            this._messageText.text = this._errorMessage;
            this._messageText.style.fill = 0xff6622;
            this._errorMessageTimeLeft -= dt;
            this._messageText.alpha = (this._errorMessageTimeLeft > 0) ? 1 : 0;
            this._messageText.visible = true;
            errorShown = true;
        } else {
            var timeLeft = this.message_timeleft.get();
            var scope = this.message_scope.get(); 
            // TODO: message scope check - would be better to do on the server side to avoid hacking - but more complicated
            if (((scope === OBJ_Pixi.CHAT_MODE.ALL) || (NGE.PIXI.myShip && (NGE.PIXI.myShip.team.get() === this.team.get()))) && 
                    ((timeLeft > 0) || (this._messageText.alpha > 0))) {
                this._messageText.text = '"' + this.message.get() + '"';
                this._messageText.style.fill = (scope === OBJ_Pixi.CHAT_MODE.ALL) ? 0xffffff : 0x00dbff; // different color for general and team chat
                if (timeLeft > 0) {
                    this._messageText.alpha = 1;
                } else {
                    this._messageText.alpha = Math.max(0, this._messageText.alpha - dt);
                }
                this._messageText.visible = true;
            } else {
                this._messageText.visible = false;
            }
        }
        if (this._messageText.visible) {
            this._messageText.x = x;
            this._messageText.y = y - this.w.get() * 0.8 - this._nameText.height - 4 - this._messageText.height;
            if (errorShown) {
                this._messageBackground.width = this._messageText.width + 4;
                this._messageBackground.height = this._messageText.height + 4;
                this._messageBackground.x = this._messageText.x - this._messageBackground.width * 0.5;
                this._messageBackground.y = this._messageText.y - this._messageBackground.height * 0.5;
                this._messageBackground.visible = true;
                this._messageBackground.alpha = this._messageText.alpha * 0.65;
            }
        } else {
            this._messageBackground.visible = false;
        }
        
    
        var defaults;

        // spawn projectile(s)
        var wasFiring = this._firing;
        this._firing = (this.id.get() === NGE.WS.netid && NGE.PIXI.Input.FIRE1);

        //AI or Other Player firing
        if(o.f1on.get() === 1) {this._firing = true}

        //tracking firing
        if(this._firing && ((this.max_ammo.get() === 0) || (this.ammo.get() > 0)) && (this.f1s.get() > 0)) {
            
            var fireInterval = this.f1s.get(); // - depends on equipped weapon

            if(!wasFiring) {
                this._fireTimer = fireInterval; // allow immediate shot
            } else {
                this._fireTimer += NGE.Core.TIME.Delta; // accumulate time to throttle subsequent shots
            }

            if(this._fireTimer >= fireInterval) {
                this._fireTimer = this._fireTimer % fireInterval;

                var pspeed = NGE.DATA.ProjectileSpeed;

                var spread = NGE.DATA.FireSpreadDegrees;
                
                var angle = this._spriteGroup.rotation + (Math.random() - 0.5) * NGE.PIXI.Math2D_degToRad(spread);

                var projectileWidth = NGE.Stats.Global.projectile_width;
                defaults = NGE.Core.Copy(NGE.DATA.Object_Defaults);
                defaults.x = this._spriteGroup.x + Math.cos(angle) * (radius + projectileWidth);
                defaults.y = this._spriteGroup.y + Math.sin(angle) * (radius + projectileWidth);
                defaults.vx = Math.cos(angle) * pspeed;
                defaults.vy = Math.sin(angle) * pspeed;
                defaults.r = angle;
                defaults.team = this.team.get();
                defaults.damage_range = this.damage_range.get();

                NGE.Core.ObjectAdd(new oProjectile(NGE, defaults, this, inPvPZone));
            }
        }
    }

    this._lastEngineOn = engineOn;
    this._lastX = this.x.get();
    this._lastY = this.y.get();
    this._lastVelocityX = this.vx.get();
    this._lastVelocityY = this.vy.get();
    this._lastRotation = this._spriteGroup.rotation;
    
    if (this._createTimeLeft > 0) {
        this._createTimeLeft = Math.max(0, this._createTimeLeft - dt);
    }
    
    // show arrow pointing in the ship's direction if it is one of the last remaining enemies
    if ((NGE.PIXI.myShip) && (!NGE.PIXI.testStationMenu.container.visible) && (NGE.WaveShipLeftCount <= NGE.Stats.Global.show_arrows_ship_count) && (this.team.get() === NGE.ALIEN_AI_TEAM)) {
        x = this.x.get();
        y = this.y.get();
        var w = this.w.get()*0.5;
        var myShip = NGE.PIXI.myShip;
        var mx = myShip.x.get();
        var my = myShip.y.get();
        // only show the arrow if ship is out of the screen
        if (((Math.abs(x - mx) - w) > window.innerWidth * 0.5) || ((Math.abs(y - my) - w) > window.innerHeight * 0.5)) {
            var dist = this.DistanceFrom(myShip);
            var arrowDist = Math.min(window.innerWidth, window.innerHeight) * 0.3;
            var dx = Math.round((x - mx) / dist * arrowDist);
            this._arrow.x = mx + dx;
            var dy = Math.round((y - my) / dist * arrowDist);
            this._arrow.y = my + dy;
            this._arrow.visible = true;
            this._arrow.rotation = Math.atan2(dy, dx);
        } else {
            this._arrow.visible = false;
        }
    } else {
        this._arrow.visible = false;
    }
    
    //DEBUG
    var mw = this.w.get() / 2;
    var mh = this.h.get() / 2;
    var mlist = [[-mw,-mh],[mw,-mh],[mw, mh],[-mw,mh], [-mw,-mh]];
    
    if (GLOBAL_DEBUG) {
        if (o._myship) {
            NGE.PIXI.DebugP.create(this.id.get(), mlist, this._svx, this._svy, this._svr);
        }
        else{
            NGE.PIXI.DebugP.create(this.id.get(), mlist, this.x.get(), this.y.get(), this.r.get());    
        }
    }
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
