//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Buff (Tech instance) - CLIENT SIDE
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function Buff()
{
    //debugging
    this.DEBUG = true;

    // instance data
    this._instanceId = undefined; // Playfab item instance ID
    this._bonuslist = [];
    this.equipped = false;
    this.level = 0;
    this.active = false;
    this.timeLeft = 0;

    // general data (same for the same type of tech)
    this.config = null;

}

// receiving data from server
Buff.prototype.fromJSON = function(json){

    var obj = JSON.parse(json);
    this._instanceId = obj.instanceId;
    this.equipped = obj.equipped;
    this.level = obj.level;
    this.active = obj.active;
    this.timeLeft = obj.timeLeft;
    this.config = obj.config;
    var bonuslist = obj.bonuslist;
    this._bonuslist = [];
    for (var i=0;i<bonuslist.length;i++){
        var newBonus = new Bonus();
        newBonus.fromJSON(bonuslist[i]);
        this._bonuslist.push(newBonus);
    }

}

Buff.prototype.toString = function() {
    return (this.config && this.config.name) + " (" + this._instanceId + ")";
}

Buff.prototype.getInstanceId = function() {
    return this._instanceId;
}

Buff.prototype.isWeapon = function() {
    return (this.config.slot === 0);
}

Buff.prototype.isFullyUpgraded = function () {
    return this.level >= this.config.levels.length - 1;
}

Buff.prototype.getValue = function() {
    return this.config.levels[this.level];
}

Buff.prototype.getNextLevelValue = function() {
    return this.config.levels[this.level + 1];
}

Buff.prototype.getSellTechPoints = function() {
    return this.config.sellTechPoints[this.level];
}

Buff.prototype.getCooldown = function() {
    return this.config.cooldown[this.level];
}

Buff.prototype.getRange = function() {
    return this.config.range[this.level];
}

Buff.prototype.getDuration = function() {
    return this.config.duration[this.level];
}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Buff.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[Buff] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = Buff;
