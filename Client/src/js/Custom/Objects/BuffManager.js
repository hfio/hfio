//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    BuffManager - CLIENT SIDE
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var Buff = require('../../Custom/Objects/Buff');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function BuffManager(ship)
{
    //debugging
    this.DEBUG = true;
    this._ship = ship;

    this._buffs = [];



}

// receiving data from server
BuffManager.prototype.fromJSON = function(json){

    this._buffs = [];
    var list = JSON.parse(json);
    for (var i=0;i<list.length;i++){
        var newBuff = new Buff();
        newBuff.fromJSON(list[i]);
        this._buffs.push(newBuff);
    }
}

BuffManager.prototype.activate = function(index){
    this.log('activate slot:'+index);
    NGE.WS.Send('activate',{ slot: index });
}

//BuffManager.prototype.toggle = function(index){
    // old code -> now duration-cooldown instead of toggling
    //this.log('toggle slot:'+index);
    //NGE.WS.Send('toggle',{ slot: index });
//}

BuffManager.prototype.upgrade = function(buff){

    this.log('upgrade buff:'+buff.config.name);
    NGE.WS.Send('upgrade',{ instanceId: buff.getInstanceId() });
}

BuffManager.prototype.equip = function(buff){

    this.log('equip buff:'+buff.config.name);
    NGE.WS.Send('equip',{ instanceId: buff.getInstanceId() });
}

BuffManager.prototype.unequip = function(buff){

    this.log('unequip buff:'+buff.config.name);
    NGE.WS.Send('unequip',{ instanceId: buff.getInstanceId() });
}

BuffManager.prototype.sell = function(buff){

    this.log('sell buff:'+buff.config.name);
    NGE.WS.Send('sellbuff',{ instanceId: buff.getInstanceId() });
}

BuffManager.prototype.reroll = function(buff){

    this.log('reroll buff:'+buff.config.name);
    NGE.WS.Send('reroll',{ instanceId: buff.getInstanceId() });
}

//BuffManager.prototype.getEnergyUsed = function(){
//    var used = 0;
//    for (var s=0;s<this._buffs.length;s++){
//        var bc = this._buffs[s].config;
//        if (bc.active){
//            used+=bc.energyCost;
//        }
//    }
//    return used;
//}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
BuffManager.prototype.log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[BuffManager] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = BuffManager;
