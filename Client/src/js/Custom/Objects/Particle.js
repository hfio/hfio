//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oParticle(Game, d, preset) {
	this.Game = Game;

	//Generate local ID
    d.id = oParticle.IDCounter++;

    //Set type automatically
    d.type = NGE.Core.goTypes.indexOf("Particle"); ; //FIXME: use enum instead of hardcoding?

    var nd = [];

    //Create Object Properly
    this.Obj = new oGameObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }

    //Load image
    if (!NGE.PIXI.imgs.hasOwnProperty(preset.imagePath)) {
        NGE.PIXI.imgs[preset.imagePath] = NGE.PIXI.TextureFromImage(preset.imagePath);
    }

    //Create emitter
    var texture = NGE.PIXI.imgs[preset.imagePath];
    this.container = new PIXI.Container();
    this.emitter = new PIXI.particles.Emitter(this.container, [texture], preset.config);

    //Set position
    this.emitter.updateOwnerPos(this.x.get(), this.y.get());

    //Add to stage for viewing
    NGE.PIXI[preset.groupName].addChild(this.container);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oParticle.prototype.Process = function(dt) {
    this.emitter.update(dt);
    if (this.emitter.particleCount === 0) {
        this.Remove();
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oParticle.prototype.Remove = function() {
    this.Obj.KILL();
    this.emitter.destroy();
    this.container.destroy();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oParticle.PRESET = {
	PROJECTILE_REMOVED: {
		imagePath: "img/particle.png",
		groupName: "PixiProjectileGroup",
		config: {
			"alpha": { "start": 0.74, "end": 0 },
		    "scale": { "start": 0.25, "end": 0.05 },
		    "color": { "start": "0000ff", "end": "0000ff" },
		    "speed": { "start": 64, "end": 0 },
		    "startRotation": { "min": 0, "max": 360 },
		    "rotationSpeed": { "min": 0, "max": 200 },
		    "lifetime": { "min": 0.5, "max": 1 },
		    "blendMode": "normal",
		    "ease": [
		        { "s": 0, "cp": 0.329, "e": 0.548 },
		        { "s": 0.548, "cp": 0.767, "e": 0.876 },
		        { "s": 0.876, "cp": 0.985, "e": 1 }
		    ],
		    "frequency": 0.001,
		    "emitterLifetime": 0.1,
		    "maxParticles": 100,
		    "pos": { "x": 0, "y": 0 },
		    "addAtBack": true,
		    "spawnType": "point"
		}
	},
	PROJECTILE_ASTEROID: {
		imagePath: "img/particle.png",
		groupName: "PixiProjectileGroup",
		config: {
			"alpha": { "start": 0.74, "end": 0 },
		    "scale": { "start": 0.25, "end": 0.05 },
		    "color": { "start": "CC0000", "end": "FFFF00" },
		    "speed": { "start": 64, "end": 0 },
		    "startRotation": { "min": 0, "max": 360 },
		    "rotationSpeed": { "min": 0, "max": 200 },
		    "lifetime": { "min": 0.5, "max": 1 },
		    "blendMode": "normal",
		    "ease": [
		        { "s": 0, "cp": 0.329, "e": 0.548 },
		        { "s": 0.548, "cp": 0.767, "e": 0.876 },
		        { "s": 0.876, "cp": 0.985, "e": 1 }
		    ],
		    "frequency": 0.001,
		    "emitterLifetime": 0.1,
		    "maxParticles": 100,
		    "pos": { "x": 0, "y": 0 },
		    "addAtBack": true,
		    "spawnType": "point"
		}
	},
	ASTEROID_DESTROYED: {
		imagePath: "img/particle.png",
		groupName: "PixiProjectileGroup",
		config: {
            "alpha":{"start":0.74,"end":0},
            "scale":{"start":3,"end":1.2,"minimumScaleMultiplier":1},
            "color":{"start":"#ffdfa0","end":"#100f0c"},
            "speed":{"start":200,"end":0,"minimumSpeedMultiplier":1},
            "acceleration":{"x":0,"y":0},
            "maxSpeed":0,
            "startRotation":{"min":0,"max":360},
            "noRotation":false,
            "rotationSpeed":{"min":0,"max":200},
            "lifetime":{"min":0.5,"max":1},
            "blendMode":"normal",
            "ease":[
                {"s":0,"cp":0.329,"e":0.548},
                {"s":0.548,"cp":0.767,"e":0.876},
                {"s":0.876,"cp":0.985,"e":1}
            ],
            "frequency":0.001,
            "emitterLifetime":0.1,
            "maxParticles":100,
            "pos":{"x":0,"y":0},
            "addAtBack":true,
            "spawnType":"circle",
            "spawnCircle":{"x":0,"y":0,"r":10}
            }
	},
	SHIP_DESTROYED: {
		imagePath: "img/particle.png",
		groupName: "PixiProjectileGroup",
		config: {
			"alpha": { "start": 0.8, "end": 0 },
		    "scale": { "start": 1.0, "end": 0.3 },
		    "color": { "start": "ffbb00", "end": "ffbb00" },
		    "speed": { "start": 200, "end": 100 },
		    "startRotation": { "min": 0, "max": 360 },
		    "rotationSpeed": { "min": 0, "max": 200 },
		    "lifetime": { "min": 0.5, "max": 0.5 },
		    "blendMode": "normal",
		    "ease": [
		        { "s": 0, "cp": 0.329, "e": 0.548 },
		        { "s": 0.548, "cp": 0.767, "e": 0.876 },
		        { "s": 0.876, "cp": 0.985, "e": 1 }
		    ],
		    "frequency": 0.001,
		    "emitterLifetime": 0.1,
		    "maxParticles": 100,
		    "pos": { "x": 0, "y": 0 },
		    "addAtBack": true,
		    "spawnType": "point"
		}
	},
	SHIP_DAMAGED: {
		imagePath: "img/particle.png",
		groupName: "PixiShipDamageParticleGroup",
		config: {
	        "alpha": { "start": 0.74, "end": 0 },
	        "scale": { "start": 0.25, "end": 0.05 }, // start: 5, end: 1.2
	        "color": { "start": "ffffff", "end": "aaaaaa" }, // start: ffdfa0, end: 100f0c
	        "speed": { "start": 64, "end": 0 }, // start: 700
	        "startRotation": { "min": 0, "max": 360 },
	        "rotationSpeed": { "min": 0, "max": 200 },
	        "lifetime": { "min": 0.5, "max": 1 },
	        "blendMode": "normal",
	        "ease": [
	            { "s": 0, "cp": 0.329, "e": 0.548 },
	            { "s": 0.548, "cp": 0.767, "e": 0.876 },
	            { "s": 0.876, "cp": 0.985, "e": 1 }
	        ],
	        "frequency": 0.001,
	        "emitterLifetime": 0.1,
	        "maxParticles": 100,
	        "pos": { "x": 0, "y": 0 },
	        "addAtBack": true,
	        "spawnType": "point"
	    }
	},
	SHIP_THRUSTER: {
		imagePath: "img/ship_ship1_engine.png",
		groupName: "PixiPlayerGroup",
		config: {
	        "alpha": { "start": 0.50, "end": 0 },
	        "scale": { "start": 1, "end": 1 },
	        "color": { "start": "ffffff", "end": "ffffff" },
	        "speed": { "start": 0, "end": 0 },
	        "startRotation": { "min": 0, "max": 0 },
	        "rotationSpeed": { "min": 1, "max": 1 }, // more than 0 else cannot rotate later
	        "lifetime": { "min": 1, "max": 1 },
	        "blendMode": "normal",
	        "frequency": 0.001,
	        "emitterLifetime": 0.1,
	        "maxParticles": 1,
	        "pos": { "x": 0, "y": 0 },
	        "addAtBack": true,
	        "spawnType": "point"
		}
	}
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oParticle.IDCounter = 0;
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************