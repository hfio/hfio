//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oEjecta(Game, d) {
    d.id = NGE.Core.GetNextID(); //Auto-generate ID
    d.type = NGE.Core.goTypes.indexOf("Ejecta"); // FIXME: use enum instead of hardcoding?

    var nd = [];

    //Create Object Properly
    this.Obj = new oGameObject(NGE.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }

    //Create graphic
    var width, height;
    if(Math.random() < 0.5) {
        width = 16;
        height = 3;
    } else {
        width = 4;
        height = 4;
    }
    this._graphic = new PIXI.Graphics();
    this._graphic.beginFill(0x333333);
    this._graphic.drawRect(0, 0, width, height);
    this._graphic.endFill();
    this._graphic.x = this.x.get();
    this._graphic.y = this.y.get();
    this._graphic.pivot.set(width/2, height/2);
    this._graphic.rotation = Math.random() * 2 * Math.PI;

    var minSpeed = NGE.DATA.ShipEjectaMinSpeed;
    var maxSpeed = NGE.DATA.ShipEjectaMaxSpeed;
    var angle = Math.random() * 2 * Math.PI;
    var speed = (Math.random() * (maxSpeed - minSpeed)) + minSpeed;
    this.vx.set(Math.cos(angle) * speed);
    this.vy.set(Math.sin(angle) * speed);

    this._rotateSpeed = Math.random() * 0.1;
    this._createdAt = NGE.Core.TIME.Current;

    var fadeDelay = NGE.DATA.ShipEjectaFadeDelay;
    var fadeDuration = NGE.DATA.ShipEjectaFadeDuration;
    this._fadeDelay = fadeDelay;
    this._fadeDuration = fadeDuration;

    NGE.PIXI.PixiEjectaGroup.addChild(this._graphic);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oEjecta.prototype.Process = function(dt) {
    //Move
    this.x.set(this.x.get() + this.vx.get() * dt);
    this.y.set(this.y.get() + this.vy.get() * dt);

    this._graphic.position.x = this.x.get();
    this._graphic.position.y = this.y.get();
    this._graphic.rotation += this._rotateSpeed;

    //Fade out after a delay period
    if(NGE.Core.TIME.Current - this._createdAt >= this._fadeDelay * 1000) {
        this._graphic.alpha = Math.max(0, 1 - (NGE.Core.TIME.Current - (this._createdAt + this._fadeDelay * 1000)) / (this._fadeDuration * 1000));
    }

    //Remove once fully faded out
    if(this._graphic.alpha <= 0) {
        this.Remove();
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oEjecta.prototype.Remove = function(m) {
    this._graphic.destroy();
    this.Obj.KILL();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************