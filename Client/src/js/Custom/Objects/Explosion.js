//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function Explosion(x, y, preset) {

    if (!Explosion.Frames) {
        // tileset not finished loading yet
        throw new Error('Explosion: requires preloaded tileset!');
    }

    this._sprite = new PIXI.extras.AnimatedSprite(Explosion.Frames[preset.sequence]);
    this._sprite.x = x;
    this._sprite.y = y;

    this._sprite.anchor.set(0.5);
    this._sprite.scale.set(preset.config.scale.x, preset.config.scale.y);
    if (preset.config.alpha !== undefined) {
        this._sprite.alpha = preset.config.alpha;
    }
    if (preset.config.color !== undefined) {
        this._sprite.tint = preset.config.color;
    }

    this._sprite.animationSpeed = preset.config.speed;

    this._sprite.loop = false;

    this._sprite.onComplete = function () {
        this._sprite.destroy();
    }.bind(this);

    this._sprite.play();

    //Add to stage for viewing
    NGE.PIXI[preset.groupName].addChild(this._sprite);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Explosion.Frames = null; // will initialize this with a callback when the PIXI loader finishes loading the images (tilesets)
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Explosion.PRESET = {
    PROJECTILE_REMOVED: {
        groupName: "PixiProjectileGroup",
        sequence: "explodeSequence",
        config: {
            "scale": {"x": 0.3, "y": 0.3},
            "speed": 0.5,
            "alpha": 0.74,
            //"color": 0xffffff // anyway white, and faster if not set
        }
    },
    PROJECTILE_ASTEROID: {
        groupName: "PixiProjectileGroup",
        sequence: "explodeSequence",
        config: {
            "scale": {"x": 0.6, "y": 0.6},
            "speed": 0.5,
            "alpha": 0.74,
            "color": 0x8800ff
        }
    },
    SHIP_ASTEROID: {
        groupName: "PixiProjectileGroup",
        sequence: "explodeSequence",
        config: {
            "scale": {"x": 1, "y": 1},
            "speed": 0.5,
            "alpha": 0.74,
            "color": 0x8800ff
        }
    },
    ASTEROID_DESTROYED: {
        groupName: "PixiProjectileGroup",
        sequence: "explodeSequence",
        config: {
            "scale": {"x": 1.5, "y": 1.5},
            "speed": 0.25,
            "alpha": 0.74,
            "color": 0x8800ff
        }
    },
    SHIP_DESTROYED: {
        groupName: "PixiProjectileGroup",
        sequence: "explodeSequence",
        config: {
            "scale": {"x": 3, "y": 3},
            "speed": 0.25,
            "alpha": 0.8,
            "color": 0xff2222
        }
    },
    SHIP_DAMAGED: {
        groupName: "PixiShipDamageParticleGroup",
        sequence: "explodeSequence",
        config: {
            "scale": {"x": 1, "y": 1},
            "speed": 0.5,
            "alpha": 0.74,
            "color": 0xffbb00
        }
    },
    TURRET_DESTROYED: {
        groupName: "PixiProjectileGroup",
        sequence: "explodeSequence",
        config: {
            "scale": {"x": 2.5, "y": 2.5},
            "speed": 0.25,
            "alpha": 0.8,
            "color": 0xff0000
        }
    }
};
Object.freeze(Explosion.PRESET);
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Explosion.SEQUENCES = {// add new sequences here with the number of frames they have
    "explodeSequence": 16
};
Object.freeze(Explosion.SEQUENCES);
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// Initialize frames
PIXI.loader.onComplete
        .add(function () {
            // load the frames of all sequences - the source (tileset JSON) files should be added to the PIXI loader in index.php
            var sequenceNames = Object.keys(Explosion.SEQUENCES);
            Explosion.Frames = {};
            for (var i = 0; i < sequenceNames.length; i++) {
                var frameCount = Explosion.SEQUENCES[sequenceNames[i]];
                var frames = Explosion.Frames[sequenceNames[i]] = [];
                for (var j = 0; j < frameCount; j++) {
                    var val = j < 10 ? '0' + j : j;
                    frames.push(PIXI.Texture.fromFrame(sequenceNames[i] + '00' + val + '.png'));
                }
            }
        });
