//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Turret on top of stations - CLIENT SIDE

*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oTurret(Game, d, d2, silent)
{
    var i;
    
    //debugging
    this.DEBUG = true;
    
    //this.Log("Creating turret: " + JSON.stringify(d));
    
    this.Game = Game;
    
    //------------------------------------------------------------------------------
    //Object State (Networked - Changes only)
    //------------------------------------------------------------------------------
    //Custom data needed (for datalist)
    // if modifies, the hardcoded indexes in ws.js need to be changed!
    //Custom data needed (for datalist)
    var nd = [
        "buff", null, // buff
        "standalone", d2.standalone,
        "f1s", 0, //action speeds (seconds) / tweakable
        "f1on", 0 //is action on
    ];
    
    //this.Log(JSON.stringify(d))
    
    //Create Object Properly
    this.Obj = new oGameObject(Game.Core, d, nd, this.Process);

    //Quick map to Objects vars (will overwrite local)
    var dkeys = this.Obj._dl.getkeys();//C.Log(dkeys)
    for(i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this.Obj._dl[dkey];
    }
    
    this._buff = new Buff();
    
    this._baseSprite = null;
    if (d2.standalone) {
        this._baseSprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/turret-" + this.subtype.get() + "-base.png"));
        this._baseSprite.x = Math.round(d.x);
        this._baseSprite.y = Math.round(d.y);
        this._baseSprite.rotation = 0;
        this._baseSprite.anchor.set(0.5,0.5);
        NGE.PIXI.PixiBaseTopGroup.addChild(this._baseSprite);
    }
    
    this._sprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/turret-" + this.subtype.get() + ".png"));
    this._sprite.x = Math.round(d.x);
    this._sprite.y = Math.round(d.y);
    this._sprite.rotation = this.Game.Core.MATH.degToRad(d.r);
    this._sprite.anchor.set(0.5,0.5);
    NGE.PIXI.PixiBaseTopGroup.addChild(this._sprite);
    
    if (d2.standalone) {
        //health bar
        this._healthBarWidth = 100;
        this._healthBarHeight = 3;
        this._healthBarBackground = new PIXI.Graphics();
        this._healthBarBackground.beginFill(0xFF0000);
        this._healthBarBackground.drawRect(0, 0, this._healthBarWidth, this._healthBarHeight);
        this._healthBarForeground = new PIXI.Graphics();
        this._healthBarForeground.beginFill(0x00FF00);
        this._healthBarForeground.drawRect(0, 0, this._healthBarWidth, this._healthBarHeight);
        this._healthBar = new PIXI.Container();
        this._healthBar.addChild(this._healthBarBackground);
        this._healthBar.addChild(this._healthBarForeground);
        this._healthBar.pivot.set(this._healthBarWidth/2, this._healthBarHeight/2);
        NGE.PIXI.PixiPlayerOverlayGroup.addChild(this._healthBar);
    }
    
    if (!silent && NGE.PIXI.myShip && (this.team.get() === NGE.PIXI.myShip.team.get())) {
        this._rangeCircle = new PIXI.Graphics();
        this._rangeCircle.lineStyle(6, 0xddeeff);
        this._rangeCircle.drawCircle(0, 0, this.damage_range.get());
        this._rangeCircle.alpha = 0.2;
        this._rangeCircle.x = this._sprite.x;
        this._rangeCircle.y = this._sprite.y;
        NGE.PIXI.PixiPlayerOverlayGroup.addChild(this._rangeCircle);
    }
    
    this.pvp = d2.standalone && !this.Game.Core.CD.inCircle(this.x.get(), this.y.get(), 0,0, this.radius.get(), this.Game.DATA.fleet_pvp_radius);

    this._fireTimer = 0;
    this._firing = false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[TURRET] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.Remove = function(silent, hpflag) {
    this.Log("Deleting turret " + this.id.get());
    // only add effects if no silent flag (i.e. game is not tabbed away)
    if(!silent && this.standalone.get()) {
        if (!hpflag) {
            // removal because the turret was destroyed -> explosion effect
            //Spawn ejecta
            NGE.PIXI.spawnEjecta(this.x.get(), this.y.get()); 
            
            //Death explosion
            new Explosion(this.x.get(), this.y.get(), Explosion.PRESET.TURRET_DESTROYED);
        } 
    } 
    this._sprite.destroy();
    if (this._baseSprite) {
        this._baseSprite.destroy();
    }
    if (this._healthBar) {
        this._healthBar.destroy();
    }
    if (this._rangeCircle) {
        this._rangeCircle.destroy();
    }
    this.Obj.KILL();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// AI terran team 2; player team 10+
// TODO: propery test if it is a terran turret
oTurret.prototype.isTerran = function () { return (this.team.get() === 2) || (this.team.get() > 10); };
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oTurret.prototype.Process = function(dt) {
    
    if (this.Obj._remove === true) { return; }
    
    
    var o = this;//deref for clarity
    var x = o.x.get();
    var y = o.y.get();
    var r = o.r.get();
    
    var C = this.Game.Core;

    o.Obj.UpdateBounds();
    
    //if coming back from tabbed away state, set instantly
    //otherwise lerp to last update
    var rotation;
    if (NGE.PIXI.WasTabbedAway || (dt >= 1000)) {
        x = this.x.get();
        y = this.y.get();
        rotation = C.MATH.degToRad(r);
    }
    else
    {
        x = C.MV.Lerp(this._sprite.position.x, this.x.get(), this.Game.DATA.LERPRATE);
        y = C.MV.Lerp(this._sprite.position.y, this.y.get(), this.Game.DATA.LERPRATE);
        rotation = C.MV.Lerp(this._sprite.rotation, C.MATH.degToRad(r), this.Game.DATA.LERPRATEROT);
    }
    
    var radius = this.radius.get();
    
    //this._sprite.position.set(x, y);
    this._sprite.rotation = rotation;
    
    this._sprite.visible = true;
    
    if (this.standalone.get()) {
        //update health bar
        this._healthBar.x = x;
        this._healthBar.y = y - 48;
        this._healthBarForeground.visible = this.hp.get() > 0;
        this._healthBarForeground.width = Math.ceil(this.hp.get() / this.max_hp.get() * this._healthBarWidth);
    }
    
    this._buff.fromJSON(this.buff.get());
    
    if (this._rangeCircle) {
        this._rangeCircle.alpha -= 0.2 * dt;
        if (this._rangeCircle.alpha <= 0) {
            this._rangeCircle.destroy();
            this._rangeCircle = null;
        }
    }
    
    var defaults;

    // spawn projectile(s)
    var wasFiring = this._firing;
    this._firing = (o.f1on.get() === 1);

    //tracking firing
    if(this._firing) {// && ((this.max_ammo.get() === 0) || (this.ammo.get() > 0)) && (this.f1s.get() > 0)) {
        var fireInterval = this.f1s.get(); // - depends on equipped weapon

        if(!wasFiring) {
            this._fireTimer = fireInterval; // allow immediate shot
        } else {
            this._fireTimer += NGE.Core.TIME.Delta; // accumulate time to throttle subsequent shots
        }

        if(this._fireTimer >= fireInterval) {
            this._fireTimer = this._fireTimer % fireInterval;

            var speed = NGE.DATA.ProjectileSpeed;

            var spread = 0; //NGE.DATA.FireSpreadDegrees;
            if(typeof speed !== "number" || isNaN(speed)) {throw new Error();}
            var angle = this._sprite.rotation + (Math.random() - 0.5) * NGE.PIXI.Math2D_degToRad(spread);

            var projectileWidth = NGE.Stats.Global.projectile_width;
            defaults = NGE.Core.Copy(NGE.DATA.Object_Defaults);
            defaults.x = this._sprite.x + Math.cos(angle) * (radius + projectileWidth);
            defaults.y = this._sprite.y + Math.sin(angle) * (radius + projectileWidth);
            defaults.vx = Math.cos(angle) * speed;
            defaults.vy = Math.sin(angle) * speed;
            defaults.r = angle;
            defaults.team = this.team.get();
            defaults.damage_range = this.damage_range.get() - (radius + projectileWidth);

            NGE.Core.ObjectAdd(new oProjectile(NGE, defaults, this, this.pvp, true));
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************


