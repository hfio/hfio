function Ripple(x, y, strength, growth, duration) {
    //console.log("adding ripple");
    // sprite
    this._sprite = new PIXI.Sprite(NGE.PIXI.TextureFromImage("img/effect/ring.png"));
    this._sprite.anchor.set(0.5);
    this._sprite.position.set(x, y);
    this._sprite.scale.set(0.1);
    //NGE.PIXI.PixiProjectileGroup.addChild(this._sprite);
    NGE.PIXI.Stage.addChild(this._sprite);
    // filter
    this.filter = new PIXI.filters.DisplacementFilter(this._sprite);
    this._strength = strength || 20;
    this._duration = duration || 1;
    this._growth = growth || 1;
    this._timeLeft = this._duration;
}

Ripple.prototype.update = function(dt) {
    //console.log("updating ripple, time left: " + this._timeLeft);
    if (this._timeLeft > 0) {
        this._sprite.scale.x += dt * this._growth;
        this._sprite.scale.y += dt * this._growth;
        this._timeLeft -= dt;
        var scale = this._strength * this._timeLeft / this._duration;
        this.filter.scale = {x: scale, y: scale};
    }
}

Ripple.prototype.toBeRemoved = function() {
    return this._timeLeft <= 0;
}

Ripple.prototype.destroy = function() {
    //console.log("destroying ripple");
    this._sprite.destroy();
}
