//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Data
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var ObjectId = require('mongodb').ObjectID;
//var mongoose = require('mongoose');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oData(Game)
{
    //debugging
    this.DEBUG = true;

	this.Game = Game;
    
    //Core Engine Settings
    this.CoreData = {
        goTypes: ["Base", "Ship", "Asteroid", "Turret", "Coin", "Projectile", "Particle", "Shattered", "Ejecta", "BGObject", "FGObject", ],
        NT: 1/10, //10 times a sec (ms)
        // size of the full map: (bg/fg objects, asteroids will be spawned within this), will be broken down into areas
        MapX: 10000,
        MapY: 10000,
        // the size of one area (square):
        MapSize: 1000
    };

    //Objects
    this.Object_Defaults = {
        id: 0, type: 0, subtype: 0, owner: 0,team: 0, //identification
        x: 0, y: 0, vx: 0, vy: 0, r: 0, w: 1, h: 1, radius: 1, //location/size
        speed: 0, acceleration: 0.2, decay: 0.2, rot_speed: 0, //movement
        hp: 100, damage: 0, damage_range: 0//basic health/damage
    };
    
    //Game Specific
    this.MapEnemyTarget=10;
    this.MapEnemyTime=3;
    this.MapEnemyCTime=0;
    
    //xp to reach next levels
    this.xp1=100; this.xp2=200; this.xp3=300; this.xp4=400; this.xp5=500; this.xp6=600; this.xp7=700; this.xp8=800; this.xp9=900; this.xp10=1000;
    this.xp11=1100; this.xp12=1200; this.xp13=1300; this.xp14=1400; this.xp15=1500; this.xp16=1600; this.xp17=1700; this.xp18=1800; this.xp19=1900; this.xp20=2000;
    this.xp21=2100; this.xp22=2200; this.xp23=2300; this.xp24=2400; this.xp25=2500; this.xp26=2600; this.xp27=2700; this.xp28=2800; this.xp29=2900; this.xp30=3000;
    this.xp31=3100; this.xp32=3200; this.xp33=3300; this.xp34=3400; this.xp35=3500; this.xp36=3600; this.xp37=3700; this.xp38=3800; this.xp39=3900; this.xp40=4000;
    this.xp41=4100; this.xp42=4200; this.xp43=4300; this.xp44=4400; this.xp45=4500; this.xp46=4600; this.xp47=4700; this.xp48=4800; this.xp49=4900; this.xp50=5000;
    this.xp51=5100; this.xp52=5200; this.xp53=5300; this.xp54=5400; this.xp55=5500; this.xp56=5600; this.xp57=5700; this.xp58=5800; this.xp59=5900; this.xp60=6000;
    this.killxp=50;
    
    //bases
    this.base_1_loc=0;//degree location on circle
    this.base_2_loc=180;
    this.base_hp=1000; 
    this.base_radius=1500;//from center 0;0
    this.base_redzone=2000;//out of bounds; lose life
    this.base_redzone_drain=10;//life drain percentage
    this.base_spawn_range= 400;//range from base to spawn team
    this.base_rstep= 0; //0-359 for current base positioning
    this.base1_obj= null;
    this.base2_obj= null;

    // Asteroids
    this.asteroidDamagePercentList = [100,100,100]; // replaced by values sent over the wire

    // the variables below can be modified from the admin panel (for dev builds):
    // (change handlers for the admin panel inputs are set up in gui.js)

    //Client Lerp Rate
    this.LERPRATE = 0.1; 
    this.LERPRATEROT = 0.1;
    
    this.ProjectileSpeed = 2000;
    this.FireSpreadDegrees = 15;
    
    this.ShipEjectaCount = 20;
    this.ShipEjectaMinSpeed = 10;
    this.ShipEjectaMaxSpeed = 250;
    this.ShipEjectaFadeDelay = 8;
    this.ShipEjectaFadeDuration = 1;
    
    this.ShipShatterPieces = 3;
    this.ShipShatterVelocityMin = 30;
    this.ShipShatterVelocityMax = 100;
    this.ShipShatterRotateMin = 2;
    this.ShipShatterRotateMax = 4;
    this.ShipShatterFadeDelay = 1;
    this.ShipShatterFadeDuration = 5;
    
    this.AsteroidShatterPieces = 5;
    this.AsteroidShatterVelocityMin = 0.1;
    this.AsteroidShatterVelocityMax = 1;
    this.AsteroidShatterRotateMin = 0;
    this.AsteroidShatterRotateMax = 1;
    this.AsteroidShatterFadeDelay = 1;
    this.AsteroidShatterFadeDuration = 4;
    
    this.ShipExplodeShakeAmplitude = 100;
    this.ShipExplodeShakeRange = 200;
    
    this.ShipEngineMinAlpha = 0.7;
    this.ShipEngineMaxAlpha = 1;
    this.ShipEngineHz = 3;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oData.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[DATA] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = oData;
