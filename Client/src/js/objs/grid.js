//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function OBJ_Grid(tilesize, stage) {
    //debugging
    this.DEBUG = true;
    
    this.tilesize = tilesize;
    this.g = new PIXI.Graphics();
    this.w = 100000;
    this.h = 100000;
    //this.renderer = renderer;
    //this.baseW = this.renderer.width;
    //this.baseH = this.renderer.height;
    //this.g.lineStyle(1, 0x333333, 1);
    stage.addChild(this.g);
    this.stage = stage;

    //this.resize(wh);    
    this.draw();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Grid.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[GRID] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Grid.prototype.resize = function(tilesize, w, h)
{
    this.tilesize = tilesize;
    this.w = w;
    this.h = h;
    
    this.draw();    
    //$("#mainedit").height(window.innerHeight - 140);
    //this.w = Math.floor(this.baseW / this.stage.scale.x);
    //this.h = Math.floor(this.baseH / this.stage.scale.y);
    //this.g.width = this.w; this.g.height = this.h;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Grid.prototype.onMinimum = function(){
    this.g.clear();
    this.Log("CLEAR");
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Grid.prototype.draw = function()
{
    this.g.clear();//reset
    this.g.lineStyle(5, 0x333333, 0.7);
    
    var gw = Math.floor( this.w / this.tilesize);
    var gh = Math.floor( this.h / this.tilesize);

    //var sW = -(this.baseW / this.stage.scale.x);
    //var sH = -(this.baseH / this.stage.scale.y);

    //verticals
    for (var x = -gw; x < gw; x++)
    {
        this.g.moveTo(x * this.tilesize, -this.h);
        this.g.lineTo(x * this.tilesize, this.h);
    }

    //var ssH = -sH;

    //horizontal
    for (var y = -gh; y < gh; y++)
    {
        this.g.moveTo(-this.w, y * this.tilesize);
        this.g.lineTo(this.w, y * this.tilesize);
    }

    //mid point
    //var sx = (this.stage.scale.x * 100) / 2;
    //var sy = (this.stage.scale.y * 100) / 2;
    this.g.lineStyle(3, 0x00FF00, 0.7);
    this.g.moveTo(0, -50);
    this.g.lineTo(0, 50);
    this.g.lineStyle(3, 0x0000FF, 0.7);
    this.g.moveTo(-50, 0);
    this.g.lineTo(50, 0);
    this.Log("DRAW");
}
