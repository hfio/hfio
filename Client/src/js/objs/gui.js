//*********************************************************************************************************************************************
// GUI Management
//*********************************************************************************************************************************************
function OBJ_GUI()
{
    //debugging
    this.DEBUG = true;
    
    this.View = "Loading";
    this.ShipTemplateList = [];
    this.ShipName = null;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[GUI] " + m); }};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.Init = function(callback)
{
    this.callback = callback;

    // Disable caching of AJAX responses
    $.ajaxSetup ({ cache: false });
    
    //loading counter
    this.Loaded = 0;
    this.LoadedAmt = 0;//count of all
    
    //Load Screens
    this.Loader("Play");
    if (GLOBAL_DEBUG) {
        this.Loader("Admin");
    }
    
    //show
    $('#Loading').show();
    this.Log("GUI Init!");
};
//*********************************************************************************************************************************************
// Sets the change handler for the element with the passed ID (which should be an input element on the Client variables page of the admin panel)
// To modify the appropriate variable in Game.DATA. Also does type checking
// If the variable name is not given, will use the same as the element name
//*********************************************************************************************************************************************
OBJ_GUI.prototype._setupClientDataChangeHandler = function(elementId, variableName){
    var e = $("#" + elementId);
    e.change(function () {
        var val = parseFloat(e.val());
        if(typeof val !== "number" || isNaN(val)) {throw new Error();}
        NGE.DATA[variableName || elementId] = val;
    });
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.LoaderDone = function(){
    //$("#LoadingStatus").html("Loading... (" + this.Loaded + " / 8)");
    //console.log(this.Loaded);
    
    //GUIs Loaded, Ready to start..
    if (this.Loaded === this.LoadedAmt) {
        this.Log("GUI: Finished Loading Content");
        
        $(".btn").jqxButton({ theme: 'mbluex'});//width: '150', height: '25',
        //---------------------------------------------------------------------------------------------
        // Startup then Play
        //---------------------------------------------------------------------------------------------
        $("#ServerStatus").html('Server is <span style="color: red;">Offline</span>');
        
        if (GLOBAL_DEBUG) {
            //---------------------------------------------------------------------------------------------
            //load Admin Panel
            //---------------------------------------------------------------------------------------------
            $( "#Admin" ).jqxWindow({
                height:800,
                width: 1000,
                theme: 'mbluex',
                maxWidth:1000,
                maxHeight:800
            });
            $("#bAdminDone").click(function(){
                $('#Admin').jqxWindow('close');
            });

            // Create jqxTabs.
            $('#jqxTabs').jqxTabs({ width: '90%', height: 700, position: 'top'});

            //Update 20 levels
            $("#bUpdateServerXP").click(function(){
                NGE.WS.Send('global',{ key: "xp1", value: parseInt($('#server_xp_1').val()) });
                NGE.WS.Send('global',{ key: "xp2", value: parseInt($('#server_xp_2').val()) });
                NGE.WS.Send('global',{ key: "xp3", value: parseInt($('#server_xp_3').val()) });
                NGE.WS.Send('global',{ key: "xp4", value: parseInt($('#server_xp_4').val()) });
                NGE.WS.Send('global',{ key: "xp5", value: parseInt($('#server_xp_5').val()) });
                NGE.WS.Send('global',{ key: "xp6", value: parseInt($('#server_xp_6').val()) });
                NGE.WS.Send('global',{ key: "xp7", value: parseInt($('#server_xp_7').val()) });
                NGE.WS.Send('global',{ key: "xp8", value: parseInt($('#server_xp_8').val()) });
                NGE.WS.Send('global',{ key: "xp9", value: parseInt($('#server_xp_9').val()) });
                NGE.WS.Send('global',{ key: "xp10", value: parseInt($('#server_xp_10').val()) });
                NGE.WS.Send('global',{ key: "xp11", value: parseInt($('#server_xp_11').val()) });
                NGE.WS.Send('global',{ key: "xp12", value: parseInt($('#server_xp_12').val()) });
                NGE.WS.Send('global',{ key: "xp13", value: parseInt($('#server_xp_13').val()) });
                NGE.WS.Send('global',{ key: "xp14", value: parseInt($('#server_xp_14').val()) });
                NGE.WS.Send('global',{ key: "xp15", value: parseInt($('#server_xp_15').val()) });
                NGE.WS.Send('global',{ key: "xp16", value: parseInt($('#server_xp_16').val()) });
                NGE.WS.Send('global',{ key: "xp17", value: parseInt($('#server_xp_17').val()) });
                NGE.WS.Send('global',{ key: "xp18", value: parseInt($('#server_xp_18').val()) });
                NGE.WS.Send('global',{ key: "xp19", value: parseInt($('#server_xp_19').val()) });
                NGE.WS.Send('global',{ key: "xp20", value: parseInt($('#server_xp_20').val()) });
                NGE.WS.Send('global',{ key: "killxp", value: parseInt($('#server_killxp').val()) });
            });

            $("#bUpdateServerbases").click(function(){
                NGE.WS.Send('global',{ key: "base_radius", value: parseInt($('#server_base_radius').val()) });
                NGE.WS.Send('global',{ key: "base_redzone", value: parseInt($('#server_base_redzone').val()) });
                NGE.WS.Send('global',{ key: "base_redzone_drain", value: parseInt($('#server_base_redzone_drain').val()) });
                NGE.WS.Send('global',{ key: "base_spawn_range", value: parseInt($('#server_base_spawn_range').val()) });
                NGE.WS.Send('global',{ key: "base_1_hp", value: parseInt($('#server_base_hp').val()) });
                NGE.WS.Send('global',{ key: "base_1_maxhp", value: parseInt($('#server_base_hp').val()) });
                NGE.WS.Send('global',{ key: "base_2_hp", value: parseInt($('#server_base_hp').val()) });
                NGE.WS.Send('global',{ key: "base_2_maxhp", value: parseInt($('#server_base_hp').val()) });
            });

            //ShipTemplateListCombo
            //<button id="bUpdateClient" class="btn">Update Client</button>
            //<button id="bUpdateServer" class="btn">Update Server</button>
            $("#bUpdateServer").click(function(){
                NGE.WS.Send('global',{ key: "fleet_limit", value: parseInt($('#fleet_limit').val()) });
                NGE.WS.Send('global',{ key: "fleet_credit_share_percent", value: parseInt($('#fleet_credit_share_percent').val()) });
                NGE.WS.Send('global',{ key: "fleet_credit_share_range", value: parseInt($('#fleet_credit_share_range').val()) });
                NGE.WS.Send('global',{ key: "fleet_pvp_radius", value: parseInt($('#fleet_pvp_radius').val()) });

                NGE.WS.Send('global',{ key: "MapEnemyTarget", value: parseInt($('#server_eparea').val()) });
                NGE.WS.Send('global',{ key: "MapEnemyTime", value: parseInt($('#server_erespawn').val()) });

                NGE.WS.Send('global',{ key: "asteroidCountPerAreaMin", value: parseInt($('#server_asteroid_count_per_area_min').val()) });
                NGE.WS.Send('global',{ key: "asteroidCountPerAreaMax", value: parseInt($('#server_asteroid_count_per_area_max').val()) });
                NGE.WS.Send('global',{ key: "asteroidRadiusMin", value: parseInt($('#server_asteroid_radius_min').val()) });
                NGE.WS.Send('global',{ key: "asteroidRadiusMax", value: parseInt($('#server_asteroid_radius_max').val()) });
                NGE.WS.Send('global',{ key: "asteroidHealthMin", value: parseInt($('#server_asteroid_health_min').val()) });
                NGE.WS.Send('global',{ key: "asteroidHealthMax", value: parseInt($('#server_asteroid_health_max').val()) });

                NGE.WS.Send('global',{ key: "asteroidDamagePercentList", value: [
                        parseInt($('#server_asteroid_damage_percent_minor').val()),
                        parseInt($('#server_asteroid_damage_percent_major').val()),
                        parseInt($('#server_asteroid_damage_percent_critical').val())
                    ]
                });
                /*NGE.WS.Send('tweak',{
                    eparea: $('#server_eparea').val(),
                    erespawn: $('#server_erespawn').val()
                });*/
            });
            $("#bDeleteShipTemplate").click(function(){ NGE.GUI.AdminShipDelete(); });
            $("#bRefreshListShipTemplate").click(function(){ NGE.WS.Send('api_list',{ obj: "ships" }); });
            $("#bUpdateShipTemplate").click(function(){ NGE.GUI.AdminShipCU(); });
            $("#bUnlockShipTemplate").click(function(){ NGE.GUI.AdminShipUnlock(); });
            $("#bPickShipTemplate").click(function(){ NGE.GUI.AdminShipPick(); });
            $("#bFreeMoney").click(function(){ NGE.WS.Send('freemoney',{}); });
            $("#bFreeTechPoints").click(function(){ NGE.WS.Send('freetechpoints',{}); });
            $("#bResetOwnedShips").click(function(){ NGE.WS.Send('reset_ships',{}); });
            $("#bResetOwnedTech").click(function(){ NGE.WS.Send('reset_tech',{}); });
            $("#bAddMissingTech").click(function(){ NGE.WS.Send('add_missing_tech',{}); });
            $("#bCompleteMission").click(function(){ NGE.WS.Send('complete_mission',{}); });
            $("#bClearWave").click(function(){ NGE.WS.Send('clear_wave',{}); });

            $("#bAdmin").click(function(){
                $('#Admin').jqxWindow('open');
            });

            $("#bLogin").click(function(){
                window.onbeforeunload = null;
                NGE.AUTH.loginFacebook();
            });

            var self = this;

            $("#send_tweak").click(function(){
                var d = {
                    //scmd: 
        //            speed: $("#tweak_speed").val(),
        //            accelerate: $("#tweak_accelerate").val(),
        //            rot_accelerate: $("#tweak_rot_accelerate").val(),
        //            sspeed: $("#tweak_sspeed").val(),
                    marea: $("#tweak_marea").val(),
        //            fdrop: $("#tweak_firedrop").val(),
        //            fdropr: $("#tweak_firedropr").val()

                    //turnrate: $("#tweak_turnrate").val(),
                    //mstyle: $("#tweak_mstyle").val(),
                };
                self.Log("SEND: " + JSON.stringify(d));
                NGE.WS.Send('tweak',d);
            });

            //------------------------------------------------------------------------------------------------
            //Pick an existing Ship
            //------------------------------------------------------------------------------------------------
            $("#pickship").click(function(){
                var d = {
                    obj: "ships",
                    name: $("#pick_ship_name").val()
                };
                self.Log("SEND: " + JSON.stringify(d));
                NGE.WS.Send('pickship',d);
            });

            $("#bdebug").click(function(){
                NGE.PIXI.DebugP.Switch();
                self.Log("DEBUG: " + NGE.PIXI.DebugP.State);
            });

            $("#respawn").click(function(){
                self.Log("RESPAWN!");

                NGE.WS.Send('respawn',{});
            });
            $("#exitfleet").click(function(){
                self.Log("exitfleet!");

                NGE.WS.Send('api_refexit',{});
            });
            
            // client data can be updated using the inputs on the client page of the admin panel - set up the change handlers here
            this._setupClientDataChangeHandler("LERPRATE");
            this._setupClientDataChangeHandler("LERPRATEROT");
            
            this._setupClientDataChangeHandler("ProjectileSpeed");
            this._setupClientDataChangeHandler("FireSpreadDegrees");
            
            this._setupClientDataChangeHandler("ShipEjectaCount");
            this._setupClientDataChangeHandler("ShipEjectaMinSpeed");
            this._setupClientDataChangeHandler("ShipEjectaMaxSpeed");
            this._setupClientDataChangeHandler("ShipEjectaFadeDelay");
            this._setupClientDataChangeHandler("ShipEjectaFadeDuration");
            
            this._setupClientDataChangeHandler("ShipShatterPieces");
            this._setupClientDataChangeHandler("ShipShatterVelocityMin");
            this._setupClientDataChangeHandler("ShipShatterVelocityMax");
            this._setupClientDataChangeHandler("ShipShatterRotateMin");
            this._setupClientDataChangeHandler("ShipShatterRotateMax");
            this._setupClientDataChangeHandler("ShipShatterFadeDelay");
            this._setupClientDataChangeHandler("ShipShatterFadeDuration");
            
            this._setupClientDataChangeHandler("AsteroidShatterPieces");
            this._setupClientDataChangeHandler("AsteroidShatterVelocityMin");
            this._setupClientDataChangeHandler("AsteroidShatterVelocityMax");
            this._setupClientDataChangeHandler("AsteroidShatterRotateMin");
            this._setupClientDataChangeHandler("AsteroidShatterRotateMax");
            this._setupClientDataChangeHandler("AsteroidShatterFadeDelay");
            this._setupClientDataChangeHandler("AsteroidShatterFadeDuration");
            
            this._setupClientDataChangeHandler("ShipExplodeShakeAmplitude");
            this._setupClientDataChangeHandler("ShipExplodeShakeRange");
            
            this._setupClientDataChangeHandler("ShipEngineMinAlpha");
            this._setupClientDataChangeHandler("ShipEngineMaxAlpha");
            this._setupClientDataChangeHandler("ShipEngineHz");
        }
        
        //copy to clipboard.js
        //new Clipboard('#urlfleet');
        //$("#urlfleet").click(function(){
          //  console.log("urlfleet!");
            
            //<p id="fleetURL">http://hyperfleet.com/?r=1x32</p>
            
        //});
        
        var clipboard = new Clipboard('#urlfleet', {
            text: function() {
                return $("#fleetURL").html();
            }
        });
        if (GLOBAL_DEBUG) {
            clipboard.on('success', function(e) {
                console.log(e);
            });

            clipboard.on('error', function(e) {
                console.log(e);
            });
        }  

        //---------------------------------------------------------------------------------------------
        //Done setup 
        //---------------------------------------------------------------------------------------------
        this.callback();
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.RequestGLOBALS = function()
{
        NGE.WS.Send('getglobal',{ key: "base_radius" });
        NGE.WS.Send('getglobal',{ key: "base_redzone" });
        NGE.WS.Send('getglobal',{ key: "base_redzone_drain" });
        NGE.WS.Send('getglobal',{ key: "base_spawn_range" });
        NGE.WS.Send('getglobal',{ key: "base_1_hp" });

        NGE.WS.Send('getglobal',{ key: "MapEnemyTarget" });
        NGE.WS.Send('getglobal',{ key: "MapEnemyTime" });

        NGE.WS.Send('getglobal',{ key: "asteroidCountPerAreaMin" });
        NGE.WS.Send('getglobal',{ key: "asteroidCountPerAreaMax" });
        NGE.WS.Send('getglobal',{ key: "asteroidRadiusMin" });
        NGE.WS.Send('getglobal',{ key: "asteroidRadiusMax" });
        NGE.WS.Send('getglobal',{ key: "asteroidHealthMin" });
        NGE.WS.Send('getglobal',{ key: "asteroidHealthMax" });
        NGE.WS.Send('getglobal',{ key: "asteroidDamagePercentList" });

        NGE.WS.Send('getglobal',{ key: "xp1" });
        NGE.WS.Send('getglobal',{ key: "xp2" });
        NGE.WS.Send('getglobal',{ key: "xp3" });
        NGE.WS.Send('getglobal',{ key: "xp4" });
        NGE.WS.Send('getglobal',{ key: "xp5" });
        NGE.WS.Send('getglobal',{ key: "xp6" });
        NGE.WS.Send('getglobal',{ key: "xp7" });
        NGE.WS.Send('getglobal',{ key: "xp8" });
        NGE.WS.Send('getglobal',{ key: "xp9" });
        NGE.WS.Send('getglobal',{ key: "xp10" });
        NGE.WS.Send('getglobal',{ key: "xp11" });
        NGE.WS.Send('getglobal',{ key: "xp12" });
        NGE.WS.Send('getglobal',{ key: "xp13" });
        NGE.WS.Send('getglobal',{ key: "xp14" });
        NGE.WS.Send('getglobal',{ key: "xp15" });
        NGE.WS.Send('getglobal',{ key: "xp16" });
        NGE.WS.Send('getglobal',{ key: "xp17" });
        NGE.WS.Send('getglobal',{ key: "xp18" });
        NGE.WS.Send('getglobal',{ key: "xp19" });
        NGE.WS.Send('getglobal',{ key: "xp20" });
        NGE.WS.Send('getglobal',{ key: "killxp" });
        
        NGE.WS.Send('getglobal',{ key: "fleet_limit" });
        NGE.WS.Send('getglobal',{ key: "fleet_credit_share_percent" });
        NGE.WS.Send('getglobal',{ key: "fleet_credit_share_range" });
        NGE.WS.Send('getglobal',{ key: "fleet_pvp_radius" });
        
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.AdminShipList = function(list)
{
    NGE.PIXI.testStationMenu.shipSection.cards.setShipList(list);
    
    var self = this;

    this.ShipTemplateList = list;
    if (GLOBAL_DEBUG) {
        this.Log("ShipList: " + JSON.stringify(list));
        $('#ShipTemplateListCombo').off('select');//remove previous handlers
        $('#ShipTemplateListCombo').off('change');


        $("#ShipTemplateListCombo").jqxComboBox({ source: list, selectedIndex: 0, width: '200px', height: '25px' });
        $('#ShipTemplateListCombo').bind('select', function (event) {

            //NGE.GUI.ShipName = $("#ShipTemplateListCombo").val()
            //console.log(NGE.GUI.ShipName)
    /*
            var args = event.args;
            var item = $('#ShipTemplateListCombo').jqxComboBox('getItem', args.index);
            console.log(item);
            if (item != null) {
                //read if exists
                if(NGE.GUI.ShipTemplateList.indexOf(item.label))
                {
                    console.log("Requesting ShipData");
                    var d = { obj: "ships", name: item.label };
                    NGE.WS.Send('api_read', d);
                }
            }
            */
        });

        $('#ShipTemplateListCombo').on('change', function (event) 
        {
            NGE.GUI.ShipName = $("#ShipTemplateListCombo").val();
            self.Log(NGE.GUI.ShipName);

             if(NGE.GUI.ShipTemplateList.indexOf(NGE.GUI.ShipName) > -1)
             {
                var d = { obj: "ships", name: NGE.GUI.ShipName };
                NGE.WS.Send('api_read', d);
             }
             else{
                self.Log(NGE.GUI.ShipTemplateList);

             }


            /*var args = event.args;
            if (args) {
                // index represents the item's index.                          
                var index = args.index;
                var item = args.item;
                // get item's label and value.
                var label = item.label;
                var value = item.value;
                var type = args.type; // keyboard, mouse or null depending on how the item was selected.
                console.log(value);
                NGE.GUI.ShipName = value;
            }*/
        });
    }

    if (list.length > 0) {
        self.Log("Has ships: " + $("#ShipTemplateListCombo").val());
        NGE.GUI.ShipName = list[0];//default first/read it
        var d = { obj: "ships", name: NGE.GUI.ShipName };
        NGE.WS.Send('api_read', d);

    }
    else{
        NGE.GUI.ShipName = null;
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.AdminShipRead = function(d)
{
    if (GLOBAL_DEBUG) {
        this.Log("READSHIP: " + JSON.stringify(d));
        //console.log("speed: " + d["speed"]);
        $("#ship_speed").val(d.speed);
        $("#ship_accelerate").val(d.accelerate);
        $("#ship_rot_accelerate").val(d.rot_accelerate);
        $("#ship_firedrop").val(d.fdrop);
        $("#ship_firedropr").val(d.fdropr);
        $("#ship_health").val(d.health);
        $("#ship_marea").val(d.marea);
        $("#ship_marea_max").val(d.marea_max);
        $("#ship_hitbox").val(d.hitbox);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.AdminShipCU = function()
{
    if (GLOBAL_DEBUG) {
        if (this.ShipName !== null && this.ShipName !== "") {
            var d = {
                obj: "ships",
                name: this.ShipName,
                speed: $("#ship_speed").val(),
                accelerate: $("#ship_accelerate").val(),
                rot_accelerate: $("#ship_rot_accelerate").val(),
                fdrop: $("#ship_firedrop").val(),
                fdropr: $("#ship_firedropr").val(),
                health: $("#ship_health").val(),
                marea: $("#ship_marea").val(),
                marea_max: $("#ship_marea_max").val(),
                hitbox: $("#ship_hitbox").val()
            };
            this.Log("SEND: " + JSON.stringify(d));
            NGE.WS.Send('api_cu',d);
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.AdminShipUnlock = function()
{
    if (GLOBAL_DEBUG) {
        this.Log("Unlock Ship");
        if(NGE.GUI.ShipName){
            var d = { name: NGE.GUI.ShipName };
            NGE.WS.Send('unlockship', d);
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.AdminShipPick = function()
{
    if (GLOBAL_DEBUG) {
        this.Log("Pick Ship");   
        if(NGE.GUI.ShipName){
            var d = { name: NGE.GUI.ShipName };
            NGE.WS.Send('pickship', d);
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.AdminShipDelete = function()
{
    if (GLOBAL_DEBUG) {
        this.Log("Delete: " + NGE.GUI.ShipName);
        if(NGE.GUI.ShipName){
            var d = { obj: "ships", name: NGE.GUI.ShipName };
            NGE.WS.Send('api_delete', d);
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.ViewChange = function(v){ this.Log(v); $('#' + this.View).hide(); $('#' + v).show(); this.View = v; NGE["Mode_" + v]();};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.Loader = function(name)
{
    NGE.GUI.LoadedAmt++;
    $( "#" + name ).load( "gui/" + name + ".html", function() {
        NGE.GUI.Loaded++; NGE.GUI.LoaderDone();
    });
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.GameAbilityOK = function(d){  }
OBJ_GUI.prototype.GameAbilityNO = function(d){ }
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.Connect = function(d){
    $("#pixi-canvas").show();
    $("#Disconnected").hide();
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.Disconnect = function(d){
    $("#editor-canvas").hide(); 
    $("#pixi-canvas").hide();
    $(".game").prop("hidden", true);
    $("#reconnectLink").attr("href", NGE.GetFleetUrl(0));
    $("#Disconnected").show();
    window.onbeforeunload = null;
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_GUI.prototype.GotGlobal = function(d){
    //console.log(d);
    if (d.key == "base_radius") { $('#server_base_radius').val(d.value); }
    if (d.key == "base_redzone") { $('#server_base_redzone').val(d.value); NGE.DATA.base_redzone = d.value; NGE.PIXI.UpdateDangerZone(); }
    if (d.key == "base_redzone_drain") { $('#server_base_redzone_drain').val(d.value); }
    if (d.key == "base_spawn_range") { $('#server_base_spawn_range').val(d.value); }
    if (d.key == "base_1_hp") { $('#server_base_hp').val(d.value); }

    if (d.key == "MapEnemyTarget") { $('#server_eparea').val(d.value); }
    if (d.key == "MapEnemyTime") { $('#server_erespawn').val(d.value); }

    if (d.key == "asteroidCountPerAreaMin") { $('#server_asteroid_count_per_area_min').val(d.value); $('#_server_asteroid_count_per_area_min').text(d.value); }
    if (d.key == "asteroidCountPerAreaMax") { $('#server_asteroid_count_per_area_max').val(d.value); $('#_server_asteroid_count_per_area_max').text(d.value); }
    if (d.key == "asteroidRadiusMin") { $('#server_asteroid_radius_min').val(d.value); $('#_server_asteroid_radius_min').text(d.value); }
    if (d.key == "asteroidRadiusMax") { $('#server_asteroid_radius_max').val(d.value); $('#_server_asteroid_radius_max').text(d.value); }
    if (d.key == "asteroidHealthMin") { $('#server_asteroid_health_min').val(d.value); $('#_server_asteroid_health_min').text(d.value); }
    if (d.key == "asteroidHealthMax") { $('#server_asteroid_health_max').val(d.value); $('#_server_asteroid_health_max').text(d.value); }
    if (d.key == "asteroidDamagePercentList") {
        NGE.DATA.asteroidDamagePercentList = d.value;
        $('#server_asteroid_damage_percent_minor').val(d.value[0]); $('#_server_asteroid_damage_percent_minor').text(d.value[0]);
        $('#server_asteroid_damage_percent_major').val(d.value[1]); $('#_server_asteroid_damage_percent_major').text(d.value[1]);
        $('#server_asteroid_damage_percent_critical').val(d.value[2]); $('#_server_asteroid_damage_percent_critical').text(d.value[2]);
    }

    if (d.key == "xp1") { $('#server_xp_1').val(d.value); }
    if (d.key == "xp2") { $('#server_xp_2').val(d.value); }
    if (d.key == "xp3") { $('#server_xp_3').val(d.value); }
    if (d.key == "xp4") { $('#server_xp_4').val(d.value); }
    if (d.key == "xp5") { $('#server_xp_5').val(d.value); }
    if (d.key == "xp6") { $('#server_xp_6').val(d.value); }
    if (d.key == "xp7") { $('#server_xp_7').val(d.value); }
    if (d.key == "xp8") { $('#server_xp_8').val(d.value); }
    if (d.key == "xp9") { $('#server_xp_9').val(d.value); }
    if (d.key == "xp10") { $('#server_xp_10').val(d.value); }
    if (d.key == "xp11") { $('#server_xp_11').val(d.value); }
    if (d.key == "xp12") { $('#server_xp_12').val(d.value); }
    if (d.key == "xp13") { $('#server_xp_13').val(d.value); }
    if (d.key == "xp14") { $('#server_xp_14').val(d.value); }
    if (d.key == "xp15") { $('#server_xp_15').val(d.value); }
    if (d.key == "xp16") { $('#server_xp_16').val(d.value); }
    if (d.key == "xp17") { $('#server_xp_17').val(d.value); }
    if (d.key == "xp18") { $('#server_xp_18').val(d.value); }
    if (d.key == "xp19") { $('#server_xp_19').val(d.value); }
    if (d.key == "xp20") { $('#server_xp_20').val(d.value); }
    if (d.key == "killxp") { $('#server_killxp').val(d.value); }
    
    if (d.key == "fleet_limit") { $('#fleet_limit').val(d.value); }
    if (d.key == "fleet_credit_share_percent") { $('#fleet_credit_share_percent').val(d.value); NGE.DATA.fleet_credit_share_percent = d.value; }
    if (d.key == "fleet_credit_share_range") { $('#fleet_credit_share_range').val(d.value); NGE.DATA.fleet_credit_share_range = d.value; NGE.DATA.fleet_credit_share_range_sq = d.value * d.value; }
    if (d.key == "fleet_pvp_radius") { $('#fleet_pvp_radius').val(d.value); NGE.DATA.fleet_pvp_radius = d.value; NGE.PIXI.UpdateDangerZone(); }
    
};