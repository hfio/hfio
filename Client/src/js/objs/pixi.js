//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function OBJ_Pixi()
{
    // only influences console logging:
    this.DEBUG = false;
    
    this.initialized = false;
    this.Canvas = null;
    this.Stage = null;
    this.GridWH = 1000;
    this.ViewPort = null;
    this.timecount = 0;
    this.timesec = 0;

    //Mobile Views
    this.ViewMode = "Landscape";
    this.ViewsProps = {
        Landscape : { x: -480, y: -320, w: 960, h: 640},
        Portrait : { x: -320, y: -480, w: 640, h: 960}
    }
    
    //Editor Input
    this.Input = {
        MOUSE_PRESSED: false,
        MOUSE_STATE: false,
        MOUSE_RELEASED: false,
        MouseDown: 0,
        MouseX: 0,
        MouseY: 0,
        UP: 0,
        DOWN: 0,
        LEFT: 0,
        RIGHT: 0,
        FIRE1: 0,
        SHIFT: 0,
        DragStarted: false,
        DragX: 0,
        DragY: 0,
        StageX: 0,
        StageY: 0,
        Active: false
    }
    
    this.ViewCenterX = 0;
    this.ViewCenterY = 0;
    this.PlayerCenterX = 0;
    this.PlayerCenterY = 0;
    this.myShip = null;
    this.mainStation = null;
    
    this.chatMode = OBJ_Pixi.CHAT_MODE.OFF;

    //background
    this.background = new PIXI.Sprite(OBJ_Pixi.prototype.TextureFromImage('img/background.png'));
    this.background.anchor.set(0.5);
    this.background.distance = 100;
    this.background.texture.baseTexture.on('loaded', ()=>{ this.StretchBackground(); });

    this.Camera = { x: 0, y: 0 };

    this.ScreenShakeAmplitude = 0;
    this.ScreenShakeDecay = 0.1;

    this.imgs = {};
    
    this._ripples = [];
    
    this.fps = 0;
    
    this.DebugMode = true; // do not change this to false - use ToggleDebug(), and only after Init() has finished (right now it is called at the end of Init())
    
    this.WasTabbedAway = false;
    
    this.TutorialFinished = false; // is set to true when the player presses the PLAY button on the startup tutorial overlay
    
    this._numberKeyHandlers = [];
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[PIXI] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.CHAT_MODE = {
    OFF: 0,
    ALL: 1,
    TEAM: 2
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.Lerp = function(start, end, amt){ return (1-amt)*start+amt*end }
OBJ_Pixi.prototype.Math2D_degToRad = function (deg) { return deg * (Math.PI/180); }//Degrees to radians
OBJ_Pixi.prototype.Math2D_radToDeg = function (rad) { return rad * (180/Math.PI); }//Radians to degrees
OBJ_Pixi.prototype.Math2D_NormalizeDegrees = function(r){ r = r % 360; if (r < 0){ r += 360; } return Math.floor(r); }//normalize to 0-360 degree range
OBJ_Pixi.prototype.Math2D_rotate_by_pivot = function(px,py,pr,offsetx,offsety)//sets location by rotating around a different pivot point
{
    //console.log(pr)
    pr = this.Math2D_NormalizeDegrees(pr);
    var c_ = Math.cos(this.Math2D_degToRad(pr));
    var s_ = Math.sin(this.Math2D_degToRad(pr));
    var x = px + ((offsetx * c_) - (offsety * s_));
    var y = py + ((offsety * c_) + (offsetx * s_));// + (this.x * s_); //sometimes this is not required x !=0 ?
    return [x, y];
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.TextureFromImage = function(path) {
    return PIXI.Texture.fromImage(noCache(path));
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.StretchBackground = function() {
    this.background.scale.set(Math.max(1, window.innerWidth / this.background.texture.width, window.innerHeight / this.background.texture.height));
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.ToggleDebug = function()
{
    if (GLOBAL_DEBUG) {
        this.DebugMode = !this.DebugMode;
        if (this.DebugMode) {
            $(".debug").show(0);
        } else {
            $(".debug").hide(0);
            this.DebugP.SetState(OBJ_DEBUG.STATE.OFF);
        }
        this.debugGraph.container.visible = this.DebugMode;
    } else {
        $(".debug").hide(0);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.addNumberKeyHandler = function (handler) {
    // avoid duplicates
    if (this._numberKeyHandlers.indexOf(handler) < 0) {
        this._numberKeyHandlers.push(handler);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.KeyDown = function(e)
{
    if (this.chatMode === OBJ_Pixi.CHAT_MODE.OFF) {
        //console.log("DOWN:" + e.keyCode);
        if (e.keyCode === 16 ) { this.Input.SHIFT = 1; }
        if (e.keyCode === 38 || e.keyCode === 87) { this.Input.UP = 1; }//38 - up
        if (e.keyCode === 40 || e.keyCode === 83) { this.Input.DOWN = 1; }//40 - dn
        if (e.keyCode === 37 || e.keyCode === 65) { this.Input.LEFT = 1; }//37 - left
        if (e.keyCode === 39 || e.keyCode === 68) { this.Input.RIGHT = 1; }//39 - right

        if (GLOBAL_DEBUG) {
            // = -> toggle debug UI elements (different keyCode for Firefox (61) and others (187))
            if (e.keyCode === 61 || e.keyCode === 187) { 
                this.ToggleDebug();
            } 
        }

        if ((e.keyCode >= 48) && (e.keyCode <= 57)) {
            for (var i = 0; i < this._numberKeyHandlers.length; i++) {
                this._numberKeyHandlers[i](e.keyCode - 48);
            }
        }
        if (this.myShip) { 
            if ((this.myShip.message_timeleft.get() <= 0) && !this.myShip.docked.get() && !this.HUD.usernameButton.isEditing() && !this.HUD.wallet.isEditing()) {
                if (e.keyCode === 13) { // enter
                    this.chatMode = OBJ_Pixi.CHAT_MODE.ALL;
                    if (GLOBAL_DEBUG) {
                        console.log("Chat edit start: say to all");
                    }
                } else if (e.keyCode === 84) { // T
                    this.chatMode = OBJ_Pixi.CHAT_MODE.TEAM;
                    if (GLOBAL_DEBUG) {
                        console.log("Chat edit start: say to team");
                    }
                }
            }
            if (this.baseToDockAt) {
                if (e.keyCode === 32) { // space
                    this.Log('docking');
                    this.myShip._dockingAt = this.baseToDockAt; // this flag will prevent us to send new dock messages until the server updates docking state
                    NGE.WS.Send('dock', {baseID: this.baseToDockAt.id.get()});
                }
            }
        }
    } else {
        if (e.keyCode === 13) {
            var text = this.HUD.chatEditPanel.input.text;
            if (GLOBAL_DEBUG) {
                console.log("Finished chat edit: " + text);
            }
            if (text) {
                // text with only spaces is not valid
                var textValid = false;
                for (i = 0; i < text.length; i++) {
                    if (text[i] !== " ") {
                        textValid = true;
                        break;
                    }
                }
                if (textValid) {
                    NGE.WS.Send('say', { text: text, scope: this.chatMode });
                }
            }
            this.chatMode = OBJ_Pixi.CHAT_MODE.OFF;
        } else if (e.keyCode === 27) {
            this.chatMode = OBJ_Pixi.CHAT_MODE.OFF;
            if (GLOBAL_DEBUG) {
                console.log("Cancelled chat edit");
            }
        } 
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.KeyUp = function(e)
{
    //console.log("UP:" + e.keyCode);
    if (e.keyCode === 16 ) { this.Input.SHIFT = 0; }
    if (e.keyCode === 38 || e.keyCode === 87) { this.Input.UP = 0; }//38 - up
    if (e.keyCode === 40 || e.keyCode === 83) { this.Input.DOWN = 0; }//40 - dn
    if (e.keyCode === 37 || e.keyCode === 65) { this.Input.LEFT = 0; }//37 - left
    if (e.keyCode === 39 || e.keyCode === 68) { this.Input.RIGHT = 0; }//39 - right
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// background objects (parallax effect)
OBJ_Pixi.prototype.spawnBGObjects = function() {
    this.bgObjects = [];
    //parseInt($('#server_bgobject_count_per_area_min').val());
    var minCount = NGE.Stats.Global.bgobject_count_per_area_min;
    var maxCount = NGE.Stats.Global.bgobject_count_per_area_max;
    var typeCount = NGE.Stats.Global.bgobject_type_count;
    var minScale = NGE.Stats.Global.bgobject_scale_min;
    var maxScale = NGE.Stats.Global.bgobject_scale_max;
    var minMove = NGE.Stats.Global.bgobject_move_min;
    var maxMove = NGE.Stats.Global.bgobject_move_max;
    var data = [];
    for(var i=0; i < NGE.Core.AREA.Areas.length; i++)
    {
        //var n = this.Core.AREA.Count(i, this.DATA.CoreData.goTypes.indexOf("Asteroid"));
        var n = 0;
        var targetCount = minCount + Math.floor(Math.random() * (maxCount - minCount));
        //console.log("Area: "+i+" BGObject targetCount: " + targetCount);
        var bounds = NGE.Core.AREA.AreaBounds(i);
        for (var s=n;s<targetCount;s++){

            var typeIndex = Math.max(1, Math.ceil(Math.random() * (typeCount)));
            var x = bounds[0] + Math.floor((Math.random() * NGE.Core.AREA.Size));
            var y = bounds[1] + Math.floor((Math.random() * NGE.Core.AREA.Size));
            var rand = Math.random();
            var scale = minScale + rand * (maxScale - minScale);
            var move = minMove + rand * (maxMove - minMove);
            
            data.push({
                typeIndex: typeIndex,
                position: {
                    x: x,
                    y: y
                },
                scale: scale,
                move: move
            });
        }
    }
    // background object need to be sorted based on how far they are
    data.sort((a,b)=>a.move - b.move);
        
    for (i=0;i<data.length;i++){
        var GO = new oBGObject(data[i]);
        this.bgObjects.push(GO);
        //NGE.Core.ObjectAdd(GO);

        //console.log("BGObject: " + GO.id.get() + " type " + typeIndex + " at " + x + "," + y + " r: " + radius);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// foreground objects (same plane as ships)
OBJ_Pixi.prototype.spawnFGObjects = function() {
    this.fgObjects = [];
    var minCount = NGE.Stats.Global.fgobject_count_per_area_min;
    var maxCount = NGE.Stats.Global.fgobject_count_per_area_max;
    var typeCount = NGE.Stats.Global.fgobject_type_count;
    var minScale = NGE.Stats.Global.fgobject_scale_min;
    var maxScale = NGE.Stats.Global.fgobject_scale_max;
    for(var i=0; i < NGE.Core.AREA.Areas.length; i++)
    {
        //var n = this.Core.AREA.Count(i, this.DATA.CoreData.goTypes.indexOf("Asteroid"));
        var n = 0;
        var targetCount = minCount + Math.floor(Math.random() * (maxCount - minCount));
        //console.log("Area: "+i+" BGObject targetCount: " + targetCount);
        var bounds = NGE.Core.AREA.AreaBounds(i);
        for (var s=n;s<targetCount;s++){

            var typeIndex = Math.max(1, Math.ceil(Math.random() * (typeCount)));
            var x = bounds[0] + Math.floor((Math.random() * NGE.Core.AREA.Size));
            var y = bounds[1] + Math.floor((Math.random() * NGE.Core.AREA.Size));
            var rand = Math.random();
            var scale = minScale + rand * (maxScale - minScale);
            
            var GO = new oFGObject({
                typeIndex: typeIndex,
                position: {
                    x: x,
                    y: y
                },
                scale: scale,
            });
            this.fgObjects.push(GO);
            
            //console.log("FGObject: " + GO.id.get() + " type " + typeIndex + " at " + x + "," + y);
        }
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.spawnEjecta = function(x, y) {
    var ejectaCount = NGE.DATA.ShipEjectaCount;
    var ejectaDefaults = NGE.Core.Copy(NGE.DATA.Object_Defaults);
    ejectaDefaults.x = x;
    ejectaDefaults.y = y;
    for (var i = 0; i < ejectaCount; i++) {
        NGE.Core.ObjectAdd(new oEjecta(NGE, ejectaDefaults));
    }
}
//*********************************************************************************************************************************************
// Called when a new danger zone or PvP radius is received from the server, to update the red circle / area
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.UpdateDangerZone = function() {
    this.dangerZone.clear();
    
    //create danger zone color fill
    var dangerZoneDiameter = NGE.DATA.base_redzone * 2;
    var dangerZoneThickness = 2000;
    this.dangerZone.lineStyle(dangerZoneThickness, 0xFF0000);
    this.dangerZone.drawCircle(0, 0, (dangerZoneDiameter + dangerZoneThickness)/2);
    this.dangerZone.alpha = 0.2;

    var PVPZoneDiameter = NGE.DATA.fleet_pvp_radius * 2;
    
    var PVPZoneThickness = 10;
    this.dangerZone.lineStyle(PVPZoneThickness, 0xFF0000);
    this.dangerZone.drawCircle(0, 0, (PVPZoneDiameter + PVPZoneThickness)/2);
    this.dangerZone.alpha = 0.2;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.Init = function()
{
    //Pixi engine
    this.Canvas = PIXI.autoDetectRenderer(800, 600, {backgroundColor : 0x000000, antialias: true});
    document.body.appendChild(this.Canvas.view);
    this.Stage = new PIXI.Container(0xFFFFFF);
    this.GUIStage = new PIXI.Container(0xFFFFFF);
    this.GUIStage.addChild(this.Stage);
    this.Grid = new OBJ_Grid(this.GridWH, this.Stage);

    this.guiManager = new GuiManager();
    this.GUIStage.addChild(this.guiManager.container);
    
    this.tutorialOverlay = new TutorialOverlay();
    this.guiManager.children.push(this.tutorialOverlay);
    this.guiManager.container.addChild(this.tutorialOverlay.container);

    this.testStationMenu = new StationMenu();
    this.testStationMenu.container.visible = false;
    this.guiManager.children.push(this.testStationMenu);
    this.guiManager.container.addChild(this.testStationMenu.container);

    this.HUD = new Hud();
    this.guiManager.children.push(this.HUD);
    this.guiManager.container.addChild(this.HUD.container);
    this.HUD.wallet.setShipBalance(0);
    this.HUD.wallet.setBankBalance(0);

    if (GLOBAL_DEBUG) {
        this.debugGraph = new DebugGraph();
        this.guiManager.children.push(this.debugGraph);
        this.guiManager.container.addChild(this.debugGraph.container);
    }
    
    //resize to max
    this.Canvas.view.id = 'pixi-canvas';
    $("#pixi-canvas").height(window.innerHeight);
    this.Canvas.resize($("#pixi-canvas").width(), $("#pixi-canvas").height());
    
    //center point of window
    this.ViewCenterX = Math.floor($("#pixi-canvas").width() / 2);
    this.ViewCenterY = Math.floor($("#pixi-canvas").height() / 2);
    
    //center stage
    this.Stage.position.x = Math.floor(window.innerWidth / 2);
    this.Stage.position.y = Math.floor(window.innerHeight / 2);
    
    //mousewheel zoom
    if (GLOBAL_DEBUG) {
        $(this.Canvas.view).on('mousewheel', this.onMouseWheel.bind(this));
    }
    
    //keyboard
    window.addEventListener( "keydown", function(e) {  NGE.PIXI.KeyDown(e); NGE.PIXI.Input.Active = true; }, false )
    window.addEventListener( "keyup", function(e) {  NGE.PIXI.KeyUp(e); NGE.PIXI.Input.Active = true; }, false )
    
    //Mouse
    window.addEventListener( "contextmenu", function(e) { e.preventDefault(); NGE.PIXI.Input.MouseDown = 1; NGE.PIXI.Input.FIRE1 = 1; }, false);
    window.addEventListener( "mousedown", function(e) {  NGE.PIXI.Input.MouseDown = 1; NGE.PIXI.Input.FIRE1 = 1; NGE.PIXI.Input.MOUSE_PRESSED = true; NGE.PIXI.Input.MOUSE_STATE = true; NGE.PIXI.Input.Active = true; }, false )
    window.addEventListener( "mouseup", function(e) { NGE.PIXI.Input.MouseDown = 0; NGE.PIXI.Input.FIRE1 = 0; NGE.PIXI.Input.MOUSE_RELEASED = true; NGE.PIXI.Input.MOUSE_STATE = false; NGE.PIXI.Input.Active = true; }, false )
    window.addEventListener( "mousemove", function(e) { NGE.PIXI.Input.MouseX = e.clientX; NGE.PIXI.Input.MouseY = e.clientY; NGE.PIXI.Input.Active = true; }, false )
    window.addEventListener( "mouseout", function(e) {
        NGE.PIXI.Input.MouseDown = 0;
        NGE.PIXI.Input.FIRE1 = 0;
        if(NGE.PIXI.Input.MOUSE_STATE) {
            NGE.PIXI.Input.MOUSE_STATE = false;
            NGE.PIXI.Input.MOUSE_RELEASED = true;
        }
        // Set mouse to center so player stops moving
        NGE.PIXI.Input.MouseX = NGE.PIXI.ViewCenterX;
        NGE.PIXI.Input.MouseY = NGE.PIXI.ViewCenterY;
        // Set mouse a bit ahead of where he is facing so he continues facing that direction
        if(NGE.PIXI.myShip) {
            var angle = NGE.PIXI.Math2D_degToRad(NGE.PIXI.myShip.r.get());
            NGE.PIXI.Input.MouseX += Math.cos(angle) * 10;
            NGE.PIXI.Input.MouseY += Math.sin(angle) * 10;
        }
    }, false);
    
    var self = this;
    
    //Autoresize
    $(window).on('resize', function(e){
        var w = window.innerWidth;
        var h = window.innerHeight;
        self.Log("RESIZE " + w + ", " + h);
        $("#pixi-canvas").height(h);
        NGE.PIXI.Canvas.resize($("#pixi-canvas").width(), $("#pixi-canvas").height());
        
        //center point of window
        NGE.PIXI.ViewCenterX = Math.floor(w / 2);
        NGE.PIXI.ViewCenterY = Math.floor(h / 2);
        
        //center pixi on player position
        NGE.PIXI.Stage.position.x = Math.floor(w / 2) + NGE.PIXI.PlayerCenterX;
        NGE.PIXI.Stage.position.y = Math.floor(h / 2) + NGE.PIXI.PlayerCenterY;
        
        NGE.PIXI.StretchBackground();
        
        NGE.WS.Send("scope", self.getScope());
    });        
    
    this.dangerZone = new PIXI.Graphics();
    // not creating danger zone now, we will only receive the radius from server later

    //pixi groups for rendering order
    this.PixiBGObjectGroup = new PIXI.Container();
    this.PixiFGObjectGroup = new PIXI.Container();
    this.PixiAsteroidGroup = new PIXI.Container();
    this.PixiBaseBottomGroup = new PIXI.Container();
    this.PixiPlayerGroup = new PIXI.Container();
    this.PixiBaseTopGroup = new PIXI.Container();
    this.PixiAlienShipGroup = new PIXI.Container();
    this.PixiPlayerOverlayGroup = new PIXI.Container(); // for e.g. health bar, above all ships
    this.PixiEjectaGroup = new PIXI.Container();
    this.PixiUnitGroup = new PIXI.Container();
    this.PixiProjectileGroup = new PIXI.Container();
    this.PixiShipDamageParticleGroup = new PIXI.Container();
    this.PixiTextGroup = new PIXI.Container();
    
    //add by priority of view
    this.Stage.addChild(this.background);
    this.Stage.addChild(this.PixiBGObjectGroup);
    this.Stage.addChild(this.PixiFGObjectGroup);
    this.Stage.addChild(this.dangerZone);
    this.Stage.addChild(this.PixiAsteroidGroup);
    this.Stage.addChild(this.PixiUnitGroup);
    this.Stage.addChild(this.PixiBaseBottomGroup);
    this.Stage.addChild(this.PixiPlayerGroup);
    this.Stage.addChild(this.PixiBaseTopGroup);
    this.Stage.addChild(this.PixiAlienShipGroup);
    this.Stage.addChild(this.PixiEjectaGroup);
    this.Stage.addChild(this.PixiProjectileGroup);
    this.Stage.addChild(this.PixiShipDamageParticleGroup);
    this.Stage.addChild(this.PixiPlayerOverlayGroup);
    this.Stage.addChild(this.PixiTextGroup);
    
    // lighting
    // the position of the light (a vector pointing towards the position of the light)
    // needs to be a unit vector!
    this.Light = {
        x: -1 / Math.SQRT2, 
        y: -1 / Math.SQRT2
    };


    this.spawnBGObjects();
    this.spawnFGObjects();

    this.imgs["Asteroid"] = NGE.PIXI.TextureFromImage("img/asteroid.png");
    
    //link to my ship when ready
    this.myShip = null;
    this.mainStation = null;
    if (GLOBAL_DEBUG) {
        this.MyShipInfo = new PIXI.Text('',{fontFamily : 'Arial', fontSize: 14, fill : 0xffffff, align : 'left'});
        this.MyShipInfo.x = 15; this.MyShipInfo.y = $("#pixi-canvas").height() - 40;
        this.GUIStage.addChild(this.MyShipInfo);
    }
    
//    this.Base1Info = new PIXI.Text('text',{fontFamily : 'Arial', fontSize: 18, fill : 0xAAAAFF, align : 'left'});
//    this.Base2Info = new PIXI.Text('text',{fontFamily : 'Arial', fontSize: 18, fill : 0xFFAAAA, align : 'left'});
//    this.Base1Info.x = 50; this.Base1Info.y = 45;
//    this.Base2Info.x = $("#pixi-canvas").width() - 200; this.Base2Info.y = 45;
//    this.GUIStage.addChild(this.Base1Info);
//    this.GUIStage.addChild(this.Base2Info);
    
    this.FPSInfo = new PIXI.Text('',{fontFamily : 'Arial', fontSize: 18, fill : 0xFFFFFF, align : 'left'});
    this.FPSInfo.x = 50; this.FPSInfo.y = 85;
    this.GUIStage.addChild(this.FPSInfo);
    
    //TOP
    if (GLOBAL_DEBUG) {
        this.DebugP = new OBJ_DEBUG(this.Stage);
    }

    // turn off debug mode by default (hide debug elements)
    this.ToggleDebug();
    
    this.initialized = true;
    this.Log("Init!");
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.getScope = function() {
    return {w: window.innerWidth / this.Stage.scale.x, h: window.innerHeight / this.Stage.scale.x};
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.onMouseWheel = function(event){
    var zoomOut, factor = 1, delta = (event.deltaFactor * event.deltaY) || -event.detail;
    if (delta > 0) { factor = 1.1; zoomOut = false; }
    else { factor = 1 / 1.1; zoomOut = true; }

    //limits
    //if(!zoomOut && this.Stage.scale.x > 9.5 || (zoomOut && this.Stage.scale.x < 0.1)){ return; }

//    if(!zoomOut && this.Stage.scale.x >= 1 || (zoomOut && this.Stage.scale.x < 0.5)){ return; }
//debug
    if(!zoomOut && this.Stage.scale.x >= 1 || (zoomOut && this.Stage.scale.x < 0.1)){ return; }

    this.Stage.scale.x = this.Stage.scale.x * factor;
    this.Stage.scale.y = this.Stage.scale.y * factor;

}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.AddRipple = function (x, y) {
    if (NGE.Stats && NGE.Stats.Global) {
        if (this._ripples.length < NGE.Stats.Global.warp_ripple_max_count) {
            this._ripples.push(new Ripple(x, y, NGE.Stats.Global.warp_ripple_strength, NGE.Stats.Global.warp_ripple_growth, NGE.Stats.Global.warp_ripple_duration));
        }
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.Process = function(dt){
    if (this.myShip)
    {       
        var shipX = this.myShip.GetPosX();// * this.Stage.scale.x;
        var shipY = this.myShip.GetPosY();// * this.Stage.scale.y;
        var reachX = 0;
        var reachY = 0;
        if (!this.myShip.Obj.docked.get() && (this.myShip.getThrottle() >= 1)) {
            var angle = Math.atan2(this.Input.MouseY - NGE.PIXI.ViewCenterY, this.Input.MouseX - NGE.PIXI.ViewCenterX);
            var moveOffsetX = Math.cos(angle) * this.myShip.move_radius_max.get();
            var moveOffsetY = Math.sin(angle) * this.myShip.move_radius_max.get();
            
            reachX = ((this.Input.MouseX - moveOffsetX) / window.innerWidth - 0.5) * 2;
            reachY = ((this.Input.MouseY - moveOffsetY) / window.innerHeight - 0.5) * 2;
        }
        this.Camera.x += ((shipX + reachX * (window.innerWidth / 12)) - this.Camera.x) * 0.1;
        this.Camera.y += ((shipY + reachY * (window.innerHeight/12)) - this.Camera.y) * 0.1;

        //Center text
        this.PixiTextGroup.x = this.myShip.GetPosX() - 200;
        this.PixiTextGroup.y = this.myShip.GetPosY() - 85;
        //console.log("STAGE: " + this.Stage.position.x + "," + this.Stage.position.y)
        
        if (this.myShip.docked.get()) {
            this.testStationMenu.container.visible = true;
            this.HUD.fleetPanel.container.visible = false;
        } else {
            this.testStationMenu.container.visible = false;
            this.HUD.fleetPanel.container.visible = true;
        }

        //debug
        if (GLOBAL_DEBUG) {
            this.DebugP.draw(this.myShip.GetPosX(), this.myShip.GetPosY(), NGE.PIXI.Math2D_radToDeg(this.myShip.GetRotation()));
        }
    } else {
        if (NGE.Respawned) {
            // lerp camera to center
            this.Camera.x -= this.Camera.x * 0.1;
            this.Camera.y -= this.Camera.y * 0.1;
            // show station menu for the player to equip new ship
            this.testStationMenu.container.visible = NGE.PIXI.TutorialFinished;
        }
        this.HUD.fleetPanel.container.visible = false;
    }

    //position background to camera
    this.background.position.x = this.Camera.x;
    this.background.position.y = this.Camera.y;

    
    var i;
    
    // update the parallax effect for background object
    for (i = 0; i < this.bgObjects.length; i++) {
        this.bgObjects[i].Process(dt);
    }
    // foreground object don't do anything at the moment
//    for (var i = 0; i < this.fgObjects.length; i++) {
//        this.fgObjects[i].Process(dt);
//    }

    this.Stage.x = window.innerWidth / 2 - this.Camera.x;
    this.Stage.y = window.innerHeight / 2 - this.Camera.y;
    
    // update ripples
    for(i=0; i<this._ripples.length; i++) {
        this._ripples[i].update(dt);
        if (this._ripples[i].toBeRemoved()) {
            this._ripples[i].destroy();
            this._ripples.splice(i,1);
            i--;
        }
    }
    this.Stage.filters = this._ripples.map(function(f) { return f.filter; });

    //do screen-shake
    if(this.ScreenShakeAmplitude) {
        this.Stage.x += this.ScreenShakeAmplitude * (Math.random() - 0.5);
        this.Stage.y += this.ScreenShakeAmplitude * (Math.random() - 0.5);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Pixi.prototype.render = function(dt){
    var p = NGE.PIXI;//global ref
    var MX, MY, txt;

    if(!p.initialized) { return; }

    //100 milisecond Timer
    p.timecount += NGE.Core.TIME.Delta;
    //if(p.timecount > 0.200)//0.100)
    if(p.timecount > 0.100)
    {
        p.timecount = 0;
        
        //Send Inputs WASD, MX, MY
        if (p.myShip) {
            
            //console.log((p.Input.MouseX - NGE.PIXI.ViewCenterX)+ " "  + (p.Input.MouseY - NGE.PIXI.ViewCenterY))
            
            //basic client side prediction on my ship (same inputs)
            MX = NGE.PIXI.Camera.x - window.innerWidth/2 + NGE.PIXI.Input.MouseX;
            MY = NGE.PIXI.Camera.y - window.innerHeight/2 + NGE.PIXI.Input.MouseY;

            //p.myShip._mx = (p.Input.MouseX - NGE.PIXI.ViewCenterX);
            //p.myShip._my = (p.Input.MouseY - NGE.PIXI.ViewCenterY);
            p.myShip._mx = MX - p.myShip.x.get();
            p.myShip._my = MY - p.myShip.y.get();
            p.myShip._f1 = p.Input.FIRE1;
            
            //sync view to player location
            p.PlayerCenterX = p.myShip.GetPosX();
            p.PlayerCenterY = p.myShip.GetPosY();
            
            NGE.WS.Send('input', {
                up: p.Input.UP, 
                down: p.Input.DOWN, 
                left: p.Input.LEFT, 
                right: p.Input.RIGHT,
                f1: p.Input.FIRE1,
                //vx: (p.Input.MouseX - NGE.PIXI.ViewCenterX), //VECTOR from center to mouse position
                //vy: (p.Input.MouseY - NGE.PIXI.ViewCenterY),
                vx: p.myShip._mx,
                vy: p.myShip._my,
                x: p.myShip.GetPosX(), y: p.myShip.GetPosY(), r: p.myShip.GetRotation()
            });
            
        }
    }

    //tween screen shake to "none"
    if(p.ScreenShakeAmplitude < 0.01) {
        p.ScreenShakeAmplitude = 0;
    } else {
        p.ScreenShakeAmplitude -= p.ScreenShakeAmplitude * p.ScreenShakeDecay;
    }

    //Second timer
    p.timesec += NGE.Core.TIME.Delta;
    if (p.timesec >= 1.0) {
        //console.log("[Game Second]");        
        p.timesec = 0;
        
        if(p.myShip) {
            
            NGE.buffManager = p.myShip.buffManager;
            NGE.stationManager = p.myShip.stationManager;
            
            if (GLOBAL_DEBUG && this.DebugMode && NGE.PIXI.DebugP.Active) {
                txt = "FPS: " + p.fps + " xy " + Math.floor(p.myShip.x.get()) + ", " + Math.floor(p.myShip.y.get());
                txt += " w/h " + Math.floor(p.myShip.w.get()) + ", " + Math.floor(p.myShip.h.get());
                txt += " hp " + Math.floor(p.myShip.hp.get()) + "/" + Math.floor(p.myShip.max_hp.get());
                txt += " xp " + Math.floor(NGE.accountData.xp);
                txt += " kills " + Math.floor(NGE.accountData.kills);
                txt += " deaths " + Math.floor(NGE.accountData.deaths);
                txt += " tp " + Math.floor(NGE.accountData.techpoints);
                txt += " spd+ " + Math.floor(p.myShip.speedboost.get());
                txt += " ships " + NGE.Core.CountDict(NGE.Core.GetGroup(NGE.Core.goTypes.indexOf("Ship")));
                NGE.PIXI.FPSInfo.text = txt;
                
                NGE.PIXI.FPSInfo.visible = true;
                //NGE.PIXI.MyShipInfo.visible = true;
            } else {
                NGE.PIXI.FPSInfo.visible = false;
                //NGE.PIXI.MyShipInfo.visible = false;
            }
        }
        else{
            if (GLOBAL_DEBUG && this.DebugMode && NGE.PIXI.DebugP.Active) {
                NGE.PIXI.FPSInfo.text = "FPS: " + p.fps;
                NGE.PIXI.FPSInfo.visible = true;
            } else {
                NGE.PIXI.FPSInfo.visible = false;
            }
                
        }
        
        if (NGE.WS.isLoggedIn) {
            NGE.PIXI.HUD.wallet.setShipBalance(NGE.accountData.credits);
            NGE.PIXI.HUD.wallet.setBankBalance(NGE.accountData.bank);
            NGE.PIXI.HUD.wallet.setTPBalance(NGE.accountData.techpoints);
        }
        
        p.fps = 0;
    }
    else
    {
        p.fps++;
        
        if(GLOBAL_DEBUG && p.myShip) {
            if (this.DebugMode) {
                //var reachX = (this.Input.MouseX / window.innerWidth - 0.5) * 2;
                //var reachY = (this.Input.MouseY / window.innerHeight - 0.5) * 2;
                MX = NGE.PIXI.Camera.x - window.innerWidth/2 + NGE.PIXI.Input.MouseX;
                MY = NGE.PIXI.Camera.y - (window.innerHeight - 5)/2 + NGE.PIXI.Input.MouseY;
                txt = "ID [" + Math.floor(p.myShip.id.get());
                txt += "] Team [" + Math.floor(p.myShip.team.get());
                txt += "] Side [" + Math.floor(p.myShip.side.get());
                txt += "] rad " + Math.floor(p.myShip.move_radius.get());
                txt += "-" + Math.floor(p.myShip.move_radius_max.get());
                txt += " thr " + Math.floor(p.myShip._throttle * 100) + "%";
                txt += " acc " + Math.floor(p.myShip.acceleration.get()); 
                txt += " spd " + Math.floor(p.myShip._speed); 
                txt += " vxy " + Math.floor(p.myShip.vx.get()) + ", " + Math.floor(p.myShip.vy.get());   
                txt += " MXY: " + Math.floor(MX - p.myShip.x.get()) + ", " + Math.floor(MY - p.myShip.y.get());
                txt += "\n";
                txt += " ammo: " + Math.floor(p.myShip.ammo.get()) + " / " + Math.floor(p.myShip.max_ammo.get());
                NGE.PIXI.MyShipInfo.text = txt;
                NGE.PIXI.MyShipInfo.visible = true;
            } else {
                NGE.PIXI.MyShipInfo.visible = false;
            }
            //+ " Dist: " + Math.floor(p.myShip._MoveDist)
            //+ " Diff: " + Math.floor(p.myShip._MoveDiff) 
            //+ " StageXY: " + Math.floor(p.myShip.x.get() - this.Stage.x ) + ", " + Math.floor(p.myShip.y.get() - this.Stage.y)
            //+ " CameraXY: " + Math.floor(this.Camera.x - p.myShip.x.get()) + ", " + Math.floor(this.Camera.y - p.myShip.y.get());
        }
        
    }

    p.guiManager.update(dt);

    p.Process(dt);
    p.Canvas.render(p.GUIStage);

    p.Input.MOUSE_PRESSED = false;
    p.Input.MOUSE_RELEASED = false;
    
    if (p.WasTabbedAway) {
        //console.log("first render after being tabbed away finished (wasTabbedAway -> false)");
        p.WasTabbedAway = false;
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
