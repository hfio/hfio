var WSLOGG = "";
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function OBJ_SocketIO()
{
    //debugging
    this.DEBUG = true;
    
    this.Domain = document.location.hostname;
    this.Server = "http://" + this.Domain + ":3010";
    this.socket = null;
    this.netid = null;
    this.connected = false;
    this.isLoggedIn = false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_SocketIO.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[IO] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_SocketIO.prototype._Delete = function(odata, flush, outOfScope) {
    var id = odata[0];
    var type = odata[1];
    var hpflag = odata[2];
    var aii = odata[3];
    var creditsAwarded = odata[4];
    aii = aii === 1 ? "AI" : "Player";

    WSLOGG += id + "\n";

    if (NGE.Core.Objects[type].hasOwnProperty(id)) {
        var o = NGE.Core.Objects[type][id];
        if(id === NGE.WS.netid) {
            this.Log("MyShip destroyed!");
            NGE.PIXI.myShip = null;
        }
        if (o === NGE.PIXI.mainStation) {
            this.Log("Main station destroyed!");
            NGE.PIXI.mainStation = null;
        }
        if (type === 1) { // ship
            // silent remove if tabbed away, view was flushed or object out of scope, warp out if not silent and greater than zero HP
            o.Remove(document.hidden || flush || outOfScope, hpflag, creditsAwarded);
        } else {
            // silent remove if tabbed away view was flushed or removing a ship with greater than zero HP
            o.Remove(document.hidden || hpflag || flush);
        }
        if (GLOBAL_DEBUG) {
            NGE.PIXI.DebugP.remove(id);
        }
        delete NGE.Core.Objects[type][id];

        //console.log("DELETE: " + id + " " +  type + " " + aii);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_SocketIO.prototype.Init = function()
{
    var O;
    this.socket = io(this.Server);
    var s = this.socket;
    var self = this;
    this.Log("Connecting to: " + this.Server);
    
    this.socket.on('connect', function() {
        $("#ServerStatus").html('Server is <span style="color: green;">Available</span>');        
        $('#bSignup').prop("disabled",false);
        $('#bLogin').prop("disabled",false);
        self.connected = true;
        NGE.GUI.Connect();
        NGE.Core.ClearObjects(true);
        NGE.WS.Send("scope", NGE.PIXI.getScope());
        NGE.AUTH.launchWhenReady();
    });
    this.socket.on('disconnect', function() {
        //location = "./";//buggy
        self.Disconnect();
    });

    s.on('m', function (d) {
        //console.log("IN: "  + d);
        d = JSON.parse(d);//decode string
        var status = d[0];
        var cmd = d[1];
        var i;
        //var hmac = d[2];
        //var data = JSON.parse(d[3]);//json'd again
        var data = d[3];//NOT JSON
        
        //just connected
        if (cmd === "con") {
            self.Log("My NETID = " + data);
            NGE.WS.netid = data;
        }
        else if (cmd === "o") {//need my id to proceed
            if (status === 1) {
                self.Log('OLD UPDATE');
                var Objs = data[0];
                var DELObjs = data[1];
                //console.log(JSON.stringify(data));
                for (i=0; i< Objs.length; i++)
                {
                    O = Objs[i];//list
                    NGE.PIXI.ObjectUpdate(O);//O[0], O[1], O[2], O[3], O[4], O[5], O[8], O[9], O[10] * (Math.PI/180));
                }
                for (i=0; i< DELObjs.length; i++)
                {
                    O = DELObjs[i];//list
                    NGE.PIXI.ObjectDelete(O);//O[0], O[1], O[2]
                }

             }
            else {  }
        }
        else if (cmd === "api_list") {//need my id to proceed
            if (status === 1) {
                NGE.GUI.AdminShipList(data);
            }
        }
        else if (cmd === "api_cu") {//need my id to proceed
            if (status === 1) {
                self.Log("ship Updated successfully! updating list");
                //NGE.WS.Send('api_list',{ obj: "ships" });
            }
        }
        else if (cmd === "api_read") {//need my id to proceed
                if (status === 1) {
                    NGE.GUI.AdminShipRead(data);
                }
            }
        else if (cmd === "api_delete") {//need my id to proceed
            if (status === 1) {
                //relist
                NGE.WS.Send('api_list',{ obj: "ships" });
            }
        }
        //else if (cmd === "pickship") { //
        //    self.Log("pickship response:", d);
        //}
        else if (cmd === "getglobal") {//global setting
            if (status === 1) {
                NGE.GUI.GotGlobal(data);
            }
        }
        else if (cmd === "account") {
            //console.log("account data update:", data);
            for (i=0;i<data.length;i+=2){
                var key = data[i];
                var val = data[i+1];
                // unpack csv
                if (key === 'ownedshipslist'){
                    if (val === '') {
                        val = [];
                    } else {
                        val = JSON.parse(val);
                    }
                }
                NGE.accountData[key] = val;
            }
            //console.log(JSON.stringify(NGE.accountData));
        }
        else if (cmd === "wave") {
            self.Log("Got wave data: " + JSON.stringify(data));
            // see server side Game.js, GetWaveData() method for format
            if ((data[0] === 0) && (data[1] > 0)) {
                NGE.PIXI.HUD.infoPopup.setText("BASE DESTROYED, RESTARTING IN\n" + data[1], 1.5);
            } else if (data[0] > NGE.WaveIndex) {
                NGE.PIXI.HUD.infoPopup.setText("WAVE " + data[0], 3);
            }
            NGE.WaveIndex = data[0];
            NGE.WaveShipCount = data[1];
            NGE.WaveShipLeftCount = data[2];
        }
        else if (cmd === "respawn") {
            if (data[0] === 0) {
                self.Log("Ship respawn (move camera back to base): " + JSON.stringify(data));
                NGE.Respawned = true;
            } else {
                NGE.PIXI.HUD.infoPopup.setText("SHIP DESTROYED, RESPAWNING IN\n" + data[0], 1.5);
            }
        }
        else if (cmd === "login") {
            if (status === 1) {
                self.Log("logged in!");
                NGE.PIXI.HUD.infoPopup.setText("LOGGED IN", 1.5);
                self.isLoggedIn = true;
            }
        }
        else if (cmd === 'deploystation') {
            if (status === 0) {
                if (NGE.PIXI.myShip) {
                    NGE.PIXI.myShip.ShowErrorMessage("Cannot deploy on top of other stations!", 2.5);
                }
            }
        }
        else if (cmd === 'cashout') {
            if (status === 0) {
                NGE.PIXI.HUD.showError("Error during cashout: " + data, 4);
            }
        }
        else if (cmd === "view") {
            // if an update happens while being tabbed away, mark that so when we render for the first time after coming back, we will know
            if (document.hidden) {
                NGE.PIXI.WasTabbedAway = true;
            }
            var j, id, type, o, odata, subdata, defaults, index, value;
            //console.log("view: "  + d);
            var oCreates = data[0];
            var oUpdates = data[1];
            var oDeletes = data[2];
            var oOutOfScope = data[3];
            var flush = ((data.length >= 5) && !!data[4]); 
            
            //WSLOGG.push("CREATE");
            //WSLOGG.push(oCreates);
            //WSLOGG.push("UPDATE");
            //WSLOGG.push(oUpdates);
            //WSLOGG.push("DELETE");
            //WSLOGG.push(oDeletes);
            WSLOGG += "------------------------------------------------------------------------------------\n";
            WSLOGG += "START: " + JSON.stringify(Object.keys(NGE.Core.Objects[1]));
            //if (oCreates.length > 0) { console.log("Create: "  + JSON.stringify(oCreates)); }
            //if (oUpdates.length > 0) { console.log("Update: "  + oUpdates); }
            //if (oDeletes.length > 0) { console.log("Delete: "  + JSON.stringify(oDeletes)); }
            //console.log("Create: "  + oCreates);
            //console.log("Update: "  + oUpdates);
            //console.log("Delete: "  + oDeletes);
            //----------------------------------------------------------------------------------------
            // starting with deletes as we might get both a create and delete with the same ID, which means we need to recreate that object
            //----------------------------------------------------------------------------------------
            // remove game objects
            //----------------------------------------------------------------------------------------
            WSLOGG += "DELETE\n";
            for( i=0; i< oDeletes.length; i++) {
                self._Delete(oDeletes[i], flush, false);
            }
            WSLOGG += "OUT OF SCOPE\n";
            for( i=0; i< oOutOfScope.length; i++) {
                self._Delete(oOutOfScope[i], flush, true);
            }
            //----------------------------------------------------------------------------------------
            // create new game objects
            //----------------------------------------------------------------------------------------
            WSLOGG += "CREATE\n";
            for(i=0; i< oCreates.length; i++) {
                odata = oCreates[i];
                var isNew = odata[0];
                id = odata[1];
                
                type = odata[2];
                defaults = NGE.Core.Copy(NGE.DATA.Object_Defaults);
                defaults.new = isNew;
                defaults.type = type;
                
                WSLOGG += id + "\n";

                //Modify Defaults to incoming data
                defaults.subtype = odata[3];
                defaults.owner = odata[4];
                defaults.team = odata[5];
                
                defaults.x = odata[6]; defaults.y = odata[7]; defaults.vx = odata[8]; defaults.vy = odata[9];
                defaults.r = odata[10]; defaults.w = odata[11]; defaults.h = odata[12]; defaults.radius = odata[13];
                
                defaults.max_speed = odata[14];
                defaults.acceleration = odata[15]; defaults.decay = odata[16];
                defaults.rot_speed = odata[17]; defaults.rot_max_speed = odata[18];
                defaults.hp = odata[19]; defaults.max_hp = odata[20];
                defaults.damage = odata[21]; defaults.max_damage = odata[22];
                defaults.damage_range = odata[23];
                
                //console.log(defaults);
                o = undefined;
                if(type === 0) { // base
                    //console.log('CREATING BASE ' +  id);
                    
                    var config = NGE.Stats.getStation(defaults.subtype);
                    
                    var isMainStation = (defaults.subtype === NGE.Stats.Global.main_station_id);
                    
                    //Modify Defaults to incoming data
                    o = new oBase(NGE, defaults, config, isMainStation);
                    
                    if (isMainStation) {
                        self.Log('***** Found Main Station **** ' +  id);
                        NGE.PIXI.mainStation = o;
                    }
                }
                else if(type === 1) { // ship
                    //console.log('CREATING SHIP ' + id);
                    
                    //console.log(odata);
                    
                    o = new oShip(NGE, defaults, document.hidden || !isNew);
                    
                    // local player ship?
                    if(id === NGE.WS.netid) {
                        self.Log('***** Found My Ship **** ' +  id);
                        NGE.PIXI.myShip = o;
                        o._myship = true;//flag as myship
                        
                        NGE.Respawned = false;
                        
                        //flag if we need to setup with buddy ship and pvp team
                        if(NGE.RefUsed === false)
                        {
                            self.Log("REF SEND: " + JSON.stringify({ id: NGE.RefShipID }));
                            NGE.WS.Send('api_ref',{ id: NGE.RefShipID });
                            NGE.RefUsed = true;//used up
                        }
                        
                        //Update Referrall link
                        var fleetUrlPath =  NGE.GetFleetPath(id);
                        $("#fleetURL").html(NGE.GetFleetUrl(id));
                        // set browser URL:
                        window.history.pushState({refServer: NGE.RefServer, refShipID: id}, document.title, fleetUrlPath);
                    }
                }
                else if(type === 2) { // asteroid
                    //console.log('CREATING ASTEROID ' +  id);
                    
                    o = new oAsteroid(NGE, defaults);
                    
                } 
                else if(type === 3) { // turret
                    
                    o = new oTurret(NGE, defaults, {standalone: odata[25]}, document.hidden || !isNew);
                    
                } else {
                    self.Log("Cannot create object: unknown object type: " + type);
                }
                if(o) {
                    //console.log("NEW " + JSON.stringify(odata))
                    //console.log("CURRENT " + JSON.stringify(o.Obj._dl._idx_.val))
                    o.Obj._dl._idx_.val = odata; // override state
                    //console.log("SET " + JSON.stringify(o.Obj._dl._idx_.val))

                    if (NGE.Core.Objects[type].hasOwnProperty(id)) {
                        self.Log("ALREADY CREATED!!!!!");
                        var oto = NGE.Core.Objects[type][id];
                        if(id === NGE.WS.netid) {
                            NGE.PIXI.myShip = o;//redirect to new
                        }
                        if (oto === NGE.PIXI.mainStation) {
                            NGE.PIXI.mainStation = o;
                        }
                        oto.Remove(true);
                        if (GLOBAL_DEBUG) {
                            NGE.PIXI.DebugP.remove(id);
                        }
                        
                    }

                    NGE.Core.ObjectAdd(o);
                }
            }
            //----------------------------------------------------------------------------------------
            // update existing game objects
            //----------------------------------------------------------------------------------------
            WSLOGG += "UPDATE\n";
            for(i=0; i< oUpdates.length; i++) {
                odata = oUpdates[i];
                id = odata[0];
                type = odata[1];
                subdata = odata[2];//[index, value, index, value]
                
                WSLOGG += id + "\n";
                
                if (NGE.Core.Objects[type].hasOwnProperty(id)) {
                    o = NGE.Core.Objects[type][id];
                    //console.log("Updating Object: " + type + " id: " + id);
                    //My Ship updates (process differently with local prediction)
                    if(id === NGE.WS.netid) {
                        var mship = NGE.PIXI.myShip;
                        for(j = 0; j < subdata.length; j += 2) { // nested arrays hurt my brain
                            index = subdata[j];
                            value = subdata[j+1];
                            //look for image change (subtype for ships)
                            if (index === 3) {
                                self.Log("Ship Change: " + value);
                                mship.subtype.set(value);
                                mship.UpdateImage();
                            }
                            else if (index === 6) { mship._svx = value; }//x
                            else if (index === 7) { mship._svy = value; }//y
                            else if (index === 8) { mship._svvx = value; }//vx
                            else if (index === 9) { mship._svvy = value; }//vy
                            else if (index === 10) { mship._svr = value; }//r
                            else //ok to set
                            {
                                o.Obj._dl._idx_.val[index] = value;
                                //console.log("MYSHIP CHANGE: " + index + "=" + value)
                            }
                            
                        }
                        
                        //console.log("[MySHIP] " + id + " " + subdata);
                        
                        //Prediction allow updates except for x, y, r for MyShip
                        //If server says  we are beyond a certain radius, we pop to that location
                        //this means we got way off location or respawned.
                        
                        //Prediction (client has some authority, server overrides in some case)
                        if (NGE.Core.MATH.VecLengthSquared(mship._svx - mship.x.get(), mship._svy - mship.y.get()) > mship._authradiusSQ)
                        {
//                            console.log("[MySHIP] Out of Range! " +  (mship._svx - mship.x.get()) + (mship._svy - mship.y.get()));
                            mship.x.set(mship._svx); mship.y.set(mship._svy);
                            
                        }
                        
                        
                    }
                    else
                    {
                        //if (type === 1) {//ship
                            //console.log("id: " + id + " type: " + type + " SUBDATA " + JSON.stringify(subdata))    
                        //}
                        
                        for(j = 0; j < subdata.length; j += 2) {
                            index = subdata[j];
                            value = subdata[j+1];
                            o.Obj._dl._idx_.val[index] = value;
                            
                            //look for image change (subtype for ships)
                            if (index === 3) {
                                if (type === 1) {//ship
                                    self.Log("Other Ship Change: " + value);
                                    o.UpdateImage();
                                }
                            }
                        }
                        
                    }
                    
                    //console.log("USET " + JSON.stringify(o.Obj._dl._idx_.val))
                }
                else{
                    self.Log("UPDATE: No Object id: " + id + " " +  type);
                }
            }
            
            //WSLOGG += "END: " + JSON.stringify(Object.keys(NGE.Core.Objects[1]));

            
            
        } else {
            //console.log("Other: "  + d);
        }
            
        
    });
    
    //console.log("WS Init!");
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_SocketIO.prototype.Send = function(cmd, d)
{
    if (this.socket) {
        d.cmd = cmd;
        //console.log("SEND: " + JSON.stringify(d));
        this.socket.emit('m', JSON.stringify(d));
        return true;
    }
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_SocketIO.prototype.Disconnect = function()
{
    if (this.connected) {
        this.Log("disconnect");
        this.connected = false;
        this.socket.disconnect();
        NGE.GUI.Disconnect();
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************