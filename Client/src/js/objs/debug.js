//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function OBJ_DEBUG(stage) {
    
    this.g = new PIXI.Graphics();
    this.w = 100000;
    this.h = 100000;

    //targetline
    this.targetcolor = 0x330000;
    this.targetlength = 3000;
    
    //this.renderer = renderer;
    //this.baseW = this.renderer.width;
    //this.baseH = this.renderer.height;
    //this.g.lineStyle(1, 0x333333, 1);
    stage.addChild(this.g);
    this.stage = stage;
    
    this.poly = {};
    
    this.line = {};
    
    this.State = OBJ_DEBUG.STATE.OFF;
    
    this.g.visible = false;

    //this.resize(wh);    
    //this.draw();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.STATE = {
    OFF: "off", // no debug info visible
    TEXTS: "texts", // debug texts are visible, but graphics are not
    FULL: "full" // all debug info visible
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
Object.defineProperty(OBJ_DEBUG.prototype, "Active", {
    get: function () { return this.State !== OBJ_DEBUG.STATE.OFF; }
});
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.SetState = function(state)
{
    this.State = state;
    if (!this.Active) {
        this.g.clear();
    }
    this.g.visible = this.Active;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.Switch = function ()
{
    switch (this.State) {
        case OBJ_DEBUG.STATE.OFF:
            this.SetState(OBJ_DEBUG.STATE.TEXTS);
            break;
        case OBJ_DEBUG.STATE.TEXTS:
            this.SetState(OBJ_DEBUG.STATE.FULL);
            break;
        case OBJ_DEBUG.STATE.FULL:
            this.SetState(OBJ_DEBUG.STATE.OFF);
            break;
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.create = function(id, localpoints, wx, wy, r)
{
    this.poly[id] = {localpoints, wx, wy, r};//local points, not world coords offset from 0,0. wx/wy are world location
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.updatexyr = function(id, wx, wy, r)
{
    if (this.poly.hasOwnProperty(id)) {
        this.poly[id].wx = wx;
        this.poly[id].wy = wy;
        this.poly[id].r = NGE.PIXI.Math2D_radToDeg(r);
        //console.log(JSON.stringify(this.poly[id]));
    }
    //else{console.log(id)}
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.remove = function(id)
{
    if (this.poly.hasOwnProperty(id)) {
        delete this.poly[id];
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.createLine = function(id, line)
{
    this.line[id] = line;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.removeLine = function(id)
{
    if (this.line.hasOwnProperty(id)) {
        delete this.line[id];
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.resize = function(tilesize, w, h)
{
    this.tilesize = tilesize;
    this.w = w;
    this.h = h;
    
    //this.draw();    
    //$("#mainedit").height(window.innerHeight - 140);
    //this.w = Math.floor(this.baseW / this.stage.scale.x);
    //this.h = Math.floor(this.baseH / this.stage.scale.y);
    //this.g.width = this.w; this.g.height = this.h;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.onMinimum = function(){
    this.g.clear();
    //console.log("GRID CLEAR");
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_DEBUG.prototype.draw = function(x, y, r)
{
    if (this.State !== OBJ_DEBUG.STATE.FULL){return;}

    var O, key, i, ox, oy, rxy, id, radius;
    this.g.clear();//reset
    this.g.lineStyle(2, 0x990000, 0.8);
    
    for (key in this.poly) {
        if (this.poly.hasOwnProperty(key)) {
            O = this.poly[key];
            var PolyPoints = [];
            for (i=0; i< O.localpoints.length; i++) {
                ox = O.localpoints[i][0];//[x,y]
                oy = O.localpoints[i][1];
                //rxy = this.Math2D_rotate_by_pivot(O.wx, O.wy, O.r, ox, oy);
                //rx = rxy[0]; ry = rxy[1];
                //PolyPoints.push(new PIXI.Point(rx, ry));
                PolyPoints.push(new PIXI.Point(O.wx + ox, O.wy + oy));
            }
            //console.log(JSON.stringify(PolyPoints[1]) + " " + O.r);
            //this.g.drawPolygon(PolyPoints);
            for (i=0; i< PolyPoints.length; i++) {
                if (i>0) {
                    this.g.moveTo(PolyPoints[i-1].x,PolyPoints[i-1].y);
                    this.g.lineTo(PolyPoints[i].x,PolyPoints[i].y);
                }
            }
            
        }
    }
    
    // lines for projectiles (uncomment create/removeLine() in projectile.js to use)
    this.g.lineStyle(2, this.targetcolor);
    var keys = Object.keys(this.line);
    for (i = 0; i < keys.length; i++) {
        var line = this.line[keys[i]];
        this.g.moveTo(line.initial_x.get(), line.initial_y.get());
        this.g.lineTo(line.x.get(), line.y.get());
    }
    
    //myship atarget
    this.g.lineStyle(2, this.targetcolor);
    this.g.moveTo(x,y);
    rxy = NGE.PIXI.Math2D_rotate_by_pivot(x, y, r, this.targetlength,0);
    this.g.lineTo(rxy[0], rxy[1]);
    
    //movebox DEBUG
    if (NGE.PIXI.myShip) {
        this.g.lineStyle(3, 0x000088, 0.7);
        //var mbh = parseInt( $("#tweak_box").val() ) / 2;
        var mbh = NGE.PIXI.myShip.move_radius.get();
        var mbh2 = NGE.PIXI.myShip.move_radius_max.get();
        //var xx = ;
        //var yy = NGE.PIXI.myShip.position.y;
        if (typeof mbh === 'number' && mbh > 2){
            //this.g.drawRect(xx - mbh, yy - mbh, mbh * 2 , mbh * 2);
            this.g.drawCircle(NGE.PIXI.myShip.GetPosX(), NGE.PIXI.myShip.GetPosY(), mbh);
        }
        this.g.lineStyle(3, 0x0000BB, 0.7);
        if (typeof mbh2 === 'number' && mbh2 > 2){
            //this.g.drawRect(xx - mbh, yy - mbh, mbh * 2 , mbh * 2);
            this.g.drawCircle(NGE.PIXI.myShip.GetPosX(), NGE.PIXI.myShip.GetPosY(), mbh2);
        }

    }

    //Base info
    this.g.lineStyle(10, 0x0000FF);
    this.g.drawCircle(0,0, parseInt($('#server_base_radius').val()));
    this.g.lineStyle(10, 0xFF0000);
    this.g.drawCircle(0,0, parseInt($('#server_base_redzone').val()));

    // asteroids
    this.g.lineStyle(3, 0xFFAA00);
    var TYPE_ASTEROID = NGE.DATA.CoreData.goTypes.indexOf("Asteroid"); // game object type
    // iterate over asteroid game objects
    var asteroids = NGE.Core.Objects[TYPE_ASTEROID];
    for (id in asteroids) {
        if (asteroids.hasOwnProperty(id)) {
            var asteroid = asteroids[id];
            // if object has sprite still in pixi scenegraph
            if (asteroid._sprite && asteroid._sprite.parent){
                // draw collision circle indicator
                radius = asteroid.radius.get();
                this.g.drawCircle(asteroid._sprite.position.x, asteroid._sprite.position.y,radius);
            }
        }
    }

    // Ship collision circle for asteroids
    if (NGE.PIXI.myShip) {
        this.g.lineStyle(2, 0xFFAA00);
        radius = NGE.PIXI.myShip.Obj.radius.get();
        this.g.drawCircle(NGE.PIXI.myShip.GetPosX(), NGE.PIXI.myShip.GetPosY(), radius);
    }

    // bases
    this.g.lineStyle(3, 0xAAFF00);
    var TYPE_BASE = NGE.DATA.CoreData.goTypes.indexOf("Base"); // game object type
    // iterate over asteroid game objects
    var bases = NGE.Core.Objects[TYPE_BASE];
    for (id in bases) {
        if (bases.hasOwnProperty(id)) {
            var base = bases[id];
            if (base.sprite && base.sprite.parent){
                // draw dock circle indicator
                radius = base.dock_radius.get();
                this.g.drawCircle(base.sprite.position.x, base.sprite.position.y,radius);
            }
        }
    }

    //console.log("DEBUG DRAW");
}
