//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function OBJ_Auth(userID) {
    //debugging
    this.DEBUG = true;
    
    this.sessionTicket = null;
    this.newlyCreated = undefined;
    this._shouldLaunch = false;
    this.init(userID);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************


function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

OBJ_Auth.UUID_LOCALSTORAGE_KEY = "PlayFabCustomId";

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Auth.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[AUTH] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Auth.prototype.init = function(userID) {
   
    if (!userID) {
        userID = generateUUID();
        if (!localStorage[OBJ_Auth.UUID_LOCALSTORAGE_KEY]) {
            localStorage[OBJ_Auth.UUID_LOCALSTORAGE_KEY] = userID;
        } else {
            localStorage[OBJ_Auth.UUID_LOCALSTORAGE_KEY] += "," + userID;
        }
    }
    this.Log('UUID:'+userID);
    PlayFab.settings.titleId = '7B61';
    var loginRequest = {
        // Currently, you need to look up the correct format for this object in the API-docs:
        // https://api.playfab.com/documentation/Client/method/LoginWithCustomID
        TitleId: PlayFab.settings.titleId,
        CustomId: userID,
        CreateAccount: true
    };

    PlayFabClientSDK.LoginWithCustomID(loginRequest, this._LoginCallback.bind(this));

};

// sets the flag to launch the auth process with the server when the playfab login is ready - launches right away if it already is
OBJ_Auth.prototype.launchWhenReady = function () {
    this._shouldLaunch = true;
    this._launch();
}

// only launches if the launch flag is set and the playfab login finishes (we have a session ticket)
OBJ_Auth.prototype._launch = function () {
    if (this._shouldLaunch && this.sessionTicket) {
        NGE.WS.Send('auth', { sessionTicket: this.sessionTicket, newlyCreated: this.newlyCreated });
    }
}

OBJ_Auth.prototype._LoginCallback = function (result, error) {
    if (result !== null) {
        var playFabId = result.data.PlayFabId;
        var sessionTicket = result.data.SessionTicket;
        var newlyCreated = result.data.NewlyCreated;
        this.Log( "Playfab auth: "+playFabId);
        this.Log("Session ticket: "+sessionTicket);
        this.Log("Newly created: "+newlyCreated);
        this.sessionTicket = sessionTicket;
        this.newlyCreated = newlyCreated;
        this._launch();
    } else if (error !== null) {
        if (GLOBAL_DEBUG) {
            console.warn( 
                "Something went wrong with your API call.\n" +
                "Here's some debug information:\n" +
                CompileErrorReport(error)
            );
        }
    }
}

OBJ_Auth.prototype.changeDisplayName = function (displayName, callback) {
    PlayFabClientSDK.UpdateUserTitleDisplayName({DisplayName: displayName}, this._NameChangeCallback.bind(this, callback));
}

OBJ_Auth.prototype._NameChangeCallback = function (customCallback, result, error) {
    if (result !== null) {
        customCallback(result.data);
    } else if (error !== null) {
        if (error.error === "NameNotAvailable") {
            customCallback(null);
        } else {
            if (GLOBAL_DEBUG) {
                console.warn( 
                    "Something went wrong with your API call.\n" +
                    "Here's some debug information:\n" +
                    CompileErrorReport(error)
                );
            }
        }
    }
}

// This is a utility function we haven't put into the core SDK yet.  Feel free to use it.
function CompileErrorReport(error) {
    if (error === null)
        return "";
    var fullErrors = error.errorMessage;
    for (var paramName in error.errorDetails)
        for (var msgIdx in error.errorDetails[paramName])
            fullErrors += "\n" + paramName + ": " + error.errorDetails[paramName][msgIdx];
    return fullErrors + "\n" + JSON.stringify(error);
}

//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Auth.prototype.loginFacebook = function() {
    /*
      this.tokenRequested = false;
      if (!firebase.auth().currentUser || firebase.auth().currentUser.isAnonymous) {
        var provider = new firebase.auth.FacebookAuthProvider();
        //provider.addScope('user_likes');
        firebase.auth().signInWithRedirect(provider);
      } else {
        console.log('Signing out');
        firebase.auth().signOut();
      }
      */
      
      
//FB.getLoginStatus(function(response) {
//  if (response.status === 'connected') {
//    console.log('facebook - Logged in.');
//    PlayFabClientSDK.LoginWithFacebook({ AccessToken: response.authResponse.accessToken, CreateAccount: true, TitleId: PlayFab.settings.titleId }, LoginCallbackNoDelay);
//  }
//  else {
//    FB.login();
//  }
//});
      
      
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Auth.prototype.authStateChanged = function(user) {
    /*
    if (user) {
        // User is signed in.
        //var displayName = user.displayName;
        //var email = user.email;
        //var emailVerified = user.emailVerified;
        //var photoURL = user.photoURL;
        //var isAnonymous = user.isAnonymous;
        //var uid = user.uid;
        //var providerData = user.providerData;
        //console.log(JSON.stringify(user));

        if (user.isAnonymous) {
            this.displayName = 'GUEST ' + user.uid.substring(0,5);
        } else {
            this.displayName = user.displayName;
        }

        // get JWT to send to backend
        if (!this.tokenRequested) {
            this.tokenRequested = true;
            firebase.auth().currentUser.getToken( true).then(function(idToken) {
                // Send token to backend
                //console.log('JWT: '+idToken);
                var d = { jwt: idToken };
                setTimeout(function(){
                    console.log('JWT: '+idToken);
                    NGE.WS.Send('auth', d);
                },5000);
            }).catch(function(error) {
                // Handle error
                console.error(error);
            });
        }

    } else {
        // User is signed out.
        // log them in as a guest (anonymous firebase user)
        this.loginGuest();
    }
    */
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Auth.prototype.loginGuest = function() {
    /*
    this.tokenRequested = false;
    firebase.auth().signInAnonymously().catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
    */
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Auth.prototype.redirectResult = function(result) {
    /*
    if (result.credential) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = result.credential.accessToken;
    }
    // The signed-in user info.
    var user = result.user;
    */
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
OBJ_Auth.prototype.redirectResultError = function(error) {
    /*
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    if (errorCode === 'auth/account-exists-with-different-credential') {
        alert('You have already signed up with a different auth provider for that email.');
        // If you are using multiple auth providers on your app you should handle linking
        // the user's accounts here.
    } else {
        console.error(error);
    }
    */
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// Closure export:
window['OBJ_Auth'] = OBJ_Auth;