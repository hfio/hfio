//*********************************************************************************************************************************************
//Engine Core Manages a Local or Remote game (client or server)
// Minimal game engine
//  Data (GD)
//  Objects
//  Timing
//  Simulation / Processing
//  Math2D
//  Collision
//  Movement
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var oTimer = require('./Core/sTimer');
//var oMath2D = require('./Core/sMath2D');
//var oCollision = require('./Core/sCollision');
//var oMovement = require('./Core/sMovement');
//var oArea = require('./Core/sArea');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oCore(Game, d) {

    //debugging
    this.DEBUG = true;
    
    //Game
    this.Game = Game;
    
    //Modules
    this.TIME = new oTimer(this);
    this.MATH = new oMath2D(this);
    this.CD = new oCollision(this);
    this.MV = new oMovement(this);
    this.AREA = new oArea(this, d.MapX, d.MapY, d.MapSize);
    
    this.Name = "CORE";
    
    //Setup Data
    this.data = d;//any data
    this.goTypes = d.goTypes;//types list [...]
    this.Objects = {};//object tracking by group (ordered by list)
    for(var i = 0; i < d.goTypes.length; i++)
    {
        this.Objects[i] = {};//new group
    }
    this.ObjectsRemovals = {};//object tracking by group (ordered by list)
    for(i = 0; i < d.goTypes.length; i++)
    {
        this.ObjectsRemovals[i] = {};//new group
    }
    
    //Internal Data
    this.LoopCounter = 0;//for update loops counting
    this.NextID = 0;//ids for objects (incremented)
    this.Process_Delta = 0;//time tracking
    this.NextID = 0;//ids for objects (incremented)
    this.NextTEAM = 1;//teams 1-2
    this.NextTEAM_MAX = 2;//teams max
    
    //Networking if used
    this.NetUpdateTime = d.NT;// times a sec (network updates)
    this.NetCounter = 0;//for Net update loops ran since heartbeat
    this.NetUpdate_Delta = 0;
    this.NetClientNextUpdate = 0;
    
    //debug info
    if (GLOBAL_DEBUG) {
        this.Log("Object Types: " + this.JSON(d.goTypes));
        this.Log("Objects: " + this.JSON(this.Objects));
        this.Log("LoopTime: " + (d.LT / 100).toFixed(2) + " per sec");
        this.Log("NetworkTime: " + d.NT + " per sec");
        //this.Log("CP: " + CP);
    }
    
    return this;
}
//*********************************************************************************************************************************************
//Helpers
//*********************************************************************************************************************************************
oCore.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[CORE] " + m); }}
oCore.prototype.JSON = function(d) { return JSON.stringify(d); }
oCore.prototype.Copy = function(d) { return JSON.parse(JSON.stringify(d)); }//simple copy
oCore.prototype.CountDict = function(d) { return Object.keys(d).length; }
oCore.prototype.DictKeys = function(d) { return Object.keys(d); }
//oCore.prototype.ObjectCreate = function() { this.Objects[GO.type][GO.id] = GO; }
oCore.prototype.ObjectAdd = function(GO) { this.Objects[GO.type.get()][GO.id.get()] = GO; }
oCore.prototype.GetNextID = function() { this.NextID++; return this.NextID; }
oCore.prototype.GetNextTeam = function() { var t = this.NextTEAM; this.NextTEAM++; if(this.NextTEAM > this.NextTEAM_MAX){this.NextTEAM=1;} return t; }
oCore.prototype.GetGroup = function(g){ return this.Objects[g]; }
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.GetObject = function(objectTypeName, objectId) { 
    var group = this.GetGroup(this.goTypes.indexOf(objectTypeName));
    return group[objectId];
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.ExecuteForObjects = function(objectTypeName, callback) { 
    var group = this.GetGroup(this.goTypes.indexOf(objectTypeName));
    var keys = Object.keys(group);
    for (var i = 0; i < keys.length; i++) {
        callback(group[keys[i]]);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.ClearObjects = function (silent) { // removes all objects (for when a new server instance is started)
    for(var i = 0; i < this.goTypes.length; i++)
    {
        var objs = this.Objects[i];//keys are numbers
        for (var id in objs) {
            if (objs.hasOwnProperty(id)) {
                objs[id].Remove(silent);
            }
        }
        this.Objects[i] = {};//new empty group
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.Init = function() {
    this.Log("Init");
    
    //Start main loop
    this.LoopBound = this.Loop.bind(this);
    this.Loop();
    return this;
}
//*********************************************************************************************************************************************
//Run Main Loop - manage simulation
//*********************************************************************************************************************************************
oCore.prototype.Loop = function() {
    requestAnimationFrame(this.LoopBound);//schedule next frame
    this.GameCounter++;
    this.TIME.UpdateTimer();
    this.TIME.TrackStart();//track time start
    this.Process(this.TIME.Delta);
    this.Process_Delta = this.TIME.TrackEnd();//track time end
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCore.prototype.Process = function(dt) 
{
    if (!NGE.WS.connected) {
        return;
    }
    
    NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.beginFrame();

    var i, id, objs;
    
    //this.Log(this.JSON(this.DictKeys(this.Objects)));
    
    //process all groups and objects
    NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.beginClock('objects_process');
    for(i = 0; i < this.goTypes.length; i++)
    {
        objs = this.Objects[i];//keys are numbers
        for (id in objs) {
            if (objs.hasOwnProperty(id)) {
                var O = objs[id];
                O.Process(dt);
                
                //flag for removal if needed
                if (O.Obj._remove === true) {
                    this.ObjectsRemovals[i][id] = O;
                }
            }
        }
    }
    NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.endClock('objects_process');
    
    //Custom Processing
    if (this.Game) {
        this.Game.Process(dt);
    }
    
    //Remove Dead Objects (Only after all have synced out)
    //this.Log("Deads")
    NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.beginClock('objects_remove');
    for(i = 0; i < this.goTypes.length; i++)
    {
        objs = this.ObjectsRemovals[i];//keys are numbers
        for (id in objs) {
            if (objs.hasOwnProperty(id)) {
                delete this.Objects[i][id];
                //this.Log("Removed: [" + this.goTypes[i] + "] " + id);
            }
        }
        //clear it now
        this.ObjectsRemovals[i] = {}
    }
    NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.endClock('objects_remove');

    NGE.PIXI.debugGraph && NGE.PIXI.debugGraph.endFrame();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = oCore;
