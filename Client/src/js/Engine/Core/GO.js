//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Game Object - Basic Object Handling
        Changes only tracking of data
        Basic Health/Damage
        Basic Movement
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//var oDataList = require('./sDataList');
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oGameObject(Core, d, new_data, CP)
{
    //debugging
    this.DEBUG = false;
    
    this.Core = Core;
    
    //------------------------------------------------------------------------------
    //DataList (Tracks Changes)
    //------------------------------------------------------------------------------
    //required variables by default
    var _data = [
        "new", d.new, // was newly created on the server (on client, objects are created when they enter the visible region (view scope) and thus sent over from server)
        //identification
        "id", d.id, //"nettype", d.nettype, //"subtype", d.subtype,
        "type", d.type, //unique type id (number)
        "subtype", d.subtype, //number or string as needed (specific to object)
        "owner", d.owner, // owner player id
        "team", d.team,
        //location + size
        "x", d.x, "y", d.y, "vx", d.vx, "vy", d.vy, "r", d.r, "w", d.w, "h", d.h, "radius", d.radius,
        //movement
        "max_speed", d.speed, "acceleration", d.acceleration, "decay", d.decay || 0.2, "rot_speed", 0, "rot_max_speed", d.rot_speed,
        // basic life/damage
        "hp", d.hp, "max_hp", d.hp, "damage", d.damage, "max_damage", d.damage, "damage_range", d.damage_range
    ];
    
    //this.Log("CONCAT: " + JSON.stringify(_data));
    
    //Append new data fields
    _data = _data.concat(new_data);
    //this.Log(new_data);
    //this.Log("CONCAT: " + JSON.stringify(_data));
    
    //create datalist (track changes)
    this._dl = new oDataList(_data);
    

    //Quick map to local vars (will overwrite local)
    var dkeys = this._dl.getkeys();//C.Log(dkeys)
    for(var i=0; i < dkeys.length; i++){
        var dkey = dkeys[i];
        this[dkey] = this._dl[dkey];
    }
    
    
    //------------------------------------------------------------------------------
    //INTERNAL
    //------------------------------------------------------------------------------
    this._AreaIndex = 0;//Which Area we are in
    this._max_speedSQ = this.max_speed.get() * this.max_speed.get();//max speed squared (faster distance calc)
    this._mw = this.w.get()/2; this._mh = this.h.get()/2;//mid width/height
    this._x1 = 0; this._y1 = 0; this._x2 = 0; this._x2 = 0;//bounds
    this._blocking = false;//are we a blocking object (collision detection)
    this._remove = false;//flag to destroy this

    //update current bounds for now
    this.UpdateBounds();
    
    //custom processing
    this.CustomProcess = CP;//define if needed
    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[GO] " + m); }}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.DistanceFrom = function(go) {
    return this.Core.MATH.VecLength(this.x.get() - go.x.get(), this.y.get() - go.y.get());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.DistanceSquaredFrom = function(go) {
    return this.Core.MATH.VecLengthSquared(this.x.get() - go.x.get(), this.y.get() - go.y.get());
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.UpdateBounds = function() {
    this._x1 = Math.floor(this.x.get() - this._mw);
    this._y1 = Math.floor(this.y.get() - this._mh);
    this._x2 = Math.floor(this.x.get() + this._mw);
    this._y2 = Math.floor(this.y.get() + this._mh);
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.Damage = function(v) {
    var hp = this.hp.get();
    hp -= v;
    if(hp <= 0) {hp = 0; this.KILL(); }
    this.hp.set(hp);//save change    
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.Turn = function(target, rate, dt) {
    var r = this.r.get();
    var rch = target - r;
    var ttr = (((((rch) % 360) + 540) % 360) - 180);//adjust for 360/rotation
    var rdiff = rate * dt;

    if (ttr > 0) {
        if(ttr < rdiff ) { rdiff = ttr; }//clamp
        this.r.set(r + rdiff);
    }
    else if (ttr < 0) {
        if(ttr > -rdiff ) { rdiff = -ttr; }
        this.r.set(r - rdiff);
    }
};
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.NetData = function(){
    return this._dl.GetDirtyAndClean();
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.KILL = function() { this._remove = true; this.Log("TO KILL: " + this.id.get()); }
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oGameObject.prototype.Process = function(dt) {
    //Custom Processing
    if (this.CustomProcess) {
        this.CustomProcess(dt);
    }
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = oGameObject;
