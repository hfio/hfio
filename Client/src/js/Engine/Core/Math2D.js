//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Math 2D
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oMath2D(Core) {
    //debugging
    this.DEBUG = true;
    
    this.Core = Core;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMath2D.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[MATH] " + m); }}
oMath2D.prototype.Lerp = function(start, end, amt){ return (1-amt)*start+amt*end }
oMath2D.prototype.Map = function(x, xStart, xStop, yStart, yStop) { return yStart + (yStop - yStart) * ((x - xStart) / (xStop - xStart)); };
oMath2D.prototype.VecScale = function (x, y, s){ return [x*s, y*s]; }
oMath2D.prototype.VecAdd = function (x1, y1, x2, y2){ return [x1+x2, y1+y2]; }
oMath2D.prototype.VecSub = function (x1, y1, x2, y2){ return [x1-x2, y1-y2]; }
oMath2D.prototype.VecNegate = function (x, y){ return [-x, -y]; }
oMath2D.prototype.VecLengthSquared = function (x, y){ return (x*x + y*y); }//Length
oMath2D.prototype.VecLength = function (x, y){ return Math.sqrt(x*x + y*y); }//Slower Length
oMath2D.prototype.degToRad = function (deg) { return deg * (Math.PI/180); }//Degrees to radians
oMath2D.prototype.radToDeg = function (rad) { return rad * (180/Math.PI); }//Radians to degrees
oMath2D.prototype.Normalize = function(x, y){ var l = this.VecLength(x, y); if(l !== 0){ x = x/l; y = y/l; } return [x, y]; }
oMath2D.prototype.NormalizeDegrees = function(r){ r = r % 360; if (r < 0){ r += 360; } return r; }//normalize to 0-360 degree range
oMath2D.prototype.XYToDegree = function(x,y){ var r = Math.atan2(y, x) * (180/Math.PI); if (r < 0.0) { r += 360.0; } return r; }
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oMath2D.prototype.rotate_by_pivot = function(px,py,pr,offsetx,offsety)//sets location by rotating around a different pivot point
{
    pr = this.NormalizeDegrees(pr);
    var c_ = Math.cos(this.degToRad(pr));
    var s_ = Math.sin(this.degToRad(pr));
    var x = px + ((offsetx * c_) - (offsety * s_));
    var y = py + ((offsety * c_) + (offsetx * s_));
    return [x, y];
}
//*********************************************************************************************************************************************
//WORKS! http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect/565282#565282
//*********************************************************************************************************************************************
oMath2D.prototype.isLineIntersect = function(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y)//, i_x, i_y)
{
    var s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    var s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1){ return true; }
    return false; // No collision
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = oMath2D;
