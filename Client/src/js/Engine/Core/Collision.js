//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
/*
    Collision Detection
*/
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
function oCollision(Core)
{
    //debugging
    this.DEBUG = true;
    
    this.Core = Core;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
oCollision.prototype.Log = function(m) { if(GLOBAL_DEBUG && this.DEBUG){ console.log("[COLLISION] " + m); }}
//*********************************************************************************************************************************************
//https://developer.mozto illa.org/en-US/docs/Games/Techniques/2D_collision_detection
//*********************************************************************************************************************************************
oCollision.prototype.inBounds = function(x1, y1, x2, y2, tx1, ty1, tx2, ty2) {//fast AABB collision detection!
    if ((x1 <= tx2 && x2 >= tx1) && (y1 <= ty2 && y2 >= ty1)) {
        return true;
    }
    return false;
}
//*********************************************************************************************************************************************
//Sphere collision detection! 
//*********************************************************************************************************************************************
//https://developer.mozilla.org/en-US/docs/Games/Techniques/2D_collision_detection
oCollision.prototype.inCircle = function(x, y, tx, ty, radius, tradius) {
    var dx = x - tx;
    var dy = y - ty;
    var distance = Math.sqrt(dx * dx + dy * dy);
    if (distance < radius + tradius) { return true; }
    return false;
}
//https://github.com/mattdesl/point-circle-collision/blob/master/index.js
oCollision.prototype.pointCircle = function(x, y, tx, ty, radius) {
    var dx = x - tx;
    var dy = y - ty
    return dx * dx + dy * dy <= radius * radius
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// Check Collision on a Group
oCollision.prototype.CheckCollision = function(x1, y1, x2, y2, objs) {
    for (var key in objs) {
        if (objs.hasOwnProperty(key)) {
            var O = objs[key];
            //this.Log(O.Obj._x1 + " " + O.Obj._y1 + " " + O.Obj._x2 + " " + O.Obj._y2);
            if(O.Obj._remove == false){
                if(this.inBounds(x1, y1, x2, y2, O.Obj._x1, O.Obj._y1, O.Obj._x2, O.Obj._y2)) { return O; }
            }
        }
    }
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
// Check Collision on a Group by Radius
oCollision.prototype.CheckCollisionByRadius = function(x, y, radius, objs) {
    for (var key in objs) {
        if (objs.hasOwnProperty(key)) {
            var O = objs[key];
            if(O.Obj._remove == false){
                if(this.inCircle(x, y, O.Obj.x.get(), O.Obj.y.get(), radius, O.radius.get())) { return O; }
            }
        }
    }
    return false;
}
//*********************************************************************************************************************************************
//*********************************************************************************************************************************************
//module.exports = oCollision;
