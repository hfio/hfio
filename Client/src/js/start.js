// Closure Compiler can override this variable to false for prod builds
/** @define {boolean} */
var GLOBAL_DEBUG = true;

function splash_ready() {
    if(fontsLoaded && imagesLoaded) {
        // ----------------
        // play button
//            $("#splashready").click(function(){
//                $("#Splash").hide();
//                boot();
//            });
        // ----------------
        // user selector
        // read list from local storage
        var previousUsers = localStorage[OBJ_Auth.UUID_LOCALSTORAGE_KEY] ? localStorage[OBJ_Auth.UUID_LOCALSTORAGE_KEY].split(",") : [];
        if (GLOBAL_DEBUG) {
            console.log("Detected previous user IDs: " + previousUsers.join(", "));
        }
        // add user IDs to the <select>
        var userselect = $("#userselect");
        for (var i = 0; i < previousUsers.length; i++) {
            userselect.append("<option>"+previousUsers[i]+"</option>");
        }
        userselect.prop("selectedIndex", previousUsers.length);
        // ----------------
        // clear users button
        $("#clearusers").click(function(){
            localStorage[OBJ_Auth.UUID_LOCALSTORAGE_KEY] = "";
            document.getElementById("userselect").innerHTML = "<option>new user</option>";
        });
        // show splash
//            $("#Splash").show();

        if(RefIDIn != "-1")
        {
            var RefV = RefIDIn.split("x");
            if(RefV.length == 2)
            {
                $("#RefIDInput").html("ServerID: " + RefV[0] + " ShipID: " + RefV[1]);
            }
        }

        //Direct to Bootup! (No Splash Screen)
        boot();

    }
}

// Closure export:
window['splash_ready'] = splash_ready; // jshint ignore:line

