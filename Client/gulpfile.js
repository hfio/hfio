/**
 * Plugins
 * -------
 */

var gulp = require('gulp');
var replace = require('gulp-string-replace');
var minifyHTML = require('gulp-minify-html');
var minifyCSS = require('gulp-minify-css');
var closureCompiler = require('google-closure-compiler').gulp();
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var clean = require('gulp-clean');

/**
 * Tasks
 * -------------------------------------------------
 */

/**
 * Clean
 */

// main clean task, deletes dist folder
gulp.task('clean', function () {
  return gulp.src('./dist', {read: false})
    .pipe(clean());
});

// main clean task, deletes code (js/php/html/css), but not assets in dist folder
gulp.task('clean-code', function () {
  return gulp.src(['./dist/**/*.js', './dist/**/*.php', './dist/**/*.html', './dist/**/*.css'], {read: false})
    .pipe(clean());
});

/**
 * Build
 */

var INDEX = "./src/index.php";
var HOSTS = './src/hosts.*';
var ANALYTICS = './src/analyticstracking.php';
var GUI = './src/gui/**/*.html';
var GUI_PROD = ['./src/gui/**/*.html', '!./src/gui/**/Admin.html'];
var CSS = './src/css/**/*.css';
var IMAGES = './src/img/**';
var JS = './src/js/**/*.js';
var FRAMEWORK = './src/framework/**';
var FONTS = './src/fonts/**';
        
// subtask of build, processes index.php
gulp.task('index', function() {
  return gulp.src(INDEX)
    .pipe(replace('_buildVersion = ""', '_buildVersion = "' + new Date().toISOString() + '"'))
    .pipe(gulp.dest('./dist'))
});

gulp.task('index-prod', function() {
  return gulp.src(INDEX)
    .pipe(replace('_buildVersion = ""', '_buildVersion = "' + new Date().toISOString() + '"'))
    .pipe(replace('"setEnabledInfoLog", true', '"setEnabledInfoLog", false'))
    .pipe(minifyHTML())
    .pipe(gulp.dest('./dist'))
});

gulp.task('watch-index', function() {
    gulp.watch(INDEX, ['index'])
});

// subtask of build, copies the hosts files
gulp.task('hosts', function() {  
  gulp.src(HOSTS)
  .pipe(gulp.dest('./dist/'));
});

gulp.task('watch-hosts', function() {
    gulp.watch(HOSTS, ['hosts'])
});

// subtask of build, copies the google analytics file
gulp.task('analytics', function() {  
  gulp.src(ANALYTICS)
  .pipe(gulp.dest('./dist/'));
});

gulp.task('watch-analytics', function() {
    gulp.watch(ANALYTICS, ['analytics'])
});

// subtask of build, minifies and copies HTML files in the gui folder
gulp.task('gui', function() {  
  return gulp.src(GUI)
    .pipe(minifyHTML())
    .pipe(gulp.dest('./dist/gui'));
});

gulp.task('gui-prod', function() {  
  return gulp.src(GUI_PROD)
    .pipe(minifyHTML())
    .pipe(gulp.dest('./dist/gui'));
});

gulp.task('watch-gui', function() {
    gulp.watch(GUI, ['gui'])
});

// subtask of build, minifies and copies CSS files
gulp.task('css', function() {  
  gulp.src(CSS)
    .pipe(minifyCSS())
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('watch-css', function() {
    gulp.watch(CSS, ['css'])
});

// subtask of build, optimizes and copies imagefiles
gulp.task('images', function() {  
  gulp.src(IMAGES)
  .pipe(imagemin({
    progressive: true,
  }))
  .pipe(gulp.dest('./dist/img'));
});

gulp.task('watch-images', function() {
    gulp.watch(IMAGES, ['images'])
});

// subtask of build, compiles and compresses JS files with Closure (development)
gulp.task('js-compile', function () {
  return gulp.src(JS, {base: './'})
      .pipe(sourcemaps.init())
      .pipe(closureCompiler({
          compilation_level: 'SIMPLE', //'ADVANCED',
          warning_level: 'DEFAULT', //'VERBOSE',
          language_in: 'ECMASCRIPT6_STRICT',
          language_out: 'ECMASCRIPT5_STRICT',
          output_wrapper: '(function(){\n%output%\n}).call(this)',
          js_output_file: 'dist.js'
        }))
      .pipe(sourcemaps.write('/')) // gulp-sourcemaps automatically adds the sourcemap url comment   
      .pipe(gulp.dest('./dist/js'));
});

// subtask of build, compiles and compresses JS files with Closure - (production)
gulp.task('js-compile-prod', function () {
  return gulp.src(JS, {base: './'})
      .pipe(closureCompiler({
          compilation_level: 'SIMPLE', //'ADVANCED',
          warning_level: 'DEFAULT', //'VERBOSE',
          language_in: 'ECMASCRIPT6_STRICT',
          language_out: 'ECMASCRIPT5_STRICT',
          output_wrapper: '(function(){\n%output%\n}).call(this)',
          js_output_file: 'dist.js',
          define: 'GLOBAL_DEBUG=false' // override debug variable
        }))
      .pipe(gulp.dest('./dist/js'));
});

gulp.task('watch-js', function() {
    gulp.watch(JS, ['js-compile'])
});

// subtask of build, copies framework sources
gulp.task('frameworks', function() {  
  gulp.src(FRAMEWORK)
  .pipe(gulp.dest('./dist/framework'));
});

gulp.task('watch-frameworks', function() {
    gulp.watch(FRAMEWORK, ['frameworks'])
});

// subtask of build, copies fonts
gulp.task('fonts', function() {  
  gulp.src(FONTS)
  .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('watch-fonts', function() {
    gulp.watch(FONTS, ['fonts'])
});

// main build task, runs all subtasks (in parallel!) - for dev version
gulp.task('build', ['index', 'hosts', 'analytics', 'gui', 'css', 'js-compile', 'images', 'frameworks', 'fonts']);
// build task for code only (assets are not rebuilt), runs all subtasks (in parallel!) - for dev version
gulp.task('build-code', ['index', 'hosts', 'analytics', 'gui', 'css', 'js-compile', 'frameworks']);
// watcher for running the appropriate parts of the dev build on file changes
gulp.task('watch', ['watch-index', 'watch-hosts', 'watch-analytics', 'watch-gui', 'watch-css', 'watch-js', 'watch-images', 'watch-frameworks', 'watch-fonts']);

// main build task for prod version
gulp.task('build-prod', ['index-prod', 'hosts', 'analytics', 'gui-prod', 'css', 'js-compile-prod', 'images', 'frameworks', 'fonts']);