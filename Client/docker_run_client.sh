# creates a new docker container from the hf-client image (created by docker_build_client.sh) and starts the game client within it
# you need to stop the game client if it is already running with docker_stop_client.sh (and make sure you have port 8080 free)
# game client is accessible via http://localhost:8080/hfio/?r=99x0

docker run -p 8080:80 -d hf-client
