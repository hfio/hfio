# builds the docker image used to test the game client locally (named hf-client)
# run again every time after you make a new build of the game client (with build.sh / build-prod.sh)

docker build -t hf-client ./
