# stop docker containers running the game client

docker rm $(docker stop $(docker ps -a -q --filter ancestor=hf-client --format="{{.ID}}"))
